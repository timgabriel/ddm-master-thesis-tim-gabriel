#include "gmsh.h"

#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshfem/Message.h>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;

using namespace gmshfem;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::problem;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;

void circlesConcentric(const unsigned int nDom, const double r0, const double rInf, const double lc)
{
  if(nDom < 2) {
    gmshfem::msg::error << "nDom should be at least equal to 2." << gmshfem::msg::endl;
    exit(0);
  }
  gmsh::model::add("circlesConcentric");

  std::vector< int > tagsCircle(nDom + 1);
  for(unsigned int i = 0; i <= nDom; ++i) {
    tagsCircle[i] = gmsh::model::occ::addCircle(0., 0., 0., std::sqrt(double(i * (rInf * rInf - r0 * r0)) / nDom + r0 * r0));
  }

  std::vector< int > tagsCurve(nDom + 1);
  for(unsigned int i = 0; i <= nDom; ++i) {
    tagsCurve[i] = gmsh::model::occ::addCurveLoop({tagsCircle[i]});
  }

  std::vector< int > tagsSurface(nDom);
  for(unsigned int i = 0; i < nDom; ++i) {
    tagsSurface[i] = gmsh::model::occ::addPlaneSurface({tagsCurve[i], tagsCurve[i + 1]});
  }

  gmsh::model::occ::synchronize();

  gmsh::model::addPhysicalGroup(1, {tagsCircle[0]}, 1);
  gmsh::model::setPhysicalName(1, 1, "gammaScat");

  gmsh::model::addPhysicalGroup(1, {tagsCircle[nDom]}, 2);
  gmsh::model::setPhysicalName(1, 2, "gammaInf");

  for(unsigned int i = 0; i < nDom; ++i) {
    gmsh::model::addPhysicalGroup(2, {tagsCircle[i]}, 1 + i);
    gmsh::model::setPhysicalName(2, 1 + i, "omega_" + std::to_string(i));

    if(i != nDom - 1) {
      gmsh::model::addPhysicalGroup(1, {tagsCircle[i + 1]}, 200 + i);
      gmsh::model::setPhysicalName(1, 200 + i, "sigma_" + std::to_string(i));
    }
  }

  std::vector< std::pair< int, int > > out;
  gmsh::model::getEntities(out, 0);
  gmsh::model::mesh::setSize(out, lc);

  gmsh::model::mesh::generate();
}

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  int nDom = 2;
  gmshDdm.userDefinedParameter(nDom, "nDom");
  double rInf = 5;
  double r0 = 1;
  double lc = 0.05;
  gmshDdm.userDefinedParameter(rInf, "rInf");
  gmshDdm.userDefinedParameter(r0, "r0");
  gmshDdm.userDefinedParameter(lc, "lc");
  std::string gauss = "Gauss4";
  gmshDdm.userDefinedParameter(gauss, "gauss");

  // Build geometry and mesh using gmsh API
  circlesConcentric(nDom, r0, rInf, lc);

  // Define domain
  Subdomain omega(nDom);
  Subdomain gammaInf(nDom);
  Subdomain gammaScat(nDom);
  Interface sigma(nDom);

  for(unsigned int i = 0; i < nDom; ++i) {
    omega(i) = Domain(2, i + 1);

    if(i == nDom - 1) {
      gammaInf(i) = Domain(1, 2);
    }

    if(i == 0) {
      gammaScat(i) = Domain(1, 1);
    }

    if(i != 0) {
      sigma(i, i - 1) = Domain(1, 200 + i - 1);
    }
    if(i != nDom - 1) {
      sigma(i, i + 1) = Domain(1, 200 + i);
    }
  }

  // Define topology
  std::vector< std::vector< unsigned int > > topology(nDom);
  for(unsigned int i = 0; i < nDom; ++i) {
    if(i != 0) {
      topology[i].push_back(i - 1);
    }
    if(i != nDom - 1) {
      topology[i].push_back(i + 1);
    }
  }

  std::complex< double > im(0., 1.);

  // Create DDM formulation
  gmshddm::problem::Formulation< std::complex< double > > formulation("Navier", topology);

  const double pi = 3.14159265358979323846264338327950288;
  const double rho = 1.; //.............[kg/m3]    masse volumique
  double f = 1.; //.............[Hz]       fréquence
  double w = 2 * pi * f; //.............[rad/s]    pulsation
  const double lambda = 1.; //.............[Pa]       premier  coefficient de Lamé
  const double mu = 1.; //.............[Pa]       deuxième coefficient de Lamé
  const double cP = sqrt((lambda + 2.0 * mu) / rho); //.............[m/s]      celerite de l'onde P
  const double cS = sqrt(mu / rho); //.............[m/s]      celerite de l'onde S
  double kP = w / cP; //.............[1/m]      nombre d'onde p
  double kS = w / cS; //.............[1/m]      nombre d'onde s

  TensorFunction< std::complex< double > > C_xx = tensor< std::complex< double > >((lambda + 2. * mu), 0., 0., 0., mu, 0., 0., 0., 0.);
  TensorFunction< std::complex< double > > C_xy = tensor< std::complex< double > >(0., lambda, 0., mu, 0., 0., 0., 0., 0.);
  TensorFunction< std::complex< double > > C_yx = tensor< std::complex< double > >(0., mu, 0., lambda, 0., 0., 0., 0., 0.);
  TensorFunction< std::complex< double > > C_yy = tensor< std::complex< double > >(mu, 0., 0., 0., (lambda + 2. * mu), 0., 0., 0., 0.);

  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > ux("ux", omega | gammaScat | gammaInf | sigma, FunctionSpaceTypeForm0::Lagrange);
  SubdomainField< std::complex< double >, Form::Form0 > uy("uy", omega | gammaScat | gammaInf | sigma, FunctionSpaceTypeForm0::Lagrange);
  SubdomainField< std::complex< double >, Form::Form0 > lambdax("lambdax", gammaScat, FunctionSpaceTypeForm0::Lagrange);
  SubdomainField< std::complex< double >, Form::Form0 > lambday("lambday", gammaScat, FunctionSpaceTypeForm0::Lagrange);
  InterfaceField< std::complex< double >, Form::Form0 > gx("gx", sigma, FunctionSpaceTypeForm0::Lagrange);
  InterfaceField< std::complex< double >, Form::Form0 > gy("gy", sigma, FunctionSpaceTypeForm0::Lagrange);

  // Tell to the formulation that gx and gy are field that have to be exchanged between subdomains
  formulation.addInterfaceField(gx);
  formulation.addInterfaceField(gy);

  ScalarFunction< std::complex< double > > nX = xComp(normal< std::complex< double > >());
  ScalarFunction< std::complex< double > > nY = yComp(normal< std::complex< double > >());

  // Add terms to the formulation
  for(unsigned int i = 0; i < nDom; ++i) {
    // VOLUME TERMS

    formulation(i).integral(C_xx * grad(dof(ux(i))), grad(tf(ux(i))), omega(i), gauss);
    formulation(i).integral(C_xy * grad(dof(uy(i))), grad(tf(ux(i))), omega(i), gauss);
    formulation(i).integral(C_yx * grad(dof(ux(i))), grad(tf(uy(i))), omega(i), gauss);
    formulation(i).integral(C_yy * grad(dof(uy(i))), grad(tf(uy(i))), omega(i), gauss);

    formulation(i).integral(-w * w * rho * dof(ux(i)), tf(ux(i)), omega(i), gauss);
    formulation(i).integral(-w * w * rho * dof(uy(i)), tf(uy(i)), omega(i), gauss);

    // Physical source
    formulation(i).integral(dof(lambdax(i)), tf(ux(i)), gammaScat(i), gauss);
    formulation(i).integral(dof(ux(i)), tf(lambdax(i)), gammaScat(i), gauss);
    formulation(i).integral(formulation.physicalSource(-(cos(kP * x< std::complex< double > >()) + im * sin(kP * x< std::complex< double > >()))), tf(lambdax(i)), gammaScat(i), gauss);

    formulation(i).integral(dof(lambday(i)), tf(uy(i)), gammaScat(i), gauss);
    formulation(i).integral(dof(uy(i)), tf(lambday(i)), gammaScat(i), gauss);
    formulation(i).integral(formulation.physicalSource(0.), tf(lambday(i)), gammaScat(i), gauss);

    // Abc
    formulation(i).integral(-im * w * cP * rho * nX * nX * dof(ux(i)), tf(ux(i)), gammaInf(i), gauss);
    formulation(i).integral(-im * w * cP * rho * nX * nY * dof(uy(i)), tf(ux(i)), gammaInf(i), gauss);
    formulation(i).integral(-im * w * cP * rho * nX * nY * dof(ux(i)), tf(uy(i)), gammaInf(i), gauss);
    formulation(i).integral(-im * w * cP * rho * nY * nY * dof(uy(i)), tf(uy(i)), gammaInf(i), gauss);

    formulation(i).integral(-im * w * cS * rho * nY * nY * dof(ux(i)), tf(ux(i)), gammaInf(i), gauss);
    formulation(i).integral(im * w * cS * rho * nX * nY * dof(uy(i)), tf(ux(i)), gammaInf(i), gauss);
    formulation(i).integral(-im * w * cS * rho * nX * nX * dof(uy(i)), tf(uy(i)), gammaInf(i), gauss);
    formulation(i).integral(im * w * cS * rho * nX * nY * dof(ux(i)), tf(uy(i)), gammaInf(i), gauss);

    // Artificial source
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i).integral(-im * w * cP * rho * nX * nX * dof(ux(i)), tf(ux(i)), sigma(i, j), gauss);
      formulation(i).integral(-im * w * cP * rho * nX * nY * dof(uy(i)), tf(ux(i)), sigma(i, j), gauss);
      formulation(i).integral(-im * w * cP * rho * nX * nY * dof(ux(i)), tf(uy(i)), sigma(i, j), gauss);
      formulation(i).integral(-im * w * cP * rho * nY * nY * dof(uy(i)), tf(uy(i)), sigma(i, j), gauss);

      formulation(i).integral(-im * w * cS * rho * nY * nY * dof(ux(i)), tf(ux(i)), sigma(i, j), gauss);
      formulation(i).integral(im * w * cS * rho * nX * nY * dof(uy(i)), tf(ux(i)), sigma(i, j), gauss);
      formulation(i).integral(-im * w * cS * rho * nX * nX * dof(uy(i)), tf(uy(i)), sigma(i, j), gauss);
      formulation(i).integral(im * w * cS * rho * nX * nY * dof(ux(i)), tf(uy(i)), sigma(i, j), gauss);

      formulation(i).integral(formulation.artificialSource(-gx(j, i)), tf(ux(i)), sigma(i, j), gauss);
      formulation(i).integral(formulation.artificialSource(-gy(j, i)), tf(uy(i)), sigma(i, j), gauss);
    }

    // INTERFACE TERMS
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i, j).integral(dof(gx(i, j)), tf(gx(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(formulation.artificialSource(gx(j, i)), tf(gx(i, j)), sigma(i, j), gauss);

      formulation(i, j).integral(dof(gy(i, j)), tf(gy(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(formulation.artificialSource(gy(j, i)), tf(gy(i, j)), sigma(i, j), gauss);

      formulation(i, j).integral(2. * im * w * cP * rho * nX * nX * ux(i), tf(gx(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(2. * im * w * cP * rho * nX * nY * uy(i), tf(gx(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(2. * im * w * cP * rho * nX * nY * ux(i), tf(gy(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(2. * im * w * cP * rho * nY * nY * uy(i), tf(gy(i, j)), sigma(i, j), gauss);

      formulation(i, j).integral(2. * im * w * cS * rho * nY * nY * ux(i), tf(gx(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(-2. * im * w * cS * rho * nX * nY * uy(i), tf(gx(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(2. * im * w * cS * rho * nX * nX * uy(i), tf(gy(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(-2. * im * w * cS * rho * nX * nY * ux(i), tf(gy(i, j)), sigma(i, j), gauss);
    }
  }

  // Solve the DDM formulation
  double tolerence = 1e-6;
  gmshDdm.userDefinedParameter(tolerence, "tol");
  int maxIter = 1000;
  gmshDdm.userDefinedParameter(maxIter, "maxIter");
  formulation.pre();
  formulation.solve("gmres", tolerence, maxIter);

  for(unsigned int i = 0; i < nDom; ++i) {
    save(ux(i));
    save(uy(i));
  }

  return 0;
}
