#include "ddm2D.h"

#include "mesh.h"
#include "SubproblemDomains.h"
#include "Subproblem2D.h"

#include <gmshfem/GmshFem.h>
#include <gmshfem/FieldInterface.h>
#include <gmshfem/Formulation.h>
#include <gmshfem/AnalyticalFunction.h>
#include <gmshfem/Post.h>
#include <gmshfem/Function.h>
#include <gmshfem/io.h>

#include <gmshddm/GmshDdm.h>
#include <gmshddm/Subdomain.h>
#include <gmshddm/Interface.h>
#include <gmshddm/SubdomainField.h>
#include <gmshddm/InterfaceField.h>
#include <gmshddm/Formulation.h>
#include <gmshddm/MPIInterface.h>

#include <algorithm>
#include <fstream>

using gmshfem::equation::dof;
using gmshfem::equation::tf;
using gmshfem::function::operator-;
using gmshfem::function::operator*;
using gmshfem::function::abs;

namespace D2 {


  void ddm()
  {
    gmshddm::common::GmshDdm *gmshDdm = gmshddm::common::GmshDdm::currentInstance();

    // ************************
    // P H Y S I C S
    // ************************
    double pi = 3.14159265359;
    double kp = 2. * pi;
    gmshDdm->userDefinedParameter(kp, "kp");
    double ks = 4. * pi;
    gmshDdm->userDefinedParameter(ks, "ks");
    double R = 0.5;
    gmshDdm->userDefinedParameter(R, "R");
    std::string benchmark = "scattering"; // scattering, marmousi
    gmshDdm->userDefinedParameter(benchmark, "benchmark");

    // ************************
    // M E S H
    // ************************
    double lc = 0.03333333333333; // Marmousi = 10; other = 0.03333333333333
    gmshDdm->userDefinedParameter(lc, "lc");
    int meshOrder = 1;
    gmshDdm->userDefinedParameter(meshOrder, "meshOrder");

    // ************************
    // M O D E L I N G
    // ************************
    std::string boundary = "sommerfeld"; // sommerfeld, pml
    gmshDdm->userDefinedParameter(boundary, "boundary");
    std::string boundaryExt = "sommerfeld"; // sommerfeld, pml
    gmshDdm->userDefinedParameter(boundaryExt, "boundaryExt");
    std::string pmlMethod = "continuous"; // continuous, continuousDecoupled
    gmshDdm->userDefinedParameter(pmlMethod, "pmlMethod");
    std::string pmlMethodExt = "continuous"; // continuous, continuousDecoupled
    gmshDdm->userDefinedParameter(pmlMethodExt, "pmlMethodExt");
    std::string pmlType = "hs"; // hs, h, q
    gmshDdm->userDefinedParameter(pmlType, "pmlType");
    std::string pmlTypeExt = "hs"; // hs, h, q
    gmshDdm->userDefinedParameter(pmlTypeExt, "pmlTypeExt");
    int scatterPosX = 0, scatterPosY = 0;
    gmshDdm->userDefinedParameter(scatterPosX, "scatterPosX");
    gmshDdm->userDefinedParameter(scatterPosY, "scatterPosY");

    unsigned int nDomX = 2, nDomY = 2;
    gmshDdm->userDefinedParameter(nDomX, "nDomX");
    gmshDdm->userDefinedParameter(nDomY, "nDomY");
    unsigned int iterMax = 1000;
    gmshDdm->userDefinedParameter(iterMax, "iterMax");
    double res = 1e-6;
    gmshDdm->userDefinedParameter(res, "res");
    double sizeX = 2., sizeY = 2.;
    gmshDdm->userDefinedParameter(sizeX, "sizeX");
    gmshDdm->userDefinedParameter(sizeY, "sizeY");

    unsigned int N = 6;
    gmshDdm->userDefinedParameter(N, "N");
    double pmlSize = N * lc;
    if(pmlSize == 0.) {
      pmlSize = 0.3;
    }

    unsigned int NExt = N;
    gmshDdm->userDefinedParameter(NExt, "NExt");
    double pmlSizeExt = NExt * lc;
    if(pmlSizeExt == 0.) {
      pmlSizeExt = 0.3;
    }

    int fieldOrder = 1;
    gmshDdm->userDefinedParameter(fieldOrder, "fieldOrder");
    int neumannOrder = 1;
    gmshDdm->userDefinedParameter(neumannOrder, "neumannOrder");
    std::string gauss = "Gauss" + std::to_string(2 * std::max(fieldOrder, neumannOrder) + 1);
    gmshDdm->userDefinedParameter(gauss, "gauss");
    double stab = 0.00016666666;
    gmshDdm->userDefinedParameter(stab, "stab");
    double thetaPade = 0.;
    gmshDdm->userDefinedParameter(thetaPade, "thetaPade");
    thetaPade *= pi;
    bool switchOffInteriorCrossPoints = false;
    gmshDdm->userDefinedParameter(switchOffInteriorCrossPoints, "switchOffInteriorCrossPoints");

    // ************************
    // P O S T
    // ************************
    bool monoDomainError = false;
    gmshDdm->userDefinedParameter(monoDomainError, "monoDomainError");
    bool analyticError = false;
    gmshDdm->userDefinedParameter(analyticError, "analyticError");
    std::string fileName = "none";
    gmshDdm->userDefinedParameter(fileName, "file");
    bool saveEMono = false;
    gmshDdm->userDefinedParameter(saveEMono, "saveEMono");
    bool saveUMono = false;
    gmshDdm->userDefinedParameter(saveUMono, "saveUMono");
    bool saveU = false;
    gmshDdm->userDefinedParameter(saveU, "saveU");
    bool saveE = false;
    gmshDdm->userDefinedParameter(saveE, "saveE");
    bool wavenumberPlot = false;
    gmshDdm->userDefinedParameter(wavenumberPlot, "wavenumber");
    std::string mesh = "";
    gmshDdm->userDefinedParameter(mesh, "mesh");
    bool saveMesh = false;
    gmshDdm->userDefinedParameter(saveMesh, "saveMesh");

    gmsh::option::setNumber("Mesh.Binary", 1);
    gmsh::option::setNumber("Mesh.Format", 1);
    if(mesh != "") {
      gmsh::open(mesh + ".msh");
    }
    else {
      if(benchmark == "scattering") {
        checkerboard(nDomX, nDomY, sizeX, sizeY, R, lc, (boundary == "pml"), pmlSize, (boundaryExt == "pml"), pmlSizeExt, meshOrder, scatterPosX, scatterPosY);
      }
      else if(benchmark == "marmousi") {
        checkerboard(nDomX, nDomY, 17000./nDomX, (3500.-800.)/nDomY, 0., lc, (boundary == "pml"), pmlSize, (boundaryExt == "pml"), pmlSizeExt, meshOrder, -1, -1, true);
        sizeX = 17000./nDomX;
        sizeY = (3500.-800.)/nDomY;
      }
      if(saveMesh) {
        gmsh::write("mesh.msh");
      }
    }


    gmshfem::msg::info << "Running 'ddm2D'" << gmshfem::msg::endl;
    gmshfem::msg::info << "Parameters:" << gmshfem::msg::endl;
    gmshfem::msg::info << " * physics:" << gmshfem::msg::endl;
    if(benchmark == "scattering") {
      gmshfem::msg::info << "   - kp: " << kp << " (" << kp/pi << "*pi" << ")" << gmshfem::msg::endl;
      gmshfem::msg::info << "   - ks: " << ks << " (" << ks/pi << "*pi" << ")" << gmshfem::msg::endl;
    }
    gmshfem::msg::info << "   - R: " << R << gmshfem::msg::endl;
    gmshfem::msg::info << " * mesh:" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - lc: " << lc << " (eta_hP = " << (2*pi/kp)/lc << "), (eta_hS = " << (2*pi/ks)/lc << ")" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - meshOrder: " << meshOrder << gmshfem::msg::endl;
    gmshfem::msg::info << " * modeling:" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - grid: (" << nDomX << ", " << nDomY << ")" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - iterMax: " << iterMax << gmshfem::msg::endl;
    gmshfem::msg::info << "   - res: " << res << gmshfem::msg::endl;
    gmshfem::msg::info << "   - subdomain size: (" << sizeX << ", " << sizeY << ")" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - boundary: " << boundary << gmshfem::msg::endl;
    gmshfem::msg::info << "   - boundaryExt: " << boundaryExt << gmshfem::msg::endl;
    if(boundary == "pml") {
      gmshfem::msg::info << "   - N: " << N << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlSize: " << pmlSize << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlType: " << pmlType << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlMethod: " << pmlMethod << gmshfem::msg::endl;
    }
    if(boundaryExt == "pml") {
      gmshfem::msg::info << "   - NExt: " << NExt << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlSizeExt: " << pmlSizeExt << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlTypeExt: " << pmlTypeExt << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlMethodExt: " << pmlMethodExt << gmshfem::msg::endl;
    }
    gmshfem::msg::info << "   - gauss: " << gauss << gmshfem::msg::endl;
    gmshfem::msg::info << "   - fieldOrder: " << fieldOrder << gmshfem::msg::endl;
    gmshfem::msg::info << "   - neumannOrder: " << neumannOrder << gmshfem::msg::endl;
    gmshfem::msg::info << "   - interior cross-points: " <<  (switchOffInteriorCrossPoints ? "switch off" : "switch on") << gmshfem::msg::endl;
    gmshfem::msg::info << "   - stab: " << stab << " * lc (= " << stab * lc << ")" << gmshfem::msg::endl;
    

    // source
    gmshfem::analytics::AnalyticalFunction< gmshfem::analytics::navier2D::SoftPWavesScatteringByACylinder< std::complex< double > > > fAnalytic(kp, ks, R, 1., 1., 2 * std::max(kp, ks));

    const unsigned int nDom = nDomX * nDomY;
    // Define domain
    gmshddm::domain::Subdomain omega(nDom);
    gmshfem::domain::Domain gamma;

    std::vector< gmshddm::domain::Subdomain > pml(4, nDom);
    std::vector< gmshddm::domain::Subdomain > pmlCorner(4, nDom);
    std::vector< gmshddm::domain::Subdomain > pmlInf(4, nDom);
    std::vector< gmshddm::domain::Subdomain > pmlCornerInf(4, nDom);
    std::vector< gmshddm::domain::Subdomain > sigma(4, nDom);
    std::vector< gmshddm::domain::Interface > sigmaInterface(4, nDom);
    std::vector< std::pair< gmshddm::domain::Subdomain, gmshddm::domain::Subdomain > > pmlBnd(4, std::make_pair(gmshddm::domain::Subdomain(nDom), gmshddm::domain::Subdomain(nDom)) );
    std::vector< std::pair< gmshddm::domain::Interface, gmshddm::domain::Interface > > pmlBndInterface(4, std::make_pair(gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom)));

    std::vector< gmshddm::domain::Subdomain > corner(4, nDom);
    std::vector< std::pair< gmshddm::domain::Interface, gmshddm::domain::Interface > > cornerInterface(4, std::make_pair(gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom)));
    // Define topology
    std::vector< std::vector< unsigned int > > topology(nDom);
    std::vector< std::string > dir {"E", "N", "W", "S"};
    for(unsigned int i = 0; i < static_cast< unsigned int >(nDomX); ++i) {
      for(unsigned int j = 0; j < static_cast< unsigned int >(nDomY); ++j) {
        unsigned int index = i * nDomY + j;

        const std::string subdomainTag = "_" + std::to_string(i) + "_" + std::to_string(j);
        omega(index) = gmshfem::domain::Domain("omega" + subdomainTag);

        for(unsigned int b = 0; b < 4; ++b) {
          if(boundary == "pml" || boundaryExt == "pml") {
            pml[b](index) = gmshfem::domain::Domain("pml" + dir[b] + subdomainTag);
            pmlInf[b](index) = gmshfem::domain::Domain("pmlInf" + dir[b] + subdomainTag);
          }
          sigma[b](index) = gmshfem::domain::Domain("sigma" + dir[b] + subdomainTag);
        }
        
        for(unsigned int c = 0; c < 4; ++c) {
          if(boundary == "pml" || boundaryExt == "pml") {
            pmlCorner[c](index) = gmshfem::domain::Domain("pmlCorner" + dir[c] + dir[(c+1)%4] + subdomainTag);
            pmlCornerInf[c](index) = gmshfem::domain::Domain("pmlCornerInf" + dir[c] + dir[(c+1)%4] + subdomainTag);
            pmlBnd[c].first(index) = gmshfem::domain::Domain("pmlBnd" + dir[c] + "_second" + subdomainTag);
            pmlBnd[c].second(index) = gmshfem::domain::Domain("pmlBnd" + dir[(c+1)%4] + "_first" + subdomainTag);
          }
          corner[c](index) = gmshfem::domain::Domain("corner" + dir[c] + dir[(c+1)%4] + subdomainTag);
        }

        if(i != static_cast< unsigned int >(nDomX) - 1) { // E
          if(boundary == "pml" || boundaryExt == "pml") {
            pmlBndInterface[3].first(index, (i+1) * nDomY + j) = pmlBnd[3].first(index);
            pmlBndInterface[0].second(index, (i+1) * nDomY + j) = pmlBnd[0].second(index);
          }
          sigmaInterface[0](index, (i+1) * nDomY + j) = sigma[0](index);
          
          cornerInterface[3].first(index, (i+1) * nDomY + j) = corner[3](index);
          cornerInterface[0].second(index, (i+1) * nDomY + j) = corner[0](index);
          
          topology[index].push_back((i+1) * nDomY + j);
        }

        if(j != static_cast< unsigned int >(nDomY) - 1) { // N
          if(boundary == "pml" || boundaryExt == "pml") {
            pmlBndInterface[0].first(index, i * nDomY + (j+1)) = pmlBnd[0].first(index);
            pmlBndInterface[1].second(index, i * nDomY + (j+1)) = pmlBnd[1].second(index);
          }
          sigmaInterface[1](index, i * nDomY + (j+1)) = sigma[1](index);
          
          cornerInterface[0].first(index, i * nDomY + (j+1)) = corner[0](index);
          cornerInterface[1].second(index, i * nDomY + (j+1)) = corner[1](index);

          topology[index].push_back(i * nDomY + (j+1));
        }

        if(i != 0) { // W
          if(boundary == "pml" || boundaryExt == "pml") {
            pmlBndInterface[1].first(index, (i-1) * nDomY + j) = pmlBnd[1].first(index);
            pmlBndInterface[2].second(index, (i-1) * nDomY + j) = pmlBnd[2].second(index);
          }
          sigmaInterface[2](index, (i-1) * nDomY + j) = sigma[2](index);
          
          cornerInterface[1].first(index, (i-1) * nDomY + j) = corner[1](index);
          cornerInterface[2].second(index, (i-1) * nDomY + j) = corner[2](index);

          topology[index].push_back((i-1) * nDomY + j);
        }

        if(j != 0) { // S
          if(boundary == "pml" || boundaryExt == "pml") {
            pmlBndInterface[2].first(index, i * nDomY + (j-1)) = pmlBnd[2].first(index);
            pmlBndInterface[3].second(index, i * nDomY + (j-1)) = pmlBnd[3].second(index);
          }
          sigmaInterface[3](index, i * nDomY + (j-1)) = sigma[3](index);
          
          cornerInterface[2].first(index, i * nDomY + (j-1)) = corner[2](index);
          cornerInterface[3].second(index, i * nDomY + (j-1)) = corner[3](index);

          topology[index].push_back(i * nDomY + (j-1));
        }
      }
    }
        
    if(benchmark == "scattering") {
      gamma = gmshfem::domain::Domain("gamma");
    }
    else if (benchmark == "marmousi") {
//      gamma = gmshfem::domain::Domain("sources");
      for(unsigned int i = 3; i < 127; i+=4) {
//      for(unsigned int i = 1; i < 5; i+=2) {
        gamma |= corner[0](i);
      }
    }

    // Kappa definition
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP, kappaS;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > C;
    std::vector< double > xP, yP, xS, yS;
    std::vector< std::vector< std::complex< double > > > dataP, dataS;
    if(benchmark == "scattering") {
      kappaP = kp;
      kappaS = ks;
      typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree4 >::Object Ctmp;
      for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
          for(int k = 0; k < 3; ++k) {
            for(int l = 0; l < 3; ++l) {
              Ctmp(i,j)(k,l) = 0.;
            }
          }
        }
      }
      for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
          for(int k = 0; k < 3; ++k) {
            for(int l = 0; l < 3; ++l) {
              if(i == j && k == l) {
                Ctmp(i,j)(k,l) += 1./(kp*kp) - 2./(ks*ks);
              }
              if(i == k && j == l) {
                Ctmp(i,j)(k,l) += 1./(ks*ks);
              }
              if(i == l && j == k) {
                Ctmp(i,j)(k,l) += 1./(ks*ks);
              }
            }
          }
        }
      }
      C = gmshfem::function::TensorFunction< std::complex< double >, 4 >(Ctmp);
    }
    else if (benchmark == "marmousi") {
      const double freq = 15.;
      // Read the vp velocity map
      {
        std::ifstream file("../marmousi_vp.dat", std::ios::binary);

        unsigned int Nx = 0, Ny = 0;
        file.read((char *) &Nx, sizeof(unsigned int));
        file.read((char *) &Ny, sizeof(unsigned int));
        gmshfem::msg::info << "Reading: Nx = " << Nx << ", Ny = " << Ny << "." << gmshfem::msg::endl;

        xP.resize(Nx);
        yP.resize(Ny);
        file.read((char *) &xP[0], Nx * sizeof(double));
        file.read((char *) &yP[0], Ny * sizeof(double));
        for(unsigned int i = 0; i < Ny; ++i) {
          yP[i] += 3500.;
        }
        // Hack to avoid problem at the boundaries
        xP[0] -= 1e-3;
        yP[0] -= 1e-3;
        xP[Nx-1] += 1e-3;
        yP[Ny-1] += 1e-3;

        dataP.resize(Nx, std::vector< std::complex< double > >(Ny));
        std::vector< double > dataRaw(Nx * Ny);
        file.read((char *) &dataRaw[0], Nx * Ny * sizeof(double));
        for(unsigned int j = 0; j < Ny; ++j) {
          for(unsigned int i = 0; i < Nx; ++i) {
            dataP[i][j] = dataRaw[i * Ny + j] * 1e3;
          }
        }
        file.close();

        const double min = *std::min_element(dataRaw.begin(), dataRaw.end());
        const double max = *std::max_element(dataRaw.begin(), dataRaw.end());
        gmshfem::msg::info << "min = " << min << ", max = " << max << gmshfem::msg::endl;

        kappaP = 2. * pi * freq / gmshfem::function::bilinearInterpolation< std::complex< double > >(&xP, &yP, &dataP);
        for(unsigned int k = 0; k < nDomX * nDomY; ++k) {
          if(gmshddm::mpi::isItMySubdomain(k)) {
            gmshfem::post::save(kappaP, omega(k), "kP_" + std::to_string(k), "pos");
          }
        }
      }
      // Read the vs velocity map
      {
        std::ifstream file("../marmousi_vs.dat", std::ios::binary);

        unsigned int Nx = 0, Ny = 0;
        file.read((char *) &Nx, sizeof(unsigned int));
        file.read((char *) &Ny, sizeof(unsigned int));
        gmshfem::msg::info << "Reading: Nx = " << Nx << ", Ny = " << Ny << "." << gmshfem::msg::endl;

        xS.resize(Nx);
        yS.resize(Ny);
        file.read((char *) &xS[0], Nx * sizeof(double));
        file.read((char *) &yS[0], Ny * sizeof(double));
        for(unsigned int i = 0; i < Ny; ++i) {
          yS[i] += 3500.;
        }
        // Hack to avoid problem at the boundaries
        xS[0] -= 1e-3;
        yS[0] -= 1e-3;
        xS[Nx-1] += 1e-3;
        yS[Ny-1] += 1e-3;

        dataS.resize(Nx, std::vector< std::complex< double > >(Ny));
        std::vector< double > dataRaw(Nx * Ny);
        file.read((char *) &dataRaw[0], Nx * Ny * sizeof(double));
        for(unsigned int j = 0; j < Ny; ++j) {
          for(unsigned int i = 0; i < Nx; ++i) {
            dataS[i][j] = dataRaw[i * Ny + j] * 1e3;
          }
        }
        file.close();

        const double min = *std::min_element(dataRaw.begin(), dataRaw.end());
        const double max = *std::max_element(dataRaw.begin(), dataRaw.end());
        gmshfem::msg::info << "min = " << min << ", max = " << max << gmshfem::msg::endl;

        kappaS = 2. * pi * freq / gmshfem::function::bilinearInterpolation< std::complex< double > >(&xS, &yS, &dataS);
        for(unsigned int k = 0; k < nDomX * nDomY; ++k) {
          if(gmshddm::mpi::isItMySubdomain(k)) {
            gmshfem::post::save(kappaS, omega(k), "kS_" + std::to_string(k), "pos");
          }
        }
      }
      
      gmshfem::function::ScalarFunction< std::complex< double > > p1 = 1./(kappaP*kappaP);
      gmshfem::function::ScalarFunction< std::complex< double > > p2 = 1./(kappaS*kappaS);
      gmshfem::function::ScalarFunction< std::complex< double > > p3 = 1./(kappaP*kappaP) - 2./(kappaS*kappaS);
      
      typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree2 >::Object C00tmp;
      for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
          C00tmp(i,j) = 0.;
        }
      }
      
      gmshfem::function::TensorFunction< std::complex< double > > Cxx = gmshfem::function::tensor< std::complex< double > >(p1, 0., 0.,  0., p3, 0.,  0., 0., 0.);
      gmshfem::function::TensorFunction< std::complex< double > > Cxy = gmshfem::function::tensor< std::complex< double > >(0., p2, 0.,  p2, 0., 0.,  0., 0., 0.);
      gmshfem::function::TensorFunction< std::complex< double > > Cyx = gmshfem::function::tensor< std::complex< double > >(0., p2, 0.,  p2, 0., 0.,  0., 0., 0.);
      gmshfem::function::TensorFunction< std::complex< double > > Cyy = gmshfem::function::tensor< std::complex< double > >(p3, 0., 0.,  0., p1, 0.,  0., 0., 0.);
      gmshfem::function::TensorFunction< std::complex< double > > C00(C00tmp);
      
      C = gmshfem::function::tensor< std::complex< double >, 4 >(Cxx, Cxy, C00,  Cyx, Cyy, C00,  C00, C00, C00);
    }

    // Fields definition
    gmshddm::field::SubdomainCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > u("u", omega | sigma[0] | sigma[1] | sigma[2] | sigma[3], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    std::vector< gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * > gContinuous;
    for(unsigned int f = 0; f < 4; ++f) {
      gContinuous.push_back(new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("gC_" + dir[f], sigmaInterface[f], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder));
    }

    std::vector< std::pair< gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *, gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * > > gCornerPmlContinuous;
    std::vector< std::pair< gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > *, gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > * > > gSymCornerPmlContinuous;
    for(unsigned int f = 0; f < 4; ++f) {
      gCornerPmlContinuous.push_back(std::make_pair(
        new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("gCornerPmlContinuous_" + dir[f] + dir[(f+1)%4] + "_first", pmlBndInterface[f].first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
        new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("gCornerPmlContinuous_" + dir[f] + dir[(f+1)%4] + "_second", pmlBndInterface[f].second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
      gSymCornerPmlContinuous.push_back(std::make_pair(
        new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("gSymCornerPmlContinuous_" + dir[f] + dir[(f+1)%4] + "_first", pmlBndInterface[f].first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
        new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >("gSymCornerPmlContinuous_" + dir[f] + dir[(f+1)%4] + "_second", pmlBndInterface[f].second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
    }
    
    gmshddm::problem::Formulation< std::complex< double > > formulation("NavierDDMScattering", topology);

    for(unsigned int f = 0; f < 4; ++f) {
      formulation.addInterfaceField(*gContinuous[f], *gContinuous[(f+2)%4]);

      // Pmls
      formulation.addInterfaceField(*gCornerPmlContinuous[f].first, *gCornerPmlContinuous[(f-1)%4].second);
      formulation.addInterfaceField(*gCornerPmlContinuous[f].second, *gCornerPmlContinuous[(f+1)%4].first);
      
      formulation.addInterfaceField(*gSymCornerPmlContinuous[f].first, *gSymCornerPmlContinuous[(f-1)%4].second);
      formulation.addInterfaceField(*gSymCornerPmlContinuous[f].second, *gSymCornerPmlContinuous[(f+1)%4].first);
    }
    
    if(benchmark == "scattering") {
      u(scatterPosX * nDomY + scatterPosY).addConstraint(gamma, fAnalytic);
    }
    else if (benchmark == "marmousi") {
      for(unsigned int i = 3; i < 127; i+=4) {
//      for(unsigned int i = 1; i < 5; i+=2) {
        u(i).addConstraint(corner[0](i), gmshfem::function::vector< std::complex< double > >(0., 1., 0.));
      }
//      for(unsigned int i = 0; i < nDomX; ++i) {
//        for(unsigned int j = 0; j < nDomY; ++j) {
//          unsigned int index = i * nDomY + j;
//          u(index).addConstraint(gamma, gmshfem::function::vector< std::complex< double > >(0., 1., 0.));
//        }
//      }
    }

    std::vector< std::vector< Subproblem * > > subproblem(nDomX);
    for(unsigned int i = 0; i < nDomX; ++i) {
      subproblem[i].resize(nDomY);
      for(unsigned int j = 0; j < nDomY; ++j) {
        unsigned int index = i * nDomY + j;
        formulation(index).integral(- C * grad(dof(u(index))), grad(tf(u(index))), omega(index), gauss);
        formulation(index).integral(dof(u(index)), tf(u(index)), omega(index), gauss);
        
        SubproblemDomains domains;
        domains.setOmega(omega(index));
        domains.setSigma({ sigma[0](index), sigma[1](index), sigma[2](index), sigma[3](index) });
        domains.setPml({ pml[0](index), pml[1](index), pml[2](index), pml[3](index) });
        domains.setPmlInf({ pmlInf[0](index), pmlInf[1](index), pmlInf[2](index), pmlInf[3](index) });
        domains.setPmlBnd({ std::make_pair(pmlBnd[0].first(index), pmlBnd[0].second(index)), std::make_pair(pmlBnd[1].first(index), pmlBnd[1].second(index)), std::make_pair(pmlBnd[2].first(index), pmlBnd[2].second(index)), std::make_pair(pmlBnd[3].first(index), pmlBnd[3].second(index)) });
        domains.setPmlCorner({ pmlCorner[0](index), pmlCorner[1](index), pmlCorner[2](index), pmlCorner[3](index) });
        domains.setPmlCornerInf({ pmlCornerInf[0](index), pmlCornerInf[1](index), pmlCornerInf[2](index), pmlCornerInf[3](index) });
        domains.setCorner({ corner[0](index), corner[1](index), corner[2](index), corner[3](index) });
        
        SubproblemParameters parameters;
        parameters.setGauss(gauss);
        parameters.setKappaP(kappaP);
        parameters.setKappaS(kappaS);
        parameters.setC(C);
        parameters.setNeumannOrder(neumannOrder);
        parameters.setFieldOrder(fieldOrder);
        parameters.setStab(stab * lc * lc);
        
        std::string bnd = boundary;
        if(boundary == "pml") {
          if(pmlMethod == "continuous") {
            bnd += "Continuous_" + std::to_string(pmlSize) + "_" + pmlType;
          }
          else if(pmlMethod == "continuousSymmetric") {
            bnd += "ContinuousSymmetric_" + std::to_string(pmlSize) + "_" + pmlType;
          }
          else if(pmlMethod == "continuousDecoupled") {
            bnd += "ContinuousDecoupled_" + std::to_string(pmlSize) + "_" + pmlType;
          }
        }
        std::string bndExt = boundaryExt;
        if(boundaryExt == "pml") {
          if(pmlMethodExt == "continuous") {
            bndExt += "Continuous_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
          }
          else if(pmlMethodExt == "continuousSymmetric") {
            bndExt += "ContinuousSymmetric_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
          }
          else if(pmlMethodExt == "continuousDecoupled") {
            bndExt += "ContinuousDecoupled_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
          }
        }
        gmshfem::msg::info << "Subdomain (" << i << ", " << j << ") has boundaries [" << (i == nDomX-1 ? bndExt : bnd) << ", " << (j == nDomY-1 ? bndExt : bnd) << ", " << (i == 0 ? bndExt : bnd) << ", " << (j == 0 ? bndExt : bnd) << "]" << gmshfem::msg::endl;
        std::vector< unsigned int > activatedCrossPoints = {0, 1, 2, 3};
        if(switchOffInteriorCrossPoints) {
          activatedCrossPoints.clear();
          if(i == nDomX-1 || j == nDomY-1) {
            activatedCrossPoints.push_back(0);
          }
          if(i == 0 || j == nDomY-1) {
            activatedCrossPoints.push_back(1);
          }
          if(i == 0 || j == 0) {
            activatedCrossPoints.push_back(2);
          }
          if(i == nDomX-1 || j == 0) {
            activatedCrossPoints.push_back(3);
          }
        }
        subproblem[i][j] = new Subproblem(formulation(index), u(index).name(), domains, parameters, (i == nDomX-1 ? bndExt : bnd), (j == nDomY-1 ? bndExt : bnd), (i == 0 ? bndExt : bnd), (j == 0 ? bndExt : bnd), activatedCrossPoints);
        subproblem[i][j]->writeFormulation();
        
        // coupling
        for(unsigned int b = 0; b < 4; ++b) {
          for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
            const unsigned int jndex = topology[index][jj];
            
            Boundary *boundary = subproblem[i][j]->getBoundary(b);
            if(dynamic_cast< Sommerfeld * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gContinuous[(b+2)%4])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(dynamic_cast< PmlContinuous * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gContinuous[(b+2)%4])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(dynamic_cast< PmlContinuousSymmetric * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gContinuous[(b+2)%4])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(dynamic_cast< PmlContinuousDecoupled * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gContinuous[(b+2)%4])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
          }
        }
        
        for(unsigned int c = 0; c < 4; ++c) {
          for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
            const unsigned int jndex = topology[index][jj];

            Corner *corner = subproblem[i][j]->getCorner(c);
            if(corner == nullptr) {
              continue;
            }
            if(auto cr = dynamic_cast< PmlContinuous_PmlContinuous * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuous_Sommerfeld * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< Sommerfeld_PmlContinuous * >(corner)) {
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                {
                  auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUPml()), pmlBndInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
                {
                  auto termId = formulation(index).integral(- (*gSymCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUSymPml()), pmlBndInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                {
                  auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUPml()), pmlBndInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
                {
                  auto termId = formulation(index).integral(- (*gSymCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUSymPml()), pmlBndInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuousSymmetric_Sommerfeld * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                {
                  auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUPml()), pmlBndInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
                {
                  auto termId = formulation(index).integral(- (*gSymCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUSymPml()), pmlBndInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
            }
            else if(auto cr = dynamic_cast< Sommerfeld_PmlContinuousSymmetric * >(corner)) {
              {
                if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                  auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUPml()), pmlBndInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
              {
                if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                  auto termId = formulation(index).integral(- (*gSymCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUSymPml()), pmlBndInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuousDecoupled_PmlContinuousDecoupled * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUxPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                termId = formulation(index).integral(- gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUyPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUxPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                termId = formulation(index).integral(- gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUyPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuous_PmlContinuousDecoupled * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUxPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                termId = formulation(index).integral(- gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUyPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuousDecoupled_PmlContinuous * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- gmshfem::function::vector< std::complex< double > >(1., 0., 0.) * (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUxPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                termId = formulation(index).integral(- gmshfem::function::vector< std::complex< double > >(0., 1., 0.) * (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUyPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
          }
        }
        
        // interface
        for(unsigned int b = 0; b < 4; ++b) {
          for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
            const unsigned int jndex = topology[index][jj];
            
            Boundary *boundary = subproblem[i][j]->getBoundary(b);
            if(auto bnd = dynamic_cast< Sommerfeld * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gContinuous[(b+2)%4])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
              }
            }
            else if(auto bnd = dynamic_cast< PmlContinuous * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gContinuous[(b+2)%4])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
              }
            }
            else if(auto bnd = dynamic_cast< PmlContinuousSymmetric * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gContinuous[(b+2)%4])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(*bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                formulation(index, jndex).integral(*bnd->getVSym(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
              }
            }
            else if(auto bnd = dynamic_cast< PmlContinuousDecoupled * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gContinuous[(b+2)%4])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
              }
            }
          }
        }
        
        for(unsigned int c = 0; c < 4; ++c) {
          for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
            const unsigned int jndex = topology[index][jj];

            Corner *corner = subproblem[i][j]->getCorner(c);
            if(corner == nullptr) {
              continue;
            }
            if(auto cr = dynamic_cast< PmlContinuous_PmlContinuous * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].first)(index, jndex)), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(0), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);

              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].second)(index, jndex)), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(1), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuous_Sommerfeld * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].first)(index, jndex)), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< Sommerfeld_PmlContinuous * >(corner)) {
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].second)(index, jndex)), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                {
                  formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].first)(index, jndex)), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(*cr->getV(0), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                  formulation(index, jndex).integral(*cr->getVSym(0,0), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                }
                {
                  formulation(index, jndex).integral(dof((*gSymCornerPmlContinuous[c].first)(index, jndex)), tf((*gSymCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gSymCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gSymCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(*cr->getVSym(1,0), tf((*gSymCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                  formulation(index, jndex).integral(*cr->getVSymSym(0), tf((*gSymCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                }
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                {
                  formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].second)(index, jndex)), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(*cr->getV(1), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                  formulation(index, jndex).integral(*cr->getVSym(1,1), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                }
                {
                  formulation(index, jndex).integral(dof((*gSymCornerPmlContinuous[c].second)(index, jndex)), tf((*gSymCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gSymCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gSymCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(*cr->getVSym(0,1), tf((*gSymCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                  formulation(index, jndex).integral(*cr->getVSymSym(1), tf((*gSymCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                }
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuousSymmetric_Sommerfeld * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                {
                  formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].first)(index, jndex)), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2 * *cr->getV(), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                }
                {
                  formulation(index, jndex).integral(dof((*gSymCornerPmlContinuous[c].first)(index, jndex)), tf((*gSymCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gSymCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gSymCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2 * *cr->getVSym(), tf((*gSymCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                }
              }
            }
            else if(auto cr = dynamic_cast< Sommerfeld_PmlContinuousSymmetric * >(corner)) {
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                {
                  formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].second)(index, jndex)), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2 * *cr->getV(), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                }
                {
                  formulation(index, jndex).integral(dof((*gSymCornerPmlContinuous[c].second)(index, jndex)), tf((*gSymCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gSymCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gSymCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2 * *cr->getVSym(), tf((*gSymCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                }
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuousDecoupled_PmlContinuousDecoupled * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].first)(index, jndex)), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(0), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);

              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].second)(index, jndex)), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(1), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuous_PmlContinuousDecoupled * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].first)(index, jndex)), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(0), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);

              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].second)(index, jndex)), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(1), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuousDecoupled_PmlContinuous * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].first)(index, jndex)), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(0), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);

              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].second)(index, jndex)), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(1), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
              }
            }
          }
        }
      }
    }
    
    gmshfem::common::Timer pre = formulation.pre();
    gmshfem::common::Timer solve = formulation.solve("gmres", res, iterMax, false, false);
    
//    gmshfem::post::save(xComp(*dynamic_cast< PmlContinuousSymmetric * >(subproblem[0][0]->getBoundary(0))->getUPml()), pml[0](0), "uPmlx", "msh");
//    gmshfem::post::save(yComp(*dynamic_cast< PmlContinuousSymmetric * >(subproblem[0][0]->getBoundary(0))->getUPml()), pml[0](0), "uPmly", "msh");
//
//    gmshfem::post::save(xComp(*dynamic_cast< PmlContinuousSymmetric * >(subproblem[0][0]->getBoundary(0))->getUSymPml()), pml[0](0), "uSymPmlx", "msh");
//    gmshfem::post::save(yComp(*dynamic_cast< PmlContinuousSymmetric * >(subproblem[0][0]->getBoundary(0))->getUSymPml()), pml[0](0), "uSymPmly", "msh");
    
    for(unsigned int i = 0; i < nDomX; ++i) {
      for(unsigned int j = 0; j < nDomY; ++j) {
        unsigned int index = i * nDomY + j;
        if(gmshddm::mpi::isItMySubdomain(index)) {
          if(saveU) {
            gmshfem::post::save(xComp(u(index)), omega(index), "ux_" + std::to_string(index), "pos", "/scratch/ulg/ace/aroyer/pml2/");
            gmshfem::post::save(yComp(u(index)), omega(index), "uy_" + std::to_string(index), "pos", "/scratch/ulg/ace/aroyer/pml2/");
            
            //gmshfem::post::save(tr(grad(u(index))), omega(index), "uP_" + std::to_string(index), "msh");
          }
        }
      }
    }
    
    if(monoDomainError) {
      gmshfem::problem::Formulation< std::complex< double > > formulationMono("NavierMonoDomain");

      std::vector< gmshfem::domain::Domain > sigmaMono(4);
      std::vector< gmshfem::domain::Domain > pmlMono(4);
      std::vector< gmshfem::domain::Domain > pmlInfMono(4);
      std::vector< gmshfem::domain::Domain > pmlCornerMono(4);
      std::vector< gmshfem::domain::Domain > pmlCornerInfMono(4);
      std::vector< std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > > pmlBndMono(4);
      std::vector< gmshfem::domain::Domain > cornerMono(4);
      for(unsigned int i = 0; i < nDomX; ++i) {
        sigmaMono[1] |= sigma[1](i * nDomY + nDomY - 1);
        sigmaMono[3] |= sigma[3](i * nDomY);

        pmlMono[1] |= pml[1](i * nDomY + nDomY - 1);
        pmlMono[3] |= pml[3](i * nDomY);
        
        pmlInfMono[1] |= pmlInf[1](i * nDomY + nDomY - 1);
        pmlInfMono[3] |= pmlInf[3](i * nDomY);
      }

      for(unsigned int i = 0; i < nDomY; ++i) {
        sigmaMono[0] |= sigma[0](i + (nDomX - 1) * nDomY);
        sigmaMono[2] |= sigma[2](i);

        pmlMono[0] |= pml[0](i + (nDomX - 1) * nDomY);
        pmlMono[2] |= pml[2](i);
        
        pmlInfMono[0] |= pmlInf[0](i + (nDomX - 1) * nDomY);
        pmlInfMono[2] |= pmlInf[2](i);
      }

      pmlCornerMono[0] = pmlCorner[0](nDomX * nDomY - 1);
      pmlCornerMono[1] = pmlCorner[1](nDomY - 1);
      pmlCornerMono[2] = pmlCorner[2](0);
      pmlCornerMono[3] = pmlCorner[3]((nDomX - 1) * nDomY);
      
      pmlCornerInfMono[0] = pmlCornerInf[0](nDomX * nDomY - 1);
      pmlCornerInfMono[1] = pmlCornerInf[1](nDomY - 1);
      pmlCornerInfMono[2] = pmlCornerInf[2](0);
      pmlCornerInfMono[3] = pmlCornerInf[3]((nDomX - 1) * nDomY);

      pmlBndMono[0] = std::make_pair( pmlBnd[0].first(nDomX * nDomY - 1), pmlBnd[0].second(nDomX * nDomY - 1) );
      pmlBndMono[1] = std::make_pair( pmlBnd[1].first(nDomY - 1), pmlBnd[1].second(nDomY - 1) );
      pmlBndMono[2] = std::make_pair( pmlBnd[2].first(0), pmlBnd[2].second(0) );
      pmlBndMono[3] = std::make_pair( pmlBnd[3].first((nDomX - 1) * nDomY), pmlBnd[3].second((nDomX - 1) * nDomY) );

      cornerMono[0] = corner[0](nDomX * nDomY - 1);
      cornerMono[1] = corner[1](nDomY - 1);
      cornerMono[2] = corner[2](0);
      cornerMono[3] = corner[3]((nDomX - 1) * nDomY);

      gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 > uMono("uMono", omega.getUnion(), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

      formulationMono.integral(-C * grad(dof(uMono)), grad(tf(uMono)), omega.getUnion(), gauss);
      formulationMono.integral(dof(uMono), tf(uMono), omega.getUnion(), gauss);

      if(benchmark == "scattering") {
        uMono.addConstraint(gamma, fAnalytic);
      }
      else if (benchmark == "marmousi") {
        for(unsigned int i = 3; i < 127; i+=4) {
//        for(unsigned int i = 1; i < 5; i+=2) {
          uMono.addConstraint(corner[0](i), gmshfem::function::vector< std::complex< double > >(0., 1., 0.));
        }
        uMono.addConstraint(gamma, gmshfem::function::vector< std::complex< double > >(0., 1., 0.));
      }
      
      SubproblemDomains domains;
      domains.setOmega(omega.getUnion());
      domains.setSigma(sigmaMono);
      domains.setPml(pmlMono);
      domains.setPmlInf(pmlInfMono);
      domains.setPmlBnd(pmlBndMono);
      domains.setPmlCorner(pmlCornerMono);
      domains.setPmlCornerInf(pmlCornerInfMono);
      domains.setCorner(cornerMono);
          
      SubproblemParameters parameters;
      parameters.setGauss(gauss);
      parameters.setKappaP(kappaP);
      parameters.setKappaS(kappaS);
      parameters.setC(C);
      parameters.setNeumannOrder(neumannOrder);
      parameters.setFieldOrder(fieldOrder);
      parameters.setStab(stab * lc);
      
      std::string bndExt = boundaryExt;
      if(boundaryExt == "pml") {
        if(pmlMethodExt == "continuous") {
          bndExt += "Continuous_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
        }
        else if(pmlMethodExt == "continuousSymmetric") {
          bndExt += "ContinuousSymmetric_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
        }
        else if(pmlMethodExt == "continuousDecoupled") {
          bndExt += "ContinuousDecoupled_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
        }
      }
      gmshfem::msg::info << "Monodomain has boundaries [" << bndExt << ", " << bndExt << ", " << bndExt << ", " << bndExt << "]" << gmshfem::msg::endl;
      Subproblem subproblem(formulationMono, uMono.name(), domains, parameters, bndExt, bndExt, bndExt, bndExt, {0,1,2,3});
      subproblem.writeFormulation();
        
      formulationMono.pre();
      formulationMono.assemble();
      formulationMono.solve();
      
//      gmshfem::post::save(xComp(*dynamic_cast< PmlContinuousSymmetric * >(subproblem.getBoundary(1))->getUPml()), pmlMono[1], "uPmlx", "msh");
//      gmshfem::post::save(yComp(*dynamic_cast< PmlContinuousSymmetric * >(subproblem.getBoundary(1))->getUPml()), pmlMono[1], "uPmly", "msh");

//      gmshfem::post::save(xComp(*dynamic_cast< PmlContinuous_PmlContinuous * >(subproblem.getCorner(0))->getUPml()), pmlCornerMono[0], "uPmlCx", "msh");
//      gmshfem::post::save(yComp(*dynamic_cast< PmlContinuous_PmlContinuous * >(subproblem.getCorner(0))->getUPml()), pmlCornerMono[0], "uPmlCy", "msh");
//
//      gmshfem::post::save(xComp(uMono), omega.getUnion(), "uMonox");
//      gmshfem::post::save(yComp(uMono), omega.getUnion(), "uMonoy");

      double num = 0.;
      double den = 0.;
      for(unsigned int i = 0; i < nDomX * nDomY; ++i) {
        num += std::real(gmshfem::post::integrate(pow(norm(u(i) - uMono), 2), omega(i), gauss));
        den += std::real(gmshfem::post::integrate(pow(norm(uMono), 2), omega(i), gauss));
      }

      gmshfem::msg::info << "Error mono L2 = " << std::sqrt(num/den) << gmshfem::msg::endl;

      for(unsigned int i = 0; i < nDomX * nDomY; ++i) {
        if(gmshddm::mpi::isItMySubdomain(i)) {
          if(saveEMono) {
            gmshfem::post::save(xComp(u(i) - uMono), omega(i), "eMonox_" + std::to_string(i));
            gmshfem::post::save(yComp(u(i) - uMono), omega(i), "eMonoy_" + std::to_string(i));
          }
          if(saveUMono) {
            gmshfem::post::save(xComp(uMono), omega(i), "uMonox_" + std::to_string(i));
            gmshfem::post::save(yComp(uMono), omega(i), "uMonoy_" + std::to_string(i));
          }
        }
      }
      
      if(fileName != "none") {
        gmshfem::common::CSVio file(fileName, ';', gmshfem::common::OpeningMode::Append);
//        file << iterMax << formulation.relativeResidual().back() << std::sqrt(num/den) << gmshfem::csv::endl;
        file.close();
      }
    }
    
    if(analyticError) {
      double num = 0., den = 0.;
      for(unsigned int i = 0; i < nDomX * nDomY; ++i) {
        if(gmshddm::mpi::isItMySubdomain(i)) {
          num += std::real(gmshfem::post::integrate(pow(norm(u(i) - fAnalytic), 2), omega(i), gauss));
          den += std::real(gmshfem::post::integrate(pow(norm(fAnalytic), 2), omega(i), gauss));
        }
        if(saveE){
          gmshfem::post::save(xComp(u(i) - fAnalytic), omega(i), "ex_" + std::to_string(i), "msh");
          gmshfem::post::save(yComp(u(i) - fAnalytic), omega(i), "ey_" + std::to_string(i), "msh");
        }
      }
      gmshfem::msg::info << "Error L2 = " << std::sqrt(num/den) << gmshfem::msg::endl;
      gmshfem::common::CSVio file("results", ';', gmshfem::common::OpeningMode::Append);
//      file << N << std::sqrt(num/den) << pre+solve << gmshfem::csv::endl;
      file.close();
    }
    
    if(wavenumberPlot) {
      gmshfem::common::CSVio file(fileName, ';', gmshfem::common::OpeningMode::Append);
//      file << kp << ks << formulation.relativeResidual().size()-1 << gmshfem::csv::endl;
      file.close();
    }
    
    for(unsigned int i = 0; i < nDomX; ++i) {
      for(unsigned int j = 0; j < nDomY; ++j) {
        delete subproblem[i][j];
      }
    }
    
    for(unsigned int f = 0; f < 4; ++f) {
      delete gContinuous[f];
      delete gCornerPmlContinuous[f].first;
      delete gCornerPmlContinuous[f].second;
    }
  }


}
