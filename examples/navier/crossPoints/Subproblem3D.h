#ifndef H_SUBPROBLEM3D
#define H_SUBPROBLEM3D

#include "SubproblemDomains.h"
#include "SubproblemParameters.h"

#include <gmshfem/Formulation.h>

namespace D3 {


  class Boundary
  {
   protected:
    const unsigned int _boundary;
    
   public:
    Boundary(const unsigned int boundary);
    virtual ~Boundary();
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
    
    std::string orientation() const;
  };
  
  class Edge
  {
   protected:
    const unsigned int _edge;
    Boundary *_bnd[2];
    
   public:
    Edge(const unsigned int edge, Boundary *first, Boundary *second);
    virtual ~Edge();
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
    
    std::string orientation() const;
  };

  class Corner
  {
   protected:
    const unsigned int _corner;
    Edge *_bnd[3];
    
   public:
    Corner(const unsigned int corner, Edge *first, Edge *second, Edge *third);
    virtual ~Corner();
    
    virtual void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
    
    std::string orientation() const;
  };

  class Subproblem
  {
   private:
    const SubproblemDomains _domains;
    const SubproblemParameters _parameters;
    gmshfem::problem::Formulation< std::complex< double > > &_formulation;
    const std::string _fieldName;
    std::vector< Boundary * > _boundary;
    std::vector< Edge * > _edge;
    std::vector< Corner * > _corner;
    
    void _parseParameters(std::string &method, std::vector< std::string > &parameters, const std::string &str) const;
    
   public:
    Subproblem(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters, const std::string &E, const std::string &N, const std::string &W, const std::string &S, const std::string &D, const std::string &U);
    ~Subproblem();
    
    void writeFormulation();
    
    Boundary *getBoundary(const unsigned int b) const;
    Edge *getEdge(const unsigned int e) const;
    Corner *getCorner(const unsigned int c) const;
  };




  class Sommerfeld : public Boundary
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _v;
    
   public:
    Sommerfeld(const unsigned int boundary);
    ~Sommerfeld();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class Pml : public Boundary
  {
   protected:
    const double _size;
    const std::string _type;
    
   public:
    Pml(const unsigned int boundary, const double pmlSize, const std::string &type);
    virtual ~Pml();
    
    virtual double getSize() const;
    virtual gmshfem::function::TensorFunction< std::complex< double >, 4 > pmlCoefficients(gmshfem::function::TensorFunction< std::complex< double >, 4 > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E, gmshfem::domain::Domain &bnd, const gmshfem::function::TensorFunction< std::complex< double >, 4 > &C, gmshfem::function::ScalarFunction< std::complex< double > > &kappaP, gmshfem::function::ScalarFunction< std::complex< double > > &kappaS) const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) = 0;
  };
  
  class PmlContinuous : public Pml
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uPml;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _v;
    
   public:
    PmlContinuous(const unsigned int boundary, const double pmlSize, const std::string &type);
    ~PmlContinuous();
        
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getUPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getV() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuousSymmetric : public Pml
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uPml;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uSymPml;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _v;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSym;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > _symX, _symY, _symZ;
    
   public:
    PmlContinuousSymmetric(const unsigned int boundary, const double pmlSize, const std::string &type);
    ~PmlContinuousSymmetric();
            
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getUPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getUSymPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getV() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVSym() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };


  
  class PmlContinuous_PmlContinuous : public Edge
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uPmlEdge;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vE[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vEdge;
    
   public:
    PmlContinuous_PmlContinuous(const unsigned int edge, PmlContinuous *first, PmlContinuous *second);
    ~PmlContinuous_PmlContinuous();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getUPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getV(const unsigned int i) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVEdge() const;
    
    PmlContinuous *firstBoundary() const;
    PmlContinuous *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuousSymmetric_PmlContinuousSymmetric : public Edge
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uPmlEdge;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uSymPmlEdge[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uSymSymPmlEdge;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vE[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymE[2][2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymSymE[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vEdge;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymEdge[2];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymSymEdge;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > _symX, _symY, _symZ;
    
   public:
    PmlContinuousSymmetric_PmlContinuousSymmetric(const unsigned int edge, PmlContinuousSymmetric *first, PmlContinuousSymmetric *second);
    ~PmlContinuousSymmetric_PmlContinuousSymmetric();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getUPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getUSymPml(const unsigned int i) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getUSymSymPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getV(const unsigned int i) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVSym(const unsigned int i, const unsigned int j) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVSymSym(const unsigned int i) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVEdge() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVSymEdge(const unsigned int i) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVSymSymEdge() const;
    
    PmlContinuousSymmetric *firstBoundary() const;
    PmlContinuousSymmetric *secondBoundary() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  
  
  
  class PmlContinuous_PmlContinuous_PmlContinuous : public Corner
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uPmlCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vC[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vEdge[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vCorner;
    
   public:
    PmlContinuous_PmlContinuous_PmlContinuous(const unsigned int corner, PmlContinuous_PmlContinuous *first, PmlContinuous_PmlContinuous *second, PmlContinuous_PmlContinuous *third);
    ~PmlContinuous_PmlContinuous_PmlContinuous();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getUPml() const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getV(const unsigned int i) const;
    
    PmlContinuous_PmlContinuous *firstEdge() const;
    PmlContinuous_PmlContinuous *secondEdge() const;
    PmlContinuous_PmlContinuous *thirdEdge() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify : public Corner
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vC[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymC[2][3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymSymC[3];
    
   public:
    PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify(const unsigned int corner, PmlContinuousSymmetric_PmlContinuousSymmetric *first, PmlContinuousSymmetric_PmlContinuousSymmetric *second, PmlContinuousSymmetric_PmlContinuousSymmetric *third);
    ~PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getV(const unsigned int i) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVSym(const unsigned int i, const unsigned int j) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVSymSym(const unsigned int i) const;
    
    PmlContinuousSymmetric_PmlContinuousSymmetric *firstEdge() const;
    PmlContinuousSymmetric_PmlContinuousSymmetric *secondEdge() const;
    PmlContinuousSymmetric_PmlContinuousSymmetric *thirdEdge() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };
  
  class PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric : public Corner
  {
   protected:
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uPmlCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uSymPmlCorner[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uSymSymPmlCorner[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _uSymSymSymPmlCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vC[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymC[3][3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymSymC[3][3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymSymSymC[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vEdge[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymEdge[3][3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymSymEdge[3][3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymSymSymEdge[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vCorner;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymCorner[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymSymCorner[3];
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * _vSymSymSymCorner;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > _symX, _symY, _symZ;
    
   public:
    PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric(const unsigned int corner, PmlContinuousSymmetric_PmlContinuousSymmetric *first, PmlContinuousSymmetric_PmlContinuousSymmetric *second, PmlContinuousSymmetric_PmlContinuousSymmetric *third);
    ~PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric();
    
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getV(const unsigned int i) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVSym(const unsigned int i, const unsigned int j) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVSymSym(const unsigned int i, const unsigned int j) const;
    gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >* getVSymSymSym(const unsigned int i) const;
    
    PmlContinuousSymmetric_PmlContinuousSymmetric *firstEdge() const;
    PmlContinuousSymmetric_PmlContinuousSymmetric *secondEdge() const;
    PmlContinuousSymmetric_PmlContinuousSymmetric *thirdEdge() const;
    
    void writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters) override;
  };


}

#endif // H_SUBPROBLEM3D
