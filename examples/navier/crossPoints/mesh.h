#ifndef H_MESH
#define H_MESH

#include <gmshfem/Function.h>

namespace D2 {

  int subdomain(const int index, const int jndex, const double xSize, const double ySize, const double circleSize, const double lc, const double pmlSizeE, const double pmlSizeN, const double pmlSizeW, const double pmlSizeS, bool transfinite);
  void monoDomain(const double lc, const double pmlSize, const double R, const int order);
  void checkerboard(const unsigned int nDomX, const unsigned int nDomY, const double xSize, const double ySize, const double circleSize, const double lc, const bool pml, const double pmlSize, const bool pmlExt, const double pmlSizeExt, const int order, const unsigned int circlePosX = 0, const unsigned int circlePosY = 0, bool transfinite = false);

}

namespace D3 {

  int subdomain(const int index, const int jndex, const int kndex, const double xSize, const double ySize, const double zSize, const double sphereSize, const double lc, const double pmlSizeE, const double pmlSizeN, const double pmlSizeW, const double pmlSizeS, const double pmlSizeD, const double pmlSizeU, bool transfinite);
  void monoDomain(const double lc, const double pmlSize, const double R, const int order);
  void checkerboard(const unsigned int nDomX, const unsigned int nDomY, const unsigned int nDomZ, const double xSize, const double ySize, const double zSize, const double sphereSize, const double lc, const bool pml, const double pmlSize, const bool pmlExt, const double pmlSizeExt, const int order, const unsigned int spherePosX = 0, const unsigned int spherePosY = 0, const unsigned int spherePosZ = 0, bool transfinite = false);
  
}

#endif // H_MESH
