#include "mesh.h"

#include "gmsh.h"

#include <gmshfem/Message.h>

#include <tuple>
#include <algorithm>

static int newPoint(const double x, const double y, const double z, const double lc)
{
  int tag = 0;
  std::vector< std::pair< int, int > > tags;
  gmsh::model::getEntitiesInBoundingBox(x, y, z, x, y, z, tags, 0);
  if(tags.size() != 0) {
    tag = tags[0].second;
  }
  else {
    tag = gmsh::model::geo::addPoint(x, y, z, lc);
  }
  return tag;
}

static int newLine(const int p0, const int p1)
{
  double xMin = 0., yMin = 0., zMin = 0., xMax = 0., yMax = 0., zMax = 0.;
  gmsh::model::getBoundingBox(0, p0, xMin, yMin, zMin, xMin, yMin, zMin);
  gmsh::model::getBoundingBox(0, p1, xMax, yMax, zMax, xMax, yMax, zMax);
  if(xMin > xMax) std::swap(xMin, xMax);
  if(yMin > yMax) std::swap(yMin, yMax);
  if(zMin > zMax) std::swap(zMin, zMax);
  
  int tag = 0;
  std::vector< std::pair< int, int > > tags;
  gmsh::model::getEntitiesInBoundingBox(xMin, yMin, zMin, xMax, yMax, zMax, tags, 1);
  if(tags.size() != 0) {
    for(unsigned int i = 0; i < tags.size(); ++i) {
      std::vector< std::pair< int, int > > outTags;
      gmsh::model::getBoundary({tags[i]}, outTags);
      if((std::abs(outTags[0].second) == p0 && std::abs(outTags[1].second) == p1) || (std::abs(outTags[0].second) == p1 && std::abs(outTags[1].second) == p0)) {
        tag = -tags[i].second;
        break;
      }
    }
  }
  
  if(tag == 0) {
    tag = gmsh::model::geo::addLine(p0, p1);
  }
  
  return tag;
}

static int newSurface(const int l0, const int l1, const int l2, const int l3)
{
  double xMin[4], yMin[4], zMin[4], xMax[4], yMax[4], zMax[4];
  gmsh::model::getBoundingBox(1, std::abs(l0), xMin[0], yMin[0], zMin[0], xMax[0], yMax[0], zMax[0]);
  gmsh::model::getBoundingBox(1, std::abs(l1), xMin[1], yMin[1], zMin[1], xMax[1], yMax[1], zMax[1]);
  gmsh::model::getBoundingBox(1, std::abs(l2), xMin[2], yMin[2], zMin[2], xMax[2], yMax[2], zMax[2]);
  gmsh::model::getBoundingBox(1, std::abs(l3), xMin[3], yMin[3], zMin[3], xMax[3], yMax[3], zMax[3]);
  xMin[0] = std::min({xMin[0], xMin[1], xMin[2], xMin[3]});
  yMin[0] = std::min({yMin[0], yMin[1], yMin[2], yMin[3]});
  zMin[0] = std::min({zMin[0], zMin[1], zMin[2], zMin[3]});
  xMax[0] = std::max({xMax[0], xMax[1], xMax[2], xMax[3]});
  yMax[0] = std::max({yMax[0], yMax[1], yMax[2], yMax[3]});
  zMax[0] = std::max({zMax[0], zMax[1], zMax[2], zMax[3]});
  
  int tag = 0;
  std::vector< std::pair< int, int > > tags;
  gmsh::model::getEntitiesInBoundingBox(xMin[0], yMin[0], zMin[0], xMax[0], yMax[0], zMax[0], tags, 2);
  if(tags.size() != 0) {
    for(unsigned int i = 0; i < tags.size(); ++i) {
      std::vector< std::pair< int, int > > outTags;
      gmsh::model::getBoundary({tags[i]}, outTags);
      std::vector< int > bndTag {std::abs(outTags[0].second), std::abs(outTags[1].second), std::abs(outTags[2].second), std::abs(outTags[3].second)};
      std::vector< int > myTag {std::abs(l0), std::abs(l1), std::abs(l2), std::abs(l3)};
      if(std::is_permutation(bndTag.begin(), bndTag.end(), myTag.begin())) {
        tag = -tags[i].second;
        break;
      }
    }
  }
  
  if(tag == 0) {
    int curveLoop = gmsh::model::geo::addCurveLoop({ l0, l1, l2, l3 }, -1, true);
    tag = gmsh::model::geo::addPlaneSurface({ curveLoop });
  }
  
  return tag;
}

static void fixOrientationOfLine(std::vector< int > &cubeLines, const std::vector< int > &cubePoints)
{
  for(unsigned int i = 0; i < 4; ++i) {
    // bottom
    {
      std::vector< std::pair< int, int > > outTags;
      gmsh::model::getBoundary({std::make_pair(1, cubeLines[i])}, outTags, false, false);
      if(outTags[0].second == cubePoints[i] && outTags[1].second == cubePoints[(i+1)%4]) {
        cubeLines[i] = cubeLines[i];
      }
      else {
        cubeLines[i] = -cubeLines[i];
      }
    }
    
    // vertical
    cubeLines[i+4] = std::abs(cubeLines[i+4]);
    
    // above
    {
      std::vector< std::pair< int, int > > outTags;
      gmsh::model::getBoundary({std::make_pair(1, cubeLines[i+8])}, outTags, false, false);
      if(outTags[0].second == cubePoints[i+4] && outTags[1].second == cubePoints[(i+1)%4+4]) {
        cubeLines[i+8] = cubeLines[i+8];
      }
      else {
        cubeLines[i+8] = -cubeLines[i+8];
      }
    }
  }
}

namespace D2 {


  int subdomain(const int index, const int jndex, const double xSize, const double ySize, const double circleSize, const double lc, const double pmlSizeE, const double pmlSizeN, const double pmlSizeW, const double pmlSizeS, bool transfinite)
  {
    // square
    double posX[4] = {xSize, xSize, 0., 0.};
    double posY[4] = {0., ySize, ySize, 0.};
    if(index != -1 && jndex != -1) {
      posX[0] = (index+1) * xSize;
      posX[1] = (index+1) * xSize;
      posX[2] = index * xSize;
      posX[3] = index * xSize;
      
      posY[0] = jndex * ySize;
      posY[1] = (jndex+1) * ySize;
      posY[2] = (jndex+1) * ySize;
      posY[3] = jndex * ySize;
    }
    std::vector< int > squarePoints(4);
    squarePoints[0] = newPoint(posX[0], posY[0], 0., lc);
    squarePoints[1] = newPoint(posX[1], posY[1], 0., lc);
    squarePoints[2] = newPoint(posX[2], posY[2], 0., lc);
    squarePoints[3] = newPoint(posX[3], posY[3], 0., lc);
    gmsh::model::geo::synchronize();
    
    std::vector< int > squareLines(4);
    for(unsigned int i = 0; i < 4; ++i) {
      squareLines[i] = newLine(squarePoints[i], squarePoints[(i+1)%4]);
    }
    gmsh::model::geo::synchronize();
    
    int squareCurveLoop = gmsh::model::geo::addCurveLoop(squareLines);
    int omega = 0;
    
    // circle
    std::vector< int > circleLines;
    if(circleSize != 0.) {
      int center = gmsh::model::geo::addPoint(posX[0] - xSize/2., posY[1] - ySize/2., 0., lc); // Center
      
      std::vector< int > circlePoints(4);
      circlePoints[0] = gmsh::model::geo::addPoint(posX[0] - xSize/2. + circleSize, posY[1] - ySize/2., 0., lc);
      circlePoints[1] = gmsh::model::geo::addPoint(posX[0] - xSize/2., posY[1] - ySize/2. + circleSize, 0., lc);
      circlePoints[2] = gmsh::model::geo::addPoint(posX[0] - xSize/2. - circleSize, posY[1] - ySize/2., 0., lc);
      circlePoints[3] = gmsh::model::geo::addPoint(posX[0] - xSize/2., posY[1] - ySize/2. - circleSize, 0., lc);
      
      circleLines.resize(4);
      for(unsigned int i = 0; i < 4; ++i) {
        circleLines[i] = gmsh::model::geo::addCircleArc(circlePoints[i], center, circlePoints[(i+1)%4]);
      }
      
      int circleCurveLoop = gmsh::model::geo::addCurveLoop(circleLines);
      omega = gmsh::model::geo::addPlaneSurface({ squareCurveLoop,circleCurveLoop });
    }
    else {
      omega = gmsh::model::geo::addPlaneSurface({ squareCurveLoop });
    }
    
    std::vector< int > pml(4);
    std::vector< int > pmlInf(4);
    std::vector< std::pair< int, int > > pmlBnd(4);
    std::vector< std::pair< int, int > > pmlPoint(4);
    const double pmlSizes[4] = {pmlSizeE, pmlSizeN, pmlSizeW, pmlSizeS};
    
    for(unsigned int i = 0; i < 4; ++i) {
      if(pmlSizes[i] != 0.) {
        const double orientation = i > 1 ? -1. : 1.;
        std::vector< std::pair< int, int > > extrudeEntities;
        gmsh::model::geo::extrude({std::make_pair(1, -squareLines[i])}, orientation * (i % 2 == 0 ? pmlSizes[i] : 0.), orientation * (i % 2 == 0 ? 0. : pmlSizes[i]), 0., extrudeEntities, {static_cast< int >(pmlSizes[i] / lc)}, std::vector< double >(), true);
        if(extrudeEntities.size() != 0) {
          pmlInf[i] = extrudeEntities[0].second;
          pml[i] = extrudeEntities[1].second;
          if(squareLines[i] < 0) {
            pmlBnd[i].first = extrudeEntities[2].second;
            pmlBnd[i].second = extrudeEntities[3].second;
          }
          else {
            pmlBnd[i].first = extrudeEntities[3].second;
            pmlBnd[i].second = extrudeEntities[4].second;
          }
        }
      }
    }
    
    // pmls corner
    std::vector< std::pair< int, int > > pmlCornerInf(4);
    std::vector< int > pmlCorner(4);
    
    for(unsigned int i = 0; i < 4; ++i) {
      if(pmlSizes[i] != 0. && pmlSizes[(i+1)%4] != 0.) {
        const double orientation = i == 0 || i == 3 ? 1. : -1.;
        std::vector< std::pair< int, int > > extrudeEntities;
        gmsh::model::geo::extrude({std::make_pair(1, -pmlBnd[i].second)}, orientation * (i % 2 == 0 ? 0. : pmlSizes[(i+1)%4]), orientation * (i % 2 == 0 ? pmlSizes[(i+1)%4] : 0.), 0., extrudeEntities, {static_cast< int >(pmlSizes[(i+1)%4] / lc)}, std::vector< double >(), true);
        if(extrudeEntities.size() != 0) {
          pmlCornerInf[i].first = extrudeEntities[0].second;
          pmlCorner[i] = extrudeEntities[1].second;
          pmlCornerInf[i].second = extrudeEntities[2].second;
        }
      }
    }
    
    gmsh::model::geo::synchronize();

    // physicals
    const unsigned int tag = (index != -1 && jndex != -1 ? index * 10000 + jndex * 100 : 0);
    std::string subdomainTag;
    if(index != -1 && jndex != -1) {
      subdomainTag = "_" + std::to_string(index) + "_" + std::to_string(jndex);
    }
    
    gmsh::model::addPhysicalGroup(2, {omega}, 1+tag);
    gmsh::model::setPhysicalName(2, 1+tag, "omega" + subdomainTag);
    if(circleLines.size() != 0) {
      gmsh::model::addPhysicalGroup(1, circleLines, 1);
      gmsh::model::setPhysicalName(1, 1, "gamma");
    }
    
    std::vector< std::string > dir {"E", "N", "W", "S"};
    for(unsigned int i = 0; i < 4; ++i) {
      gmsh::model::addPhysicalGroup(1, {squareLines[i]}, i+10+tag);
      gmsh::model::setPhysicalName(1, i+10+tag, "sigma" + dir[i] + subdomainTag);
      
      if(pml[i] != 0) {
        gmsh::model::addPhysicalGroup(2, {pml[i]}, i+10+tag);
        gmsh::model::setPhysicalName(2, i+10+tag, "pml" + dir[i] + subdomainTag);
      }
      
      if(pmlInf[i] != 0) {
        gmsh::model::addPhysicalGroup(1, {pmlInf[i]}, i+20+tag);
        gmsh::model::setPhysicalName(1, i+20+tag, "pmlInf" + dir[i] + subdomainTag);
      }
      
      if(pmlBnd[i].first != 0) {
        gmsh::model::addPhysicalGroup(1, {pmlBnd[i].first}, i+30+tag);
        gmsh::model::setPhysicalName(1, i+30+tag, "pmlBnd" + dir[i] + "_first" + subdomainTag);
      }
      
      if(pmlBnd[i].second != 0) {
        gmsh::model::addPhysicalGroup(1, {pmlBnd[i].second}, i+40+tag);
        gmsh::model::setPhysicalName(1, i+40+tag, "pmlBnd" + dir[i] + "_second" + subdomainTag);
      }
      
      if(pmlCorner[i] != 0) {
        gmsh::model::addPhysicalGroup(2, {pmlCorner[i]}, i+20+tag);
        gmsh::model::setPhysicalName(2, i+20+tag, "pmlCorner" + dir[i] + dir[(i+1)%4] + subdomainTag);
      }
      
      if(pmlCorner[i] != 0) {
        gmsh::model::addPhysicalGroup(1, {pmlCornerInf[i].first, pmlCornerInf[i].second}, i+50+tag);
        gmsh::model::setPhysicalName(1, i+50+tag, "pmlCornerInf" + dir[i] + dir[(i+1)%4] + subdomainTag);
      }
      
      gmsh::model::addPhysicalGroup(0, {squarePoints[(i+1)%4]}, i+1+tag);
      gmsh::model::setPhysicalName(0, i+1+tag, "corner" + dir[i] + dir[(i+1)%4] + subdomainTag);
    }

    gmsh::model::geo::synchronize();
    
    if(transfinite) {
      gmsh::model::mesh::setTransfiniteSurface(omega);
      gmsh::model::mesh::setRecombine(2, omega);
    }
    
    return omega;
  }
  
  void monoDomain(const double lc, const double pmlSize, const double R, const int order)
  {
    gmsh::model::add("mesh");
    
    subdomain(-1, -1, 2., 2., R, lc, pmlSize, pmlSize, pmlSize, pmlSize, false);
    
    gmsh::model::mesh::generate();
    gmsh::model::mesh::setOrder(order);
  }

  void checkerboard(const unsigned int nDomX, const unsigned int nDomY, const double xSize, const double ySize, const double circleSize, const double lc, const bool pml, const double pmlSize, const bool pmlExt, const double pmlSizeExt, const int order, const unsigned int circlePosX, const unsigned int circlePosY, bool transfinite)
  {
    std::vector< int > sources;
    for(int i = 0; i < nDomX; ++i) {
      for(int j = 0; j < nDomY; ++j) {
        double pmlSizeE = 0.;
        if(i == nDomX - 1) {
          if(pmlExt) {
            pmlSizeE = pmlSizeExt;
          }
        }
        else {
          if(pml) {
            pmlSizeE = pmlSize;
          }
        }
        
        double pmlSizeN = 0.;
        if(j == nDomY - 1) {
          if(pmlExt) {
            pmlSizeN = pmlSizeExt;
          }
        }
        else {
          if(pml) {
            pmlSizeN = pmlSize;
          }
        }
        
        double pmlSizeW = 0.;
        if(i == 0) {
          if(pmlExt) {
            pmlSizeW = pmlSizeExt;
          }
        }
        else {
          if(pml) {
            pmlSizeW = pmlSize;
          }
        }
        
        double pmlSizeS = 0.;
        if(j == 0) {
          if(pmlExt) {
            pmlSizeS = pmlSizeExt;
          }
        }
        else {
          if(pml) {
            pmlSizeS = pmlSize;
          }
        }
        
        int sur = subdomain(i, j, xSize, ySize, (circlePosX == i && circlePosY == j ? circleSize : 0.), lc, pmlSizeE, pmlSizeN, pmlSizeW, pmlSizeS, transfinite);
        
//        if(j == nDomY - 1) {
//          int pEmbeded = gmsh::model::geo::addPoint((i+0.5) * xSize, (j+0.5) * ySize, 0., lc);
//          sources.push_back(pEmbeded);
//          gmsh::model::geo::synchronize();
//          gmsh::model::mesh::embed(0, {pEmbeded}, 2, sur);
//        }
      }
    }
    
//    int tag = gmsh::model::addPhysicalGroup(0, sources);
//    gmsh::model::setPhysicalName(0, tag, "sources");

    gmsh::model::mesh::generate();
    gmsh::model::mesh::setOrder(order);
  }


}

namespace D3 {


  int subdomain(const int index, const int jndex, const int kndex, const double xSize, const double ySize, const double zSize, const double sphereSize, const double lc, const double pmlSizeE, const double pmlSizeN, const double pmlSizeW, const double pmlSizeS, const double pmlSizeD, const double pmlSizeU, bool transfinite)
  {
    double posX[8] = {xSize, xSize, 0., 0., xSize, xSize, 0., 0.};
    double posY[8] = {0., ySize, ySize, 0., 0., ySize, ySize, 0.};
    double posZ[8] = {0., 0., 0., 0., zSize, zSize, zSize, zSize};
    if(index != -1 && jndex != -1) {
      posX[0] = (index+1) * xSize;
      posX[1] = (index+1) * xSize;
      posX[2] = index * xSize;
      posX[3] = index * xSize;
      posX[4] = (index+1) * xSize;
      posX[5] = (index+1) * xSize;
      posX[6] = index * xSize;
      posX[7] = index * xSize;
      
      posY[0] = jndex * ySize;
      posY[1] = (jndex+1) * ySize;
      posY[2] = (jndex+1) * ySize;
      posY[3] = jndex * ySize;
      posY[4] = jndex * ySize;
      posY[5] = (jndex+1) * ySize;
      posY[6] = (jndex+1) * ySize;
      posY[7] = jndex * ySize;
      
      posZ[0] = kndex * zSize;
      posZ[1] = kndex * zSize;
      posZ[2] = kndex * zSize;
      posZ[3] = kndex * zSize;
      posZ[4] = (kndex+1) * zSize;
      posZ[5] = (kndex+1) * zSize;
      posZ[6] = (kndex+1) * zSize;
      posZ[7] = (kndex+1) * zSize;
    }
    
    // square
    std::vector< int > cubePoints(8);
    cubePoints[0] = newPoint(posX[0], posY[0], posZ[0], lc);
    cubePoints[1] = newPoint(posX[1], posY[1], posZ[1], lc);
    cubePoints[2] = newPoint(posX[2], posY[2], posZ[2], lc);
    cubePoints[3] = newPoint(posX[3], posY[3], posZ[3], lc);

    cubePoints[4] = newPoint(posX[4], posY[4], posZ[4], lc);
    cubePoints[5] = newPoint(posX[5], posY[5], posZ[5], lc);
    cubePoints[6] = newPoint(posX[6], posY[6], posZ[6], lc);
    cubePoints[7] = newPoint(posX[7], posY[7], posZ[7], lc);
    gmsh::model::geo::synchronize();
//    gmshfem::msg::warning << "Points : " << cubePoints[0] << " " << cubePoints[1] << " " << cubePoints[2] << " " << cubePoints[3] << " " << cubePoints[4] << " " << cubePoints[5] << " " << cubePoints[6] << " " << cubePoints[7] << gmshfem::msg::endl;
    
    std::vector< int > cubeLines(12);
    cubeLines[0] = newLine(cubePoints[0], cubePoints[1]);
    cubeLines[1] = newLine(cubePoints[1], cubePoints[2]);
    cubeLines[2] = newLine(cubePoints[2], cubePoints[3]);
    cubeLines[3] = newLine(cubePoints[3], cubePoints[0]);
    
    cubeLines[4] = newLine(cubePoints[0], cubePoints[4]);
    cubeLines[5] = newLine(cubePoints[1], cubePoints[5]);
    cubeLines[6] = newLine(cubePoints[2], cubePoints[6]);
    cubeLines[7] = newLine(cubePoints[3], cubePoints[7]);
    
    cubeLines[8] = newLine(cubePoints[4], cubePoints[5]);
    cubeLines[9] = newLine(cubePoints[5], cubePoints[6]);
    cubeLines[10] = newLine(cubePoints[6], cubePoints[7]);
    cubeLines[11] = newLine(cubePoints[7], cubePoints[4]);
    gmsh::model::geo::synchronize();
    fixOrientationOfLine(cubeLines, cubePoints);
//    gmshfem::msg::warning << "Lines : " << cubeLines[0] << " " << cubeLines[1] << " " << cubeLines[2] << " " << cubeLines[3] << " " << cubeLines[4] << " " << cubeLines[5] << " " << cubeLines[6] << " " << cubeLines[7] << " " << cubeLines[8] << " " << cubeLines[9] << " " << cubeLines[10] << " " << cubeLines[11] << gmshfem::msg::endl;
    
    std::vector< int > cubeSurface(6);
    cubeSurface[0] = newSurface(cubeLines[0], cubeLines[5], cubeLines[8], cubeLines[4]);
    cubeSurface[1] = newSurface(cubeLines[1], cubeLines[6], cubeLines[9], cubeLines[5]);
    cubeSurface[2] = newSurface(cubeLines[2], cubeLines[7], cubeLines[10], cubeLines[6]);
    cubeSurface[3] = newSurface(cubeLines[3], cubeLines[4], cubeLines[11], cubeLines[7]);
    cubeSurface[4] = newSurface(-cubeLines[0], cubeLines[1], cubeLines[2], cubeLines[3]);
    cubeSurface[5] = newSurface(cubeLines[8], cubeLines[9], cubeLines[10], cubeLines[11]);
//    gmshfem::msg::warning << "Surfaces : " << cubeSurface[0] << " " << cubeSurface[1] << " " << cubeSurface[2] << " " << cubeSurface[3] << " " << cubeSurface[4] << " " << cubeSurface[5] << gmshfem::msg::endl;
    
    int cubeSurfaceLoop = gmsh::model::geo::addSurfaceLoop(cubeSurface);
    int omega = 0;
    
    // sphere
    std::vector< int > sphereSurface;
    if(sphereSize != 0.) {
      int center = gmsh::model::geo::addPoint(posX[0] - xSize/2., posY[1]- ySize/2., posZ[4] - zSize/2., lc); // Center
      
      std::vector< int > spherePoints(6);
      spherePoints[0] = gmsh::model::geo::addPoint(posX[0] - xSize/2. + sphereSize, posY[1] - ySize/2., posZ[4] - zSize/2., lc);
      spherePoints[1] = gmsh::model::geo::addPoint(posX[0] - xSize/2., posY[1] - ySize/2. + sphereSize, posZ[4] - zSize/2., lc);
      spherePoints[2] = gmsh::model::geo::addPoint(posX[0] - xSize/2. - sphereSize, posY[1] - ySize/2., posZ[4] - zSize/2., lc);
      spherePoints[3] = gmsh::model::geo::addPoint(posX[0] - xSize/2., posY[1] - ySize/2. - sphereSize, posZ[4] - zSize/2., lc);
      spherePoints[4] = gmsh::model::geo::addPoint(posX[0] - xSize/2., posY[1] - ySize/2., posZ[4] - zSize/2 + sphereSize, lc);
      spherePoints[5] = gmsh::model::geo::addPoint(posX[0] - xSize/2., posY[1] - ySize/2., posZ[4] - zSize/2 - sphereSize, lc);
      
      std::vector< int > sphereLines(12);
      sphereLines[0] = gmsh::model::geo::addCircleArc(spherePoints[0], center, spherePoints[1]);
      sphereLines[1] = gmsh::model::geo::addCircleArc(spherePoints[1], center, spherePoints[2]);
      sphereLines[2] = gmsh::model::geo::addCircleArc(spherePoints[2], center, spherePoints[3]);
      sphereLines[3] = gmsh::model::geo::addCircleArc(spherePoints[3], center, spherePoints[0]);
      
      sphereLines[4] = gmsh::model::geo::addCircleArc(spherePoints[0], center, spherePoints[4]);
      sphereLines[5] = gmsh::model::geo::addCircleArc(spherePoints[4], center, spherePoints[2]);
      sphereLines[6] = gmsh::model::geo::addCircleArc(spherePoints[2], center, spherePoints[5]);
      sphereLines[7] = gmsh::model::geo::addCircleArc(spherePoints[5], center, spherePoints[0]);
      
      sphereLines[8] = gmsh::model::geo::addCircleArc(spherePoints[1], center, spherePoints[4]);
      sphereLines[9] = gmsh::model::geo::addCircleArc(spherePoints[4], center, spherePoints[3]);
      sphereLines[10] = gmsh::model::geo::addCircleArc(spherePoints[3], center, spherePoints[5]);
      sphereLines[11] = gmsh::model::geo::addCircleArc(spherePoints[5], center, spherePoints[1]);
      
      std::vector< int > sphereCurveLoop(8);
      sphereCurveLoop[0] = gmsh::model::geo::addCurveLoop({ sphereLines[0], sphereLines[8], sphereLines[4] }, -1, true);
      sphereCurveLoop[1] = gmsh::model::geo::addCurveLoop({ sphereLines[1], sphereLines[5], sphereLines[8] }, -1, true);
      sphereCurveLoop[2] = gmsh::model::geo::addCurveLoop({ sphereLines[2], sphereLines[9], sphereLines[5] }, -1, true);
      sphereCurveLoop[3] = gmsh::model::geo::addCurveLoop({ sphereLines[3], sphereLines[4], sphereLines[9] }, -1, true);
      
      sphereCurveLoop[4] = gmsh::model::geo::addCurveLoop({ -sphereLines[0], sphereLines[7], sphereLines[11] }, -1, true);
      sphereCurveLoop[5] = gmsh::model::geo::addCurveLoop({ -sphereLines[1], sphereLines[11], sphereLines[6] }, -1, true);
      sphereCurveLoop[6] = gmsh::model::geo::addCurveLoop({ -sphereLines[2], sphereLines[6], sphereLines[10] }, -1, true);
      sphereCurveLoop[7] = gmsh::model::geo::addCurveLoop({ -sphereLines[3], sphereLines[10], sphereLines[7] }, -1, true);
      
      sphereSurface.resize(8);
      sphereSurface[0] = gmsh::model::geo::addSurfaceFilling({ sphereCurveLoop[0] }, -1, center);
      sphereSurface[1] = gmsh::model::geo::addSurfaceFilling({ sphereCurveLoop[1] }, -1, center);
      sphereSurface[2] = gmsh::model::geo::addSurfaceFilling({ sphereCurveLoop[2] }, -1, center);
      sphereSurface[3] = gmsh::model::geo::addSurfaceFilling({ sphereCurveLoop[3] }, -1, center);
      
      sphereSurface[4] = gmsh::model::geo::addSurfaceFilling({ sphereCurveLoop[4] }, -1, center);
      sphereSurface[5] = gmsh::model::geo::addSurfaceFilling({ sphereCurveLoop[5] }, -1, center);
      sphereSurface[6] = gmsh::model::geo::addSurfaceFilling({ sphereCurveLoop[6] }, -1, center);
      sphereSurface[7] = gmsh::model::geo::addSurfaceFilling({ sphereCurveLoop[7] }, -1, center);
      
      int sphereSurfaceLoop = gmsh::model::geo::addSurfaceLoop(sphereSurface);
      omega = gmsh::model::geo::addVolume({sphereSurfaceLoop,cubeSurfaceLoop});
    }
    else {
      omega = gmsh::model::geo::addVolume({cubeSurfaceLoop});
    }
        
    // pmls
    std::vector< int > pml(6);
    std::vector< int > pmlInf(6);
    std::vector< std::tuple< int, int, int, int > > pmlBnd(6);
    const double pmlSizes[6] = {pmlSizeE, pmlSizeN, pmlSizeW, pmlSizeS, pmlSizeD, pmlSizeU};
    const int extrudePmlFace[6][4][6] = {
      {
        {0, 0, 0, 1, 1, 0},
        {0, 1, 0, 1, 1, 1},
        {0, 0, 1, 1, 1, 1},
        {0, 0, 0, 1, 0, 1}
      },
      {
        {0, 0, 0, 1, 1, 0},
        {0, 0, 0, 0, 1, 1},
        {0, 0, 1, 1, 1, 1},
        {1, 0, 0, 1, 1, 1}
      },
      {
        {0, 0, 0, 1, 1, 0},
        {0, 0, 0, 1, 0, 1},
        {0, 0, 1, 1, 1, 1},
        {0, 1, 0, 1, 1, 1}
      },
      {
        {0, 0, 0, 1, 1, 0},
        {1, 0, 0, 1, 1, 1},
        {0, 0, 1, 1, 1, 1},
        {0, 0, 0, 0, 1, 1}
      },
      {
        {1, 0, 0, 1, 1, 1},
        {0, 0, 0, 1, 0, 1},
        {0, 0, 0, 0, 1, 1},
        {0, 1, 0, 1, 1, 1}
      },
      {
        {1, 0, 0, 1, 1, 1},
        {0, 1, 0, 1, 1, 1},
        {0, 0, 0, 0, 1, 1},
        {0, 0, 0, 1, 0, 1}
      }
    };

    for(unsigned int i = 0; i < 6; ++i) {
      if(pmlSizes[i] != 0.) {
        const double orientation = i > 1 ? -1. : 1.;
        std::vector< std::pair< int, int > > extrudeEntities;
        gmsh::model::geo::extrude({std::make_pair(2, cubeSurface[i])}, orientation * (i < 4 ? ((i%4) % 2 == 0 ? pmlSizes[i] : 0.) : 0.), orientation * (i < 4 ? ((i%4) % 2 == 0 ? 0. : pmlSizes[i]) : 0.), (i > 3 ? (i == 4 ? -pmlSizes[i] : pmlSizes[i]) : 0.), extrudeEntities, {static_cast< int >(pmlSizes[i] / lc)}, std::vector< double >(), true);
        pmlInf[i] = extrudeEntities[0].second;
        pml[i] = extrudeEntities[1].second;
      }
    }
    
    gmsh::model::geo::synchronize();
    
    for(unsigned int i = 0; i < 6; ++i) {
      if(pmlSizes[i] != 0.) {
        double bbx[2], bby[2], bbz[2];
        gmsh::model::getBoundingBox(3, pml[i], bbx[0], bby[0], bbz[0], bbx[1], bby[1], bbz[1]);
        
        for(unsigned int j = 0; j < 4; ++j) {
          std::vector< std::pair< int, int > > tags;
          gmsh::model::getEntitiesInBoundingBox(bbx[extrudePmlFace[i][j][0]], bby[extrudePmlFace[i][j][1]], bbz[extrudePmlFace[i][j][2]], bbx[extrudePmlFace[i][j][3]], bby[extrudePmlFace[i][j][4]], bbz[extrudePmlFace[i][j][5]], tags, 2);
          if(tags.size() == 0) {
            gmshfem::msg::error << "Unable to find a PML boundary" << gmshfem::msg::endl;
            return -1;
          }
          else if(tags.size() > 1) {
            for(unsigned int k = 0; k < tags.size(); ++k) {
              double tmpbbx[2], tmpbby[2], tmpbbz[2];
              gmsh::model::getBoundingBox(tags[k].first, tags[k].second, tmpbbx[0], tmpbby[0], tmpbbz[0], tmpbbx[1], tmpbby[1], tmpbbz[1]);
              if(tmpbbx[0] == bbx[extrudePmlFace[i][j][0]] && tmpbbx[1] == bbx[extrudePmlFace[i][j][3]] && tmpbby[0] == bby[extrudePmlFace[i][j][1]] && tmpbby[1] == bby[extrudePmlFace[i][j][4]] && tmpbbz[0] == bbz[extrudePmlFace[i][j][2]] && tmpbbz[1] == bbz[extrudePmlFace[i][j][5]]) {
                tags[0].first = tags[k].first;
                tags[0].second = tags[k].second;
                break;
              }
            }
          }
          
          if(j == 0) {
            std::get<0>(pmlBnd[i]) = tags[0].second;
          }
          else if(j == 1) {
            std::get<1>(pmlBnd[i]) = tags[0].second;
          }
          else if(j == 2) {
            std::get<2>(pmlBnd[i]) = tags[0].second;
          }
          else if(j == 3) {
            std::get<3>(pmlBnd[i]) = tags[0].second;
          }
        }
      }
    }

    // pmls edge
    std::vector< int > pmlEdge(12);
    std::vector< std::vector< int > > pmlEdgeInf(12);
    std::vector< std::pair< int, int > > pmlEdgeBnd(12);
    std::vector< int > edge(12);
    const std::vector< std::vector< double > > pmlEdgeExtrude {
      std::vector< double > {0., pmlSizeN, 0.}, // E
      std::vector< double > {-pmlSizeW, 0., 0.}, // N
      std::vector< double > {0., -pmlSizeS, 0.}, // W
      std::vector< double > {pmlSizeE, 0., 0.}, // S
    };
    const int extrudePmlEdge[12][3][6] = {
      // Down
      {
        {0, 0, 0, 1, 0, 1},
        {0, 1, 0, 1, 1, 1},
        {1, 0, 0, 1, 1, 1}
      },
      {
        {1, 0, 0, 1, 1, 1},
        {0, 0, 0, 0, 1, 1},
        {0, 1, 0, 1, 1, 1}
      },
      {
        {0, 1, 0, 1, 1, 1},
        {0, 0, 0, 1, 0, 1},
        {0, 0, 0, 0, 1, 1}
      },
      {
        {0, 0, 0, 0, 1, 1},
        {1, 0, 0, 1, 1, 1},
        {0, 0, 0, 1, 0, 1}
      },
      // Rot
      {
        {0, 0, 0, 1, 1, 0},
        {0, 0, 1, 1, 1, 1},
        {1, 0, 0, 1, 1, 1}
      },
      {
        {0, 0, 0, 1, 1, 0},
        {0, 0, 1, 1, 1, 1},
        {0, 1, 0, 1, 1, 1}
      },
      {
        {0, 0, 0, 1, 1, 0},
        {0, 0, 1, 1, 1, 1},
        {0, 0, 0, 0, 1, 1}
      },
      {
        {0, 0, 0, 1, 1, 0},
        {0, 0, 1, 1, 1, 1},
        {0, 0, 0, 1, 0, 1}
      },
      // Up
      {
        {0, 0, 0, 1, 0, 1},
        {0, 1, 0, 1, 1, 1},
        {1, 0, 0, 1, 1, 1}
      },
      {
        {1, 0, 0, 1, 1, 1},
        {0, 0, 0, 0, 1, 1},
        {0, 1, 0, 1, 1, 1}
      },
      {
        {0, 1, 0, 1, 1, 1},
        {0, 0, 0, 1, 0, 1},
        {0, 0, 0, 0, 1, 1}
      },
      {
        {0, 0, 0, 0, 1, 1},
        {1, 0, 0, 1, 1, 1},
        {0, 0, 0, 1, 0, 1}
      }
    };
    
    for(unsigned int i = 0; i < 4; ++i) {
      std::vector< std::pair< int, int > > extrudeEntities;
      // Down
      if(pmlSizeD != 0.) {
        gmsh::model::geo::extrude({std::make_pair(2, std::get<0>(pmlBnd[i]))}, 0., 0., -pmlSizeD, extrudeEntities, {static_cast< int >(pmlSizeD / lc)}, std::vector< double >(), true);
        pmlEdgeInf[i].push_back(extrudeEntities[0].second);
        pmlEdge[i] = extrudeEntities[1].second;
        extrudeEntities.clear();
      }
      
      // Rot
      if(std::abs(pmlEdgeExtrude[i][0] + pmlEdgeExtrude[i][1]) != 0.) {
        gmsh::model::geo::extrude({std::make_pair(2, std::get<1>(pmlBnd[i]))}, pmlEdgeExtrude[i][0], pmlEdgeExtrude[i][1], pmlEdgeExtrude[i][2], extrudeEntities, {static_cast< int >(std::abs(pmlEdgeExtrude[i][0] + pmlEdgeExtrude[i][1]) / lc)}, std::vector< double >(), true);
        pmlEdgeInf[i+4].push_back(extrudeEntities[0].second);
        pmlEdge[i+4] = extrudeEntities[1].second;
        extrudeEntities.clear();
      }
      
      // Up
      if(pmlSizeU != 0.) {
        gmsh::model::geo::extrude({std::make_pair(2, std::get<2>(pmlBnd[i]))}, 0., 0., pmlSizeU, extrudeEntities, {static_cast< int >(pmlSizeU / lc)}, std::vector< double >(), true);
        pmlEdgeInf[i+8].push_back(extrudeEntities[0].second);
        pmlEdge[i+8] = extrudeEntities[1].second;
        extrudeEntities.clear();
      }
    }
    
    gmsh::model::geo::synchronize();
    
    for(unsigned int i = 0; i < 4; ++i) {
      // Down
      if(pmlSizeD != 0.) {
        double bbx[2], bby[2], bbz[2];
        gmsh::model::getBoundingBox(3, pmlEdge[i], bbx[0], bby[0], bbz[0], bbx[1], bby[1], bbz[1]);
        
        for(unsigned int j = 0; j < 3; ++j) {
          std::vector< std::pair< int, int > > tags;
          gmsh::model::getEntitiesInBoundingBox(bbx[extrudePmlEdge[i][j][0]], bby[extrudePmlEdge[i][j][1]], bbz[extrudePmlEdge[i][j][2]], bbx[extrudePmlEdge[i][j][3]], bby[extrudePmlEdge[i][j][4]], bbz[extrudePmlEdge[i][j][5]], tags, 2);
          if(tags.size() == 0) {
            gmshfem::msg::error << "Unable to find a edge-PML boundary" << gmshfem::msg::endl;
            return -1;
          }
          else if(tags.size() > 1) {
            for(unsigned int k = 0; k < tags.size(); ++k) {
              double tmpbbx[2], tmpbby[2], tmpbbz[2];
              gmsh::model::getBoundingBox(tags[k].first, tags[k].second, tmpbbx[0], tmpbby[0], tmpbbz[0], tmpbbx[1], tmpbby[1], tmpbbz[1]);
              if(tmpbbx[0] == bbx[extrudePmlEdge[i][j][0]] && tmpbbx[1] == bbx[extrudePmlEdge[i][j][3]] && tmpbby[0] == bby[extrudePmlEdge[i][j][1]] && tmpbby[1] == bby[extrudePmlEdge[i][j][4]] && tmpbbz[0] == bbz[extrudePmlEdge[i][j][2]] && tmpbbz[1] == bbz[extrudePmlEdge[i][j][5]]) {
                tags[0].first = tags[k].first;
                tags[0].second = tags[k].second;
                break;
              }
            }
          }
          
          if(j == 0) {
            pmlEdgeBnd[i].first = tags[0].second;
          }
          else if(j == 1) {
            pmlEdgeBnd[i].second = tags[0].second;
          }
          else if(j == 2) {
            pmlEdgeInf[i].push_back(tags[0].second);
          }
        }
      }
      edge[i] = cubeLines[i];
      
      // Rot
      if(std::abs(pmlEdgeExtrude[i][0] + pmlEdgeExtrude[i][1]) != 0.) {
        double bbx[2], bby[2], bbz[2];
        gmsh::model::getBoundingBox(3, pmlEdge[i+4], bbx[0], bby[0], bbz[0], bbx[1], bby[1], bbz[1]);
        
        for(unsigned int j = 0; j < 3; ++j) {
          std::vector< std::pair< int, int > > tags;
          gmsh::model::getEntitiesInBoundingBox(bbx[extrudePmlEdge[i+4][j][0]], bby[extrudePmlEdge[i+4][j][1]], bbz[extrudePmlEdge[i+4][j][2]], bbx[extrudePmlEdge[i+4][j][3]], bby[extrudePmlEdge[i+4][j][4]], bbz[extrudePmlEdge[i+4][j][5]], tags, 2);
          if(tags.size() == 0) {
            gmshfem::msg::error << "Unable to find a edge-PML boundary" << gmshfem::msg::endl;
            return -1;
          }
          else if(tags.size() > 1) {
            for(unsigned int k = 0; k < tags.size(); ++k) {
              double tmpbbx[2], tmpbby[2], tmpbbz[2];
              gmsh::model::getBoundingBox(tags[k].first, tags[k].second, tmpbbx[0], tmpbby[0], tmpbbz[0], tmpbbx[1], tmpbby[1], tmpbbz[1]);
              if(tmpbbx[0] == bbx[extrudePmlEdge[i][j][0]] && tmpbbx[1] == bbx[extrudePmlEdge[i][j][3]] && tmpbby[0] == bby[extrudePmlEdge[i][j][1]] && tmpbby[1] == bby[extrudePmlEdge[i][j][4]] && tmpbbz[0] == bbz[extrudePmlEdge[i][j][2]] && tmpbbz[1] == bbz[extrudePmlEdge[i][j][5]]) {
                tags[0].first = tags[k].first;
                tags[0].second = tags[k].second;
                break;
              }
            }
          }
          
          if(j == 0) {
            pmlEdgeBnd[i+4].first = tags[0].second;
          }
          else if(j == 1) {
            pmlEdgeBnd[i+4].second = tags[0].second;
          }
          else if(j == 2) {
            pmlEdgeInf[i+4].push_back(tags[0].second);
          }
        }
      }
      edge[i+4] = cubeLines[(i+1)%4+4];
      
      // Up
      if(pmlSizeU != 0.) {
        double bbx[2], bby[2], bbz[2];
        gmsh::model::getBoundingBox(3, pmlEdge[i+8], bbx[0], bby[0], bbz[0], bbx[1], bby[1], bbz[1]);
        
        for(unsigned int j = 0; j < 3; ++j) {
          std::vector< std::pair< int, int > > tags;
          gmsh::model::getEntitiesInBoundingBox(bbx[extrudePmlEdge[i+8][j][0]], bby[extrudePmlEdge[i+8][j][1]], bbz[extrudePmlEdge[i+8][j][2]], bbx[extrudePmlEdge[i+8][j][3]], bby[extrudePmlEdge[i+8][j][4]], bbz[extrudePmlEdge[i+8][j][5]], tags, 2);
          if(tags.size() == 0) {
            gmshfem::msg::error << "Unable to find a edge-PML boundary" << gmshfem::msg::endl;
            return -1;
          }
          else if(tags.size() > 1) {
            for(unsigned int k = 0; k < tags.size(); ++k) {
              double tmpbbx[2], tmpbby[2], tmpbbz[2];
              gmsh::model::getBoundingBox(tags[k].first, tags[k].second, tmpbbx[0], tmpbby[0], tmpbbz[0], tmpbbx[1], tmpbby[1], tmpbbz[1]);
              if(tmpbbx[0] == bbx[extrudePmlEdge[i][j][0]] && tmpbbx[1] == bbx[extrudePmlEdge[i][j][3]] && tmpbby[0] == bby[extrudePmlEdge[i][j][1]] && tmpbby[1] == bby[extrudePmlEdge[i][j][4]] && tmpbbz[0] == bbz[extrudePmlEdge[i][j][2]] && tmpbbz[1] == bbz[extrudePmlEdge[i][j][5]]) {
                tags[0].first = tags[k].first;
                tags[0].second = tags[k].second;
                break;
              }
            }
          }
          
          if(j == 0) {
            pmlEdgeBnd[i+8].first = tags[0].second;
          }
          else if(j == 1) {
            pmlEdgeBnd[i+8].second = tags[0].second;
          }
          else if(j == 2) {
            pmlEdgeInf[i+8].push_back(tags[0].second);
          }
        }
      }
      edge[i+8] = cubeLines[i+8];
    }
        
    // pmls corner
    std::vector< int > pmlCorner(8);
    std::vector< std::tuple< int, int, int > > pmlCornerEdge(8);
    std::vector< std::vector< int > > pmlCornerInf(8);
    std::vector< int > corner(8);
    const int extrudePmlCorner[8][2][6] = {
      // Down
      {
        {0, 1, 0, 1, 1, 1},
        {1, 0, 0, 1, 1, 1}
      },
      {
        {0, 0, 0, 0, 1, 1},
        {0, 1, 0, 1, 1, 1}
      },
      {
        {0, 0, 0, 1, 0, 1},
        {0, 0, 0, 0, 1, 1}
      },
      {
        {0, 0, 0, 1, 0, 1},
        {1, 0, 0, 1, 1, 1}
      },
      // Up
      {
        {0, 1, 0, 1, 1, 1},
        {1, 0, 0, 1, 1, 1}
      },
      {
        {0, 0, 0, 0, 1, 1},
        {0, 1, 0, 1, 1, 1}
      },
      {
        {0, 0, 0, 1, 0, 1},
        {0, 0, 0, 0, 1, 1}
      },
      {
        {0, 0, 0, 1, 0, 1},
        {1, 0, 0, 1, 1, 1}
      }
    };
    
    for(unsigned int i = 0; i < 4; ++i) {
      std::vector< std::pair< int, int > > extrudeEntities;
      // Down
      if(pmlSizeD != 0.) {
        gmsh::model::geo::extrude({std::make_pair(2, pmlEdgeBnd[i+4].first)}, 0., 0., -pmlSizeD, extrudeEntities, {static_cast< int >(pmlSizeD / lc)}, std::vector< double >(), true);
        pmlCornerInf[i].push_back(extrudeEntities[0].second);
        pmlCorner[i] = extrudeEntities[1].second;
        extrudeEntities.clear();
      }
      
      // Up
      if(pmlSizeU != 0.) {
        gmsh::model::geo::extrude({std::make_pair(2, pmlEdgeBnd[i+4].second)}, 0., 0., pmlSizeU, extrudeEntities, {static_cast< int >(pmlSizeU / lc)}, std::vector< double >(), true);
        pmlCornerInf[i+4].push_back(extrudeEntities[0].second);
        pmlCorner[i+4] = extrudeEntities[1].second;
        extrudeEntities.clear();
      }
    }
    
    gmsh::model::geo::synchronize();

    for(unsigned int i = 0; i < 4; ++i) {
      std::vector< std::pair< int, int > > extrudeEntities;
      // Down
      if(pmlSizeD != 0.) {
        double bbx[2], bby[2], bbz[2];
        gmsh::model::getBoundingBox(3, pmlCorner[i], bbx[0], bby[0], bbz[0], bbx[1], bby[1], bbz[1]);
        
        for(unsigned int j = 0; j < 2; ++j) {
          std::vector< std::pair< int, int > > tags;
          gmsh::model::getEntitiesInBoundingBox(bbx[extrudePmlCorner[i][j][0]], bby[extrudePmlCorner[i][j][1]], bbz[extrudePmlCorner[i][j][2]], bbx[extrudePmlCorner[i][j][3]], bby[extrudePmlCorner[i][j][4]], bbz[extrudePmlCorner[i][j][5]], tags, 2);
          if(tags.size() == 0) {
            gmshfem::msg::error << "Unable to find a corner-PML boundary" << gmshfem::msg::endl;
            return -1;
          }
          else if(tags.size() > 1) {
            for(unsigned int k = 0; k < tags.size(); ++k) {
              double tmpbbx[2], tmpbby[2], tmpbbz[2];
              gmsh::model::getBoundingBox(tags[k].first, tags[k].second, tmpbbx[0], tmpbby[0], tmpbbz[0], tmpbbx[1], tmpbby[1], tmpbbz[1]);
              if(tmpbbx[0] == bbx[extrudePmlCorner[i][j][0]] && tmpbbx[1] == bbx[extrudePmlCorner[i][j][3]] && tmpbby[0] == bby[extrudePmlCorner[i][j][1]] && tmpbby[1] == bby[extrudePmlCorner[i][j][4]] && tmpbbz[0] == bbz[extrudePmlCorner[i][j][2]] && tmpbbz[1] == bbz[extrudePmlCorner[i][j][5]]) {
                tags[0].first = tags[k].first;
                tags[0].second = tags[k].second;
                break;
              }
            }
          }
          
          pmlCornerInf[i].push_back(tags[0].second);
        }
      }
      corner[i] = cubePoints[(i+1)%4];
      
      // Up
      if(pmlSizeU != 0.) {
        double bbx[2], bby[2], bbz[2];
        gmsh::model::getBoundingBox(3, pmlCorner[i+4], bbx[0], bby[0], bbz[0], bbx[1], bby[1], bbz[1]);
        
        for(unsigned int j = 0; j < 2; ++j) {
          std::vector< std::pair< int, int > > tags;
          gmsh::model::getEntitiesInBoundingBox(bbx[extrudePmlCorner[i+4][j][0]], bby[extrudePmlCorner[i+4][j][1]], bbz[extrudePmlCorner[i+4][j][2]], bbx[extrudePmlCorner[i+4][j][3]], bby[extrudePmlCorner[i+4][j][4]], bbz[extrudePmlCorner[i+4][j][5]], tags, 2);
          if(tags.size() == 0) {
            gmshfem::msg::error << "Unable to find a corner-PML boundary" << gmshfem::msg::endl;
            return -1;
          }
          else if(tags.size() > 1) {
            for(unsigned int k = 0; k < tags.size(); ++k) {
              double tmpbbx[2], tmpbby[2], tmpbbz[2];
              gmsh::model::getBoundingBox(tags[k].first, tags[k].second, tmpbbx[0], tmpbby[0], tmpbbz[0], tmpbbx[1], tmpbby[1], tmpbbz[1]);
              if(tmpbbx[0] == bbx[extrudePmlCorner[i][j][0]] && tmpbbx[1] == bbx[extrudePmlCorner[i][j][3]] && tmpbby[0] == bby[extrudePmlCorner[i][j][1]] && tmpbby[1] == bby[extrudePmlCorner[i][j][4]] && tmpbbz[0] == bbz[extrudePmlCorner[i][j][2]] && tmpbbz[1] == bbz[extrudePmlCorner[i][j][5]]) {
                tags[0].first = tags[k].first;
                tags[0].second = tags[k].second;
                break;
              }
            }
          }
          
          pmlCornerInf[i+4].push_back(tags[0].second);
        }
      }
      corner[i+4] = cubePoints[(i+1)%4+4];
    }

    std::vector< std::pair< int, int > > entites;
    std::vector< std::pair< int, int > > boundary;
    gmsh::model::getEntities(entites, 1);
    for(unsigned int i = 0; i < entites.size(); ++i) {
      double xmin = 0., ymin = 0., zmin = 0.;
      double xmax = 0., ymax = 0., zmax = 0.;
      gmsh::model::getBoundingBox(entites[i].first, entites[i].second, xmin, ymin, zmin, xmax, ymax, zmax);
      gmsh::model::getBoundary({ entites[i] }, boundary, true, false, false);

      // down
      if(boundary[0].second == cubePoints[1] || boundary[1].second  == cubePoints[1]) {
        if(xmin == posX[1] && xmax == posX[1] + pmlSizeE && ymin == posY[1] && ymax == posY[1] && zmin == posZ[1] && zmax == posZ[1]) {
          std::get< 0 >(pmlCornerEdge[0]) = entites[i].second;
        }
        else if(xmin == posX[1] && xmax == posX[1] && ymin == posY[1] && ymax == posY[1] + pmlSizeN && zmin == posZ[1] && zmax == posZ[1]) {
          std::get< 1 >(pmlCornerEdge[0]) = entites[i].second;
        }
        else if(xmin == posX[1] && xmax == posX[1] && ymin == posY[1] && ymax == posY[1] && zmin == posZ[1] - pmlSizeD && zmax == posZ[1]) {
          std::get< 2 >(pmlCornerEdge[0]) = entites[i].second;
        }
      }
      else if(boundary[0].second == cubePoints[2] || boundary[1].second  == cubePoints[2]) {
        if(xmin == posX[2] - pmlSizeW && xmax == posX[2] && ymin == posY[2] && ymax == posY[2] && zmin == posZ[2] && zmax == posZ[2]) {
          std::get< 1 >(pmlCornerEdge[1]) = entites[i].second;
        }
        else if(xmin == posX[2] && xmax == posX[2] && ymin == posY[2] && ymax == posY[2] + pmlSizeN && zmin == posZ[2] && zmax == posZ[2]) {
          std::get< 0 >(pmlCornerEdge[1]) = entites[i].second;
        }
        else if(xmin == posX[2] && xmax == posX[2] && ymin == posY[2] && ymax == posY[2] && zmin == posZ[2] - pmlSizeD && zmax == posZ[2]) {
          std::get< 2 >(pmlCornerEdge[1]) = entites[i].second;
        }
      }
      else if(boundary[0].second == cubePoints[3] || boundary[1].second  == cubePoints[3]) {
        if(xmin == posX[3] - pmlSizeW && xmax == posX[3] && ymin == posY[3] && ymax == posY[3] && zmin == posZ[3] && zmax == posZ[3]) {
          std::get< 0 >(pmlCornerEdge[2]) = entites[i].second;
        }
        else if(xmin == posX[3] && xmax == posX[3] && ymin == posY[3] - pmlSizeS && ymax == posY[3] && zmin == posZ[3] && zmax == posZ[3]) {
          std::get< 1 >(pmlCornerEdge[2]) = entites[i].second;
        }
        else if(xmin == posX[3] && xmax == posX[3] && ymin == posY[3] && ymax == posY[3] && zmin == posZ[3] - pmlSizeD && zmax == posZ[3]) {
          std::get< 2 >(pmlCornerEdge[2]) = entites[i].second;
        }
      }
      else if(boundary[0].second == cubePoints[0] || boundary[1].second  == cubePoints[0]) {
        if(xmin == posX[0] && xmax == posX[0] + pmlSizeE && ymin == posY[0] && ymax == posY[0] && zmin == posZ[0] && zmax == posZ[0]) {
          std::get< 1 >(pmlCornerEdge[3]) = entites[i].second;
        }
        else if(xmin == posX[0] && xmax == posX[0] && ymin == posY[0] - pmlSizeS && ymax == posY[0] && zmin == posZ[0] && zmax == posZ[0]) {
          std::get< 0 >(pmlCornerEdge[3]) = entites[i].second;
        }
        else if(xmin == posX[0] && xmax == posX[0] && ymin == posY[0] && ymax == posY[0] && zmin == posZ[0] - pmlSizeD && zmax == posZ[0]) {
          std::get< 2 >(pmlCornerEdge[3]) = entites[i].second;
        }
      }
      
      // up
      if(boundary[0].second == cubePoints[5] || boundary[1].second  == cubePoints[5]) {
        if(xmin == posX[5] && xmax == posX[5] + pmlSizeE && ymin == posY[5] && ymax == posY[5] && zmin == posZ[5] && zmax == posZ[5]) {
          std::get< 0 >(pmlCornerEdge[4]) = entites[i].second;
        }
        else if(xmin == posX[5] && xmax == posX[5] && ymin == posY[5] && ymax == posY[5] + pmlSizeN && zmin == posZ[5] && zmax == posZ[5]) {
          std::get< 1 >(pmlCornerEdge[4]) = entites[i].second;
        }
        else if(xmin == posX[5] && xmax == posX[5] && ymin == posY[5] && ymax == posY[5] && zmin == posZ[5] && zmax == posZ[5] + pmlSizeU) {
          std::get< 2 >(pmlCornerEdge[4]) = entites[i].second;
        }
      }
      else if(boundary[0].second == cubePoints[6] || boundary[1].second  == cubePoints[6]) {
        if(xmin == posX[6] - pmlSizeW && xmax == posX[6] && ymin == posY[6] && ymax == posY[6] && zmin == posZ[6] && zmax == posZ[6]) {
          std::get< 1 >(pmlCornerEdge[5]) = entites[i].second;
        }
        else if(xmin == posX[6] && xmax == posX[6] && ymin == posY[6] && ymax == posY[6] + pmlSizeN && zmin == posZ[6] && zmax == posZ[6]) {
          std::get< 0 >(pmlCornerEdge[5]) = entites[i].second;
        }
        else if(xmin == posX[6] && xmax == posX[6] && ymin == posY[6] && ymax == posY[6] && zmin == posZ[6] && zmax == posZ[6] + pmlSizeU) {
          std::get< 2 >(pmlCornerEdge[5]) = entites[i].second;
        }
      }
      else if(boundary[0].second == cubePoints[7] || boundary[1].second  == cubePoints[7]) {
        if(xmin == posX[7] - pmlSizeW && xmax == posX[7] && ymin == posY[7] && ymax == posY[7] && zmin == posZ[7] && zmax == posZ[7]) {
          std::get< 0 >(pmlCornerEdge[6]) = entites[i].second;
        }
        else if(xmin == posX[7] && xmax == posX[7] && ymin == posY[7] - pmlSizeS && ymax == posY[7] && zmin == posZ[7] && zmax == posZ[7]) {
          std::get< 1 >(pmlCornerEdge[6]) = entites[i].second;
        }
        else if(xmin == posX[7] && xmax == posX[7] && ymin == posY[7] && ymax == posY[7] && zmin == posZ[7] && zmax == posZ[7] + pmlSizeU) {
          std::get< 2 >(pmlCornerEdge[6]) = entites[i].second;
        }
      }
      else if(boundary[0].second == cubePoints[4] || boundary[1].second  == cubePoints[4]) {
        if(xmin == posX[4] && xmax == posX[4] + pmlSizeE && ymin == posY[4] && ymax == posY[4] && zmin == posZ[4] && zmax == posZ[4]) {
          std::get< 1 >(pmlCornerEdge[7]) = entites[i].second;
        }
        else if(xmin == posX[4] && xmax == posX[4] && ymin == posY[4] - pmlSizeS && ymax == posY[4] && zmin == posZ[4] && zmax == posZ[4]) {
          std::get< 0 >(pmlCornerEdge[7]) = entites[i].second;
        }
        else if(xmin == posX[4] && xmax == posX[4] && ymin == posY[4] && ymax == posY[4] && zmin == posZ[4] && zmax == posZ[4] + pmlSizeU) {
          std::get< 2 >(pmlCornerEdge[7]) = entites[i].second;
        }
      }
      
      // up
    }
    
    // physicals
    const unsigned int tag = (index != -1 && jndex != -1 && kndex != -1 ? index * 10000000 + jndex * 100000 + kndex * 1000 : 0);
    std::string subdomainTag;
    if(index != -1 && jndex != -1) {
      subdomainTag = "_" + std::to_string(index) + "_" + std::to_string(jndex) + "_" + std::to_string(kndex);
    }
    
    gmsh::model::addPhysicalGroup(3, {omega}, 1 + tag);
    gmsh::model::setPhysicalName(3, 1 + tag, "omega" + subdomainTag);
    if(sphereSurface.size() != 0) {
      gmsh::model::addPhysicalGroup(2, sphereSurface, 1);
      gmsh::model::setPhysicalName(2, 1, "gamma");
    }

    std::vector< std::string > dir {"E", "N", "W", "S", "D", "U"};
    for(unsigned int i = 0; i < 6; ++i) {
      gmsh::model::addPhysicalGroup(2, {cubeSurface[i]}, i+10 + tag);
      gmsh::model::setPhysicalName(2, i+10 + tag, "sigma" + dir[i] + subdomainTag);

      if(pml[i] != 0) {
        gmsh::model::addPhysicalGroup(3, {pml[i]}, i+10 + tag);
        gmsh::model::setPhysicalName(3, i+10 + tag, "pml" + dir[i] + subdomainTag);
      }

      if(pmlInf[i] != 0) {
        gmsh::model::addPhysicalGroup(2, {pmlInf[i]}, i+20 + tag);
        gmsh::model::setPhysicalName(2, i+20 + tag, "pmlInf" + dir[i] + subdomainTag);
      }

      if(i == 4) { // Down
        if(std::get<0>(pmlBnd[i]) != 0) {
          gmsh::model::addPhysicalGroup(2, {std::get<0>(pmlBnd[i])}, i+30 + tag);
          gmsh::model::setPhysicalName(2, i+30 + tag, "pmlBnd" + dir[i] + "_first" + subdomainTag);
        }

        if(std::get<1>(pmlBnd[i]) != 0) {
          gmsh::model::addPhysicalGroup(2, {std::get<1>(pmlBnd[i])}, i+40 + tag);
          gmsh::model::setPhysicalName(2, i+40 + tag, "pmlBnd" + dir[i] + "_fourth" + subdomainTag);
        }
        
        if(std::get<2>(pmlBnd[i]) != 0) {
          gmsh::model::addPhysicalGroup(2, {std::get<2>(pmlBnd[i])}, i+50 + tag);
          gmsh::model::setPhysicalName(2, i+50 + tag, "pmlBnd" + dir[i] + "_third" + subdomainTag);
        }
        
        if(std::get<3>(pmlBnd[i]) != 0) {
          gmsh::model::addPhysicalGroup(2, {std::get<3>(pmlBnd[i])}, i+60 + tag);
          gmsh::model::setPhysicalName(2, i+60 + tag, "pmlBnd" + dir[i] + "_second" + subdomainTag);
        }
      }
      else {
        if(std::get<0>(pmlBnd[i]) != 0) {
          gmsh::model::addPhysicalGroup(2, {std::get<0>(pmlBnd[i])}, i+30 + tag);
          gmsh::model::setPhysicalName(2, i+30 + tag, "pmlBnd" + dir[i] + "_first" + subdomainTag);
        }

        if(std::get<1>(pmlBnd[i]) != 0) {
          gmsh::model::addPhysicalGroup(2, {std::get<1>(pmlBnd[i])}, i+40 + tag);
          gmsh::model::setPhysicalName(2, i+40 + tag, "pmlBnd" + dir[i] + "_second" + subdomainTag);
        }
        
        if(std::get<2>(pmlBnd[i]) != 0) {
          gmsh::model::addPhysicalGroup(2, {std::get<2>(pmlBnd[i])}, i+50 + tag);
          gmsh::model::setPhysicalName(2, i+50 + tag, "pmlBnd" + dir[i] + "_third" + subdomainTag);
        }
        
        if(std::get<3>(pmlBnd[i]) != 0) {
          gmsh::model::addPhysicalGroup(2, {std::get<3>(pmlBnd[i])}, i+60 + tag);
          gmsh::model::setPhysicalName(2, i+60 + tag, "pmlBnd" + dir[i] + "_fourth" + subdomainTag);
        }
      }
    }
    
    for(unsigned int i = 0; i < 4; ++i) {
      if(pmlEdge[i] != 0) {
        gmsh::model::addPhysicalGroup(3, {pmlEdge[i]}, i+20 + tag);
        gmsh::model::setPhysicalName(3, i+20 + tag, "pml" + dir[i] + "D" + subdomainTag);
      }
      
      if(pmlEdgeInf[i].size() != 0) {
        gmsh::model::addPhysicalGroup(2, pmlEdgeInf[i], i+100 + tag);
        gmsh::model::setPhysicalName(2, i+100 + tag, "pmlInf" + dir[i] + "D" + subdomainTag);
      }
      
      if(pmlEdgeBnd[i].first != 0) {
        gmsh::model::addPhysicalGroup(2, {pmlEdgeBnd[i].first}, i+200 + tag);
        gmsh::model::setPhysicalName(2, i+200 + tag, "pmlBnd" + dir[i] + "D_first" + subdomainTag);
      }
      
      if(pmlEdgeBnd[i].second != 0) {
        gmsh::model::addPhysicalGroup(2, {pmlEdgeBnd[i].second}, i+210 + tag);
        gmsh::model::setPhysicalName(2, i+210 + tag, "pmlBnd" + dir[i] + "D_second" + subdomainTag);
      }
      
      if(edge[i] != 0) {
        gmsh::model::addPhysicalGroup(1, {edge[i]}, i+20 + tag);
        gmsh::model::setPhysicalName(1, i+20 + tag, "edge" + dir[i] + "D" + subdomainTag);
      }
      
      
      if(pmlEdge[i+4] != 0) {
        gmsh::model::addPhysicalGroup(3, {pmlEdge[i+4]}, i+30 + tag);
        gmsh::model::setPhysicalName(3, i+30 + tag, "pml" + dir[i] + dir[(i+1)%4] + subdomainTag);
      }
      
      if(pmlEdgeInf[i+4].size() != 0) {
        gmsh::model::addPhysicalGroup(2, pmlEdgeInf[i+4], i+110 + tag);
        gmsh::model::setPhysicalName(2, i+110 + tag, "pmlInf" + dir[i] + dir[(i+1)%4] + subdomainTag);
      }
      
      if(pmlEdgeBnd[i+4].first != 0) {
        gmsh::model::addPhysicalGroup(2, {pmlEdgeBnd[i+4].first}, i+220 + tag);
        gmsh::model::setPhysicalName(2, i+220 + tag, "pmlBnd" + dir[i] + dir[(i+1)%4] + "_first" + subdomainTag);
      }
      
      if(pmlEdgeBnd[i+4].second != 0) {
        gmsh::model::addPhysicalGroup(2, {pmlEdgeBnd[i+4].second}, i+230 + tag);
        gmsh::model::setPhysicalName(2, i+230 + tag, "pmlBnd" + dir[i] + dir[(i+1)%4] + "_second" + subdomainTag);
      }
      
      if(edge[i+4] != 0) {
        gmsh::model::addPhysicalGroup(1, {edge[i+4]}, i+30 + tag);
        gmsh::model::setPhysicalName(1, i+30 + tag, "edge" + dir[i] + dir[(i+1)%4] + subdomainTag);
      }
      
      
      if(pmlEdge[i+8] != 0) {
        gmsh::model::addPhysicalGroup(3, {pmlEdge[i+8]}, i+40 + tag);
        gmsh::model::setPhysicalName(3, i+40 + tag, "pml" + dir[i] + "U" + subdomainTag);
      }
      
      if(pmlEdgeInf[i+8].size() != 0) {
        gmsh::model::addPhysicalGroup(2, pmlEdgeInf[i+8], i+120 + tag);
        gmsh::model::setPhysicalName(2, i+120 + tag, "pmlInf" + dir[i] + "U" + subdomainTag);
      }
      
      if(pmlEdgeBnd[i+8].first != 0) {
        gmsh::model::addPhysicalGroup(2, {pmlEdgeBnd[i+8].first}, i+240 + tag);
        gmsh::model::setPhysicalName(2, i+240 + tag, "pmlBnd" + dir[i] + "U_first" + subdomainTag);
      }
      
      if(pmlEdgeBnd[i+8].second != 0) {
        gmsh::model::addPhysicalGroup(2, {pmlEdgeBnd[i+8].second}, i+250 + tag);
        gmsh::model::setPhysicalName(2, i+250 + tag, "pmlBnd" + dir[i] + "U_second" + subdomainTag);
      }
      
      if(edge[i+8] != 0) {
        gmsh::model::addPhysicalGroup(1, {edge[i+8]}, i+40 + tag);
        gmsh::model::setPhysicalName(1, i+40 + tag, "edge" + dir[i] + "U" + subdomainTag);
      }
      
      
      
      if(pmlCorner[i] != 0) {
        gmsh::model::addPhysicalGroup(3, {pmlCorner[i]}, i+50 + tag);
        gmsh::model::setPhysicalName(3, i+50 + tag, "pml" + dir[i] + dir[(i+1)%4] + "D" + subdomainTag);
      }
      
      if(pmlCornerInf[i].size() != 0) {
        gmsh::model::addPhysicalGroup(2, pmlCornerInf[i], i+130 + tag);
        gmsh::model::setPhysicalName(2, i+130 + tag, "pmlInf" + dir[i] + dir[(i+1)%4] + "D" + subdomainTag);
      }
      
      if(corner[i] != 0) {
        gmsh::model::addPhysicalGroup(0, {corner[i]}, i+10 + tag);
        gmsh::model::setPhysicalName(0, i+10 + tag, "corner" + dir[i] + dir[(i+1)%4] + "D" + subdomainTag);
      }
      
      if(std::get< 0 >(pmlCornerEdge[i]) != 0) {
        gmsh::model::addPhysicalGroup(1, {std::get< 0 >(pmlCornerEdge[i])}, i+100 + tag);
        gmsh::model::setPhysicalName(1, i+100 + tag, "cornerEdge" + dir[i] + dir[(i+1)%4] + "D_first" + subdomainTag);
      }
      
      if(std::get< 1 >(pmlCornerEdge[i]) != 0) {
        gmsh::model::addPhysicalGroup(1, {std::get< 1 >(pmlCornerEdge[i])}, i+110 + tag);
        gmsh::model::setPhysicalName(1, i+110 + tag, "cornerEdge" + dir[i] + dir[(i+1)%4] + "D_second" + subdomainTag);
      }
      
      if(std::get< 2 >(pmlCornerEdge[i]) != 0) {
        gmsh::model::addPhysicalGroup(1, {std::get< 2 >(pmlCornerEdge[i])}, i+120 + tag);
        gmsh::model::setPhysicalName(1, i+120 + tag, "cornerEdge" + dir[i] + dir[(i+1)%4] + "D_third" + subdomainTag);
      }
      
      if(pmlCorner[i+4] != 0) {
        gmsh::model::addPhysicalGroup(3, {pmlCorner[i+4]}, i+60 + tag);
        gmsh::model::setPhysicalName(3, i+60 + tag, "pml" + dir[i] + dir[(i+1)%4] + "U" + subdomainTag);
      }
      
      if(pmlCornerInf[i+4].size() != 0) {
        gmsh::model::addPhysicalGroup(2, pmlCornerInf[i+4], i+140 + tag);
        gmsh::model::setPhysicalName(2, i+140 + tag, "pmlInf" + dir[i] + dir[(i+1)%4] + "U" + subdomainTag);
      }
      
      if(corner[i+4] != 0) {
        gmsh::model::addPhysicalGroup(0, {corner[i+4]}, i+20 + tag);
        gmsh::model::setPhysicalName(0, i+20 + tag, "corner" + dir[i] + dir[(i+1)%4] + "U" + subdomainTag);
      }
      
      if(std::get< 0 >(pmlCornerEdge[i+4]) != 0) {
        gmsh::model::addPhysicalGroup(1, {std::get< 0 >(pmlCornerEdge[i+4])}, i+130 + tag);
        gmsh::model::setPhysicalName(1, i+130 + tag, "cornerEdge" + dir[i] + dir[(i+1)%4] + "U_first" + subdomainTag);
      }
      
      if(std::get< 1 >(pmlCornerEdge[i+4]) != 0) {
        gmsh::model::addPhysicalGroup(1, {std::get< 1 >(pmlCornerEdge[i+4])}, i+140 + tag);
        gmsh::model::setPhysicalName(1, i+140 + tag, "cornerEdge" + dir[i] + dir[(i+1)%4] + "U_second" + subdomainTag);
      }
      
      if(std::get< 2 >(pmlCornerEdge[i+4]) != 0) {
        gmsh::model::addPhysicalGroup(1, {std::get< 2 >(pmlCornerEdge[i+4])}, i+150 + tag);
        gmsh::model::setPhysicalName(1, i+150 + tag, "cornerEdge" + dir[i] + dir[(i+1)%4] + "U_third" + subdomainTag);
      }
    }
    
    if(transfinite) {
      gmsh::model::mesh::setTransfiniteSurface(cubeSurface[0]);
      gmsh::model::mesh::setTransfiniteSurface(cubeSurface[1]);
      gmsh::model::mesh::setTransfiniteSurface(cubeSurface[2]);
      gmsh::model::mesh::setTransfiniteSurface(cubeSurface[3]);
      gmsh::model::mesh::setTransfiniteSurface(cubeSurface[4]);
      gmsh::model::mesh::setTransfiniteSurface(cubeSurface[5]);
      
      gmsh::model::mesh::setTransfiniteVolume(omega, cubePoints);
    }
    
    return omega;
  }
  
  void monoDomain(const double lc, const double pmlSize, const double R, const int order)
  {
    gmsh::model::add("mesh");
    
    subdomain(-1, -1, -1, 2., 2., 2., R, lc, pmlSize, pmlSize, pmlSize, pmlSize, pmlSize, pmlSize, false);
    
    gmsh::model::mesh::generate();
    gmsh::model::mesh::setOrder(order);
  }
  
  void checkerboard(const unsigned int nDomX, const unsigned int nDomY, const unsigned int nDomZ, const double xSize, const double ySize, const double zSize, const double sphereSize, const double lc, const bool pml, const double pmlSize, const bool pmlExt, const double pmlSizeExt, const int order, const unsigned int spherePosX, const unsigned int spherePosY, const unsigned int spherePosZ, bool transfinite)
  {
    int numSubdomain = 0;
    for(int i = 0; i < nDomX; ++i) {
      for(int j = 0; j < nDomY; ++j) {
        for(int k = 0; k < nDomZ; ++k) {
          double pmlSizeE = 0.;
          if(i == nDomX - 1) {
            if(pmlExt) {
              pmlSizeE = pmlSizeExt;
            }
          }
          else {
            if(pml) {
              pmlSizeE = pmlSize;
            }
          }
          
          double pmlSizeN = 0.;
          if(j == nDomY - 1) {
            if(pmlExt) {
              pmlSizeN = pmlSizeExt;
            }
          }
          else {
            if(pml) {
              pmlSizeN = pmlSize;
            }
          }
          
          double pmlSizeW = 0.;
          if(i == 0) {
            if(pmlExt) {
              pmlSizeW = pmlSizeExt;
            }
          }
          else {
            if(pml) {
              pmlSizeW = pmlSize;
            }
          }
          
          double pmlSizeS = 0.;
          if(j == 0) {
            if(pmlExt) {
              pmlSizeS = pmlSizeExt;
            }
          }
          else {
            if(pml) {
              pmlSizeS = pmlSize;
            }
          }
          
          double pmlSizeD = 0.;
          if(k == 0) {
            if(pmlExt) {
              pmlSizeD = pmlSizeExt;
            }
          }
          else {
            if(pml) {
              pmlSizeD = pmlSize;
            }
          }
          
          double pmlSizeU = 0.;
          if(k == nDomZ - 1) {
            if(pmlExt) {
              pmlSizeU = pmlSizeExt;
            }
          }
          else {
            if(pml) {
              pmlSizeU = pmlSize;
            }
          }
          
          subdomain(i, j, k, xSize, ySize, zSize, (spherePosX == i && spherePosY == j && spherePosZ == k ? sphereSize : 0.), lc, pmlSizeE, pmlSizeN, pmlSizeW, pmlSizeS, pmlSizeD, pmlSizeU, transfinite);
          gmshfem::msg::info << "Geometry of subdomain " << numSubdomain++ << " is done" << gmshfem::msg::endl;
        }
      }
    }

    gmsh::model::mesh::generate();
    gmsh::model::mesh::setOrder(order);
  }
  
  
}
