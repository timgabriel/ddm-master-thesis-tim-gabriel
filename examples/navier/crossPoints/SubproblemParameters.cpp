#include "SubproblemParameters.h"

SubproblemParameters::SubproblemParameters()
{

}

SubproblemParameters::~SubproblemParameters()
{
}

void SubproblemParameters::setGauss(const std::string &gauss)
{
  _gauss = gauss;
}

std::string SubproblemParameters::getGauss() const
{
  return _gauss;
}

void SubproblemParameters::setKappaP(const gmshfem::function::ScalarFunction< std::complex< double > > &kappa)
{
  _kappaP = kappa;
}

gmshfem::function::ScalarFunction< std::complex< double > > SubproblemParameters::getKappaP() const
{
  return _kappaP;
}

void SubproblemParameters::setKappaS(const gmshfem::function::ScalarFunction< std::complex< double > > &kappa)
{
  _kappaS = kappa;
}

gmshfem::function::ScalarFunction< std::complex< double > > SubproblemParameters::getKappaS() const
{
  return _kappaS;
}

void SubproblemParameters::setC(const gmshfem::function::TensorFunction< std::complex< double >, 4 > &C)
{
  _C = C;
}

gmshfem::function::TensorFunction< std::complex< double >, 4 > SubproblemParameters::getC() const
{
  return _C;
}

void SubproblemParameters::setNeumannOrder(const unsigned int neumannOrder)
{
  _neumannOrder = neumannOrder;
}

unsigned int SubproblemParameters::getNeumannOrder() const
{
  return _neumannOrder;
}

void SubproblemParameters::setFieldOrder(const unsigned int fieldOrder)
{
  _fieldOrder = fieldOrder;
}

unsigned int SubproblemParameters::getFieldOrder() const
{
  return _fieldOrder;
}

void SubproblemParameters::setStab(const double stab)
{
  _stab = stab;
}

double SubproblemParameters::getStab() const
{
  return _stab;
}
