# Navier cross-points
> Anthony Royer

Dependency(ies):
* GmshDDM : 1.0.0
* GmshFEM : 1.0.0
* ...

## Project description

In this work, we present a non-overlapping substructured DDM with PML transmission conditions for checkerboard (Cartesian) decompositions that takes cross-points into account. 
In such decompositions, each subdomain is surrounded by PMLs associated to edges and corners. 

## Installation

```
mkdir build && cd build
cmake ..
make
sudo make install
```

## Usage

Simply run:
```
  ./example [PARAM]
```

## References

