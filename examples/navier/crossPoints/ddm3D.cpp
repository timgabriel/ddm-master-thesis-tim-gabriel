#include "ddm3D.h"

#include "mesh.h"
#include "SubproblemDomains.h"
#include "Subproblem3D.h"

#include <gmshfem/GmshFem.h>
#include <gmshfem/FieldInterface.h>
#include <gmshfem/Formulation.h>
#include <gmshfem/AnalyticalFunction.h>
#include <gmshfem/Post.h>
#include <gmshfem/Function.h>
#include <gmshfem/io.h>

#include <gmshddm/GmshDdm.h>
#include <gmshddm/Subdomain.h>
#include <gmshddm/Interface.h>
#include <gmshddm/SubdomainField.h>
#include <gmshddm/InterfaceField.h>
#include <gmshddm/Formulation.h>
#include <gmshddm/MPIInterface.h>

#include <algorithm>
#include <fstream>
#include <vector>

using gmshfem::equation::dof;
using gmshfem::equation::tf;
using gmshfem::function::operator-;
using gmshfem::function::operator*;
using gmshfem::function::abs;

namespace D3 {


  void ddm()
  {
    gmshddm::common::GmshDdm *gmshDdm = gmshddm::common::GmshDdm::currentInstance();

    // ************************
    // P H Y S I C S
    // ************************
    double pi = 3.14159265359;
    double kp = 0.5 * pi;
    gmshDdm->userDefinedParameter(kp, "kp");
    double ks = 1. * pi;
    gmshDdm->userDefinedParameter(ks, "ks");
    double R = 0.5;
    gmshDdm->userDefinedParameter(R, "R");
    std::string benchmark = "scattering"; // scattering, salt
    gmshDdm->userDefinedParameter(benchmark, "benchmark");

    // ************************
    // M E S H
    // ************************
    double lc = 0.2; // other = 0.06666666666666
    gmshDdm->userDefinedParameter(lc, "lc");
    int meshOrder = 1;
    gmshDdm->userDefinedParameter(meshOrder, "meshOrder");

    // ************************
    // M O D E L I N G
    // ************************
    std::string boundary = "sommerfeld"; // sommerfeld, pml
    gmshDdm->userDefinedParameter(boundary, "boundary");
    std::string boundaryExt = "sommerfeld"; // sommerfeld, pml
    gmshDdm->userDefinedParameter(boundaryExt, "boundaryExt");
    std::string pmlMethod = "continuous"; // continuous, continuousSymmetric
    gmshDdm->userDefinedParameter(pmlMethod, "pmlMethod");
    std::string pmlMethodExt = "continuous"; // continuous, continuousSymmetric
    gmshDdm->userDefinedParameter(pmlMethodExt, "pmlMethodExt");
    std::string pmlType = "hs"; // hs, h, q
    gmshDdm->userDefinedParameter(pmlType, "pmlType");
    std::string pmlTypeExt = "hs"; // hs, h, q
    gmshDdm->userDefinedParameter(pmlTypeExt, "pmlTypeExt");
    int scatterPosX = 0, scatterPosY = 0;
    gmshDdm->userDefinedParameter(scatterPosX, "scatterPosX");
    gmshDdm->userDefinedParameter(scatterPosY, "scatterPosY");


    unsigned int nDomX = 2, nDomY = 2, nDomZ = 2;
    gmshDdm->userDefinedParameter(nDomX, "nDomX");
    gmshDdm->userDefinedParameter(nDomY, "nDomY");
    gmshDdm->userDefinedParameter(nDomZ, "nDomZ");
    unsigned int iterMax = 1000;
    gmshDdm->userDefinedParameter(iterMax, "iterMax");
    double res = 1e-6;
    gmshDdm->userDefinedParameter(res, "res");
    double sizeX = 2., sizeY = 2., sizeZ = 2.;
    gmshDdm->userDefinedParameter(sizeX, "sizeX");
    gmshDdm->userDefinedParameter(sizeY, "sizeY");
    gmshDdm->userDefinedParameter(sizeZ, "sizeZ");

    unsigned int N = 6;
    gmshDdm->userDefinedParameter(N, "N");
    double pmlSize = N * lc;
    if(pmlSize == 0.) {
      pmlSize = 0.3;
    }

    unsigned int NExt = N;
    gmshDdm->userDefinedParameter(NExt, "NExt");
    double pmlSizeExt = NExt * lc;
    if(pmlSizeExt == 0.) {
      pmlSizeExt = 0.3;
    }

    int fieldOrder = 1;
    gmshDdm->userDefinedParameter(fieldOrder, "fieldOrder");
    int neumannOrder = 1;
    gmshDdm->userDefinedParameter(neumannOrder, "neumannOrder");
    std::string gauss = "Gauss" + std::to_string(2 * std::max(fieldOrder, neumannOrder) + 1);
    gmshDdm->userDefinedParameter(gauss, "gauss");
    double stab = 0.11;
    gmshDdm->userDefinedParameter(stab, "stab");
    double thetaPade = 0.;
    gmshDdm->userDefinedParameter(thetaPade, "thetaPade");
    thetaPade *= pi;

    // ************************
    // P O S T
    // ************************
    bool onPlane = false;
    gmshDdm->userDefinedParameter(onPlane, "onPlane");
    bool monoDomainError = false;
    gmshDdm->userDefinedParameter(monoDomainError, "monoDomainError");
    std::string fileName = "none";
    gmshDdm->userDefinedParameter(fileName, "file");
    bool saveEMono = false;
    gmshDdm->userDefinedParameter(saveEMono, "saveEMono");
    bool saveUMono = false;
    gmshDdm->userDefinedParameter(saveUMono, "saveUMono");
    bool saveU = false;
    gmshDdm->userDefinedParameter(saveU, "saveU");
    bool wavenumberPlot = false;
    gmshDdm->userDefinedParameter(wavenumberPlot, "wavenumber");
    std::string mesh = "";
    gmshDdm->userDefinedParameter(mesh, "mesh");
    std::string inputPath = "../";
    gmshDdm->userDefinedParameter(inputPath, "inputPath");
    std::string outputPath = "";
    gmshDdm->userDefinedParameter(outputPath, "outputPath");
    bool saveMesh = false;
    gmshDdm->userDefinedParameter(saveMesh, "saveMesh");

    gmsh::option::setNumber("Mesh.Binary", 1);
    gmsh::option::setNumber("Mesh.Format", 1);
    if(mesh != "") {
      gmsh::open(mesh + ".msh");
    }
    else {
      if(benchmark == "scattering") {
        checkerboard(nDomX, nDomY, nDomZ, sizeX, sizeY, sizeZ, R, lc, (boundary == "pml"), pmlSize, (boundaryExt == "pml"), pmlSizeExt, meshOrder, 0, 0, 0);
      }
      else if (benchmark == "salt") {
        checkerboard(nDomX, nDomY, nDomZ, 13500./nDomX, 13500./nDomY, 4180./nDomZ, 0., lc, (boundary == "pml"), pmlSize, (boundaryExt == "pml"), pmlSizeExt, meshOrder, -1, -1, true);
        sizeX = 13500./nDomX;
        sizeY = 13500./nDomY;
        sizeZ = 4180./nDomZ;
      }
      if(saveMesh) {
        gmsh::write("mesh.msh");
      }
    }

    if(gmshddm::mpi::getMPIRank() == 0) {
      gmshfem::msg::info << "Running 'ddm3D'" << gmshfem::msg::endl;
      gmshfem::msg::info << "Parameters:" << gmshfem::msg::endl;
      gmshfem::msg::info << " * physics:" << gmshfem::msg::endl;
      if(benchmark == "scattering") {
        gmshfem::msg::info << "   - kp: " << kp << " (" << kp/pi << "*pi" << ")" << gmshfem::msg::endl;
        gmshfem::msg::info << "   - ks: " << ks << " (" << ks/pi << "*pi" << ")" << gmshfem::msg::endl;
      }
      gmshfem::msg::info << "   - R: " << R << gmshfem::msg::endl;
      gmshfem::msg::info << " * mesh:" << gmshfem::msg::endl;
      gmshfem::msg::info << "   - lc: " << lc << " (eta_hP = " << (2*pi/kp)/lc << "), (eta_hS = " << (2*pi/ks)/lc << ")" << gmshfem::msg::endl;
      gmshfem::msg::info << "   - meshOrder: " << meshOrder << gmshfem::msg::endl;
      gmshfem::msg::info << " * modeling:" << gmshfem::msg::endl;
      gmshfem::msg::info << "   - grid: (" << nDomX << ", " << nDomY << ", " << nDomZ << ")" << gmshfem::msg::endl;
      gmshfem::msg::info << "   - iterMax: " << iterMax << gmshfem::msg::endl;
      gmshfem::msg::info << "   - res: " << res << gmshfem::msg::endl;
      gmshfem::msg::info << "   - subdomain size: (" << sizeX << ", " << sizeY << ", " << sizeZ << ")" << gmshfem::msg::endl;
      gmshfem::msg::info << "   - boundary: " << boundary << gmshfem::msg::endl;
      gmshfem::msg::info << "   - boundaryExt: " << boundaryExt << gmshfem::msg::endl;
      if(boundary == "pml") {
        gmshfem::msg::info << "   - N: " << N << gmshfem::msg::endl;
        gmshfem::msg::info << "   - pmlSize: " << pmlSize << gmshfem::msg::endl;
        gmshfem::msg::info << "   - pmlType: " << pmlType << gmshfem::msg::endl;
        gmshfem::msg::info << "   - pmlMethod: " << pmlMethod << gmshfem::msg::endl;
      }
      if(boundaryExt == "pml") {
        gmshfem::msg::info << "   - NExt: " << NExt << gmshfem::msg::endl;
        gmshfem::msg::info << "   - pmlSizeExt: " << pmlSizeExt << gmshfem::msg::endl;
        gmshfem::msg::info << "   - pmlTypeExt: " << pmlTypeExt << gmshfem::msg::endl;
        gmshfem::msg::info << "   - pmlMethodExt: " << pmlMethodExt << gmshfem::msg::endl;
      }
      gmshfem::msg::info << "   - gauss: " << gauss << gmshfem::msg::endl;
      gmshfem::msg::info << "   - fieldOrder: " << fieldOrder << gmshfem::msg::endl;
      gmshfem::msg::info << "   - neumannOrder: " << neumannOrder << gmshfem::msg::endl;
      gmshfem::msg::info << "   - stab: " << stab << gmshfem::msg::endl;
    }

    const unsigned int nDom = nDomX * nDomY * nDomZ;
    // Define domain
    gmshddm::domain::Subdomain omega(nDom);
    gmshfem::domain::Domain gamma;

    std::vector< gmshddm::domain::Subdomain > pml(6, nDom);
    std::vector< gmshddm::domain::Subdomain > pmlEdge(12, nDom);
    std::vector< gmshddm::domain::Subdomain > pmlCorner(8, nDom);
    std::vector< gmshddm::domain::Subdomain > pmlInf(6, nDom);
    std::vector< gmshddm::domain::Subdomain > pmlEdgeInf(12, nDom);
    std::vector< gmshddm::domain::Subdomain > pmlCornerInf(8, nDom);
    std::vector< gmshddm::domain::Subdomain > sigma(6, nDom);
    std::vector< gmshddm::domain::Interface > sigmaInterface(6, nDom);
    std::vector< std::pair< gmshddm::domain::Subdomain, gmshddm::domain::Subdomain > > pmlBnd(12, std::make_pair(gmshddm::domain::Subdomain(nDom), gmshddm::domain::Subdomain(nDom)) );
    std::vector< std::pair< gmshddm::domain::Interface, gmshddm::domain::Interface > > pmlBndInterface(12, std::make_pair(gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom)) );
    std::vector< std::tuple< gmshddm::domain::Subdomain, gmshddm::domain::Subdomain, gmshddm::domain::Subdomain > > pmlEdgeBnd(8, std::make_tuple(gmshddm::domain::Subdomain(nDom), gmshddm::domain::Subdomain(nDom), gmshddm::domain::Subdomain(nDom)) );
    std::vector< std::tuple< gmshddm::domain::Interface, gmshddm::domain::Interface, gmshddm::domain::Interface > > pmlEdgeBndInterface(8, std::make_tuple(gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom)) );

    std::vector< gmshddm::domain::Subdomain > edge(12, nDom);
    std::vector< std::pair< gmshddm::domain::Interface, gmshddm::domain::Interface > > edgeInterface(12, std::make_pair(gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom)) );
    std::vector< gmshddm::domain::Subdomain > corner(8, nDom);
    std::vector< std::tuple< gmshddm::domain::Interface, gmshddm::domain::Interface, gmshddm::domain::Interface > > cornerInterface(8, std::make_tuple(gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom)) );
    std::vector< std::tuple< gmshddm::domain::Subdomain, gmshddm::domain::Subdomain, gmshddm::domain::Subdomain > > cornerEdge(8, std::make_tuple(gmshddm::domain::Subdomain(nDom), gmshddm::domain::Subdomain(nDom), gmshddm::domain::Subdomain(nDom)));
    // Define topology
    std::vector< std::vector< unsigned int > > topology(nDom);
    std::vector< std::string > dir {"E", "N", "W", "S", "D", "U"};
    for(unsigned int i = 0; i < static_cast< unsigned int >(nDomX); ++i) {
      for(unsigned int j = 0; j < static_cast< unsigned int >(nDomY); ++j) {
        for(unsigned int k = 0; k < static_cast< unsigned int >(nDomZ); ++k) {
          unsigned int index = i * nDomZ * nDomY + j * nDomZ + k;

          const std::string subdomainTag = "_" + std::to_string(i) + "_" + std::to_string(j) + "_" + std::to_string(k);
          omega(index) = gmshfem::domain::Domain("omega" + subdomainTag);

          for(unsigned int b = 0; b < 6; ++b) {
            if(boundary == "pml" || boundaryExt == "pml") {
              pml[b](index) = gmshfem::domain::Domain("pml" + dir[b] + subdomainTag);
              pmlInf[b](index) = gmshfem::domain::Domain("pmlInf" + dir[b] + subdomainTag);
            }
            sigma[b](index) = gmshfem::domain::Domain("sigma" + dir[b] + subdomainTag);
          }

          for(unsigned int e = 0; e < 12; ++e) {
            std::vector< std::string > count {"first", "second", "third", "fourth"};
            if(e < 4) {
              if(boundary == "pml" || boundaryExt == "pml") {
                pmlEdge[e](index) = gmshfem::domain::Domain("pml" + dir[e%4] + "D" + subdomainTag);
                pmlEdgeInf[e](index) = gmshfem::domain::Domain("pmlInf" + dir[e%4] + "D" + subdomainTag);
                pmlBnd[e].first(index) = gmshfem::domain::Domain("pmlBnd" + dir[e%4] + "_first" + subdomainTag);
                pmlBnd[e].second(index) = gmshfem::domain::Domain("pmlBndD_" + count[e%4] + subdomainTag);
              }
              edge[e](index) = gmshfem::domain::Domain("edge" + dir[e%4] + "D" + subdomainTag);
            }
            else if(e >= 4 && e < 8) {
              if(boundary == "pml" || boundaryExt == "pml") {
                pmlEdge[e](index) = gmshfem::domain::Domain("pml" + dir[e%4] + dir[(e+1)%4] + subdomainTag);
                pmlEdgeInf[e](index) = gmshfem::domain::Domain("pmlInf" + dir[e%4] + dir[(e+1)%4] + subdomainTag);
                pmlBnd[e].first(index) = gmshfem::domain::Domain("pmlBnd" + dir[e%4] + "_second" + subdomainTag);
                pmlBnd[e].second(index) = gmshfem::domain::Domain("pmlBnd" + dir[(e+1)%4] + "_fourth" + subdomainTag);
              }
              edge[e](index) = gmshfem::domain::Domain("edge" + dir[e%4] + dir[(e+1)%4] + subdomainTag);
            }
            else {
              if(boundary == "pml" || boundaryExt == "pml") {
                pmlEdge[e](index) = gmshfem::domain::Domain("pml" + dir[e%4] + "U" + subdomainTag);
                pmlEdgeInf[e](index) = gmshfem::domain::Domain("pmlInf" + dir[e%4] + "U" + subdomainTag);
                pmlBnd[e].first(index) = gmshfem::domain::Domain("pmlBnd" + dir[e%4] + "_third" + subdomainTag);
                pmlBnd[e].second(index) = gmshfem::domain::Domain("pmlBndU_" + count[e%4] + subdomainTag);
              }
              edge[e](index) = gmshfem::domain::Domain("edge" + dir[e%4] + "U" + subdomainTag);
            }
          }

          for(unsigned int c = 0; c < 8; ++c) {
            if(c < 4) {
              if(boundary == "pml" || boundaryExt == "pml") {
                pmlCorner[c](index) = gmshfem::domain::Domain("pml" + dir[c%4] + dir[(c+1)%4] + "D" + subdomainTag);
                pmlCornerInf[c](index) = gmshfem::domain::Domain("pmlInf" + dir[c%4] + dir[(c+1)%4] + "D" + subdomainTag);

                std::get<0>(pmlEdgeBnd[c])(index) = gmshfem::domain::Domain("pmlBnd" + dir[c%4] + "D_second" + subdomainTag);
                std::get<1>(pmlEdgeBnd[c])(index) = gmshfem::domain::Domain("pmlBnd" + dir[(c+1)%4] + "D_first" + subdomainTag);
                std::get<2>(pmlEdgeBnd[c])(index) = gmshfem::domain::Domain("pmlBnd" + dir[c%4] + dir[(c+1)%4] + "_first" + subdomainTag);

                std::get<0>(cornerEdge[c])(index) = gmshfem::domain::Domain("cornerEdge" + dir[c%4] + dir[(c+1)%4] + "D_first" + subdomainTag);
                std::get<1>(cornerEdge[c])(index) = gmshfem::domain::Domain("cornerEdge" + dir[c%4] + dir[(c+1)%4] + "D_second" + subdomainTag);
                std::get<2>(cornerEdge[c])(index) = gmshfem::domain::Domain("cornerEdge" + dir[c%4] + dir[(c+1)%4] + "D_third" + subdomainTag);
              }
              corner[c](index) = gmshfem::domain::Domain("corner" + dir[c%4] + dir[(c+1)%4] + "D" + subdomainTag);
            }
            else {
              if(boundary == "pml" || boundaryExt == "pml") {
                pmlCorner[c](index) = gmshfem::domain::Domain("pml" + dir[c%4] + dir[(c+1)%4] + "U" + subdomainTag);
                pmlCornerInf[c](index) = gmshfem::domain::Domain("pmlInf" + dir[c%4] + dir[(c+1)%4] + "U" + subdomainTag);

                std::get<0>(pmlEdgeBnd[c])(index) = gmshfem::domain::Domain("pmlBnd" + dir[c%4] + "U_second" + subdomainTag);
                std::get<1>(pmlEdgeBnd[c])(index) = gmshfem::domain::Domain("pmlBnd" + dir[(c+1)%4] + "U_first" + subdomainTag);
                std::get<2>(pmlEdgeBnd[c])(index) = gmshfem::domain::Domain("pmlBnd" + dir[c%4] + dir[(c+1)%4] + "_second" + subdomainTag);

                std::get<0>(cornerEdge[c])(index) = gmshfem::domain::Domain("cornerEdge" + dir[c%4] + dir[(c+1)%4] + "U_first" + subdomainTag);
                std::get<1>(cornerEdge[c])(index) = gmshfem::domain::Domain("cornerEdge" + dir[c%4] + dir[(c+1)%4] + "U_second" + subdomainTag);
                std::get<2>(cornerEdge[c])(index) = gmshfem::domain::Domain("cornerEdge" + dir[c%4] + dir[(c+1)%4] + "U_third" + subdomainTag);
              }
              corner[c](index) = gmshfem::domain::Domain("corner" + dir[c%4] + dir[(c+1)%4] + "U" + subdomainTag);
            }
          }

          if(i != static_cast< unsigned int >(nDomX) - 1) { // E
            if(boundary == "pml" || boundaryExt == "pml") {
              pmlBndInterface[0].second(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = pmlBnd[0].second(index);
              pmlBndInterface[4].second(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = pmlBnd[4].second(index);
              pmlBndInterface[8].second(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = pmlBnd[8].second(index);
              pmlBndInterface[7].first(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = pmlBnd[7].first(index);

              std::get<1>(pmlEdgeBndInterface[0])(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = std::get<1>(pmlEdgeBnd[0])(index);
              std::get<1>(pmlEdgeBndInterface[4])(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = std::get<1>(pmlEdgeBnd[4])(index);
              std::get<0>(pmlEdgeBndInterface[7])(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = std::get<0>(pmlEdgeBnd[7])(index);
              std::get<0>(pmlEdgeBndInterface[3])(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = std::get<0>(pmlEdgeBnd[3])(index);

            }
            sigmaInterface[0](index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = sigma[0](index);

            edgeInterface[0].second(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = edge[0](index);
            edgeInterface[4].second(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = edge[4](index);
            edgeInterface[8].second(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = edge[8](index);
            edgeInterface[7].first(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = edge[7](index);

            std::get<1>(cornerInterface[0])(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = corner[0](index);
            std::get<1>(cornerInterface[4])(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = corner[4](index);
            std::get<0>(cornerInterface[7])(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = corner[7](index);
            std::get<0>(cornerInterface[3])(index, (i+1) * nDomY * nDomZ + j * nDomZ + k) = corner[3](index);

            topology[index].push_back((i+1) * nDomY * nDomZ + j * nDomZ + k);
          }

          if(j != static_cast< unsigned int >(nDomY) - 1) { // N
            if(boundary == "pml" || boundaryExt == "pml") {
              pmlBndInterface[1].second(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = pmlBnd[1].second(index);
              pmlBndInterface[5].second(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = pmlBnd[5].second(index);
              pmlBndInterface[9].second(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = pmlBnd[9].second(index);
              pmlBndInterface[4].first(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = pmlBnd[4].first(index);

              std::get<1>(pmlEdgeBndInterface[1])(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = std::get<1>(pmlEdgeBnd[1])(index);
              std::get<1>(pmlEdgeBndInterface[5])(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = std::get<1>(pmlEdgeBnd[5])(index);
              std::get<0>(pmlEdgeBndInterface[4])(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = std::get<0>(pmlEdgeBnd[4])(index);
              std::get<0>(pmlEdgeBndInterface[0])(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = std::get<0>(pmlEdgeBnd[0])(index);
            }
            sigmaInterface[1](index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = sigma[1](index);

            edgeInterface[1].second(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = edge[1](index);
            edgeInterface[5].second(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = edge[5](index);
            edgeInterface[9].second(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = edge[9](index);
            edgeInterface[4].first(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = edge[4](index);

            std::get<1>(cornerInterface[1])(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = corner[1](index);
            std::get<1>(cornerInterface[5])(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = corner[5](index);
            std::get<0>(cornerInterface[4])(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = corner[4](index);
            std::get<0>(cornerInterface[0])(index, i * nDomY * nDomZ + (j+1) * nDomZ + k) = corner[0](index);

            topology[index].push_back(i * nDomY * nDomZ + (j+1) * nDomZ + k);
          }

          if(i != 0) { // W
            if(boundary == "pml" || boundaryExt == "pml") {
              pmlBndInterface[2].second(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = pmlBnd[2].second(index);
              pmlBndInterface[6].second(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = pmlBnd[6].second(index);
              pmlBndInterface[10].second(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = pmlBnd[10].second(index);
              pmlBndInterface[5].first(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = pmlBnd[5].first(index);

              std::get<1>(pmlEdgeBndInterface[2])(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = std::get<1>(pmlEdgeBnd[2])(index);
              std::get<1>(pmlEdgeBndInterface[6])(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = std::get<1>(pmlEdgeBnd[6])(index);
              std::get<0>(pmlEdgeBndInterface[5])(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = std::get<0>(pmlEdgeBnd[5])(index);
              std::get<0>(pmlEdgeBndInterface[1])(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = std::get<0>(pmlEdgeBnd[1])(index);
            }
            sigmaInterface[2](index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = sigma[2](index);

            edgeInterface[2].second(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = edge[2](index);
            edgeInterface[6].second(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = edge[6](index);
            edgeInterface[10].second(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = edge[10](index);
            edgeInterface[5].first(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = edge[5](index);

            std::get<1>(cornerInterface[2])(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = corner[2](index);
            std::get<1>(cornerInterface[6])(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = corner[6](index);
            std::get<0>(cornerInterface[5])(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = corner[5](index);
            std::get<0>(cornerInterface[1])(index, (i-1) * nDomY * nDomZ + j * nDomZ + k) = corner[1](index);

            topology[index].push_back((i-1) * nDomY * nDomZ + j * nDomZ + k);
          }

          if(j != 0) { // S
            if(boundary == "pml" || boundaryExt == "pml") {
              pmlBndInterface[3].second(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = pmlBnd[3].second(index);
              pmlBndInterface[7].second(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = pmlBnd[7].second(index);
              pmlBndInterface[11].second(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = pmlBnd[11].second(index);
              pmlBndInterface[6].first(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = pmlBnd[6].first(index);

              std::get<1>(pmlEdgeBndInterface[3])(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = std::get<1>(pmlEdgeBnd[3])(index);
              std::get<1>(pmlEdgeBndInterface[7])(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = std::get<1>(pmlEdgeBnd[7])(index);
              std::get<0>(pmlEdgeBndInterface[6])(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = std::get<0>(pmlEdgeBnd[6])(index);
              std::get<0>(pmlEdgeBndInterface[2])(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = std::get<0>(pmlEdgeBnd[2])(index);
            }
            sigmaInterface[3](index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = sigma[3](index);

            edgeInterface[3].second(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = edge[3](index);
            edgeInterface[7].second(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = edge[7](index);
            edgeInterface[11].second(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = edge[11](index);
            edgeInterface[6].first(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = edge[6](index);

            std::get<1>(cornerInterface[3])(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = corner[3](index);
            std::get<1>(cornerInterface[7])(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = corner[7](index);
            std::get<0>(cornerInterface[6])(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = corner[6](index);
            std::get<0>(cornerInterface[2])(index, i * nDomY * nDomZ + (j-1) * nDomZ + k) = corner[2](index);

            topology[index].push_back(i * nDomY * nDomZ + (j-1) * nDomZ + k);
          }

          if(k != 0) { // D
            if(boundary == "pml" || boundaryExt == "pml") {
              pmlBndInterface[0].first(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = pmlBnd[0].first(index);
              pmlBndInterface[1].first(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = pmlBnd[1].first(index);
              pmlBndInterface[2].first(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = pmlBnd[2].first(index);
              pmlBndInterface[3].first(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = pmlBnd[3].first(index);

              std::get<2>(pmlEdgeBndInterface[0])(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = std::get<2>(pmlEdgeBnd[0])(index);
              std::get<2>(pmlEdgeBndInterface[1])(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = std::get<2>(pmlEdgeBnd[1])(index);
              std::get<2>(pmlEdgeBndInterface[2])(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = std::get<2>(pmlEdgeBnd[2])(index);
              std::get<2>(pmlEdgeBndInterface[3])(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = std::get<2>(pmlEdgeBnd[3])(index);
            }
            sigmaInterface[4](index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = sigma[4](index);

            edgeInterface[0].first(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = edge[0](index);
            edgeInterface[1].first(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = edge[1](index);
            edgeInterface[2].first(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = edge[2](index);
            edgeInterface[3].first(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = edge[3](index);

            std::get<2>(cornerInterface[0])(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = corner[0](index);
            std::get<2>(cornerInterface[1])(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = corner[1](index);
            std::get<2>(cornerInterface[2])(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = corner[2](index);
            std::get<2>(cornerInterface[3])(index, i * nDomY * nDomZ + j * nDomZ + (k-1)) = corner[3](index);

            topology[index].push_back(i * nDomY * nDomZ + j * nDomZ + (k-1));
          }

          if(k != static_cast< unsigned int >(nDomZ) - 1) { // U
            if(boundary == "pml" || boundaryExt == "pml") {
              pmlBndInterface[8].first(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = pmlBnd[8].first(index);
              pmlBndInterface[9].first(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = pmlBnd[9].first(index);
              pmlBndInterface[10].first(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = pmlBnd[10].first(index);
              pmlBndInterface[11].first(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = pmlBnd[11].first(index);

              std::get<2>(pmlEdgeBndInterface[4])(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = std::get<2>(pmlEdgeBnd[4])(index);
              std::get<2>(pmlEdgeBndInterface[5])(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = std::get<2>(pmlEdgeBnd[5])(index);
              std::get<2>(pmlEdgeBndInterface[6])(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = std::get<2>(pmlEdgeBnd[6])(index);
              std::get<2>(pmlEdgeBndInterface[7])(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = std::get<2>(pmlEdgeBnd[7])(index);
            }
            sigmaInterface[5](index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = sigma[5](index);

            edgeInterface[8].first(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = edge[8](index);
            edgeInterface[9].first(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = edge[9](index);
            edgeInterface[10].first(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = edge[10](index);
            edgeInterface[11].first(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = edge[11](index);

            std::get<2>(cornerInterface[4])(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = corner[4](index);
            std::get<2>(cornerInterface[5])(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = corner[5](index);
            std::get<2>(cornerInterface[6])(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = corner[6](index);
            std::get<2>(cornerInterface[7])(index, i * nDomY * nDomZ + j * nDomZ + (k+1)) = corner[7](index);

            topology[index].push_back(i * nDomY * nDomZ + j * nDomZ + (k+1));
          }
        }
      }
    }

    std::vector< int > sourcesDom;
    for(unsigned int i = 0; i < nDomX-1; ++i) {
      for(unsigned int j = 0; j < nDomY-1; ++j) {
        sourcesDom.push_back(i * nDomY * nDomZ + j * nDomZ + nDomZ-1);
      }
    }
    if(benchmark == "scattering") {
      gamma = gmshfem::domain::Domain("gamma");
    }
    else if(benchmark == "salt") {
      for(auto i : sourcesDom) {
        gamma |= corner[4](i);
      }
    }

    // Kappa definition
    gmshfem::function::ScalarFunction< std::complex< double > > kappaP, kappaS;
    gmshfem::function::TensorFunction< std::complex< double >, 4 > C;
    std::vector< double > x, y, z;
    std::vector< std::vector< std::vector< std::complex< double > > > > data;
    if(benchmark == "scattering") {
      kappaP = kp;
      kappaS = ks;
      typename gmshfem::MathObject< std::complex< double >, gmshfem::Degree::Degree4 >::Object Ctmp;
      for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
          for(int k = 0; k < 3; ++k) {
            for(int l = 0; l < 3; ++l) {
              Ctmp(i,j)(k,l) = 0.;
            }
          }
        }
      }
      for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
          for(int k = 0; k < 3; ++k) {
            for(int l = 0; l < 3; ++l) {
              if(i == j && k == l) {
                Ctmp(i,j)(k,l) += 1./(kp*kp) - 2./(ks*ks);
              }
              if(i == k && j == l) {
                Ctmp(i,j)(k,l) += 1./(ks*ks);
              }
              if(i == l && j == k) {
                Ctmp(i,j)(k,l) += 1./(ks*ks);
              }
            }
          }
        }
      }
      C = gmshfem::function::TensorFunction< std::complex< double >, 4 >(Ctmp);
    }
    else if (benchmark == "salt") {
      const double freq = 2.;
      // Read the velocity map
      std::ifstream file(inputPath + "salt.dat", std::ios::binary);

      unsigned int Nx = 0, Ny = 0, Nz = 0;
      file.read((char *) &Nx, sizeof(unsigned int));
      file.read((char *) &Ny, sizeof(unsigned int));
      file.read((char *) &Nz, sizeof(unsigned int));
      if(gmshddm::mpi::getMPIRank() == 0) {
        gmshfem::msg::info << "Reading: Nx = " << Nx << ", Ny = " << Ny << ", Nz = " << Nz << "." << gmshfem::msg::endl;
      }

      x.resize(Nx);
      y.resize(Ny);
      z.resize(Nz);
      file.read((char *) &x[0], Nx * sizeof(double));
      file.read((char *) &y[0], Ny * sizeof(double));
      file.read((char *) &z[0], Nz * sizeof(double));
      for(unsigned int i = 0; i < Nz; ++i) {
        z[i] += 4180.;
      }

      // Hack to avoid problem at the boundaries
      x[0] -= 1e-3;
      y[0] -= 1e-3;
      z[0] -= 1e-3;
      x[Nx-1] += 1e-3;
      y[Ny-1] += 1e-3;
      z[Nz-1] += 1e-3;

      data.resize(Nx, std::vector< std::vector< std::complex< double > > >(Ny, std::vector< std::complex< double > >(Nz)));
      std::vector< double > dataRaw(Nx * Ny * Nz);
      file.read((char *) &dataRaw[0], Nx * Ny * Nz * sizeof(double));

      for(unsigned int k = 0; k < Nz; ++k) {
        for(unsigned int j = 0; j < Ny; ++j) {
          for(unsigned int i = 0; i < Nx; ++i) {
            data[i][j][k] = dataRaw[k * Nx * Ny + j * Nx + i];
          }
        }
      }
      file.close();

      const double min = *std::min_element(dataRaw.begin(), dataRaw.end());
      const double max = *std::max_element(dataRaw.begin(), dataRaw.end());
      if(gmshddm::mpi::getMPIRank() == 0) {
        gmshfem::msg::info << "min = " << min << ", max = " << max << gmshfem::msg::endl;
      }

      kappaP = 2. * pi * freq / gmshfem::function::trilinearInterpolation< std::complex< double > >(&x, &y, &z, &data);
      kappaS = 2. * kappaP;
      for(unsigned int k = 0; k < nDomX * nDomY * nDomZ; ++k) {
        if(gmshddm::mpi::isItMySubdomain(k)) {
//          gmshfem::post::save(kappaP, omega(k), "kP_" + std::to_string(k), "msh");
//          gmshfem::post::save(kappaS, omega(k), "kS_" + std::to_string(k), "msh");
        }
      }

      // tensor C
      gmshfem::function::ScalarFunction< std::complex< double > > p1 = 1./(kappaP*kappaP);
      gmshfem::function::ScalarFunction< std::complex< double > > p2 = 1./(kappaS*kappaS);
      gmshfem::function::ScalarFunction< std::complex< double > > p3 = 1./(kappaP*kappaP) - 2./(kappaS*kappaS);

      gmshfem::function::TensorFunction< std::complex< double > > Cxx = gmshfem::function::tensor< std::complex< double > >(p1, 0., 0.,  0., p3, 0.,  0., 0., p3);
      gmshfem::function::TensorFunction< std::complex< double > > Cyy = gmshfem::function::tensor< std::complex< double > >(p3, 0., 0.,  0., p1, 0.,  0., 0., p3);
      gmshfem::function::TensorFunction< std::complex< double > > Czz = gmshfem::function::tensor< std::complex< double > >(p3, 0., 0.,  0., p3, 0.,  0., 0., p1);
      gmshfem::function::TensorFunction< std::complex< double > > Cxy = gmshfem::function::tensor< std::complex< double > >(0., p2, 0.,  p2, 0., 0.,  0., 0., 0.);
      gmshfem::function::TensorFunction< std::complex< double > > Cyx = Cxy;
      gmshfem::function::TensorFunction< std::complex< double > > Cxz = gmshfem::function::tensor< std::complex< double > >(0., 0., p2,  0., 0., 0.,  p2, 0., 0.);
      gmshfem::function::TensorFunction< std::complex< double > > Czx = Cxz;
      gmshfem::function::TensorFunction< std::complex< double > > Cyz = gmshfem::function::tensor< std::complex< double > >(0., 0., 0.,  0., 0., p2,  0., p2, 0.);
      gmshfem::function::TensorFunction< std::complex< double > > Czy = Cyz;

      C = gmshfem::function::tensor< std::complex< double >, 4 >(Cxx, Cxy, Cxz,  Cyx, Cyy, Cyz,  Czx, Czy, Czz);
    }

    // Fields definition
    gmshddm::field::SubdomainCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > u("u", omega | sigma[0] | sigma[1] | sigma[2] | sigma[3], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    std::vector< gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * > gContinuous;
    for(unsigned int b = 0; b < 6; ++b) {
      gContinuous.push_back(new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gC_" + dir[b], sigmaInterface[b], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder));
    }

    std::vector< std::pair< gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *, gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * > > gEdgePmlContinuous;
    std::vector< std::pair< gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *, gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * > > gSymEdgePmlContinuous;
    for(unsigned int e = 0; e < 12; ++e) {
      if(e < 4) {
        gEdgePmlContinuous.push_back(std::make_pair(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gEdgePmlContinuous_" + dir[e%4] + "D_first", pmlBndInterface[e].first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gEdgePmlContinuous_" + dir[e%4] + "D_second", pmlBndInterface[e].second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
        gSymEdgePmlContinuous.push_back(std::make_pair(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymEdgePmlContinuous_" + dir[e%4] + "D_first", pmlBndInterface[e].first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymEdgePmlContinuous_" + dir[e%4] + "D_second", pmlBndInterface[e].second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
      }
      else if(e >= 4 && e < 8) {
        gEdgePmlContinuous.push_back(std::make_pair(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gEdgePmlContinuous_" + dir[e%4] + dir[(e+1)%4] + "_first", pmlBndInterface[e].first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gEdgePmlContinuous_" + dir[e%4] + dir[(e+1)%4] + "_second", pmlBndInterface[e].second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
        gSymEdgePmlContinuous.push_back(std::make_pair(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymEdgePmlContinuous_" + dir[e%4] + dir[(e+1)%4] + "_first", pmlBndInterface[e].first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymEdgePmlContinuous_" + dir[e%4] + dir[(e+1)%4] + "_second", pmlBndInterface[e].second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
      }
      else {
        gEdgePmlContinuous.push_back(std::make_pair(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gEdgePmlContinuous_" + dir[e%4] + "U_first", pmlBndInterface[e].first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gEdgePmlContinuous_" + dir[e%4] + "U_second", pmlBndInterface[e].second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
        gSymEdgePmlContinuous.push_back(std::make_pair(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymEdgePmlContinuous_" + dir[e%4] + "U_first", pmlBndInterface[e].first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymEdgePmlContinuous_" + dir[e%4] + "U_second", pmlBndInterface[e].second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
      }
    }

    std::vector< std::tuple< gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *, gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *, gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * > > gCornerPmlContinuous;
    std::vector< std::tuple< gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *, gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *, gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * > > gSymCornerPmlContinuous[2];
    std::vector< std::tuple< gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *, gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > *, gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > * > > gSymSymCornerPmlContinuous;
    for(unsigned int c = 0; c < 8; ++c) {
      if(c < 4) {
        gCornerPmlContinuous.push_back(std::make_tuple(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gCornerPmlContinuous_" + dir[c%4] + dir[(c+1)%4] + "D_first", std::get<0>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gCornerPmlContinuous_" + dir[c%4] + dir[(c+1)%4] + "D_second", std::get<1>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gCornerPmlContinuous_" + dir[c%4] + dir[(c+1)%4] + "D_third", std::get<2>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
        gSymCornerPmlContinuous[0].push_back(std::make_tuple(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_first_" + dir[c%4] + dir[(c+1)%4] + "D_first", std::get<0>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_first_" + dir[c%4] + dir[(c+1)%4] + "D_second", std::get<1>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_first_" + dir[c%4] + dir[(c+1)%4] + "D_third", std::get<2>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
        gSymCornerPmlContinuous[1].push_back(std::make_tuple(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_second_" + dir[c%4] + dir[(c+1)%4] + "D_first", std::get<0>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_second_" + dir[c%4] + dir[(c+1)%4] + "D_second", std::get<1>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_second_" + dir[c%4] + dir[(c+1)%4] + "D_third", std::get<2>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
        gSymSymCornerPmlContinuous.push_back(std::make_tuple(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymSymCornerPmlContinuous" + dir[c%4] + dir[(c+1)%4] + "D_first", std::get<0>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymSymCornerPmlContinuous" + dir[c%4] + dir[(c+1)%4] + "D_second", std::get<1>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymSymCornerPmlContinuous" + dir[c%4] + dir[(c+1)%4] + "D_third", std::get<2>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
      }
      else {
        gCornerPmlContinuous.push_back(std::make_tuple(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gCornerPmlContinuous_" + dir[c%4] + dir[(c+1)%4] + "U_first", std::get<0>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gCornerPmlContinuous_" + dir[c%4] + dir[(c+1)%4] + "U_second", std::get<1>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gCornerPmlContinuous_" + dir[c%4] + dir[(c+1)%4] + "U_third", std::get<2>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
        gSymCornerPmlContinuous[0].push_back(std::make_tuple(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_first_" + dir[c%4] + dir[(c+1)%4] + "U_first", std::get<0>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_first_" + dir[c%4] + dir[(c+1)%4] + "U_second", std::get<1>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_first_" + dir[c%4] + dir[(c+1)%4] + "U_third", std::get<2>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
        gSymCornerPmlContinuous[1].push_back(std::make_tuple(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_second_" + dir[c%4] + dir[(c+1)%4] + "U_first", std::get<0>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_second_" + dir[c%4] + dir[(c+1)%4] + "U_second", std::get<1>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymCornerPmlContinuous_second_" + dir[c%4] + dir[(c+1)%4] + "U_third", std::get<2>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
        gSymSymCornerPmlContinuous.push_back(std::make_tuple(
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymSymCornerPmlContinuous" + dir[c%4] + dir[(c+1)%4] + "U_first", std::get<0>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymSymCornerPmlContinuous" + dir[c%4] + dir[(c+1)%4] + "U_second", std::get<1>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >("gSymSymCornerPmlContinuous" + dir[c%4] + dir[(c+1)%4] + "U_third", std::get<2>(pmlEdgeBndInterface[c]), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
      }
    }

    gmshddm::problem::Formulation< std::complex< double > > formulation("NavierDDMScattering", topology);

    const int faceNeighbor[6] = {
      2, 3, 0, 1, 5, 4
    };

    const int edgeNeighbor[12][2] = {
      // Down
      {8, 2},
      {9, 3},
      {10, 0},
      {11, 1},
      // Rot
      {7, 5},
      {4, 6},
      {5, 7},
      {6, 4},
      // Up
      {0, 10},
      {1, 11},
      {2, 8},
      {3, 9}
    };

    const int cornerNeighbor[8][3] = {
      // Down
      {3, 1, 4},
      {0, 2, 5},
      {1, 3, 6},
      {2, 0, 7},
      // Up
      {7, 5, 0},
      {4, 6, 1},
      {5, 7, 2},
      {6, 4, 3}
    };

    for(unsigned int b = 0; b < 6; ++b) {
      formulation.addInterfaceField(*gContinuous[b], *gContinuous[faceNeighbor[b]]);
    }

    for(unsigned int e = 0; e < 12; ++e) {
      if(e >= 4 && e < 8) {
        formulation.addInterfaceField(*gEdgePmlContinuous[e].first, *gEdgePmlContinuous[edgeNeighbor[e][0]].second);
        formulation.addInterfaceField(*gEdgePmlContinuous[e].second, *gEdgePmlContinuous[edgeNeighbor[e][1]].first);

        formulation.addInterfaceField(*gSymEdgePmlContinuous[e].first, *gSymEdgePmlContinuous[edgeNeighbor[e][0]].second);
        formulation.addInterfaceField(*gSymEdgePmlContinuous[e].second, *gSymEdgePmlContinuous[edgeNeighbor[e][1]].first);
      }
      else {
        formulation.addInterfaceField(*gEdgePmlContinuous[e].first, *gEdgePmlContinuous[edgeNeighbor[e][0]].first);
        formulation.addInterfaceField(*gEdgePmlContinuous[e].second, *gEdgePmlContinuous[edgeNeighbor[e][1]].second);

        formulation.addInterfaceField(*gSymEdgePmlContinuous[e].first, *gSymEdgePmlContinuous[edgeNeighbor[e][0]].first);
        formulation.addInterfaceField(*gSymEdgePmlContinuous[e].second, *gSymEdgePmlContinuous[edgeNeighbor[e][1]].second);
      }
    }

    for(unsigned int c = 0; c < 8; ++c) {
      formulation.addInterfaceField(*std::get<0>(gCornerPmlContinuous[c]), *std::get<1>(gCornerPmlContinuous[cornerNeighbor[c][0]]));
      formulation.addInterfaceField(*std::get<1>(gCornerPmlContinuous[c]), *std::get<0>(gCornerPmlContinuous[cornerNeighbor[c][1]]));
      formulation.addInterfaceField(*std::get<2>(gCornerPmlContinuous[c]), *std::get<2>(gCornerPmlContinuous[cornerNeighbor[c][2]]));

      formulation.addInterfaceField(*std::get<0>(gSymCornerPmlContinuous[0][c]), *std::get<1>(gSymCornerPmlContinuous[0][cornerNeighbor[c][0]]));
      formulation.addInterfaceField(*std::get<1>(gSymCornerPmlContinuous[0][c]), *std::get<0>(gSymCornerPmlContinuous[0][cornerNeighbor[c][1]]));
      formulation.addInterfaceField(*std::get<2>(gSymCornerPmlContinuous[0][c]), *std::get<2>(gSymCornerPmlContinuous[0][cornerNeighbor[c][2]]));

      formulation.addInterfaceField(*std::get<0>(gSymCornerPmlContinuous[1][c]), *std::get<1>(gSymCornerPmlContinuous[1][cornerNeighbor[c][0]]));
      formulation.addInterfaceField(*std::get<1>(gSymCornerPmlContinuous[1][c]), *std::get<0>(gSymCornerPmlContinuous[1][cornerNeighbor[c][1]]));
      formulation.addInterfaceField(*std::get<2>(gSymCornerPmlContinuous[1][c]), *std::get<2>(gSymCornerPmlContinuous[1][cornerNeighbor[c][2]]));

      formulation.addInterfaceField(*std::get<0>(gSymSymCornerPmlContinuous[c]), *std::get<1>(gSymSymCornerPmlContinuous[cornerNeighbor[c][0]]));
      formulation.addInterfaceField(*std::get<1>(gSymSymCornerPmlContinuous[c]), *std::get<0>(gSymSymCornerPmlContinuous[cornerNeighbor[c][1]]));
      formulation.addInterfaceField(*std::get<2>(gSymSymCornerPmlContinuous[c]), *std::get<2>(gSymSymCornerPmlContinuous[cornerNeighbor[c][2]]));
    }

    if(benchmark == "scattering") {
      u(scatterPosX * nDomY + scatterPosY).addConstraint(gamma, gmshfem::function::vector< std::complex< double > >(1., 0., 0.));
    }
    else if (benchmark == "salt") {
      for(auto i : sourcesDom) {
        u(i).addConstraint(corner[4](i), gmshfem::function::vector< std::complex< double > >(0., 0., 1.));
      }
    }

    std::vector< std::vector< std::vector< Subproblem * > > > subproblem(nDomX);
    for(unsigned int i = 0; i < nDomX; ++i) {
      subproblem[i].resize(nDomY);
      for(unsigned int j = 0; j < nDomY; ++j) {
        subproblem[i][j].resize(nDomZ);
        for(unsigned int k = 0; k < nDomZ; ++k) {
          unsigned int index = i * nDomY * nDomZ + j * nDomZ + k;
          formulation(index).integral(- C * grad(dof(u(index))), grad(tf(u(index))), omega(index), gauss);
          formulation(index).integral(dof(u(index)), tf(u(index)), omega(index), gauss);

          SubproblemDomains domains;
          domains.setOmega(omega(index));
          domains.setSigma({ sigma[0](index), sigma[1](index), sigma[2](index), sigma[3](index), sigma[4](index), sigma[5](index) });
          domains.setPml({ pml[0](index), pml[1](index), pml[2](index), pml[3](index), pml[4](index), pml[5](index) });
          domains.setPmlInf({ pmlInf[0](index), pmlInf[1](index), pmlInf[2](index), pmlInf[3](index), pmlInf[4](index), pmlInf[5](index) });
          domains.setPmlBnd({ std::make_pair(pmlBnd[0].first(index), pmlBnd[0].second(index)), std::make_pair(pmlBnd[1].first(index), pmlBnd[1].second(index)), std::make_pair(pmlBnd[2].first(index), pmlBnd[2].second(index)), std::make_pair(pmlBnd[3].first(index), pmlBnd[3].second(index)), std::make_pair(pmlBnd[4].first(index), pmlBnd[4].second(index)), std::make_pair(pmlBnd[5].first(index), pmlBnd[5].second(index)), std::make_pair(pmlBnd[6].first(index), pmlBnd[6].second(index)), std::make_pair(pmlBnd[7].first(index), pmlBnd[7].second(index)), std::make_pair(pmlBnd[8].first(index), pmlBnd[8].second(index)), std::make_pair(pmlBnd[9].first(index), pmlBnd[9].second(index)), std::make_pair(pmlBnd[10].first(index), pmlBnd[10].second(index)), std::make_pair(pmlBnd[11].first(index), pmlBnd[11].second(index)) });
          domains.setPmlEdge({ pmlEdge[0](index), pmlEdge[1](index), pmlEdge[2](index), pmlEdge[3](index), pmlEdge[4](index), pmlEdge[5](index), pmlEdge[6](index), pmlEdge[7](index), pmlEdge[8](index), pmlEdge[9](index), pmlEdge[10](index), pmlEdge[11](index) });
          domains.setPmlEdgeInf({ pmlEdgeInf[0](index), pmlEdgeInf[1](index), pmlEdgeInf[2](index), pmlEdgeInf[3](index), pmlEdgeInf[4](index), pmlEdgeInf[5](index), pmlEdgeInf[6](index), pmlEdgeInf[7](index), pmlEdgeInf[8](index), pmlEdgeInf[9](index), pmlEdgeInf[10](index), pmlEdgeInf[11](index) });
          domains.setEdge({ edge[0](index), edge[1](index), edge[2](index), edge[3](index), edge[4](index), edge[5](index), edge[6](index), edge[7](index), edge[8](index), edge[9](index), edge[10](index), edge[11](index) });
          domains.setPmlEdgeBnd({ std::make_tuple(std::get<0>(pmlEdgeBnd[0])(index), std::get<1>(pmlEdgeBnd[0])(index), std::get<2>(pmlEdgeBnd[0])(index)), std::make_tuple(std::get<0>(pmlEdgeBnd[1])(index), std::get<1>(pmlEdgeBnd[1])(index), std::get<2>(pmlEdgeBnd[1])(index)), std::make_tuple(std::get<0>(pmlEdgeBnd[2])(index), std::get<1>(pmlEdgeBnd[2])(index), std::get<2>(pmlEdgeBnd[2])(index)), std::make_tuple(std::get<0>(pmlEdgeBnd[3])(index), std::get<1>(pmlEdgeBnd[3])(index), std::get<2>(pmlEdgeBnd[3])(index)), std::make_tuple(std::get<0>(pmlEdgeBnd[4])(index), std::get<1>(pmlEdgeBnd[4])(index), std::get<2>(pmlEdgeBnd[4])(index)), std::make_tuple(std::get<0>(pmlEdgeBnd[5])(index), std::get<1>(pmlEdgeBnd[5])(index), std::get<2>(pmlEdgeBnd[5])(index)), std::make_tuple(std::get<0>(pmlEdgeBnd[6])(index), std::get<1>(pmlEdgeBnd[6])(index), std::get<2>(pmlEdgeBnd[6])(index)), std::make_tuple(std::get<0>(pmlEdgeBnd[7])(index), std::get<1>(pmlEdgeBnd[7])(index), std::get<2>(pmlEdgeBnd[7])(index)) });
          domains.setPmlCorner({ pmlCorner[0](index), pmlCorner[1](index), pmlCorner[2](index), pmlCorner[3](index), pmlCorner[4](index), pmlCorner[5](index), pmlCorner[6](index), pmlCorner[7](index) });
          domains.setPmlCornerInf({ pmlCornerInf[0](index), pmlCornerInf[1](index), pmlCornerInf[2](index), pmlCornerInf[3](index), pmlCornerInf[4](index), pmlCornerInf[5](index), pmlCornerInf[6](index), pmlCornerInf[7](index) });
          domains.setCorner({ corner[0](index), corner[1](index), corner[2](index), corner[3](index), corner[4](index), corner[5](index), corner[6](index), corner[7](index) });
          domains.setCornerEdge({ std::make_tuple(std::get<0>(cornerEdge[0])(index), std::get<1>(cornerEdge[0])(index), std::get<2>(cornerEdge[0])(index)), std::make_tuple(std::get<0>(cornerEdge[1])(index), std::get<1>(cornerEdge[1])(index), std::get<2>(cornerEdge[1])(index)), std::make_tuple(std::get<0>(cornerEdge[2])(index), std::get<1>(cornerEdge[2])(index), std::get<2>(cornerEdge[2])(index)), std::make_tuple(std::get<0>(cornerEdge[3])(index), std::get<1>(cornerEdge[3])(index), std::get<2>(cornerEdge[3])(index)), std::make_tuple(std::get<0>(cornerEdge[4])(index), std::get<1>(cornerEdge[4])(index), std::get<2>(cornerEdge[4])(index)), std::make_tuple(std::get<0>(cornerEdge[5])(index), std::get<1>(cornerEdge[5])(index), std::get<2>(cornerEdge[5])(index)), std::make_tuple(std::get<0>(cornerEdge[6])(index), std::get<1>(cornerEdge[6])(index), std::get<2>(cornerEdge[6])(index)), std::make_tuple(std::get<0>(cornerEdge[7])(index), std::get<1>(cornerEdge[7])(index), std::get<2>(cornerEdge[7])(index)) });

          SubproblemParameters parameters;
          parameters.setGauss(gauss);
          parameters.setKappaP(kappaP);
          parameters.setKappaS(kappaS);
          parameters.setC(C);
          parameters.setNeumannOrder(neumannOrder);
          parameters.setFieldOrder(fieldOrder);
          parameters.setStab(stab);

          std::string bnd = boundary;
          if(boundary == "pml") {
            if(pmlMethod == "continuous") {
              bnd += "Continuous_" + std::to_string(pmlSize) + "_" + pmlType;
            }
            else if(pmlMethod == "continuousSymmetric") {
              bnd += "ContinuousSymmetric_" + std::to_string(pmlSize) + "_" + pmlType;
            }
          }
          std::string bndExt = boundaryExt;
          if(boundaryExt == "pml") {
            if(pmlMethodExt == "continuous") {
              bndExt += "Continuous_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
            }
            else if(pmlMethodExt == "continuousSymmetric") {
              bndExt += "ContinuousSymmetric_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
            }
          }
          if(gmshddm::mpi::getMPIRank() == 0) {
            gmshfem::msg::info << "Subdomain (" << i << ", " << j << ", " << k << ") has boundaries [" << (i == nDomX-1 ? bndExt : bnd) << ", " << (j == nDomY-1 ? bndExt : bnd) << ", " << (i == 0 ? bndExt : bnd) << ", " << (j == 0 ? bndExt : bnd) << ", " << (k == 0 ? bndExt : bnd) << ", " << (k == nDomZ-1 ? bndExt : bnd) << "]" << gmshfem::msg::endl;
          }
          subproblem[i][j][k] = new Subproblem(formulation(index), u(index).name(), domains, parameters, (i == nDomX-1 ? bndExt : bnd), (j == nDomY-1 ? bndExt : bnd), (i == 0 ? bndExt : bnd), (j == 0 ? bndExt : bnd), (k == 0 ? bndExt : bnd), (k == nDomZ-1 ? bndExt : bnd));
          subproblem[i][j][k]->writeFormulation();

          // coupling
          for(unsigned int b = 0; b < 6; ++b) {
            for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
              const unsigned int jndex = topology[index][jj];

              Boundary *boundary = subproblem[i][j][k]->getBoundary(b);
              if(dynamic_cast< Sommerfeld * >(boundary)) {
                if(!sigmaInterface[b](index, jndex).isEmpty()) {
                  auto termId = formulation(index).integral(- (*gContinuous[faceNeighbor[b]])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
              else if(dynamic_cast< PmlContinuous * >(boundary)) {
                if(!sigmaInterface[b](index, jndex).isEmpty()) {
                  auto termId = formulation(index).integral(- (*gContinuous[faceNeighbor[b]])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
              else if(dynamic_cast< PmlContinuousSymmetric * >(boundary)) {
                if(!sigmaInterface[b](index, jndex).isEmpty()) {
                  auto termId = formulation(index).integral(- (*gContinuous[faceNeighbor[b]])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
            }
          }

          for(unsigned int e = 0; e < 12; ++e) {
            for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
              const unsigned int jndex = topology[index][jj];

              Edge *edge = subproblem[i][j][k]->getEdge(e);
              if(edge == nullptr) {
                continue;
              }
              if(auto eg = dynamic_cast< PmlContinuous_PmlContinuous * >(edge)) {
                if(!pmlBndInterface[e].first(index, jndex).isEmpty()) {
                  if(e >= 4 && e < 8) {
                    auto termId = formulation(index).integral(- (*gEdgePmlContinuous[edgeNeighbor[e][0]].second)(jndex, index), tf(*eg->firstBoundary()->getUPml()), pmlBndInterface[e].first(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  else {
                    auto termId = formulation(index).integral(- (*gEdgePmlContinuous[edgeNeighbor[e][0]].first)(jndex, index), tf(*eg->firstBoundary()->getUPml()), pmlBndInterface[e].first(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                }
                if(!pmlBndInterface[e].second(index, jndex).isEmpty()) {
                  if(e >= 4 && e < 8) {
                    auto termId = formulation(index).integral(- (*gEdgePmlContinuous[edgeNeighbor[e][1]].first)(jndex, index), tf(*eg->secondBoundary()->getUPml()), pmlBndInterface[e].second(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  else {
                    auto termId = formulation(index).integral(- (*gEdgePmlContinuous[edgeNeighbor[e][1]].second)(jndex, index), tf(*eg->secondBoundary()->getUPml()), pmlBndInterface[e].second(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                }
              }
              else if(auto eg = dynamic_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(edge)) {
                if(!pmlBndInterface[e].first(index, jndex).isEmpty()) {
                  if(e >= 4 && e < 8) {
                    {
                      auto termId = formulation(index).integral(- (*gEdgePmlContinuous[edgeNeighbor[e][0]].second)(jndex, index), tf(*eg->firstBoundary()->getUPml()), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                    }
                    {
                      auto termId = formulation(index).integral(- (*gSymEdgePmlContinuous[edgeNeighbor[e][0]].second)(jndex, index), tf(*eg->firstBoundary()->getUSymPml()), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                    }
                  }
                  else {
                    {
                      auto termId = formulation(index).integral(- (*gEdgePmlContinuous[edgeNeighbor[e][0]].first)(jndex, index), tf(*eg->firstBoundary()->getUPml()), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                    }
                    {
                      auto termId = formulation(index).integral(- (*gSymEdgePmlContinuous[edgeNeighbor[e][0]].first)(jndex, index), tf(*eg->firstBoundary()->getUSymPml()), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                    }
                  }
                }
                if(!pmlBndInterface[e].second(index, jndex).isEmpty()) {
                  if(e >= 4 && e < 8) {
                    {
                      auto termId = formulation(index).integral(- (*gEdgePmlContinuous[edgeNeighbor[e][1]].first)(jndex, index), tf(*eg->secondBoundary()->getUPml()), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                    }
                    {
                      auto termId = formulation(index).integral(- (*gSymEdgePmlContinuous[edgeNeighbor[e][1]].first)(jndex, index), tf(*eg->secondBoundary()->getUSymPml()), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                    }
                  }
                  else {
                    {
                      auto termId = formulation(index).integral(- (*gEdgePmlContinuous[edgeNeighbor[e][1]].second)(jndex, index), tf(*eg->secondBoundary()->getUPml()), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                    }
                    {
                      auto termId = formulation(index).integral(- (*gSymEdgePmlContinuous[edgeNeighbor[e][1]].second)(jndex, index), tf(*eg->secondBoundary()->getUSymPml()), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                    }
                  }
                }
              }
            }
          }

          for(unsigned int c = 0; c < 8; ++c) {
            for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
              const unsigned int jndex = topology[index][jj];

              Corner *corner = subproblem[i][j][k]->getCorner(c);
              if(corner == nullptr) {
                continue;
              }
              if(auto cr = dynamic_cast< PmlContinuous_PmlContinuous_PmlContinuous * >(corner)) {
                if(!std::get<0>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  auto termId = formulation(index).integral(- (*std::get<1>(gCornerPmlContinuous[cornerNeighbor[c][0]]))(jndex, index), tf(*cr->firstEdge()->getUPml()), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
                if(!std::get<1>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  auto termId = formulation(index).integral(- (*std::get<0>(gCornerPmlContinuous[cornerNeighbor[c][1]]))(jndex, index), tf(*cr->secondEdge()->getUPml()), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
                if(!std::get<2>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  auto termId = formulation(index).integral(- (*std::get<2>(gCornerPmlContinuous[cornerNeighbor[c][2]]))(jndex, index), tf(*cr->thirdEdge()->getUPml()), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
              else if(auto cr = dynamic_cast< PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify * >(corner)) {
                if(!std::get<0>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    auto termId = formulation(index).integral(- (*std::get<1>(gCornerPmlContinuous[cornerNeighbor[c][0]]))(jndex, index), tf(*cr->firstEdge()->getUPml()), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<1>(gSymCornerPmlContinuous[0][cornerNeighbor[c][0]]))(jndex, index), tf(*cr->firstEdge()->getUSymPml(0)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<1>(gSymCornerPmlContinuous[1][cornerNeighbor[c][0]]))(jndex, index), tf(*cr->firstEdge()->getUSymPml(1)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<1>(gSymSymCornerPmlContinuous[cornerNeighbor[c][0]]))(jndex, index), tf(*cr->firstEdge()->getUSymSymPml()), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                }
                if(!std::get<1>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    auto termId = formulation(index).integral(- (*std::get<0>(gCornerPmlContinuous[cornerNeighbor[c][1]]))(jndex, index), tf(*cr->secondEdge()->getUPml()), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<0>(gSymCornerPmlContinuous[0][cornerNeighbor[c][1]]))(jndex, index), tf(*cr->secondEdge()->getUSymPml(0)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<0>(gSymCornerPmlContinuous[1][cornerNeighbor[c][1]]))(jndex, index), tf(*cr->secondEdge()->getUSymPml(1)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<0>(gSymSymCornerPmlContinuous[cornerNeighbor[c][1]]))(jndex, index), tf(*cr->secondEdge()->getUSymSymPml()), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                }
                if(!std::get<2>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    auto termId = formulation(index).integral(- (*std::get<2>(gCornerPmlContinuous[cornerNeighbor[c][2]]))(jndex, index), tf(*cr->thirdEdge()->getUPml()), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<2>(gSymCornerPmlContinuous[0][cornerNeighbor[c][2]]))(jndex, index), tf(*cr->thirdEdge()->getUSymPml(0)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<2>(gSymCornerPmlContinuous[1][cornerNeighbor[c][2]]))(jndex, index), tf(*cr->thirdEdge()->getUSymPml(1)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<2>(gSymSymCornerPmlContinuous[cornerNeighbor[c][2]]))(jndex, index), tf(*cr->thirdEdge()->getUSymSymPml()), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                }
              }
              else if(auto cr = dynamic_cast< PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric * >(corner)) {
                if(!std::get<0>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    auto termId = formulation(index).integral(- (*std::get<1>(gCornerPmlContinuous[cornerNeighbor[c][0]]))(jndex, index), tf(*cr->firstEdge()->getUPml()), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<1>(gSymCornerPmlContinuous[0][cornerNeighbor[c][0]]))(jndex, index), tf(*cr->firstEdge()->getUSymPml(0)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<1>(gSymCornerPmlContinuous[1][cornerNeighbor[c][0]]))(jndex, index), tf(*cr->firstEdge()->getUSymPml(1)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<1>(gSymSymCornerPmlContinuous[cornerNeighbor[c][0]]))(jndex, index), tf(*cr->firstEdge()->getUSymSymPml()), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                }
                if(!std::get<1>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    auto termId = formulation(index).integral(- (*std::get<0>(gCornerPmlContinuous[cornerNeighbor[c][1]]))(jndex, index), tf(*cr->secondEdge()->getUPml()), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<0>(gSymCornerPmlContinuous[0][cornerNeighbor[c][1]]))(jndex, index), tf(*cr->secondEdge()->getUSymPml(0)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<0>(gSymCornerPmlContinuous[1][cornerNeighbor[c][1]]))(jndex, index), tf(*cr->secondEdge()->getUSymPml(1)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<0>(gSymSymCornerPmlContinuous[cornerNeighbor[c][1]]))(jndex, index), tf(*cr->secondEdge()->getUSymSymPml()), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                }
                if(!std::get<2>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    auto termId = formulation(index).integral(- (*std::get<2>(gCornerPmlContinuous[cornerNeighbor[c][2]]))(jndex, index), tf(*cr->thirdEdge()->getUPml()), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<2>(gSymCornerPmlContinuous[0][cornerNeighbor[c][2]]))(jndex, index), tf(*cr->thirdEdge()->getUSymPml(0)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<2>(gSymCornerPmlContinuous[1][cornerNeighbor[c][2]]))(jndex, index), tf(*cr->thirdEdge()->getUSymPml(1)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                  {
                    auto termId = formulation(index).integral(- (*std::get<2>(gSymSymCornerPmlContinuous[cornerNeighbor[c][2]]))(jndex, index), tf(*cr->thirdEdge()->getUSymSymPml()), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                  }
                }
              }
            }
          }

          // interface
          for(unsigned int b = 0; b < 6; ++b) {
            for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
              const unsigned int jndex = topology[index][jj];

              Boundary *boundary = subproblem[i][j][k]->getBoundary(b);
              if(auto bnd = dynamic_cast< Sommerfeld * >(boundary)) {
                if(!sigmaInterface[b](index, jndex).isEmpty()) {
                  formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gContinuous[faceNeighbor[b]])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                }
              }
              else if(auto bnd = dynamic_cast< PmlContinuous * >(boundary)) {
                if(!sigmaInterface[b](index, jndex).isEmpty()) {
                  formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gContinuous[faceNeighbor[b]])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                }
              }
              else if(auto bnd = dynamic_cast< PmlContinuousSymmetric * >(boundary)) {
                if(!sigmaInterface[b](index, jndex).isEmpty()) {
                  formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gContinuous[faceNeighbor[b]])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(*bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                  formulation(index, jndex).integral(*bnd->getVSym(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                }
              }
            }
          }

          for(unsigned int e = 0; e < 12; ++e) {
            for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
              const unsigned int jndex = topology[index][jj];

              Edge *edge = subproblem[i][j][k]->getEdge(e);
              if(edge == nullptr) {
                continue;
              }
              if(auto eg = dynamic_cast< PmlContinuous_PmlContinuous * >(edge)) {
                if(!pmlBndInterface[e].first(index, jndex).isEmpty()) {
                  if(e >= 4 && e < 8) {
                    formulation(index, jndex).integral(dof((*gEdgePmlContinuous[e].first)(index, jndex)), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*gEdgePmlContinuous[edgeNeighbor[e][0]].second)(jndex, index), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *eg->getV(0), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                  }
                  else {
                    formulation(index, jndex).integral(dof((*gEdgePmlContinuous[e].first)(index, jndex)), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*gEdgePmlContinuous[edgeNeighbor[e][0]].first)(jndex, index), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *eg->getV(0), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                  }
                }
                if(!pmlBndInterface[e].second(index, jndex).isEmpty()) {
                  if(e >= 4 && e < 8) {
                    formulation(index, jndex).integral(dof((*gEdgePmlContinuous[e].second)(index, jndex)), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*gEdgePmlContinuous[edgeNeighbor[e][1]].first)(jndex, index), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *eg->getV(1), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                  }
                  else {
                    formulation(index, jndex).integral(dof((*gEdgePmlContinuous[e].second)(index, jndex)), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*gEdgePmlContinuous[edgeNeighbor[e][1]].second)(jndex, index), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *eg->getV(1), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                  }
                }
              }
              else if(auto eg = dynamic_cast< PmlContinuousSymmetric_PmlContinuousSymmetric * >(edge)) {
                if(!pmlBndInterface[e].first(index, jndex).isEmpty()) {
                  if(e >= 4 && e < 8) {
                    {
                      formulation(index, jndex).integral(dof((*gEdgePmlContinuous[e].first)(index, jndex)), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      auto termId = formulation(index, jndex).integral((*gEdgePmlContinuous[edgeNeighbor[e][0]].second)(jndex, index), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                      formulation(index, jndex).integral(*eg->getV(0), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation(index, jndex).integral(*eg->getVSym(0,0), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                    }
                    {
                      formulation(index, jndex).integral(dof((*gSymEdgePmlContinuous[e].first)(index, jndex)), tf((*gSymEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      auto termId = formulation(index, jndex).integral((*gSymEdgePmlContinuous[edgeNeighbor[e][0]].second)(jndex, index), tf((*gSymEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                      formulation(index, jndex).integral(*eg->getVSym(1,0), tf((*gSymEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation(index, jndex).integral(*eg->getVSymSym(0), tf((*gSymEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                    }
                  }
                  else {
                    {
                      formulation(index, jndex).integral(dof((*gEdgePmlContinuous[e].first)(index, jndex)), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      auto termId = formulation(index, jndex).integral((*gEdgePmlContinuous[edgeNeighbor[e][0]].first)(jndex, index), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                      formulation(index, jndex).integral(*eg->getV(0), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation(index, jndex).integral(*eg->getVSym(0,0), tf((*gEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                    }
                    {
                      formulation(index, jndex).integral(dof((*gSymEdgePmlContinuous[e].first)(index, jndex)), tf((*gSymEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      auto termId = formulation(index, jndex).integral((*gSymEdgePmlContinuous[edgeNeighbor[e][0]].first)(jndex, index), tf((*gSymEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                      formulation(index, jndex).integral(*eg->getVSym(1,0), tf((*gSymEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                      formulation(index, jndex).integral(*eg->getVSymSym(0), tf((*gSymEdgePmlContinuous[e].first)(index, jndex)), pmlBndInterface[e].first(index, jndex), gauss);
                    }
                  }
                }
                if(!pmlBndInterface[e].second(index, jndex).isEmpty()) {
                  if(e >= 4 && e < 8) {
                    {
                      formulation(index, jndex).integral(dof((*gEdgePmlContinuous[e].second)(index, jndex)), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      auto termId = formulation(index, jndex).integral((*gEdgePmlContinuous[edgeNeighbor[e][1]].first)(jndex, index), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                      formulation(index, jndex).integral(*eg->getV(1), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation(index, jndex).integral(*eg->getVSym(1,1), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                    }
                    {
                      formulation(index, jndex).integral(dof((*gSymEdgePmlContinuous[e].second)(index, jndex)), tf((*gSymEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      auto termId = formulation(index, jndex).integral((*gSymEdgePmlContinuous[edgeNeighbor[e][1]].first)(jndex, index), tf((*gSymEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                      formulation(index, jndex).integral(*eg->getVSym(0,1), tf((*gSymEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation(index, jndex).integral(*eg->getVSymSym(1), tf((*gSymEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                    }
                  }
                  else {
                    {
                      formulation(index, jndex).integral(dof((*gEdgePmlContinuous[e].second)(index, jndex)), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      auto termId = formulation(index, jndex).integral((*gEdgePmlContinuous[edgeNeighbor[e][1]].second)(jndex, index), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                      formulation(index, jndex).integral(*eg->getV(1), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation(index, jndex).integral(*eg->getVSym(1,1), tf((*gEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                    }
                    {
                      formulation(index, jndex).integral(dof((*gSymEdgePmlContinuous[e].second)(index, jndex)), tf((*gSymEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      auto termId = formulation(index, jndex).integral((*gSymEdgePmlContinuous[edgeNeighbor[e][1]].second)(jndex, index), tf((*gSymEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation.artificialSourceTerm(termId);
                      formulation(index, jndex).integral(*eg->getVSym(0,1), tf((*gSymEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                      formulation(index, jndex).integral(*eg->getVSymSym(1), tf((*gSymEdgePmlContinuous[e].second)(index, jndex)), pmlBndInterface[e].second(index, jndex), gauss);
                    }
                  }
                }
              }
            }
          }

          for(unsigned int c = 0; c < 8; ++c) {
            for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
              const unsigned int jndex = topology[index][jj];

              Corner *corner = subproblem[i][j][k]->getCorner(c);
              if(corner == nullptr) {
                continue;
              }
              if(auto cr = dynamic_cast< PmlContinuous_PmlContinuous_PmlContinuous * >(corner)) {
                if(!std::get<0>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  formulation(index, jndex).integral(dof((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*std::get<1>(gCornerPmlContinuous[cornerNeighbor[c][0]]))(jndex, index), tf((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2. * *cr->getV(0), tf((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                }
                if(!std::get<1>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  formulation(index, jndex).integral(dof((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*std::get<0>(gCornerPmlContinuous[cornerNeighbor[c][1]]))(jndex, index), tf((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2. * *cr->getV(1), tf((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                }
                if(!std::get<2>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  formulation(index, jndex).integral(dof((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*std::get<2>(gCornerPmlContinuous[cornerNeighbor[c][2]]))(jndex, index), tf((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2. * *cr->getV(2), tf((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                }
              }
              else if(auto cr = dynamic_cast< PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric_simplify * >(corner)) {
                if(!std::get<0>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    formulation(index, jndex).integral(dof((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<1>(gCornerPmlContinuous[cornerNeighbor[c][0]]))(jndex, index), tf((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getV(0), tf((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<0>(gSymCornerPmlContinuous[0][c]))(index, jndex)), tf((*std::get<0>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<1>(gSymCornerPmlContinuous[0][cornerNeighbor[c][0]]))(jndex, index), tf((*std::get<0>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getVSym(0,0), tf((*std::get<0>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<0>(gSymCornerPmlContinuous[1][c]))(index, jndex)), tf((*std::get<0>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<1>(gSymCornerPmlContinuous[1][cornerNeighbor[c][0]]))(jndex, index), tf((*std::get<0>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getVSym(1,0), tf((*std::get<0>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<0>(gSymSymCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<0>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<1>(gSymSymCornerPmlContinuous[cornerNeighbor[c][0]]))(jndex, index), tf((*std::get<0>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getVSymSym(0), tf((*std::get<0>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                }
                if(!std::get<1>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    formulation(index, jndex).integral(dof((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<0>(gCornerPmlContinuous[cornerNeighbor[c][1]]))(jndex, index), tf((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getV(1), tf((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<1>(gSymCornerPmlContinuous[0][c]))(index, jndex)), tf((*std::get<1>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<0>(gSymCornerPmlContinuous[0][cornerNeighbor[c][1]]))(jndex, index), tf((*std::get<1>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getVSym(0,1), tf((*std::get<1>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<1>(gSymCornerPmlContinuous[1][c]))(index, jndex)), tf((*std::get<1>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<0>(gSymCornerPmlContinuous[1][cornerNeighbor[c][1]]))(jndex, index), tf((*std::get<1>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getVSym(1,1), tf((*std::get<1>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<1>(gSymSymCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<1>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<0>(gSymSymCornerPmlContinuous[cornerNeighbor[c][1]]))(jndex, index), tf((*std::get<1>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getVSymSym(1), tf((*std::get<1>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                }
                if(!std::get<2>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    formulation(index, jndex).integral(dof((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<2>(gCornerPmlContinuous[cornerNeighbor[c][2]]))(jndex, index), tf((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getV(2), tf((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<2>(gSymCornerPmlContinuous[0][c]))(index, jndex)), tf((*std::get<2>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<2>(gSymCornerPmlContinuous[0][cornerNeighbor[c][2]]))(jndex, index), tf((*std::get<2>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getVSym(0,2), tf((*std::get<2>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<2>(gSymCornerPmlContinuous[1][c]))(index, jndex)), tf((*std::get<2>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<2>(gSymCornerPmlContinuous[1][cornerNeighbor[c][2]]))(jndex, index), tf((*std::get<2>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getVSym(1,2), tf((*std::get<2>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<2>(gSymSymCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<2>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<2>(gSymSymCornerPmlContinuous[cornerNeighbor[c][2]]))(jndex, index), tf((*std::get<2>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(2. * *cr->getVSymSym(2), tf((*std::get<2>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                }
              }
              else if(auto cr = dynamic_cast< PmlContinuousSymmetric_PmlContinuousSymmetric_PmlContinuousSymmetric * >(corner)) {
                if(!std::get<0>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    formulation(index, jndex).integral(dof((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<1>(gCornerPmlContinuous[cornerNeighbor[c][0]]))(jndex, index), tf((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getV(0), tf((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSym(0,0), tf((*std::get<0>(gCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<0>(gSymCornerPmlContinuous[0][c]))(index, jndex)), tf((*std::get<0>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<1>(gSymCornerPmlContinuous[0][cornerNeighbor[c][0]]))(jndex, index), tf((*std::get<0>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getVSym(2,0), tf((*std::get<0>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSymSym(2,0), tf((*std::get<0>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<0>(gSymCornerPmlContinuous[1][c]))(index, jndex)), tf((*std::get<0>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<1>(gSymCornerPmlContinuous[1][cornerNeighbor[c][0]]))(jndex, index), tf((*std::get<0>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getVSym(1,0), tf((*std::get<0>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSymSym(1,0), tf((*std::get<0>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<0>(gSymSymCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<0>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<1>(gSymSymCornerPmlContinuous[cornerNeighbor[c][0]]))(jndex, index), tf((*std::get<0>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getVSymSym(0,0), tf((*std::get<0>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSymSymSym(0), tf((*std::get<0>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<0>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                }
                if(!std::get<1>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    formulation(index, jndex).integral(dof((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<0>(gCornerPmlContinuous[cornerNeighbor[c][1]]))(jndex, index), tf((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getV(1), tf((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSym(1,1), tf((*std::get<1>(gCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<1>(gSymCornerPmlContinuous[0][c]))(index, jndex)), tf((*std::get<1>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<0>(gSymCornerPmlContinuous[0][cornerNeighbor[c][1]]))(jndex, index), tf((*std::get<1>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getVSym(2,1), tf((*std::get<1>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSymSym(2,1), tf((*std::get<1>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<1>(gSymCornerPmlContinuous[1][c]))(index, jndex)), tf((*std::get<1>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<0>(gSymCornerPmlContinuous[1][cornerNeighbor[c][1]]))(jndex, index), tf((*std::get<1>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getVSym(0,1), tf((*std::get<1>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSymSym(0,1), tf((*std::get<1>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<1>(gSymSymCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<1>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<0>(gSymSymCornerPmlContinuous[cornerNeighbor[c][1]]))(jndex, index), tf((*std::get<1>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getVSymSym(1,1), tf((*std::get<1>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSymSymSym(1), tf((*std::get<1>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<1>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                }
                if(!std::get<2>(pmlEdgeBndInterface[c])(index, jndex).isEmpty()) {
                  {
                    formulation(index, jndex).integral(dof((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<2>(gCornerPmlContinuous[cornerNeighbor[c][2]]))(jndex, index), tf((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getV(2), tf((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSym(2,2), tf((*std::get<2>(gCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<2>(gSymCornerPmlContinuous[0][c]))(index, jndex)), tf((*std::get<2>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<2>(gSymCornerPmlContinuous[0][cornerNeighbor[c][2]]))(jndex, index), tf((*std::get<2>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getVSym(1,2), tf((*std::get<2>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSymSym(1,2), tf((*std::get<2>(gSymCornerPmlContinuous[0][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<2>(gSymCornerPmlContinuous[1][c]))(index, jndex)), tf((*std::get<2>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<2>(gSymCornerPmlContinuous[1][cornerNeighbor[c][2]]))(jndex, index), tf((*std::get<2>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getVSym(0,2), tf((*std::get<2>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSymSym(0,2), tf((*std::get<2>(gSymCornerPmlContinuous[1][c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                  {
                    formulation(index, jndex).integral(dof((*std::get<2>(gSymSymCornerPmlContinuous[c]))(index, jndex)), tf((*std::get<2>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    auto termId = formulation(index, jndex).integral((*std::get<2>(gSymSymCornerPmlContinuous[cornerNeighbor[c][2]]))(jndex, index), tf((*std::get<2>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation.artificialSourceTerm(termId);
                    formulation(index, jndex).integral(*cr->getVSymSym(2,2), tf((*std::get<2>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                    formulation(index, jndex).integral(*cr->getVSymSymSym(2), tf((*std::get<2>(gSymSymCornerPmlContinuous[c]))(index, jndex)), std::get<2>(pmlEdgeBndInterface[c])(index, jndex), gauss);
                  }
                }
              }
            }
          }
        }
      }
    }

    formulation.pre();
    formulation.solve("gmres", res, iterMax, false, false);

    for(unsigned int i = 0; i < nDomX; ++i) {
      for(unsigned int j = 0; j < nDomY; ++j) {
        for(unsigned int k = 0; k < nDomZ; ++k) {
          unsigned int index = i * nDomY * nDomZ + j * nDomZ + k;
          if(gmshddm::mpi::isItMySubdomain(index)) {
            if(saveU) {
              gmshfem::post::save(xComp(u(index)), omega(index), "ux_" + std::to_string(index), "pos", outputPath);
              gmshfem::post::save(yComp(u(index)), omega(index), "uy_" + std::to_string(index), "pos", outputPath);
              gmshfem::post::save(zComp(u(index)), omega(index), "uz_" + std::to_string(index), "pos", outputPath);

              //gmshfem::post::save(tr(grad(u(index))), omega(index), "uP_" + std::to_string(index), "msh");
            }
          }
        }
      }
    }

    if(monoDomainError) {
      gmshfem::problem::Formulation< std::complex< double > > formulationMono("NavierMonoDomain");

      std::vector< gmshfem::domain::Domain > sigmaMono(6);
      std::vector< gmshfem::domain::Domain > pmlMono(6);
      std::vector< gmshfem::domain::Domain > pmlInfMono(6);
      std::vector< gmshfem::domain::Domain > pmlEdgeMono(12);
      std::vector< gmshfem::domain::Domain > pmlEdgeInfMono(12);
      std::vector< gmshfem::domain::Domain > pmlCornerMono(8);
      std::vector< gmshfem::domain::Domain > pmlCornerInfMono(8);
      std::vector< std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > > pmlBndMono(12);
      std::vector< std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > > pmlEdgeBndMono(8);
      std::vector< gmshfem::domain::Domain > edgeMono(12);
      std::vector< gmshfem::domain::Domain > cornerMono(8);
      std::vector< std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > > cornerEdgeMono(8);

      for(unsigned int i = 0; i < nDomX; ++i) {
        for(unsigned int j = 0; j < nDomY; ++j) {
          for(unsigned int k = 0; k < nDomZ; ++k) {
            unsigned int index = i * nDomY * nDomZ + j * nDomZ + k;
            if(i == 0) {
              sigmaMono[2] |= sigma[2](index);
              pmlMono[2] |= pml[2](index);
              pmlInfMono[2] |= pmlInf[2](index);
              if(j == 0) {
                pmlEdgeMono[6] |= pmlEdge[6](index);
                pmlEdgeInfMono[6] |= pmlEdgeInf[6](index);
                edgeMono[6] |= edge[6](index);
                pmlBndMono[6].first |= pmlBnd[6].first(index);
                pmlBndMono[6].second |= pmlBnd[6].second(index);
                if(k == 0) {
                  pmlCornerMono[2] |= pmlCorner[2](index);
                  pmlCornerInfMono[2] |= pmlCornerInf[2](index);
                  cornerMono[2] |= corner[2](index);
                  std::get<0>(pmlEdgeBndMono[2]) |= std::get<0>(pmlEdgeBnd[2])(index);
                  std::get<1>(pmlEdgeBndMono[2]) |= std::get<1>(pmlEdgeBnd[2])(index);
                  std::get<2>(pmlEdgeBndMono[2]) |= std::get<2>(pmlEdgeBnd[2])(index);
                  std::get<0>(cornerEdgeMono[2]) |= std::get<0>(cornerEdge[2])(index);
                  std::get<1>(cornerEdgeMono[2]) |= std::get<1>(cornerEdge[2])(index);
                  std::get<2>(cornerEdgeMono[2]) |= std::get<2>(cornerEdge[2])(index);
                }
                if(k == nDomZ-1) {
                  pmlCornerMono[6] |= pmlCorner[6](index);
                  pmlCornerInfMono[6] |= pmlCornerInf[6](index);
                  cornerMono[6] |= corner[6](index);
                  std::get<0>(pmlEdgeBndMono[6]) |= std::get<0>(pmlEdgeBnd[6])(index);
                  std::get<1>(pmlEdgeBndMono[6]) |= std::get<1>(pmlEdgeBnd[6])(index);
                  std::get<2>(pmlEdgeBndMono[6]) |= std::get<2>(pmlEdgeBnd[6])(index);
                  std::get<0>(cornerEdgeMono[6]) |= std::get<0>(cornerEdge[6])(index);
                  std::get<1>(cornerEdgeMono[6]) |= std::get<1>(cornerEdge[6])(index);
                  std::get<2>(cornerEdgeMono[6]) |= std::get<2>(cornerEdge[6])(index);
                }
              }
              if(k == 0) {
                pmlEdgeMono[2] |= pmlEdge[2](index);
                pmlEdgeInfMono[2] |= pmlEdgeInf[2](index);
                edgeMono[2] |= edge[2](index);
                pmlBndMono[2].first |= pmlBnd[2].first(index);
                pmlBndMono[2].second |= pmlBnd[2].second(index);
              }
              if(j == nDomY-1) {
                pmlEdgeMono[5] |= pmlEdge[5](index);
                pmlEdgeInfMono[5] |= pmlEdgeInf[5](index);
                edgeMono[5] |= edge[5](index);
                pmlBndMono[5].first |= pmlBnd[5].first(index);
                pmlBndMono[5].second |= pmlBnd[5].second(index);
                if(k == 0) {
                  pmlCornerMono[1] |= pmlCorner[1](index);
                  pmlCornerInfMono[1] |= pmlCornerInf[1](index);
                  cornerMono[1] |= corner[1](index);
                  std::get<0>(pmlEdgeBndMono[1]) |= std::get<0>(pmlEdgeBnd[1])(index);
                  std::get<1>(pmlEdgeBndMono[1]) |= std::get<1>(pmlEdgeBnd[1])(index);
                  std::get<2>(pmlEdgeBndMono[1]) |= std::get<2>(pmlEdgeBnd[1])(index);
                  std::get<0>(cornerEdgeMono[1]) |= std::get<0>(cornerEdge[1])(index);
                  std::get<1>(cornerEdgeMono[1]) |= std::get<1>(cornerEdge[1])(index);
                  std::get<2>(cornerEdgeMono[1]) |= std::get<2>(cornerEdge[1])(index);
                }
                if(k == nDomZ-1) {
                  pmlCornerMono[5] |= pmlCorner[5](index);
                  pmlCornerInfMono[5] |= pmlCornerInf[5](index);
                  cornerMono[5] |= corner[5](index);
                  std::get<0>(pmlEdgeBndMono[5]) |= std::get<0>(pmlEdgeBnd[5])(index);
                  std::get<1>(pmlEdgeBndMono[5]) |= std::get<1>(pmlEdgeBnd[5])(index);
                  std::get<2>(pmlEdgeBndMono[5]) |= std::get<2>(pmlEdgeBnd[5])(index);
                  std::get<0>(cornerEdgeMono[5]) |= std::get<0>(cornerEdge[5])(index);
                  std::get<1>(cornerEdgeMono[5]) |= std::get<1>(cornerEdge[5])(index);
                  std::get<2>(cornerEdgeMono[5]) |= std::get<2>(cornerEdge[5])(index);
                }
              }
              if(k == nDomZ-1) {
                pmlEdgeMono[10] |= pmlEdge[10](index);
                pmlEdgeInfMono[10] |= pmlEdgeInf[10](index);
                edgeMono[10] |= edge[10](index);
                pmlBndMono[10].first |= pmlBnd[10].first(index);
                pmlBndMono[10].second |= pmlBnd[10].second(index);
              }
            }
            if(j == 0) {
              sigmaMono[3] |= sigma[3](index);
              pmlMono[3] |= pml[3](index);
              pmlInfMono[3] |= pmlInf[3](index);
              if(k == 0) {
                pmlEdgeMono[3] |= pmlEdge[3](index);
                pmlEdgeInfMono[3] |= pmlEdgeInf[3](index);
                edgeMono[3] |= edge[3](index);
                pmlBndMono[3].first |= pmlBnd[3].first(index);
                pmlBndMono[3].second |= pmlBnd[3].second(index);
              }
              if(k == nDomZ-1) {
                pmlEdgeMono[11] |= pmlEdge[11](index);
                pmlEdgeInfMono[11] |= pmlEdgeInf[11](index);
                edgeMono[11] |= edge[11](index);
                pmlBndMono[11].first |= pmlBnd[11].first(index);
                pmlBndMono[11].second |= pmlBnd[11].second(index);
              }
            }
            if(k == 0) {
              sigmaMono[4] |= sigma[4](index);
              pmlMono[4] |= pml[4](index);
              pmlInfMono[4] |= pmlInf[4](index);
            }
            if(i == nDomX-1) {
              sigmaMono[0] |= sigma[0](index);
              pmlMono[0] |= pml[0](index);
              pmlInfMono[0] |= pmlInf[0](index);
              if(j == 0) {
                pmlEdgeMono[7] |= pmlEdge[7](index);
                pmlEdgeInfMono[7] |= pmlEdgeInf[7](index);
                edgeMono[7] |= edge[7](index);
                pmlBndMono[7].first |= pmlBnd[7].first(index);
                pmlBndMono[7].second |= pmlBnd[7].second(index);
                if(k == 0) {
                  pmlCornerMono[3] |= pmlCorner[3](index);
                  pmlCornerInfMono[3] |= pmlCornerInf[3](index);
                  cornerMono[3] |= corner[3](index);
                  std::get<0>(pmlEdgeBndMono[3]) |= std::get<0>(pmlEdgeBnd[3])(index);
                  std::get<1>(pmlEdgeBndMono[3]) |= std::get<1>(pmlEdgeBnd[3])(index);
                  std::get<2>(pmlEdgeBndMono[3]) |= std::get<2>(pmlEdgeBnd[3])(index);
                  std::get<0>(cornerEdgeMono[3]) |= std::get<0>(cornerEdge[3])(index);
                  std::get<1>(cornerEdgeMono[3]) |= std::get<1>(cornerEdge[3])(index);
                  std::get<2>(cornerEdgeMono[3]) |= std::get<2>(cornerEdge[3])(index);
                }
                if(k == nDomZ-1) {
                  pmlCornerMono[7] |= pmlCorner[7](index);
                  pmlCornerInfMono[7] |= pmlCornerInf[7](index);
                  cornerMono[7] |= corner[7](index);
                  std::get<0>(pmlEdgeBndMono[7]) |= std::get<0>(pmlEdgeBnd[7])(index);
                  std::get<1>(pmlEdgeBndMono[7]) |= std::get<1>(pmlEdgeBnd[7])(index);
                  std::get<2>(pmlEdgeBndMono[7]) |= std::get<2>(pmlEdgeBnd[7])(index);
                  std::get<0>(cornerEdgeMono[7]) |= std::get<0>(cornerEdge[7])(index);
                  std::get<1>(cornerEdgeMono[7]) |= std::get<1>(cornerEdge[7])(index);
                  std::get<2>(cornerEdgeMono[7]) |= std::get<2>(cornerEdge[7])(index);
                }
              }
              if(k == 0) {
                pmlEdgeMono[0] |= pmlEdge[0](index);
                pmlEdgeInfMono[0] |= pmlEdgeInf[0](index);
                edgeMono[0] |= edge[0](index);
                pmlBndMono[0].first |= pmlBnd[0].first(index);
                pmlBndMono[0].second |= pmlBnd[0].second(index);
              }
              if(j == nDomY-1) {
                pmlEdgeMono[4] |= pmlEdge[4](index);
                pmlEdgeInfMono[4] |= pmlEdgeInf[4](index);
                edgeMono[4] |= edge[4](index);
                pmlBndMono[4].first |= pmlBnd[4].first(index);
                pmlBndMono[4].second |= pmlBnd[4].second(index);
                if(k == 0) {
                  pmlCornerMono[0] |= pmlCorner[0](index);
                  pmlCornerInfMono[0] |= pmlCornerInf[0](index);
                  cornerMono[0] |= corner[0](index);
                  std::get<0>(pmlEdgeBndMono[0]) |= std::get<0>(pmlEdgeBnd[0])(index);
                  std::get<1>(pmlEdgeBndMono[0]) |= std::get<1>(pmlEdgeBnd[0])(index);
                  std::get<2>(pmlEdgeBndMono[0]) |= std::get<2>(pmlEdgeBnd[0])(index);
                  std::get<0>(cornerEdgeMono[0]) |= std::get<0>(cornerEdge[0])(index);
                  std::get<1>(cornerEdgeMono[0]) |= std::get<1>(cornerEdge[0])(index);
                  std::get<2>(cornerEdgeMono[0]) |= std::get<2>(cornerEdge[0])(index);
                }
                if(k == nDomZ-1) {
                  pmlCornerMono[4] |= pmlCorner[4](index);
                  pmlCornerInfMono[4] |= pmlCornerInf[4](index);
                  cornerMono[4] |= corner[4](index);
                  std::get<0>(pmlEdgeBndMono[4]) |= std::get<0>(pmlEdgeBnd[4])(index);
                  std::get<1>(pmlEdgeBndMono[4]) |= std::get<1>(pmlEdgeBnd[4])(index);
                  std::get<2>(pmlEdgeBndMono[4]) |= std::get<2>(pmlEdgeBnd[4])(index);
                  std::get<0>(cornerEdgeMono[4]) |= std::get<0>(cornerEdge[4])(index);
                  std::get<1>(cornerEdgeMono[4]) |= std::get<1>(cornerEdge[4])(index);
                  std::get<2>(cornerEdgeMono[4]) |= std::get<2>(cornerEdge[4])(index);
                }
              }
              if(k == nDomZ-1) {
                pmlEdgeMono[8] |= pmlEdge[8](index);
                pmlEdgeInfMono[8] |= pmlEdgeInf[8](index);
                edgeMono[8] |= edge[8](index);
                pmlBndMono[8].first |= pmlBnd[8].first(index);
                pmlBndMono[8].second |= pmlBnd[8].second(index);
              }
            }
            if(j == nDomY-1) {
              sigmaMono[1] |= sigma[1](index);
              pmlMono[1] |= pml[1](index);
              pmlInfMono[1] |= pmlInf[1](index);
              if(k == 0) {
                pmlEdgeMono[1] |= pmlEdge[1](index);
                pmlEdgeInfMono[1] |= pmlEdgeInf[1](index);
                edgeMono[1] |= edge[1](index);
                pmlBndMono[1].first |= pmlBnd[1].first(index);
                pmlBndMono[1].second |= pmlBnd[1].second(index);
              }
              if(k == nDomZ-1) {
                pmlEdgeMono[9] |= pmlEdge[9](index);
                pmlEdgeInfMono[9] |= pmlEdgeInf[9](index);
                edgeMono[9] |= edge[9](index);
                pmlBndMono[9].first |= pmlBnd[9].first(index);
                pmlBndMono[9].second |= pmlBnd[9].second(index);
              }
            }
            if(k == nDomZ-1) {
              sigmaMono[5] |= sigma[5](index);
              pmlMono[5] |= pml[5](index);
              pmlInfMono[5] |= pmlInf[5](index);
            }
          }
        }
      }

      gmshfem::field::CompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 > uMono("uMono", omega.getUnion(), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

      formulationMono.integral(- C * grad(dof(uMono)), grad(tf(uMono)), omega.getUnion(), gauss);
      formulationMono.integral(dof(uMono), tf(uMono), omega.getUnion(), gauss);

      if(benchmark == "scattering") {
        uMono.addConstraint(gamma, gmshfem::function::vector< std::complex< double > >(1., 0., 0.));
      }

      SubproblemDomains domains;
      domains.setOmega(omega.getUnion());
      domains.setSigma(sigmaMono);
      domains.setPml(pmlMono);
      domains.setPmlInf(pmlInfMono);
      domains.setPmlBnd(pmlBndMono);
      domains.setPmlEdge(pmlEdgeMono);
      domains.setPmlEdgeInf(pmlEdgeInfMono);
      domains.setEdge(edgeMono);
      domains.setPmlEdgeBnd(pmlEdgeBndMono);
      domains.setPmlCorner(pmlCornerMono);
      domains.setPmlCornerInf(pmlCornerInfMono);
      domains.setCorner(cornerMono);
      domains.setCornerEdge(cornerEdgeMono);

      SubproblemParameters parameters;
      parameters.setGauss(gauss);
      parameters.setKappaP(kappaP);
      parameters.setKappaS(kappaS);
      parameters.setC(C);
      parameters.setNeumannOrder(neumannOrder);
      parameters.setFieldOrder(fieldOrder);
      parameters.setStab(stab);

      std::string bndExt = boundaryExt;
      if(boundaryExt == "pml") {
        if(pmlMethodExt == "continuous") {
          bndExt += "Continuous_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt + "_ext";;
        }
        else if(pmlMethodExt == "continuousSymmetric") {
          bndExt += "ContinuousSymmetric_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt + "_ext";;
        }
      }
      if(gmshddm::mpi::getMPIRank() == 0) {
        gmshfem::msg::info << "Monodomain has boundaries [" << bndExt << ", " << bndExt << ", " << bndExt << ", " << bndExt << ", " << bndExt << ", " << bndExt  << "]" << gmshfem::msg::endl;
      }
      Subproblem subproblem(formulationMono, uMono.name(), domains, parameters, bndExt, bndExt, bndExt, bndExt, bndExt, bndExt);
      subproblem.writeFormulation();

      formulationMono.pre();
      formulationMono.assemble();
      formulationMono.solve();

      double num = 0.;
      double den = 0.;
      for(unsigned int i = 0; i < nDomX * nDomY * nDomZ; ++i) {
        num += std::real(gmshfem::post::integrate(pow(norm(u(i) - uMono), 2), omega(i), gauss));
        den += std::real(gmshfem::post::integrate(pow(norm(uMono), 2), omega(i), gauss));
      }

      if(gmshddm::mpi::getMPIRank() == 0) {
        gmshfem::msg::info << "Error mono L2 = " << std::sqrt(num/den) << gmshfem::msg::endl;
      }

      for(unsigned int i = 0; i < nDomX * nDomY * nDomZ; ++i) {
        if(gmshddm::mpi::isItMySubdomain(i)) {
          if(saveEMono) {
            gmshfem::post::save(xComp(u(i) - uMono), omega(i), "eMonox_" + std::to_string(i));
            gmshfem::post::save(yComp(u(i) - uMono), omega(i), "eMonoy_" + std::to_string(i));
            gmshfem::post::save(zComp(u(i) - uMono), omega(i), "eMonoz_" + std::to_string(i));
          }
          if(saveUMono) {
            gmshfem::post::save(xComp(uMono), omega(i), "uMonox_" + std::to_string(i));
            gmshfem::post::save(yComp(uMono), omega(i), "uMonoy_" + std::to_string(i));
            gmshfem::post::save(zComp(uMono), omega(i), "uMonoz_" + std::to_string(i));
          }
        }
      }

      if(fileName != "none") {
        gmshfem::common::CSVio file(fileName, ';', gmshfem::common::OpeningMode::Append);
//        file << iterMax << formulation.relativeResidual().back() << std::sqrt(num/den) << gmshfem::csv::endl;
        file.close();
      }
    }

    if(wavenumberPlot) {
      gmshfem::common::CSVio file(fileName, ';', gmshfem::common::OpeningMode::Append);
//      file << kp << ks << formulation.relativeResidual().size()-1 << gmshfem::csv::endl;
      file.close();
    }
  }


}
