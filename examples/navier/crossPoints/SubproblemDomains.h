#ifndef H_SUBPROBLEMDOMAINS
#define H_SUBPROBLEMDOMAINS

#include <gmshfem/Domain.h>

#include <tuple>

namespace D2 {


  class SubproblemDomains
  {
   private:
    gmshfem::domain::Domain _omega;
    // boundary
    std::vector< gmshfem::domain::Domain > _sigma;
    std::vector< gmshfem::domain::Domain > _pml;
    std::vector< gmshfem::domain::Domain > _pmlInf;
    // corner
    std::vector< std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > > _pmlBnd;
    std::vector< gmshfem::domain::Domain > _pmlCorner;
    std::vector< gmshfem::domain::Domain > _pmlCornerInf;
    std::vector< gmshfem::domain::Domain > _corner;
    
   public:
    SubproblemDomains();
    ~SubproblemDomains();
    
    void setOmega(const gmshfem::domain::Domain &omega);
    
    void setSigma(const std::vector< gmshfem::domain::Domain > &sigma);
    gmshfem::domain::Domain getSigma(const unsigned int b) const;
    
    void setPml(const std::vector< gmshfem::domain::Domain > &pml);
    gmshfem::domain::Domain getPml(const unsigned int b) const;
    
    void setPmlInf(const std::vector< gmshfem::domain::Domain > &pmlInf);
    gmshfem::domain::Domain getPmlInf(const unsigned int b) const;
    
    void setPmlBnd(const std::vector< std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > > &pmlBnd);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > getPmlBnd(const unsigned int c) const;
    
    void setPmlCorner(const std::vector< gmshfem::domain::Domain > &pmlCorner);
    gmshfem::domain::Domain getPmlCorner(const unsigned int c) const;
    
    void setPmlCornerInf(const std::vector< gmshfem::domain::Domain > &pmlCornerInf);
    gmshfem::domain::Domain getPmlCornerInf(const unsigned int c) const;
    
    void setCorner(const std::vector< gmshfem::domain::Domain > &corner);
    gmshfem::domain::Domain getCorner(const unsigned int c) const;
  };


}

namespace D3 {


  class SubproblemDomains
  {
   private:
    gmshfem::domain::Domain _omega;
    // boundary
    std::vector< gmshfem::domain::Domain > _sigma;
    std::vector< gmshfem::domain::Domain > _pml;
    std::vector< gmshfem::domain::Domain > _pmlInf;
    // edge
    std::vector< std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > > _pmlBnd;
    std::vector< gmshfem::domain::Domain > _pmlEdge;
    std::vector< gmshfem::domain::Domain > _pmlEdgeInf;
    std::vector< gmshfem::domain::Domain > _edge;
    // corner
    std::vector< std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > > _pmlEdgeBnd;
    std::vector< gmshfem::domain::Domain > _pmlCorner;
    std::vector< gmshfem::domain::Domain > _pmlCornerInf;
    std::vector< gmshfem::domain::Domain > _corner;
    std::vector< std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > > _cornerEdge;
    
   public:
    SubproblemDomains();
    ~SubproblemDomains();
    
    void setOmega(const gmshfem::domain::Domain &omega);
    
    void setSigma(const std::vector< gmshfem::domain::Domain > &sigma);
    gmshfem::domain::Domain getSigma(const unsigned int b) const;
    
    void setPml(const std::vector< gmshfem::domain::Domain > &pml);
    gmshfem::domain::Domain getPml(const unsigned int b) const;
    
    void setPmlInf(const std::vector< gmshfem::domain::Domain > &pmlInf);
    gmshfem::domain::Domain getPmlInf(const unsigned int b) const;
    
    void setPmlBnd(const std::vector< std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > > &pmlBnd);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > getPmlBnd(const unsigned int e) const;
    
    void setPmlEdge(const std::vector< gmshfem::domain::Domain > &pmlEdge);
    gmshfem::domain::Domain getPmlEdge(const unsigned int e) const;
    
    void setPmlEdgeInf(const std::vector< gmshfem::domain::Domain > &pmlEdgeInf);
    gmshfem::domain::Domain getPmlEdgeInf(const unsigned int e) const;
  
    void setEdge(const std::vector< gmshfem::domain::Domain > &edge);
    gmshfem::domain::Domain getEdge(const unsigned int e) const;
    
    void setPmlEdgeBnd(const std::vector< std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > > &pmlEdgeBnd);
    std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > getPmlEdgeBnd(const unsigned int c) const;
    
    void setPmlCorner(const std::vector< gmshfem::domain::Domain > &pmlCorner);
    gmshfem::domain::Domain getPmlCorner(const unsigned int c) const;
    
    void setPmlCornerInf(const std::vector< gmshfem::domain::Domain > &pmlCornerInf);
    gmshfem::domain::Domain getPmlCornerInf(const unsigned int c) const;
    
    void setCorner(const std::vector< gmshfem::domain::Domain > &corner);
    gmshfem::domain::Domain getCorner(const unsigned int c) const;
    
    void setCornerEdge(const std::vector< std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > > &cornerEdge);
    std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > getCornerEdge(const unsigned int c) const;
    
    void saveDebug() const;
  };


}

#endif // H_SUBPROBLEMDOMAINS
