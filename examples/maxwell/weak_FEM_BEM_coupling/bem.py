import bempp.api
import numpy as np
from scipy.linalg import lu_solve, lu_factor
from math import sqrt

import gmsh
def _getEdgeSize(v1,v2):
    return sqrt((v1[0]-v2[0])**2+(v1[1]-v2[1])**2+(v1[2]-v2[2])**2)

def pre(ke, elementsNode0, elementsNode1, elementsNode2, nodesCoordX, nodesCoordY, nodesCoordZ,opT, Ze,mapBemppToGmsh) :
    global trace_Einc
    global trace_Hinc
    global R
    global I
    global tagEdge
    global PrHdivBemToGMSH
    global lu
    global piv
    global div_space
    global curl_space
    global kk
    kk=ke
    nodesCoord =[]
    nodesCoord.append(nodesCoordX)
    nodesCoord.append(nodesCoordY)
    nodesCoord.append(nodesCoordZ)
    elementsNode =[]
    elementsNode0t=np.zeros(len(elementsNode0))
    elementsNode1t=np.zeros(len(elementsNode1))
    elementsNode2t=np.zeros(len(elementsNode2))
    elementsNode.append(elementsNode0)
    elementsNode.append(elementsNode1)
    elementsNode.append(elementsNode2)
    nodesCoord = np.array(nodesCoord)
    elementsNode = np.array(elementsNode)
    grid = bempp.api.grid.Grid(nodesCoord, elementsNode)
    #grid.plot()
    print("Info	: Number of elements in surface mesh:", grid.number_of_elements)
    print("Info	: Number of vertices in surface mesh", grid.number_of_vertices)
    div_space = bempp.api.function_space(grid, "RWG", 0)
    curl_space = bempp.api.function_space(grid, "SNC", 0)
    elec = bempp.api.operators.boundary.maxwell.electric_field(
    div_space, div_space, curl_space, ke).weak_form().A
    I = bempp.api.operators.boundary.sparse.identity(
    div_space, div_space, div_space).weak_form().A
    R=bempp.api.operators.boundary.sparse.identity(
    div_space, div_space,curl_space).weak_form().A
    magn=bempp.api.operators.boundary.maxwell.magnetic_field(
    div_space, div_space, curl_space, ke).weak_form().A
    direction= np.array([0,0,1.])
    polarization= np.array([1.,0,0])
    @bempp.api.complex_callable
    def tangential_trace_einc(x, n, domain_index, result):
        incident_field = polarization*np.exp(1j *ke* np.dot(x, direction))
        result[:] = np.cross(incident_field, n)
    @bempp.api.complex_callable
    def tangential_trace_hinc(x, n, domain_index, result):
        value = np.cross(direction, polarization) * np.exp(1j * ke * np.dot(x, direction))/Ze
        result[:] =np.cross(value, n)
    trace_Einc = bempp.api.GridFunction(div_space, fun=tangential_trace_einc,dual_space=curl_space)
    trace_Hinc = bempp.api.GridFunction(div_space, fun=tangential_trace_hinc,dual_space=curl_space)
    Matrixlhs=np.block([[I/(2*opT)+elec/Ze,-magn],[magn/opT,I/2.+Ze/opT*elec]])
    print("Info	: End assemble Matrixlhs")
    lu, piv = lu_factor(Matrixlhs)
    print("Info	: End factorization")
    print("Info	: Size Bem System",Matrixlhs.shape)
    #Note : - Local Orientation is the same for gmsh and bempp
    #       - e0bempp=e0GMSH; e1bempp=e2GMSH
    mapLocalEdgeBemToGmsh=[0,2,1]
    numberOfEdges=grid.number_of_edges
    edgeList=[]
    PrHdivBemToGMSH=np.zeros(numberOfEdges)
    lengthEdge=[]
    reverse=[]
    for i in range(numberOfEdges):
        idElementBem=div_space.global2local[i][0][0]
        idEdgeLocaGmsh=mapLocalEdgeBemToGmsh[div_space.global2local[i][0][1]]
        v1=grid.edges[:,i][0]
        v2=grid.edges[:,i][1]
        v1EdgeOriented=0
        v1EdgeOriented=elementsNode[idEdgeLocaGmsh][idElementBem]
        if (v1!=v1EdgeOriented): #Reverse sign
            reverse.append(-1)
        else:
            reverse.append(1)
        edgeList.append(mapBemppToGmsh[v1])
        edgeList.append(mapBemppToGmsh[v2])
        lengthEdge.append(_getEdgeSize(nodesCoord[:,v1],nodesCoord[:,v2]))
    tagEdge,localOrientationGmsh=gmsh.model.mesh.getEdges(edgeList)
    for i in range(numberOfEdges):#numDof==numEdges
        PrHdivBemToGMSH[i]=localOrientationGmsh[i]*reverse[i]*2./lengthEdge[i]


def getEdgeNumInBemDofOrder():
    return tagEdge

def getChangeOfBasisVector():
    return PrHdivBemToGMSH

def clearGlobalVariable():
    global tagEdge
    global PrHdivBemToGMSH
    del tagEdge
    del PrHdivBemToGMSH


def solveIE(g_bem,physicalSourceForIE,artificialSourceForIE,opT) :
    if physicalSourceForIE==True and artificialSourceForIE==True:
        rhs=np.array(np.concatenate((0.5*R.dot(g_bem)-trace_Hinc.projections(),0.5*I.dot(g_bem)+1./opT*trace_Einc.projections())))
    if  physicalSourceForIE==True and artificialSourceForIE==False:
        rhs =np.array(np.concatenate((-trace_Hinc.projections(),1./opT*trace_Einc.projections())))
    if  physicalSourceForIE==False and artificialSourceForIE==True:
        rhs =np.array(np.concatenate((0.5*R.dot(g_bem),0.5*I.dot(g_bem))))
    x=lu_solve((lu, piv), rhs)
    return x

def getL2errorJM(g_bem,Zi,ki,ke,Ze,opT):
    #Strong BEM BEM coupling :**************
    Tkp = bempp.api.operators.boundary.maxwell.electric_field(div_space, div_space, curl_space, ke).weak_form().A
    Kp=bempp.api.operators.boundary.maxwell.magnetic_field(div_space, div_space, curl_space, ke).weak_form().A
    Tkm = bempp.api.operators.boundary.maxwell.electric_field( div_space, div_space, curl_space, ki).weak_form().A
    Km=bempp.api.operators.boundary.maxwell.magnetic_field(div_space, div_space, curl_space, ki).weak_form().A
    BEM_matrix=np.block([[Kp+Km,Tkm*Zi+Tkp*Ze],[-Tkm/Zi-Tkp/Ze,Kp+Km]])
    rhsbembem=np.array(np.concatenate((trace_Einc.projections(),trace_Hinc.projections())))
    sol=np.linalg.solve(BEM_matrix, rhsbembem)
    M=sol[0:int(len(sol)/2)]
    J=sol[int(len(sol)/2):len(sol)]
    #**********************************
    rhs=np.array(np.concatenate((0.5*R.dot(g_bem)-trace_Hinc.projections(),0.5*I.dot(g_bem)+1./opT*trace_Einc.projections())))
    x=lu_solve((lu, piv), rhs)
    sub=M- x[0:int(len(x)/2)]
    domin=np.vdot(I.dot(sub),sub)
    error=[]
    error.append(sqrt(domin.real/np.vdot(I.dot(M),M).real))
    sub2=J- x[int(len(x)/2):len(x)]
    domin2=np.vdot(I.dot(sub2),sub2)
    error.append(sqrt(domin2.real/np.vdot(I.dot(J),J).real))
    return error
