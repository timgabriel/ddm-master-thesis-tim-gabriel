#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshddm/MPIInterface.h>
#include <gmshfem/Message.h>
// #include "mpi.h"
// #include "petscsys.h"
#include <gmsh.h>
#include <gmshfem/AnalyticalFunction.h>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;

using namespace gmshfem;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::problem;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::analytics;
using namespace gmshfem::post;

void waveguide(const unsigned int nDom, const double l, const double L, const double lc)
{
  if(nDom < 2) {
    gmshfem::msg::error << "nDom should be at least equal to 2." << gmshfem::msg::endl;
    exit(0);
  }
  gmsh::model::add("waveguide");

  const double domainSize = L / nDom;

  std::vector< std::vector< int > > borders(nDom);
  std::vector< int > omegas(nDom);
  std::vector< int > sigmas(nDom - 1);

  int p[2];

  p[0] = gmsh::model::geo::addPoint(0., l, 0., lc);
  p[1] = gmsh::model::geo::addPoint(0., 0., 0., lc);

  int line = gmsh::model::geo::addLine(p[0], p[1]);
  gmsh::model::addPhysicalGroup(1, {line}, 1);
  gmsh::model::setPhysicalName(1, 1, "gammaDir");

  for(unsigned int i = 0; i < nDom; ++i) {
    std::vector< std::pair< int, int > > extrudeEntities;
    gmsh::model::geo::extrude({std::make_pair(1, line)}, domainSize, 0., 0., extrudeEntities, {static_cast< int >((domainSize) / lc)}, std::vector< double >(), true);
    line = extrudeEntities[0].second;
    omegas[i] = extrudeEntities[1].second;
    borders[i].push_back(extrudeEntities[2].second);
    borders[i].push_back(extrudeEntities[3].second);
    sigmas[i] = line;
  }

  gmsh::model::addPhysicalGroup(1, {line}, 2);
  gmsh::model::setPhysicalName(1, 2, "gammaInf");

  for(unsigned int i = 0; i < borders.size(); ++i) {
    gmsh::model::addPhysicalGroup(1, borders[i], 100 + i);
    gmsh::model::setPhysicalName(1, 100 + i, "border_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < omegas.size(); ++i) {
    gmsh::model::addPhysicalGroup(2, {omegas[i]}, i + 1);
    gmsh::model::setPhysicalName(2, i + 1, "omega_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < sigmas.size(); ++i) {
    gmsh::model::addPhysicalGroup(1, {sigmas[i]}, 200 + i);
    gmsh::model::setPhysicalName(1, 200 + i, "sigma_" + std::to_string(i) + "_" + std::to_string(i + 1));
  }

  gmsh::model::geo::synchronize();
  gmsh::model::mesh::generate();
  //gmsh::write("m.msh");
}

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  double pi = 3.14159265359;
  int nDom = 4;
  gmshDdm.userDefinedParameter(nDom, "nDom");

  // duct geometry parameters
  double L = 0.5;
  double H = 0.25;
  double lc = sqrt(L * H) / (40.0);
  gmshDdm.userDefinedParameter(lc, "lc");

  // upper wall boundary condition
  std::string wallType = "hard";
  gmshDdm.userDefinedParameter(wallType, "wallType");

  // physical parameters
  double k = 70;
  gmshDdm.userDefinedParameter(k, "k");
  int mode = 3;
  double ky = mode * pi / H;

  // relative Mach number
  double M = 0.8;
  gmshDdm.userDefinedParameter(M, "M");
  double beta = sqrt(1 - M * M);
  double beta2 = beta * beta;

  // transmission conditions
  std::string Transmission = "DtN";
  gmshDdm.userDefinedParameter(Transmission, "transmission");
  // init transmission operators
  ScalarFunction< std::complex< double > > Si, Sj, dS;
  double alpha = 0.; // rotation branch-cut, in [0, -pi]
  gmshDdm.userDefinedParameter(alpha, "alpha");
  if(Transmission != "DtN") {
    msg::info << " use " << Transmission << " as transmission condition with alpha = " << alpha << msg::endl;
  }
  else {
    msg::info << " use analytical DtN as transmission condition " << msg::endl;
  }
  if(wallType == "soft" && Transmission == "Pade") {
    msg::error << "Pade condition not available yet for Dirichlet wall " << msg::endl;
  }

  // numerical parameters
  int FEMorder = 4;
  gmshDdm.userDefinedParameter(FEMorder, "FEMorder");
  std::string gauss = "Gauss10";
  gmshDdm.userDefinedParameter(gauss, "gauss");

  // Build geometry and mesh using gmsh API
  // for the moment all the procs do the meshing
  waveguide(nDom, H, L, lc);

  // Define domain
  Subdomain omega(nDom);
  Subdomain gammaInf(nDom);
  Subdomain gammaDir(nDom);
  Subdomain border(nDom);
  Interface sigma(nDom);

  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    omega(i) = Domain(2, i + 1);
    if(i == (static_cast< unsigned int >(nDom - 1))) {
      gammaInf(i) = Domain(1, 2); // outgoing boundary
    }

    if(i == 0) {
      gammaDir(i) = Domain(1, 1); // ingoing boundary
    }
    border(i) = Domain(1, 100 + i);

    if(i != 0) {
      sigma(i, i - 1) = Domain(1, 200 + i - 1);
    }
    if(i != (static_cast< unsigned int >(nDom - 1))) {
      sigma(i, i + 1) = Domain(1, 200 + i);
    }
  }

  // Define topology
  std::vector< std::vector< unsigned int > > topology(nDom);
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    omega(i) = Domain(2, i + 1);
    if(i != 0) {
      topology[i].push_back(i - 1);
    }
    if(i != (static_cast< unsigned int >(nDom - 1))) {
      topology[i].push_back(i + 1);
    }
  }

  // Create DDM formulation
  std::vector< FieldInterface< std::complex< double > > * > fieldBucket;
  gmshddm::problem::Formulation< std::complex< double > > formulation("HelmholtzFlow", topology);

  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega | gammaDir | gammaInf | border | sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);

  // define analytical solution
  Function< std::complex< double >, Degree::Degree0 > *solution = nullptr;
  if(wallType == "hard") {
    solution = new AnalyticalFunction< helmholtz2D::DuctModeSolution< std::complex< double > > >(k, M, H, 0., 0., mode, 0);
  }
  else if(wallType == "soft") {
    solution = new AnalyticalFunction< helmholtz2D::DuctModeSolution< std::complex< double > > >(k, M, H, 0., 0., mode, 1);
    for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
      u(i).addConstraint(border(i), 0.);
    }
  }

  std::complex< double > im(0., 1.);
  std::complex< double > kx; // propagative wavenumber
  double Argsqrt = k * k - beta2 * ky * ky;
  if(Argsqrt >= 0) {
    kx = (1 / (beta2)) * (-M * k + sqrt(Argsqrt));
    msg::info << " propagative mode " << msg::endl;
  }
  else {
    kx = (1 / (beta2)) * (-M * k - im * sqrt(abs(Argsqrt)));
    msg::info << " evanescent mode " << msg::endl;
  }

  if(k > (beta * ky) && k < ky) {
    msg::info << " - inverse upstream mode ! " << msg::endl;
  }

  float pointsByWl = (2 * pi * FEMorder * (1 + M) / k) / lc;
  msg::info << " - Approximate dofs by wavelength = " << pointsByWl << "" << msg::endl;
  msg::info << " - Mach number = " << M << "" << msg::endl;

  // define output absorbing boundary condition
  std::complex< double > ABC = im * kx;

  // Tell to the formulation that g is field that have to be exchanged between subdomains
  formulation.addInterfaceField(g);

  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    // VOLUME TERMS
    formulation(i).integral(vector< std::complex< double > >(beta2, 0., 0.) * grad(dof(u(i))), vector< std::complex< double > >(1., 0., 0.) * grad(tf(u(i))), omega(i), gauss);
    formulation(i).integral(vector< std::complex< double > >(0., 1., 0.) * grad(dof(u(i))), vector< std::complex< double > >(0., 1., 0.) * grad(tf(u(i))), omega(i), gauss);
    formulation(i).integral(vector< std::complex< double > >(im * k * M, 0., 0.) * grad(dof(u(i))), tf(u(i)), omega(i), gauss);
    formulation(i).integral(- im * k * dof(u(i)), vector< std::complex< double > >(M, 0., 0.) * grad(tf(u(i))), omega(i), gauss);
    formulation(i).integral(-k * k * dof(u(i)), tf(u(i)), omega(i), gauss);
    // SURFACE TERMS
    // artificial boundary condition, use analytical DtN
    formulation(i).integral(beta2 * ABC * dof(u(i)), tf(u(i)), gammaInf(i), gauss);
    // Contribution from the mass boundary matrix
    formulation(i).integral((im * k * M) * dof(u(i)), tf(u(i)), gammaInf(i), gauss);

    // Physical source
    if(wallType == "soft") {
      formulation(i).integral(formulation.physicalSource(-im * kx * beta2 * sin< std::complex< double > >(ky * y< std::complex< double > >())), tf(u(i)), gammaDir(i), gauss);
    }
    else if(wallType == "hard") {
      formulation(i).integral(formulation.physicalSource(-im * kx * beta2 * cos< std::complex< double > >(ky * y< std::complex< double > >())), tf(u(i)), gammaDir(i), gauss);
    }
    // Contribution from the mass boundary matrix
    formulation(i).integral((-im * k * M) * dof(u(i)), tf(u(i)), gammaDir(i), gauss);

    // Artificial source
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i).integral(formulation.artificialSource(-g(j, i)), tf(u(i)), sigma(i, j), gauss);
      // Contribution from the mass boundary matrix
      formulation(i).integral((im * k * M) * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
    }

    // INTERFACE TERMS
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i, j).integral(dof(g(i, j)), tf(g(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(formulation.artificialSource(g(j, i)), tf(g(i, j)), sigma(i, j), gauss);
    }

    // Transmission condition
    if(Transmission == "Taylor0") {
      Si = im * k * (exp(im * alpha / 2.) - M) / beta2;
      Sj = im * k * (exp(im * alpha / 2.) + M) / beta2;
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(beta2 * Si * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
        formulation(i, j).integral(-beta2 * (Si + Sj) * u(i), tf(g(i, j)), sigma(i, j), gauss);
      }
    }
    else if(Transmission == "Taylor2") {
      Si = im * k * (cos(alpha / 2.) - M) / beta2;
      Sj = im * k * (cos(alpha / 2.) + M) / beta2;
      dS = im * exp(-im * alpha / 2.0) / (2.0 * k);
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(beta2 * Si * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
        formulation(i).integral(-beta2 * dS * grad(dof(u(i))), grad(tf(u(i))), sigma(i, j), gauss);
        formulation(i, j).integral(-beta2 * (Si + Sj) * u(i), tf(g(i, j)), sigma(i, j), gauss);
        formulation(i, j).integral(+beta2 * (2. * dS) * grad(u(i)), grad(tf(g(i, j))), sigma(i, j), gauss);
      }
    }
    else if(Transmission == "DtN") { // analytic DtN relative the normal of the artificial boundary, for a given mode
      Si = im * kx;
      Sj = im * (kx + 2 * M * k / (beta2)); // or Sj = im*( (M*k + sqrt(Argsqrt)) / beta2 );
      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(beta2 * Si * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
        formulation(i, j).integral(-beta2 * (Si + Sj) * u(i), tf(g(i, j)), sigma(i, j), gauss);
      }
    }
    else if(Transmission == "Pade") {
      int padeOrder = 4;
      gmshDdm.userDefinedParameter(padeOrder, "padeOrder");
      if(i == 0) {
        msg::info << "Use Pade transmission condition of order " << padeOrder << msg::endl;
      }

      const double Np = 2. * padeOrder + 1.;
      const std::complex< double > exp1 = std::complex< double >(std::cos(alpha), std::sin(alpha));
      const std::complex< double > exp2 = std::complex< double >(std::cos(alpha / 2.), std::sin(alpha / 2.));
      const std::complex< double > coef = 2. / Np;

      for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
        const unsigned int j = topology[i][jj];
        formulation(i).integral(im * k * exp2 * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
        formulation(i).integral(-(im * M * k) * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
        // interface unknowns
        formulation(i, j).integral(-2. * (im * k * exp2) * u(i), tf(g(i, j)), sigma(i, j), gauss);

        // define Pade coefficients
        std::vector< std::complex< double > > c(padeOrder, 0.);
        for(int l = 0; l < padeOrder; ++l) {
          c[l] = std::tan((l + 1) * pi / Np);
          c[l] *= c[l];
        }

        // define the auxiliary fields
        std::vector< Field< std::complex< double >, Form::Form0 > * > phi;
        for(int l = 0; l < padeOrder; ++l) {
          phi.push_back(new Field< std::complex< double >, Form::Form0 >("phi_" + std::to_string(l), sigma(i, j), FunctionSpaceTypeForm0::HierarchicalH1, FEMorder));
          fieldBucket.push_back(phi.back());
        }

        // write the augmented weak form
        for(int l = 0; l < padeOrder; ++l) {
          // boundary integral terms relating the auxiliary fields
          formulation(i).integral(im * k * exp2 * coef * c[l] * dof(*phi[l]), tf(u(i)), sigma(i, j), gauss);
          formulation(i).integral(im * k * exp2 * coef * c[l] * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
          // coupling of the auxiliary equations
          formulation(i).integral(grad(dof(*phi[l])), grad(tf(*phi[l])), sigma(i, j), gauss);
          formulation(i).integral(-(k * k) / beta2 * (exp1 * c[l] + 1.) * dof(*phi[l]), tf(*phi[l]), sigma(i, j), gauss);
          formulation(i).integral(-(k * k) / beta2 * exp1 * (c[l] + 1.) * dof(u(i)), tf(*phi[l]), sigma(i, j), gauss);
        }

        // interfaces unknowns
        for(int l = 0; l < padeOrder; ++l) {
          formulation(i, j).integral(-2. * (im * k * exp2) * coef * c[l] * (*phi[l]), tf(g(i, j)), sigma(i, j), gauss);
          formulation(i, j).integral(-2. * (im * k * exp2) * coef * c[l] * u(i), tf(g(i, j)), sigma(i, j), gauss);
        }
      }
    }
    else {
      msg::error << "Transmission condition not available !" << msg::endl;
      exit(0);
    }
  }

  // Solve the DDM formulation
  formulation.pre();
  formulation.solve("jacobi", 1e-6);
  //formulation.solve("gmres");

  double num = 0., den = 0.;
  double local_err; // L2 local error
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    if(gmshddm::mpi::isItMySubdomain(i)) {
      save(+u(i), omega(i), "u_" + std::to_string(i));
      save(*solution, omega(i), "uex_" + std::to_string(i));
      save(*solution - u(i), omega(i), "err_" + std::to_string(i));

      std::complex< double > denLocal = integrate(pow(abs(*solution), 2), omega(i), gauss);
      std::complex< double > numLocal = integrate(pow(abs(*solution - u(i)), 2), omega(i), gauss);
      num += numLocal.real();
      den += denLocal.real();
      local_err = sqrt(num / den);
    }
  }

  // Compute global err - requires "mpi.h"
  /* double L2_err;
  MPI_Reduce(&local_err, &L2_err, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  
  msg::info << "Local L2 error = " << local_err << " on rank " << MPI_Rank << msg::endl;
  if ( MPI_Rank == 0 ) {
    msg::info << "Global L2 error = " << L2_err << msg::endl;
  }
  */

  msg::info << "Local L2 error = " << local_err << " on rank " << gmshddm::mpi::getMPIRank() << msg::endl;

  for(unsigned int i = 0; i < fieldBucket.size(); ++i) {
    delete fieldBucket[i];
  }

  return 0;
}
