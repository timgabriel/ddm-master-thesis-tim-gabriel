#include "gmsh.h"
#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshfem/Message.h>
#include "mesh.h"
#include "flowPmlFunctions.h"

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  const double pi = 3.14159265358979323846264338327950288;
  std::complex< double > im(0., 1.);
  
  // FEM parameters
  int FEMorder = 4;
  std::string gauss = "Gauss" + std::to_string(2 * FEMorder + 1);

  // Non-reflecting boundary
  std::string abcType = "pml"; // available choices: sommerfeld, pml
  gmshDdm.userDefinedParameter(abcType, "abcType");

  // ddm parameters
  std::string Transmission = "Taylor2";
  gmshDdm.userDefinedParameter(Transmission, "TC");
  double alpha = -pi / 2.;
  gmshDdm.userDefinedParameter(alpha, "alpha");
  
  // meshing parameters
  std::string mesh = "";
  gmshDdm.userDefinedParameter(mesh, "mesh");

  unsigned int nDom = 8;
  gmshDdm.userDefinedParameter(nDom, "nDom");

  double Sigma = 1.0018;
  gmshDdm.userDefinedParameter(Sigma, "Sigma");
  int ElemOrder = 2;
  int Npml = 2;
  gmshDdm.userDefinedParameter(Npml, "Npml");
  int RefSource = 18;

  std::pair<double, double> xlim;
  std::pair<double, double> ylim;
  double lc;
  gmsh::option::setNumber("Mesh.Binary", 1);
  if(mesh.empty()) {
    meshJet(Sigma, RefSource, Npml, ElemOrder, xlim, ylim, lc);
    gmsh::option::setNumber("Mesh.ElementOrder", FEMorder);
    gmsh::option::setNumber("Mesh.SecondOrderLinear", 1);
    // save all, as partitioning will create some entities without physical groups
    gmsh::option::setNumber("Mesh.SaveAll", 1);
    gmsh::model::mesh::generate(2);
    gmsh::model::mesh::partition(nDom);
    // save partitioned mesh in single file for mono-process runs
    gmsh::option::setNumber("Mesh.PartitionSplitMeshFiles", 0);
    gmsh::write("hotjet.msh");
    // save partitioned mesh in separate files for distributed runs
    gmsh::option::setNumber("Mesh.PartitionSplitMeshFiles", 1);
    gmsh::write("hotjet.msh");
    mesh = "hotjet";
  }
  
  gmsh::model::add(mesh);
  gmsh::merge(mesh + ".msh");
  if(gmsh::model::getNumberOfPartitions() != nDom)
    msg::error << "Wrong number of partitions in mesh file " << gmsh::model::getNumberOfPartitions() << " vs. " << nDom << msg::endl;

  Domain omegaPhy = gmshfem::domain::Domain("omega");
  Domain omegaPml = gmshfem::domain::Domain("omega_pml");
  Domain gammaExtMono = gmshfem::domain::Domain("gamma");

  Subdomain omega_phy = Subdomain::buildSubdomainOf(omegaPhy);
  Subdomain omega_pml = Subdomain::buildSubdomainOf(omegaPml);
  Subdomain gamma_ext = Subdomain::buildSubdomainOf(gammaExtMono);

  std::vector< std::vector< unsigned int > > topology;
  Interface sigma = Interface::buildInterface(topology);

  msg::info << "mesh size: " << lc << msg::endl;
  if (abcType == "pml") {
    msg::info << "x-pml-limit-left: " << xlim.first << msg::endl;
    msg::info << "x-pml-limit-right: " << xlim.second << msg::endl;
    msg::info << "y-pml-limit-down: " << ylim.first << msg::endl;
    msg::info << "y-pml-limit-up: " << ylim.second << msg::endl;
  }

  // mean flow specification
  double w = 200*pi;
  double SigmaS = 0.136*Sigma;
  // define source
  ScalarFunction< std::complex< double > > source = SigmaS*exp(- r2d<std::complex< double > >() / (2.*SigmaS*SigmaS) );
  // define mean flow
  double Tj = 600; // Jet temperature [Kelvin]
  double gamma = 1.4;
  double R = 287.;
  double Mj = 0.756;
  double cj = sqrt(gamma*R*Tj);
  double uj = Mj*cj;
  ScalarFunction< std::complex< double > > u0y = uj*exp(- y<std::complex< double > >()*y<std::complex< double > >() / (2.*Sigma*Sigma) );
  
  double Tinf = 300; // Reference Temperature [Kelvin]
  double p0 = 103330;
  double rhoj = gamma*p0/(cj*cj);
  ScalarFunction< std::complex< double > > rho0_inv = Tinf/Tj - (Tinf/Tj - 1.) * (u0y/uj) + ((gamma-1)/2) * (Mj*Mj) * (u0y/uj) * (1-u0y/uj);
  ScalarFunction< std::complex< double > > rho0 = rhoj/rho0_inv;
  ScalarFunction< std::complex< double > > c0 = sqrt(gamma*p0/rho0);
   
  ScalarFunction< std::complex< double > > Mx = u0y/c0;
  ScalarFunction< std::complex< double > > My = 0.;
  ScalarFunction< std::complex< double > > k0 = w/c0;
  VectorFunction< std::complex< double > > MM = vector< std::complex< double > >(Mx, My, 0.);
  ScalarFunction< std::complex<double> > Mn = MM*normal< std::complex< double > >();
  ScalarFunction< std::complex<double> > Mt = MM*tangent< std::complex< double > >();
  ScalarFunction< std::complex< double > > beta = sqrt(1-Mx*Mx-My*My);

  // ddm problem
  gmshddm::problem::Formulation< std::complex< double > > formulation("Pierce", topology);
  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega_phy | omega_pml | gamma_ext, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  formulation.addInterfaceField(g);

  // define transmission conditions 
  ScalarFunction< std::complex< double > > Si=0., Sj=0., Sti=0., Stj=0., dS=0.;
  if ( Transmission == "Taylor0" ) {
    //if (!getMPIRank())
      msg::info << " - Use Sommerfeld transmission condition with rotation angle = " << alpha << "" << msg::endl;
    Si = im*k0*(exp(im*alpha/2.)-Mn); // if alpha=0, we get beta*beta*Si = i*k*(1-Mn)
    Sj = im*k0*(exp(im*alpha/2.)+Mn);
    Sti = -Mn*Mt;
    Stj = +Mn*Mt;
    dS = 0.;
  }
  else if ( Transmission == "Taylor2" ) {
    //if (!getMPIRank())
      msg::info << " - Use second order transmission condition with rotation angle = " << alpha << "" << msg::endl;
    Si = im*k0*(cos(alpha/2.)-Mn) ; // im*k0*cos(alpha/2.)
    Sj = im*k0*(cos(alpha/2.)+Mn) ;
    Sti = ( exp(-im*alpha/2.) - Mn ) * Mt ; // exp(-im*alpha/2.) * Mt
    Stj = ( exp(-im*alpha/2.) + Mn ) * Mt ;
    dS = im*(beta*beta)*exp(-im*alpha/2.) / (2.0*k0);
  }

  // ddm variational formulation
  TensorFunction< std::complex< double > > TFlow = tensor< std::complex< double > > (1-Mx*Mx, -Mx*My, 0., -Mx*My, 1-My*My, 0., 0., 0., 0.);
  for(unsigned int i = 0; i < nDom; ++i) {
    if (abcType == "pml") {
      TensorFunction< std::complex< double > > Linv;
      Linv = getInverseLorentzTensor(Mx, My, beta);

      ScalarFunction< std::complex< double > > detJ;
      TensorFunction< std::complex< double > > JPml_invT;
      JPml_invT = getInversePMLJacobian(k0, xlim, ylim, Npml*lc, beta, detJ);

      VectorFunction< std::complex< double > > JPml_invT_M = JPml_invT * MM;
      TensorFunction< std::complex< double > > JPml_Linv = JPml_invT * Linv;
      // convected Helmholz weak form with PML - Lorentz formulation
      formulation(i).integral(+(1./rho0) *  detJ * JPml_Linv*grad(dof(u(i))) , JPml_Linv*grad(tf(u(i))) , omega_pml(i), gauss, term::ProductType::Scalar);
      formulation(i).integral(+(1./rho0) * k0*k0/(beta*beta) * detJ * JPml_invT_M * dof(u(i)) , JPml_invT_M * tf(u(i)), omega_pml(i), gauss, term::ProductType::Scalar);
      formulation(i).integral(+(1./rho0) * im*k0/beta* detJ * JPml_Linv * grad(dof(u(i))), JPml_invT_M * tf(u(i)), omega_pml(i), gauss, term::ProductType::Scalar);
      formulation(i).integral(-(1./rho0) * im*k0/beta* detJ * JPml_invT_M * dof(u(i)), JPml_Linv * grad(tf(u(i))), omega_pml(i), gauss, term::ProductType::Scalar);
      formulation(i).integral(-(1./rho0) * k0*k0/(beta*beta) * detJ * dof(u(i)), tf(u(i)), omega_pml(i), gauss);
    }
    else {
      formulation(i).integral((1./rho0) * im * k0 * dof(u(i)), tf(u(i)), gamma_ext(i), gauss);
    }
    // convected Helmholz weak form
    formulation(i).integral( (1./rho0) * TFlow * grad(dof(u(i))), grad(tf(u(i))), omega_phy(i), gauss);
    formulation(i).integral(-(1./rho0) * im * k0 * dof(u(i)), MM * grad(tf(u(i))), omega_phy(i), gauss);
    formulation(i).integral(+(1./rho0) * im * k0 * MM * grad(dof(u(i))), tf(u(i)), omega_phy(i), gauss);
    formulation(i).integral(-(1./rho0) * k0 * k0 * dof(u(i)), tf(u(i)), omega_phy(i), gauss);
    formulation.physicalSourceTerm( formulation(i).integral(source, tf(u(i)), omega_phy(i), gauss) );

    // Artificial source
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      if(!sigma(i,j).isEmpty()) {
        formulation.artificialSourceTerm(formulation(i).integral(-g(j,i), tf(u(i)), sigma(i,j), gauss));
        // Contribution from the boundary terms
        formulation(i).integral((1./rho0) * (im*k0*Mn) * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        formulation(i).integral((1./rho0) * Mn * Mt * tangent< std::complex< double > >() * grad( dof(u(i)) ), dof(u(i)), sigma(i,j), gauss);
        // Transmission conditions terms
        formulation(i).integral((1./rho0) * Si * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        formulation(i).integral((1./rho0) * Sti * tangent< std::complex< double > >() * grad( dof(u(i)) ), dof(u(i)), sigma(i,j), gauss);
        formulation(i).integral(-(1./rho0) * dS * grad( dof(u(i)) ), grad( tf(u(i)) ), sigma(i,j), gauss);
      }
    }
    // Interface terms
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      if(!sigma(i,j).isEmpty()) {
        formulation(i,j).integral(dof(g(i,j)), tf(g(i,j)), sigma(i,j), gauss);
        formulation.artificialSourceTerm(formulation(i,j).integral(g(j,i), tf(g(i,j)), sigma(i,j), gauss));
        // Transmission conditions terms
        formulation(i,j).integral(-(1./rho0) * (Si+Sj) * u(i) , tf(g(i,j)), sigma(i,j), gauss);
        formulation(i,j).integral(-(1./rho0) * (Sti+Stj) * tangent< std::complex< double > >() * grad( u(i) ) , tf(g(i,j)), sigma(i,j), gauss);
        formulation(i,j).integral( (2./rho0) * dS * grad( u(i) ), grad( tf(g(i,j)) ), sigma(i,j), gauss);
      }
    }
  }

  // solve DDM problem
  std::string solver = "gmres";
  gmshDdm.userDefinedParameter(solver, "solver");
  double tol = 1e-6;
  gmshDdm.userDefinedParameter(tol, "tol");
  int maxIt = 200;
  gmshDdm.userDefinedParameter(maxIt, "maxIt");

  formulation.pre();
  formulation.solve(solver, tol, maxIt, true);

  // save solution
  for(unsigned int i = 0; i < nDom; ++i) {
    save(+u(i), omega_phy(i) | omega_pml(i), "u_" + std::to_string(i));
  }

  bool ComputeMono = true;
  gmshDdm.userDefinedParameter(ComputeMono, "ComputeMono");
  if (ComputeMono) {
    gmshfem::field::Field< std::complex< double >, Form::Form0 > uMono("uMono", omegaPml | omegaPhy | gammaExtMono, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
    gmshfem::problem::Formulation< std::complex< double > > monodomain("Pierce");
    if (abcType == "pml") {
      TensorFunction< std::complex< double > > Linv;
      Linv = getInverseLorentzTensor(Mx, My, beta);

      ScalarFunction< std::complex< double > > detJ;
      TensorFunction< std::complex< double > > JPml_invT;
      JPml_invT = getInversePMLJacobian(k0, xlim, ylim, Npml*lc, beta, detJ);

      VectorFunction< std::complex< double > > JPml_invT_M = JPml_invT * MM;
      TensorFunction< std::complex< double > > JPml_Linv = JPml_invT * Linv;
      // convected Helmholz weak form with PML - Lorentz formulation
      monodomain.integral(+(1./rho0) *  detJ * JPml_Linv*grad(dof(uMono)) , JPml_Linv*grad(tf(uMono)) , omegaPml, gauss, term::ProductType::Scalar);
      monodomain.integral(+(1./rho0) * k0*k0/(beta*beta) * detJ * JPml_invT_M * dof(uMono) , JPml_invT_M * tf(uMono), omegaPml, gauss, term::ProductType::Scalar);
      monodomain.integral(+(1./rho0) * im*k0/beta* detJ * JPml_Linv * grad(dof(uMono)), JPml_invT_M * tf(uMono), omegaPml, gauss, term::ProductType::Scalar);
      monodomain.integral(-(1./rho0) * im*k0/beta* detJ * JPml_invT_M * dof(uMono), JPml_Linv * grad(tf(uMono)), omegaPml, gauss, term::ProductType::Scalar);
      monodomain.integral(-(1./rho0) * k0*k0/(beta*beta) * detJ * dof(uMono), tf(uMono), omegaPml, gauss);
    }
    else {
      monodomain.integral((1./rho0) * im * k0 * dof(uMono), tf(uMono), gammaExtMono, gauss);
    }
    // convected Helmholz weak form
    monodomain.integral( (1./rho0) * TFlow * grad(dof(uMono)), grad(tf(uMono)), omegaPhy, gauss);
    monodomain.integral(-(1./rho0) * im * k0 * dof(uMono), MM * grad(tf(uMono)), omegaPhy, gauss);
    monodomain.integral(+(1./rho0) * im * k0 * MM * grad(dof(uMono)), tf(uMono), omegaPhy, gauss);
    monodomain.integral(-(1./rho0) * k0 * k0 * dof(uMono), tf(uMono), omegaPhy, gauss);
    monodomain.integral(source, tf(uMono), omegaPhy, gauss);

    monodomain.pre();
    monodomain.assemble();
    msg::info << "Memory usage " << monodomain.getEstimatedFactorizationMemoryUsage() << msg::endl;
    monodomain.solve();

    // Monodomain L2 error
    double local_err;
    double global_err=0.;
    for(unsigned int i = 0; i < nDom; ++i) {
      std::complex< double > denLocal = integrate(pow(abs( uMono ), 2), omega_phy(i) | omega_pml(i), gauss);
      std::complex< double > numLocal = integrate(pow(abs(uMono - u(i)), 2), omega_phy(i) | omega_pml(i), gauss);
      local_err = sqrt(numLocal.real() / denLocal.real());
      //gmshfem::msg::info << "ddm-monodomain L2 error = " << local_err << " on subdomain " << i << msg::endl;
      global_err += local_err;
    }
    gmshfem::msg::info << "ddm-monodomain global error = " << global_err/nDom  << msg::endl;

  } // end Monodomain

  return 0;
}