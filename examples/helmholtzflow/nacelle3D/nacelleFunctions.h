void Nacelle(const double h_near=0.4, const double h_far=0.4, const double D=4., const double R=3., const double L_duct=-0.4, const double L_pml=0.6, const double L_pml_a=0.4, const int nDom=6);
void getInputMode(int m, int n, double w, double M, std::complex<double> &kzmn, double &krmn, gmshfem::function::ScalarFunction< std::complex<double> > &psi,
  gmshfem::function::ScalarFunction< std::complex<double> > &dy_psi, gmshfem::function::ScalarFunction< std::complex<double> > &dz_psi, int dim, double Rout = 1.2, double Rin = 0.3586206896556000);
void GetCylindricalPMLFlow(double L_duct, double Lpml, double Lpml_a, double k0_inf, double k0f, double M_inf, double Mf, double R, double D,
  gmshfem::function::VectorFunction< double > MM, gmshfem::function::TensorFunction< std::complex< double > > &J_PML_Linv, gmshfem::function::TensorFunction< std::complex< double > > &J_PML_LinvA,
  gmshfem::function::VectorFunction< std::complex< double > > &J_PML_inv_T_M, gmshfem::function::VectorFunction< std::complex< double > > &J_PML_inv_T_MA,
  gmshfem::function::ScalarFunction< std::complex< double > > &detJpml, gmshfem::function::ScalarFunction< std::complex< double > > &detJpmlA);
void InterpolateMeanFlow(int nDom, gmshfem::domain::Domain omegaFlow, gmshfem::field::Field< double, gmshfem::field::Form::Form0 > &rho0, gmshfem::field::Field< double, gmshfem::field::Form::Form0 > &c0,
  gmshfem::field::Field< double, gmshfem::field::Form::Form0 > &Vx, gmshfem::field::Field< double, gmshfem::field::Form::Form0 > &Vy, gmshfem::field::Field< double, gmshfem::field::Form::Form0 > &Vz);
