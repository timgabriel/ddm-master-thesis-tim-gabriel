# Flow acoustics problem - radiation from a turbofan engine intake (draft)

Dependency(ies):
* GmshDDM
* GmshFEM
* Gmsh

## Problem description

The routine solves a three-dimensional convected Helmholtz problem by a spatially varying mean flow. It represents the acoustic radiation from a generic turbofan engine intake.
The mean flow is an external input from the computation, given as a CGNS file and read by Gmsh in a pre-processing step. The different mean flow configurations (Approach, Sideline, etc.) are a specific flight configuration such as landing or take-off.

The specificities of the boundary value problem are:

* an annular acoustic mode of azimuthal and radial order $`(m,n)`$ is imposed at the fan face. It represents the tonal noise generated from the blades of the rotating fan at a fixed frequency, called the Blade Passing Frequency (BPF). We consider a fan of 24 blades, such that the relevant computations include all propagative modes of the form $`(p*24,0)`$ for the frequency $`p*BPF`$.
For the Sideline mean flow configuration, the first BPF is 1300 Hz and can be associated to the mode (24,0). The second BPF is 2600 Hz and can be associated to the modes (24,0) or (48,0).
The modes are set through the parameters `-m [x]` and `-n [x]`, and the frequency is set relatively to the BPF through `-bpf_ratio [x]`.

* an acoustic treatment, called the acoustic lining or liner, may be imposed on the nacelle. It is described by an acoustic impedance in the frequency domain $`Z(\omega)`$, and models a single degree-of-freedom (SDOF) perforated plate. It is set by the boolean parameter `-SetLiner [x]`.

* a radiation condition is required to truncate the computational domain. Here we use a PML written in cylindrical coordinates $`(x, r, \theta)`$. We also use a PML in the negative x direction to impose the input mode. It is called an active PML, and allows to absorb back-scattered modes.

* the other walls are perfectly reflecting, that is we use a homogeneous Neumann boundary condition.

For a parallel domain decomposition run, a partitioned mesh with `-nDom [x]` subdomains is first generated thanks to the METIS partitioner. The running command is then
```
  mpirun -np [x] ./example -mesh Nacelle -nDom [x]
```
To run the first BPF, just use
```
  mpirun -np [x] ./example -mesh Nacelle -nDom [x] -m 24 -bpf_ratio 1
```
DDM transmission conditions are set up by default to be rotated second order Taylor conditions.

## Installation

* Gmsh must be compiled with OpenCascade and cgns
* GmshFEM must be compiled with the Complex Bessel library
* MPI must be enabled
