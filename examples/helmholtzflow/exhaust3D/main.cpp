#include "gmsh.h"
#include <gmshddm/Formulation.h>
#include <gmshddm/MPIInterface.h>
#include <gmshddm/GmshDdm.h>
#include <gmshfem/Message.h>
#include "mesh3D.h"
#include "flowPmlfunctions.h"

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;
using namespace gmshddm::mpi;

using namespace gmshfem;
using namespace gmshfem::common;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  const double pi = 3.14159265358979323846264338327950288;
  std::complex< double > im(0., 1.);
  
  // FEM parameters
  int FEMorder = 2;
  std::string gauss = "Gauss" + std::to_string(2 * FEMorder + 1);

  // ddm parameters
  std::string Transmission = "Taylor2";
  gmshDdm.userDefinedParameter(Transmission, "TC");
  double alpha = -pi / 2.;
  gmshDdm.userDefinedParameter(alpha, "alpha");
  
  // simulation parameters
  double freq = 7497/2;

  // mesh parameters
  double Ly=0.3;
  double lc=0.02;
  double lcI=lc/5.;

  // Mean flow parameters on bypass and core duct surfaces (assumed to be constant)
  msg::info << " - Running plane wave case in core duct " << msg::endl;
  double MCore = 0.1;
  double c0Core = 530;
  double rho0Core = 0.48;
  
  double MBypass = 0.35;
  double c0Bypass = 349.6;
  double rho0Bypass = 1.2209;
  
  double xCorePML = -0.03528577462; // PML interface x-location following the geometry
  double xBypassPML = -0.3483471572; // PML interface x-location following the geometry
  double xLeftExtPML = -0.2623840272; // exterior PML interface
  double xRightExtPML = 0.5; // exterior PML interface
  double deltaPml = Ly/10.; // PMLs width
  
  // meshing parameters
  std::string mesh = "";
  gmshDdm.userDefinedParameter(mesh, "mesh");

  unsigned int nDom = 12;
  gmshDdm.userDefinedParameter(nDom, "nDom");
  
  // reading mean flow .pos axisymetric data
  std::string FlowDataPath = "../../exhaust/meanflow/"; // path to the flow files
  gmsh::open(FlowDataPath + "density_bl.pos");
  gmsh::open(FlowDataPath + "SpeedOfSound_bl.pos");
  gmsh::open(FlowDataPath + "ux_bl.pos");
  gmsh::open(FlowDataPath + "ur_bl.pos");

  gmsh::option::setNumber("Mesh.Binary", 1);
  if(mesh.empty()) {
    if(!getMPIRank()) {
      meshExhaust3D(Ly, lc, lcI, FEMorder, true, true);
      gmsh::option::setNumber("Mesh.ElementOrder", FEMorder);
      gmsh::option::setNumber("Mesh.SecondOrderLinear", 1);
      // save all, as partitioning will create some entities without physical groups
      gmsh::option::setNumber("Mesh.SaveAll", 1);
      gmsh::model::mesh::generate(3);
      gmsh::model::mesh::partition(nDom);
      // save partitioned mesh in single file for mono-process runs
      gmsh::option::setNumber("Mesh.PartitionSplitMeshFiles", 0);
      gmsh::write("turnex3D.msh");
      // save partitioned mesh in separate files for distributed runs
      gmsh::option::setNumber("Mesh.PartitionSplitMeshFiles", 1);
      gmsh::write("turnex3D.msh");
    }
    mesh = "turnex3D";
    exit(1);
  }

  barrier();
  gmsh::model::add(mesh);

  // read partitioned mesh
  if(getMPISize() == 1) {
    gmsh::merge(mesh + ".msh");
  }
  else {
    for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
      if (mpi::isItMySubdomain(i)) {
        gmsh::merge(mesh + "_" + std::to_string(i + 1) + ".msh");
      }
    }
  }
  if(gmsh::model::getNumberOfPartitions() != static_cast<int>(nDom))
    msg::error << "Wrong number of partitions in mesh file " << gmsh::model::getNumberOfPartitions() << " vs. " << nDom << msg::endl;
  
  Domain omegaPhy = gmshfem::domain::Domain("omega");
  Domain omegaPml = gmshfem::domain::Domain("omega_pml");
  Domain omegaPmlBypass = gmshfem::domain::Domain("omega_pml_bypass");  
  Domain omegaPmlCore = gmshfem::domain::Domain("omega_pml_core"); // active PML

  Subdomain omega_phy = Subdomain::buildSubdomainOf(omegaPhy);
  Subdomain omega_pml = Subdomain::buildSubdomainOf(omegaPml);
  Subdomain omega_pml_bypass = Subdomain::buildSubdomainOf(omegaPmlBypass);
  Subdomain omega_pml_core = Subdomain::buildSubdomainOf(omegaPmlCore);

  //Domain bypassfaceMono = gmshfem::domain::Domain("bypass_surf");
  Domain corefaceMono = gmshfem::domain::Domain("core_surf");
  //Domain gammaextMono = gmshfem::domain::Domain("gamma_ext");

  //Subdomain bypassface = Subdomain::buildSubdomainOf(bypassfaceMono);
  Subdomain coreface = Subdomain::buildSubdomainOf(corefaceMono);
  //Subdomain gammaext = Subdomain::buildSubdomainOf(gammaextMono);

  std::vector< std::vector< unsigned int > > topology;
  Interface sigma = Interface::buildInterface(topology);
  
  // mean flow domain for interpolation
  Domain omegaFlow;
  for(unsigned int i = 0; i < nDom; ++i) {
    if (mpi::isItMySubdomain(i)) {
      omegaFlow |= omega_phy(i) | omega_pml(i);// | omega_pml_bypass(i) | omega_pml_core(i);
    }
  }

  // linear mean flow data interpolation on the mesh
  Field< double, Form::Form0 > rho0d("rho0d", omegaFlow, FunctionSpaceTypeForm0::Lagrange);
  Field< double, Form::Form0 > c0("c0", omegaFlow, FunctionSpaceTypeForm0::Lagrange);
  Field< double, Form::Form0 > Vx("Vx", omegaFlow, FunctionSpaceTypeForm0::Lagrange);
  Field< double, Form::Form0 > Vy("Vy", omegaFlow, FunctionSpaceTypeForm0::Lagrange);
  Field< double, Form::Form0 > Vz("Vz", omegaFlow, FunctionSpaceTypeForm0::Lagrange);
  InterpolateMeanFlow(nDom, omegaFlow, rho0d, c0, Vx, Vy, Vz);

  ScalarFunction<double> Mx = Vx/c0;
  ScalarFunction<double> My = Vy/c0;
  ScalarFunction<double> Mz = Vz/c0;

  // check mean flow data
  bool saveFlow = true;
  if (saveFlow) {
    for(unsigned int i = 0; i < nDom; ++i) {
      if (mpi::isItMySubdomain(i)) {
        //save(c0, omegaFlow, "c0_"+ std::to_string(i + 1), "pos");
        save(Mx, omegaFlow, "Mx_"+ std::to_string(i + 1), "pos");
      }
    }
  }
  //gmsh::plugin::run("Smooth");
  
  // Mach field complex valued functions for acoustic formulation
  VectorFunction< double > MM = vector< double > (Mx,My,Mz);
  VectorFunction< double > VV = vector< double > (Vx,Vy,Vz);
  ScalarFunction< double > beta = sqrt(1 - pow(norm( vector< double > (Mx,My,Mz) ), 2) );
  ScalarFunction< std::complex<double> > Mn = complex( MM*normal< double >() );
  ScalarFunction< std::complex<double> > Mt = complex( MM*tangent< double >() );
  TensorFunction< double > TFlow = tensor< double > (1-Mx*Mx, -Mx*My, -Mx*Mz, -Mx*My, 1-My*My, -My*Mz, -Mx*Mz, -My*Mz, 1-Mz*Mz);

  ScalarFunction< std::complex< double > > k0 = 2*pi*freq/complex(c0);
  ScalarFunction< std::complex< double > > rho0inv = 1./complex(rho0d);

  float pointsByWlFar = 340.0*FEMorder / (freq*lc);
  float pointsByWlBypass = c0Bypass*(1-abs(MBypass))*FEMorder / (freq*lc);
  float pointsByWlCore = c0Core*(1-abs(MCore))*FEMorder / (freq*lc);
  if (!getMPIRank()) {
    msg::info << " - approx. dof density by wavelength (far field) = " << pointsByWlFar << "" << msg::endl;
    msg::info << " - approx. dof density by wavelength (bypass) = " << pointsByWlBypass << "" << msg::endl;
    msg::info << " - approx. dof density by wavelength (core) = " << pointsByWlCore << "" << msg::endl;
    if(pointsByWlFar < 6 || pointsByWlBypass < 6 || pointsByWlCore < 6) {
      msg::warning << " - less than 6 dofs per wavelength !" << msg::endl;
    }
  }
  
  // exterior PML parameters
  TensorFunction< double > Linv;
  Linv = getInverseLorentzTensor(Mx, My, Mz, beta);
  ScalarFunction< std::complex< double > > detJ;
  TensorFunction< std::complex< double > > JPml_invT;
  JPml_invT = getInversePMLJacobian(k0, std::make_pair(xLeftExtPML, xRightExtPML), std::make_pair(0,Ly), deltaPml, beta, detJ);
  VectorFunction< std::complex< double > > JPml_invT_M = JPml_invT * complex(MM);
  TensorFunction< std::complex< double > > JPml_Linv = JPml_invT * complex(Linv);
  
  double k0Bypass = 2*pi*freq/c0Bypass;
  // bypass PML parameters - use constant mean flow value in the PML
  TensorFunction< std::complex< double > > LinvBypass;
  LinvBypass = getInverseLorentzTensor(MBypass, 0., 0., sqrt(1-MBypass*MBypass));
  ScalarFunction< std::complex< double > > detJBypass;
  TensorFunction< std::complex< double > > JPml_invTBypass;
  JPml_invTBypass = getInversePMLJacobian(k0Bypass, std::make_pair(xBypassPML,0), std::make_pair(0,Ly), deltaPml, sqrt(1-MBypass*MBypass), detJBypass);
  VectorFunction< std::complex< double > > JPml_invT_MBypass = JPml_invTBypass * vector< std::complex< double > >(MBypass, 0., 0.);
  TensorFunction< std::complex< double > > JPml_LinvBypass = JPml_invTBypass * LinvBypass;
  
  double k0Core = 2*pi*freq/c0Core;
  // core active PML parameters - use constant mean flow value in the PML
  TensorFunction< std::complex< double > > LinvCore;
  LinvCore = getInverseLorentzTensor(MCore, 0., 0., sqrt(1-MCore*MCore));
  ScalarFunction< std::complex< double > > detJCore;
  TensorFunction< std::complex< double > > JPml_invTCore;
  JPml_invTCore = getInversePMLJacobian(k0Core, std::make_pair(xCorePML,0), std::make_pair(0,Ly), deltaPml, sqrt(1-MCore*MCore), detJCore);
  VectorFunction< std::complex< double > > JPml_invT_MCore = JPml_invTCore * vector< std::complex< double > >(MCore, 0., 0.);
  TensorFunction< std::complex< double > > JPml_LinvCore = JPml_invTCore * LinvCore;
  
  // source - simple plane wave e^-(i*k_x*x)
  double kxCore = k0Core/(1+MCore);
  ScalarFunction< std::complex<double> > psi = exp(-im*kxCore*x<std::complex<double>>());
  VectorFunction< std::complex<double> > grad_psi = vector< std::complex<double> > (-im*kxCore*psi, 0., 0.);

  gmshddm::problem::Formulation< std::complex< double > > formulation("Pierce", topology);
  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega_phy | omega_pml | omega_pml_bypass | coreface | omega_pml_core , FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  formulation.addInterfaceField(g);
  
  // DDM transmission conditions
  ScalarFunction< std::complex< double > > Si=0., Sj=0., Sti=0., Stj=0., dS=0.;
  if ( Transmission == "Taylor0" ) {
    if (!getMPIRank())
      msg::info << " - Use Sommerfeld transmission condition with rotation angle = " << alpha << "" << msg::endl;
    Si = im*k0*exp(im*alpha/2.); // if alpha=0, we get beta*beta*Si = i*k*(1-Mn)
    Sj = im*k0*exp(im*alpha/2.);
    Sti = 0.;
    Stj = 0.;
    dS = 0.;
  }
  else if ( Transmission == "Taylor2" ) {
    if (!getMPIRank())
      msg::info << " - Use second order transmission condition with rotation angle = " << alpha << "" << msg::endl;
    Si = im*k0*cos(alpha/2.) ;
    Sj = im*k0*cos(alpha/2.) ;
    Sti = exp(-im*alpha/2.) * Mt ; 
    Stj = exp(-im*alpha/2.) * Mt ;
    dS = im*(complex(beta)*complex(beta))*exp(-im*alpha/2.) / (2.0*k0);
  }

  // DDM variational formulation - Pierce operator
  for(unsigned int i = 0; i < nDom; ++i) {
    // weak form exterior PML - Lorentz formulation
    formulation(i).integral(rho0inv *  detJ * JPml_Linv*grad(dof(u(i))) , JPml_Linv*grad(tf(u(i))) , omega_pml(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(rho0inv * k0*k0/(complex(beta*beta)) * detJ * JPml_invT_M * dof(u(i)) , JPml_invT_M * tf(u(i)), omega_pml(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(rho0inv * im*k0/complex(beta)* detJ * JPml_Linv * grad(dof(u(i))), JPml_invT_M * tf(u(i)), omega_pml(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(-rho0inv * im*k0/complex(beta)* detJ * JPml_invT_M * dof(u(i)), JPml_Linv * grad(tf(u(i))), omega_pml(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(-rho0inv * k0*k0/(complex(beta*beta)) * detJ * dof(u(i)), tf(u(i)), omega_pml(i), gauss);
    
    // weak form Bypass PML - Lorentz formulation
    formulation(i).integral((1./rho0Bypass) *  detJBypass * JPml_LinvBypass*grad(dof(u(i))) , JPml_LinvBypass*grad(tf(u(i))) , omega_pml_bypass(i), gauss, term::ProductType::Scalar);
    formulation(i).integral((1./rho0Bypass) * k0Bypass*k0Bypass/(1-MBypass*MBypass) * detJBypass * JPml_invT_MBypass * dof(u(i)) , JPml_invT_MBypass * tf(u(i)), omega_pml_bypass(i), gauss, term::ProductType::Scalar);
    formulation(i).integral((1./rho0Bypass) * im*k0Bypass/sqrt(1-MBypass*MBypass)* detJBypass * JPml_LinvBypass * grad(dof(u(i))), JPml_invT_MBypass * tf(u(i)), omega_pml_bypass(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(-(1./rho0Bypass) * im*k0Bypass/sqrt(1-MBypass*MBypass)* detJBypass * JPml_invT_MBypass * dof(u(i)), JPml_LinvBypass * grad(tf(u(i))), omega_pml_bypass(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(-(1./rho0Bypass) * k0Bypass*k0Bypass/(1-MBypass*MBypass) * detJBypass * dof(u(i)), tf(u(i)), omega_pml_bypass(i), gauss);

    // weak form physical domain
    formulation(i).integral( rho0inv * complex(TFlow) * grad(dof(u(i))), grad(tf(u(i))), omega_phy(i), gauss);
    formulation(i).integral(-rho0inv * im * k0 * dof(u(i)), complex(MM) * grad(tf(u(i))), omega_phy(i), gauss);
    formulation(i).integral(+rho0inv * im * k0 * complex(MM) * grad(dof(u(i))), tf(u(i)), omega_phy(i), gauss);
    formulation(i).integral(-rho0inv * k0 * k0 * dof(u(i)), tf(u(i)), omega_phy(i), gauss);
    
    // Input plane wave core - classical way
    // formulation.physicalSourceTerm(formulation(i).integral(-rho0inv * im * (1.) * (1-MCore*MCore), tf(u(i)), coreface(i), gauss));
    // formulation(i).integral(rho0inv * im * k0 * abs(MCore) * dof(u(i)), tf(u(i)), coreface(i), gauss);

    // source core
    // Active PML - Lorentz formulation 
    formulation(i).integral(+(1./rho0Core) * detJCore * JPml_LinvCore*grad(dof(u(i))) , JPml_LinvCore*grad(tf(u(i))) , omega_pml_core(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(+(1./rho0Core) * k0Core*k0Core/(1-MCore*MCore) * detJCore * JPml_invT_MCore * dof(u(i)) , JPml_invT_MCore * tf(u(i)), omega_pml_core(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(+(1./rho0Core) * im*k0Core/sqrt(1-MCore*MCore)* detJCore * JPml_LinvCore * grad(dof(u(i))), JPml_invT_MCore * tf(u(i)), omega_pml_core(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(-(1./rho0Core) * im*k0Core/sqrt(1-MCore*MCore)* detJCore * JPml_invT_MCore * dof(u(i)), JPml_LinvCore * grad(tf(u(i))), omega_pml_core(i), gauss, term::ProductType::Scalar);
    formulation(i).integral(-(1./rho0Core) * k0Core*k0Core/(1-MCore*MCore) * detJCore * dof(u(i)), tf(u(i)), omega_pml_core(i), gauss);
    // Active PML - incident field
    formulation.physicalSourceTerm(formulation(i).integral(-(1./rho0Core) * detJCore * JPml_LinvCore * grad_psi, JPml_LinvCore*grad(tf(u(i))) , omega_pml_core(i), gauss, term::ProductType::Scalar));
    formulation.physicalSourceTerm(formulation(i).integral(-(1./rho0Core) * k0Core*k0Core/(1-MCore*MCore) * detJCore * JPml_invT_MCore * psi, JPml_invT_MCore * tf(u(i)), omega_pml_core(i), gauss, term::ProductType::Scalar));
    formulation.physicalSourceTerm(formulation(i).integral(-(1./rho0Core) * im*k0Core/sqrt(1-MCore*MCore)* detJCore * JPml_LinvCore * grad_psi, JPml_invT_MCore * tf(u(i)), omega_pml_core(i), gauss, term::ProductType::Scalar));
    formulation.physicalSourceTerm(formulation(i).integral(+(1./rho0Core) * im*k0Core/sqrt(1-MCore*MCore)* detJCore * JPml_invT_MCore * psi, JPml_LinvCore * grad(tf(u(i))), omega_pml_core(i), gauss, term::ProductType::Scalar));
    formulation.physicalSourceTerm(formulation(i).integral(+(1./rho0Core) * k0Core*k0Core/(1-MCore*MCore) * detJCore * psi, tf(u(i)), omega_pml_core(i), gauss));
    // Flux term
    formulation.physicalSourceTerm(formulation(i).integral(-(1./rho0Core) * im * kxCore * (1-MCore*MCore) * psi, tf(u(i)), coreface(i), gauss));
    formulation.physicalSourceTerm(formulation(i).integral((1./rho0Core) * im * k0Core * MCore * psi, tf(u(i)), coreface(i), gauss));

    // Sommerfeld bypass
    //formulation(i).integral(rho0inv * im * k0 * dof(u(i)), tf(u(i)), bypassface(i), gauss);

    // Sommerfeld ext
    //formulation(i).integral(rho0inv * im * k0 * dof(u(i)), tf(u(i)), gammaext(i), gauss);

    // Artificial source
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      if(!sigma(i,j).isEmpty()) {
        formulation.artificialSourceTerm(formulation(i).integral(-g(j,i), tf(u(i)), sigma(i,j), gauss));
        // Transmission conditions terms
        formulation(i).integral(rho0inv * Si * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        formulation(i).integral(rho0inv * Sti * tangent< std::complex< double > >() * grad( dof(u(i)) ), dof(u(i)), sigma(i,j), gauss);
        formulation(i).integral(-rho0inv * dS * grad( dof(u(i)) ), grad( tf(u(i)) ), sigma(i,j), gauss);
      }
    }
    // Interface terms
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      if(!sigma(i,j).isEmpty()) {
        formulation(i,j).integral(dof(g(i,j)), tf(g(i,j)), sigma(i,j), gauss);
        formulation.artificialSourceTerm(formulation(i,j).integral(g(j,i), tf(g(i,j)), sigma(i,j), gauss));
        // Transmission conditions terms
        formulation(i,j).integral(-rho0inv * (Si+Sj) * u(i) , tf(g(i,j)), sigma(i,j), gauss);
        formulation(i,j).integral(-rho0inv * (Sti+Stj) * tangent< std::complex< double > >() * grad( u(i) ) , tf(g(i,j)), sigma(i,j), gauss);
        formulation(i,j).integral( 2.*rho0inv * dS * grad( u(i) ), grad( tf(g(i,j)) ), sigma(i,j), gauss);
      }
    }
  }

  // solve DDM problem
  std::string solver = "gmres";
  gmshDdm.userDefinedParameter(solver, "solver");
  double tol = 1e-6;
  gmshDdm.userDefinedParameter(tol, "tol");
  int maxIt = 150;
  gmshDdm.userDefinedParameter(maxIt, "maxIt");

  formulation.pre();
  formulation.solve(solver, tol, maxIt, true);
   
   // save solutions
  gmsh::option::setNumber("Mesh.Binary", 1);
  gmsh::option::setNumber("PostProcessing.Binary", 1);
  gmsh::option::setNumber("PostProcessing.SaveMesh", 0);
  for(unsigned int i = 0; i < nDom; ++i) {
    if(gmshddm::mpi::isItMySubdomain(i)) {
      int tag = save(u(i), omega_phy(i) | omega_pml(i) | omega_pml_bypass(i), "u", "", "", true, 0, 0., i + 1);
      gmsh::view::write(tag, "u_" + std::to_string(i + 1) + ".msh");

      // also save some cuts
      for(int step = 0; step < 1; step++) {
        gmsh::view::option::setNumber(tag, "TimeStep", step);
        gmsh::view::option::setNumber(tag, "AdaptVisualizationGrid", 1);
        gmsh::plugin::setNumber("CutPlane", "View", gmsh::view::getIndex(tag));
        gmsh::plugin::setNumber("CutPlane", "RecurLevel", FEMorder);
        gmsh::plugin::setNumber("CutPlane", "TargetError", 1e-4); // 1e-4 to get decent AMR
        { // z = 0
          gmsh::plugin::setNumber("CutPlane", "A", 0);
          gmsh::plugin::setNumber("CutPlane", "B", 0);
          gmsh::plugin::setNumber("CutPlane", "C", 1);
          gmsh::plugin::setNumber("CutPlane", "D", 0);
          int tag2 = gmsh::plugin::run("CutPlane");
          std::string name2 = std::string("u_cut_z0_step") + std::to_string(step) + "_" + std::to_string(i + 1);
          gmsh::view::option::setString(tag2, "Name", name2);
          gmsh::view::write(tag2, name2 + ".pos");
        }
        { // y = 0
          gmsh::plugin::setNumber("CutPlane", "A", 0);
          gmsh::plugin::setNumber("CutPlane", "B", 1);
          gmsh::plugin::setNumber("CutPlane", "C", 0);
          gmsh::plugin::setNumber("CutPlane", "D", 0);
          int tag2 = gmsh::plugin::run("CutPlane");
          std::string name2 = std::string("u_cut_y0_step") + std::to_string(step) + "_" + std::to_string(i + 1);
          gmsh::view::option::setString(tag2, "Name", name2);
          gmsh::view::write(tag2, name2 + ".pos");
        }
      }
    }
  }

  // save solution
  for(unsigned int i = 0; i < nDom; ++i) {
    if (mpi::isItMySubdomain(i)) {
      save(+u(i), omega_phy(i), "u_" + std::to_string(i));
    }
  }

  /* bool ComputeMono = false; // compute monodomain solution for comparison
  gmshDdm.userDefinedParameter(ComputeMono, "ComputeMono");
  if (ComputeMono) {
    gmshfem::field::Field< std::complex< double >, Form::Form0 > uMono("uMono", omegaPhy | omegaPml | omegaPmlBypass | corefaceMono , FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
    gmshfem::problem::Formulation< std::complex< double > > monodomain("PierceMono");
      
    // convected Helmholz weak form with PML - Lorentz formulation
    monodomain.integral(rho0inv *  detJ * JPml_Linv*grad(dof(uMono)) , JPml_Linv*grad(tf(uMono)) , omegaPml, gauss, term::ProductType::Scalar);
    monodomain.integral(rho0inv * k0*k0/(complex(beta*beta)) * detJ * JPml_invT_M * dof(uMono) , JPml_invT_M * tf(uMono), omegaPml, gauss, term::ProductType::Scalar);
    monodomain.integral(+rho0inv * im*k0/complex(beta)* detJ * JPml_Linv * grad(dof(uMono)), JPml_invT_M * tf(uMono), omegaPml, gauss, term::ProductType::Scalar);
    monodomain.integral(-rho0inv * im*k0/complex(beta)* detJ * JPml_invT_M * dof(uMono), JPml_Linv * grad(tf(uMono)), omegaPml, gauss, term::ProductType::Scalar);
    monodomain.integral(-rho0inv * k0*k0/(complex(beta*beta)) * detJ * dof(uMono), tf(uMono), omegaPml, gauss);
    
    // weak form Bypass PML - Lorentz formulation
    monodomain.integral(rho0inv *  detJBypass * JPml_LinvBypass*grad(dof(uMono)) , JPml_LinvBypass*grad(tf(uMono)) , omegaPmlBypass, gauss, term::ProductType::Scalar);
    monodomain.integral(rho0inv * k0Bypass*k0Bypass/(1-MBypass*MBypass) * detJBypass * JPml_invT_MBypass * dof(uMono) , JPml_invT_MBypass * tf(uMono), omegaPmlBypass, gauss, term::ProductType::Scalar);
    monodomain.integral(rho0inv * im*k0Bypass/sqrt(1-MBypass*MBypass)* detJBypass * JPml_LinvBypass * grad(dof(uMono)), JPml_invT_MBypass * tf(uMono), omegaPmlBypass, gauss, term::ProductType::Scalar);
    monodomain.integral(-rho0inv * im*k0Bypass/sqrt(1-MBypass*MBypass)* detJBypass * JPml_invT_MBypass * dof(uMono), JPml_LinvBypass * grad(tf(uMono)), omegaPmlBypass, gauss, term::ProductType::Scalar);
    monodomain.integral(-rho0inv * k0Bypass*k0Bypass/(1-MBypass*MBypass) * detJBypass * dof(uMono), tf(uMono), omegaPmlBypass, gauss);

    // convected Helmholz weak form
    monodomain.integral( rho0inv * complex(TFlow) * grad(dof(uMono)), grad(tf(uMono)), omegaPhy, gauss);
    monodomain.integral(-rho0inv * im * k0 * dof(uMono), complex(MM) * grad(tf(uMono)), omegaPhy, gauss);
    monodomain.integral(+rho0inv * im * k0 * complex(MM) * grad(dof(uMono)), tf(uMono), omegaPhy, gauss);
    monodomain.integral(-rho0inv * k0 * k0 * dof(uMono), tf(uMono), omegaPhy, gauss);
    // source
    monodomain.integral(-rho0inv * im * (1.) * (1-MCore*MCore), tf(uMono), corefaceMono, gauss);
    monodomain.integral(rho0inv * im * k0 * abs(MCore) * dof(uMono), tf(uMono), corefaceMono, gauss);

    //monodomain.integral(rho0inv * im * k0 * dof(uMono), tf(uMono), bypassfaceMono, gauss);
    // monodomain.integral(rho0inv * im * k0 * dof(uMono), tf(uMono), gammaextMono, gauss);

    monodomain.pre();
    monodomain.assemble();
    msg::info << "Memory usage " << monodomain.getEstimatedFactorizationMemoryUsage() << msg::endl;
    monodomain.solve();
    
    save(+uMono, omegaPhy | omegaPml | omegaPmlBypass , "uMono");
    // Monodomain L2 error
    double local_err;
    double global_err=0.;
    for(unsigned int i = 0; i < nDom; ++i) {
      std::complex< double > denLocal = integrate(pow(abs( uMono ), 2), omega_phy(i) , gauss);
      std::complex< double > numLocal = integrate(pow(abs(uMono - u(i)), 2), omega_phy(i) , gauss);
      local_err = sqrt(numLocal.real() / denLocal.real());
      gmshfem::msg::info << "ddm-monodomain L2 error = " << local_err << " on subdomain " << i << msg::endl;
      global_err += local_err;
    }
    gmshfem::msg::info << "ddm-monodomain global error = " << global_err/nDom  << msg::endl;

    }; // end Monodomain
  */


  return 0;
}
  