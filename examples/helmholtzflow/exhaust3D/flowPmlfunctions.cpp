#include "flowPmlFunctions.h"

void InterpolateMeanFlow(int nDom, Domain omegaFlow, Field< double, Form::Form0 > &rho0, Field< double, Form::Form0 > &c0, Field< double, Form::Form0 > &Vx, Field< double, Form::Form0 > &Vy, Field< double, Form::Form0 > &Vz)
{
  // Mean flow pre-pro
  ScalarFunction<double> ryz = sqrt(y<double>()*y<double>()+z<double>()*z<double>());
  ScalarFunction<double> theta = atan2( z<double>(), y<double>() );
  ScalarFunction < double > rho0P = changeOfCoordinates( probeScalarView< double >(1,0,2) , x< double >() , ryz, 0.);
  ScalarFunction < double > c0P = changeOfCoordinates( probeScalarView< double >(2,0,2) , x< double >() , ryz, 0.);
  ScalarFunction < double > VxP = changeOfCoordinates( probeScalarView< double >(3,0,2) , x< double >() , ryz, 0.);
  ScalarFunction < double > VyP = cos(theta)*changeOfCoordinates( probeScalarView< double >(4,0,2), x< double >() , ryz, 0.);
  ScalarFunction < double > VzP = sin(theta)*changeOfCoordinates( probeScalarView< double >(4,0,2), x< double >() , ryz, 0.);

  rho0.addConstraint(omegaFlow, rho0P);
  c0.addConstraint(omegaFlow, c0P);
  Vx.addConstraint(omegaFlow, VxP);
  Vy.addConstraint(omegaFlow, VyP);
  Vz.addConstraint(omegaFlow, VzP);

  for(int i = 0; i < nDom; ++i) {
    if (mpi::isItMySubdomain(i)) {
      gmshfem::problem::Formulation< double > Interp("Interpflow"); // create interpolation formulation per subdomain
      Interp.integral(dof(rho0), tf(rho0), omegaFlow, "Gauss1");
      Interp.integral(dof(c0), tf(c0), omegaFlow, "Gauss1");
      Interp.integral(dof(Vx), tf(Vx), omegaFlow, "Gauss1"); 
      Interp.integral(dof(Vy), tf(Vy), omegaFlow, "Gauss1");
      Interp.integral(dof(Vz), tf(Vz), omegaFlow, "Gauss1");
      Interp.pre();
    }
  }
}

void InterpolateMeanFlowMx(int nDom, Domain omegaFlow, Field< double, Form::Form0 > &c0, Field< double, Form::Form0 > &Vx)
{
  // Mean flow pre-pro
  ScalarFunction<double> ryz = sqrt(y<double>()*y<double>()+z<double>()*z<double>());
  //ScalarFunction<double> theta = atan2( z<double>(), y<double>() );
  ScalarFunction < double > c0P = changeOfCoordinates( probeScalarView< double >(1,0,2) , x< double >() , ryz, 0.);
  ScalarFunction < double > VxP = changeOfCoordinates( probeScalarView< double >(2,0,2) , x< double >() , ryz, 0.);
  c0.addConstraint(omegaFlow, c0P);
  Vx.addConstraint(omegaFlow, VxP);

  for(int i = 0; i < nDom; ++i) {
    if (mpi::isItMySubdomain(i)) {
      gmshfem::problem::Formulation< double > InterpM("InterpflowM"); // create interpolation formulation per subdomain
      InterpM.integral(dof(c0), tf(c0), omegaFlow, "Gauss1"); 
      InterpM.integral(dof(Vx), tf(Vx), omegaFlow, "Gauss1"); 
      InterpM.pre();
    }
  }
}

TensorFunction< double > getInverseLorentzTensor(
    ScalarFunction< double > Mx, ScalarFunction< double > My, ScalarFunction< double > Mz
    , ScalarFunction< double > beta)
{
  ScalarFunction< double > Alphax = 1 - Mx*Mx/(1+beta);
  ScalarFunction< double > Alphay = 1 - My*My/(1+beta);
  ScalarFunction< double > Alphaz = 1 - Mz*Mz/(1+beta);
  ScalarFunction< double > Kxy = -Mx*My/(1+beta);
  ScalarFunction< double > Kyz = -My*Mz/(1+beta);
  ScalarFunction< double > Kxz = -Mx*Mz/(1+beta);
  return tensor< double >(Alphax, Kxy, Kxz, Kxy, Alphay, Kyz, Kxz, Kyz, Alphaz);
}

TensorFunction< std::complex< double > > getInversePMLJacobian(
    ScalarFunction< std::complex< double > > k0, std::pair<double, double> xlim, std::pair<double, double> ylim, double Lpml,
    ScalarFunction< double > beta,
    ScalarFunction< std::complex< double > > &detJ)
{
  std::complex< double > im(0., 1.);
  ScalarFunction< double > Sigma0 = beta;
  double R = ylim.second;
  double D = xlim.second;
  double L_duct = xlim.first;
  /*msg::info << " ************************* " << msg::endl;
  msg::info << " - x outer pml left = " << L_duct << "" << msg::endl;
  msg::info << " - x outer pml right = " << D << "" << msg::endl;
  msg::info << " - Rpml = " << R << "" << msg::endl;
  msg::info << " - pml width = " << Lpml << "" << msg::endl;
  msg::info << " ************************* " << msg::endl;*/
  // PML (Bermudez) - Cylindrical coordinates (x,r,theta) <-> (x,y,z)
  // (r,theta) = (y,z)
  ScalarFunction<double> ryz = sqrt( y<double>()*y<double>() + z<double>()*z<double>() );
  ScalarFunction<double> cosT = y< double >() / ryz;
  ScalarFunction<double> sinT = z< double >() / ryz;
  ScalarFunction<double> dampingProfileR = heaviside(ryz - R) * Sigma0 / ( (Lpml+R) - ryz ); 
  ScalarFunction<double> dampingProfileInt = - heaviside(ryz - R) * Sigma0 *ln( ( (Lpml+R) - ryz) / Lpml); 
  ScalarFunction< std::complex< double > > gammaR = 1. - im * complex(dampingProfileR) / k0;
  ScalarFunction< std::complex< double > > gammaR_hat = 1. - im * (1./complex(ryz)) * complex(dampingProfileInt) / k0;
  // x
  double L_ductpml = L_duct - Lpml;
  ScalarFunction<double> SigmaX = heaviside(x<double>() - D) * Sigma0 / ( (Lpml+D) - abs(x<double>()) ) + heaviside(-x<double>() + L_duct ) * Sigma0 / ( abs(L_ductpml) - abs(x<double>()) );
  ScalarFunction< std::complex< double > > gammaX = 1. - im * complex(SigmaX) / k0;
  TensorFunction< std::complex< double > > J_PML_inv_T = tensor< std::complex <double > >(1./gammaX,0.,0.,
                                                        0., complex(cosT)/gammaR, complex(sinT)/gammaR,
                                                        0., -complex(sinT)/gammaR_hat, complex(cosT)/gammaR_hat );
  detJ = gammaR_hat * gammaR * gammaX;

  return J_PML_inv_T;
}