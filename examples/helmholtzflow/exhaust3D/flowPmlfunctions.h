#include <gmshfem/Formulation.h>
#include <gmshfem/GmshFem.h>

#include <gmshddm/GmshDdm.h>
#include <gmshddm/Formulation.h>
#include <gmshddm/MPIInterface.h>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;
using namespace gmshddm::mpi;

using namespace gmshfem;
using namespace gmshfem::function;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::equation;

void InterpolateMeanFlow(int nDom, Domain omegaFlow, Field< double, Form::Form0 > &rho0, Field< double, Form::Form0 > &c0,
  Field< double, Form::Form0 > &Vx, Field< double, Form::Form0 > &Vy, Field< double, Form::Form0 > &Vz);

void InterpolateMeanFlowMx(int nDom, Domain omegaFlow, Field< double, Form::Form0 > &c0, Field< double, Form::Form0 > &Vx);

TensorFunction< double > getInverseLorentzTensor(
    ScalarFunction< double > Mx, 
    ScalarFunction<  double > My, 
    ScalarFunction< double > Mz
    , ScalarFunction< double > beta);

TensorFunction< std::complex< double > > getInversePMLJacobian(
    ScalarFunction< std::complex< double > > k0, 
    std::pair<double, double> xlim, std::pair<double, double> ylim, double Lpml,
    ScalarFunction< double > beta,
    ScalarFunction< std::complex< double > > &detJ);
