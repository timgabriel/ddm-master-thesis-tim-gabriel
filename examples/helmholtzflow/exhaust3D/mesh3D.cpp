#include "mesh3D.h"

void meshExhaust3D(const double Ly, const double lc, const double lcI, const int MeshElemOrder, bool ActivePMLCore, bool ActivePMLByPass)
{
  //gmsh::initialize();
  
  gmsh::model::add("geometry");
  namespace factory = gmsh::model::occ;

  double lcPML = lc;
  double Rpml = Ly/10.;
  double Lx = 0.5;
  double lcTip = lc/5.;
  gmsh::merge("../../exhaust/geometry/TURNEX_countour_points.geo");

  std::vector<std::pair<int, int> > entities_c;
  for(int i = 100; i <= 1315; i++) entities_c.push_back(std::make_pair(0,i));

  int BackScatteringPoint = 149;
  std::vector<double> coord;
  gmsh::model::getValue(0, BackScatteringPoint, {}, coord);
  std::cout << "x : " << coord[0] << "y : " << coord[1] << "z : " << coord[2];

  gmsh::merge("../../exhaust/geometry/TURNEX_interface_points.geo");
  std::vector<std::pair<int, int> > entities_i;
  for(int i = 2001; i <= 2156; i++) entities_i.push_back(std::make_pair(0,i));

  factory::addPoint(0., 0., 0, lcPML, 1); 
  factory::addPoint(Lx, 0., 0, lcPML, 2); 
  factory::addPoint(Lx, Ly, 0, lcPML, 3); 
  factory::addPoint(Ly, 0, 0, lcPML, 4); 
  factory::addPoint(coord[0], Ly, 0, lcPML, 5);
  // cartesian PML
  factory::addPoint(coord[0] - Rpml, coord[1], 0, lcPML, 6); 
  factory::addPoint(coord[0] - Rpml, Ly + Rpml, 0, lcPML, 7); 
  factory::addPoint(Lx + Rpml, Ly + Rpml, 0, lcPML, 8); 
  factory::addPoint(Lx + Rpml, 0, 0, lcPML, 9); 
  
  int l60 = factory::addLine(149 ,6);
  int l70 = factory::addLine(6,7);
  int l80 = factory::addLine(7,8);
  int l90 = factory::addLine(8,9);
  int l100 = factory::addLine(9,2);
  
  // ByPass Active PML
  int ByPassUp = 500;
  int ByPassDown = 501;
  std::vector<double> coord1, coord2;
  gmsh::model::getValue(0, ByPassUp, {}, coord1);
  gmsh::model::getValue(0, ByPassDown, {}, coord2);

  factory::addPoint(coord1[0] - Rpml, coord1[1], 0, lc, 10); 
  factory::addPoint(coord2[0] - Rpml, coord2[1], 0, lc, 11); 
  int l17 = factory::addLine(10,500);
  int l18 = factory::addLine(11,10);
  int l19 = factory::addLine(501,11);
  
  // Core Active PML
  int CoreUp = 1003;
  int CoreDown = 1004;
  std::vector<double> coord3, coord4;
  gmsh::model::getValue(0, CoreUp, {}, coord3);
  gmsh::model::getValue(0, CoreDown, {}, coord4);

  factory::addPoint(coord3[0] - Rpml, coord3[1], 0, lc, 13); 
  factory::addPoint(coord4[0] - Rpml, coord4[1], 0, lc, 14); 
  int l20 = factory::addLine(13,1003);
  int l21 = factory::addLine(14,13);
  int l22 = factory::addLine(1004,14);

  // 
  int P11 = 282;
  int P12 = 298;
  int P21 = 828;
  int P22 = 914;

  int Tip1 = 289;
  int Tip2 = 872;  
  int Tip3 = 1315;
  
  std::vector<std::pair<int, int> > entities_tip;
  for(int i = P11; i <= P12; i++) entities_tip.push_back(std::make_pair(0,i));
  for(int i = P21; i <= P22; i++) entities_tip.push_back(std::make_pair(0,i));
  entities_tip.push_back(std::make_pair(0,Tip3));

  std::vector<int> t;
  for(int i = BackScatteringPoint; i <= P11; i++) t.push_back(i);
  int l1 = factory::addSpline(t);

  t.clear();
  for(int i = P11; i <= Tip1; i++) t.push_back(i);
  int l2 = factory::addSpline(t);
  
  t.clear();
  for(int i = Tip1; i <= P12; i++) t.push_back(i);
  int l3 = factory::addSpline(t);

  t.clear();
  for(int i = P12; i <= 500; i++) t.push_back(i);
  int l4 = factory::addSpline(t);

  int l5 = factory::addLine(500,501);
  
  t.clear();
  for(int i = 501; i <= P21; i++) t.push_back(i);
  int l6 = factory::addSpline(t);

  t.clear();
  for(int i = P21; i <= Tip2; i++) t.push_back(i);
  int l7 = factory::addSpline(t);

  t.clear();
  for(int i = Tip2; i <= P22; i++) t.push_back(i);
  int l8 = factory::addSpline(t);

  t.clear();
  for(int i = P22; i <= 1003; i++) t.push_back(i);
  int l9 = factory::addSpline(t);
  
  int l11 = factory::addLine(1003,1004);

  t.clear();
  for(int i = 1004; i <= Tip3; i++) t.push_back(i);
  int l12 = factory::addSpline(t);

  int l13 = factory::addLine(Tip3,2);
  int l14 = factory::addLine(2,3);
  int l15 = factory::addLine(3,5);
  int l16 = factory::addLine(5, BackScatteringPoint);

  // Physical surfaces
  int c1 = factory::addCurveLoop({l13, l14, l15, l16, l1, l2, l3, l4, l5, l6, l7, l8, l9, l11, l12});
  int surf1 = factory::addPlaneSurface({c1});

  factory::synchronize();
  for(int i = 2001; i <= 2156; i++) gmsh::model::mesh::embed(0, {i}, 2, surf1);
  
  // PML surface
  int c2 = factory::addCurveLoop({l60, l70, l80, l90, l100, l14, l15, l16});
  int surf2 = factory::addPlaneSurface({c2});
  
  // ByPass duct active PML surface 
  int surf3, c3;
  if (ActivePMLByPass) {
    c3 = factory::addCurveLoop({l5, l19, l18, l17});
    surf3 = factory::addPlaneSurface({c3});
  }

  // Core duct active PML surface
  int c4, surf4;
  if (ActivePMLCore) {
    c4 = factory::addCurveLoop({l11, l22, l21, l20});
    surf4 = factory::addPlaneSurface({c4});
  }
  
  /*if (ActivePMLByPass) {
    gmsh::model::addPhysicalGroup(2, {surf3}, 3);
    gmsh::model::setPhysicalName(2, 3, "pml_surface_bypass");
  }
  if (ActivePMLCore) {
    gmsh::model::addPhysicalGroup(2, {surf4}, 4);
    gmsh::model::setPhysicalName(2, 4, "pml_surface_core");
  }*/

  factory::synchronize();
  gmsh::model::mesh::setSize(entities_c, lc);
  gmsh::model::mesh::setSize(entities_i, lcI);
  gmsh::model::mesh::setSize(entities_tip, lcTip);

  // 3D rotation
  std::vector<std::pair<int,int> >ov_phy;
  factory::revolve({{2,surf1},{2,surf2}}, 0, 0, 0, 1, 0, 0, 2*M_PI, ov_phy); 
  
  std::vector<std::pair<int,int> >ov_phy3;
  factory::revolve({{2,surf3}}, 0, 0, 0, 1, 0, 0, 2*M_PI, ov_phy3); 
  
  std::vector<std::pair<int,int> >ov_phy4;
  factory::revolve({{2,surf4}}, 0, 0, 0, 1, 0, 0, 2*M_PI, ov_phy4); 

  factory::remove({{2,surf1},{2,surf2}},true);
  factory::remove({{2,surf3}},true);
  factory::remove({{2,surf4}},true);
  factory::removeAllDuplicates();
  
  factory::synchronize();

  /*std::vector<std::pair<int,int> >ov_phy2;
  factory::revolve({{2,surf3}}, 0, 0, 0, 1, 0, 0, 2*M_PI, ov_phy2);
  factory::remove({{2,surf3}},true);
  factory::removeAllDuplicates();
  factory::synchronize();*/

  // physical groups
  int om_phy = gmsh::model::addPhysicalGroup(3, {1});
  gmsh::model::setPhysicalName(3, om_phy, "omega");

  int om_pml = gmsh::model::addPhysicalGroup(3, {2});
  gmsh::model::setPhysicalName(3, om_pml, "omega_pml");

  int om_pml_bypass = gmsh::model::addPhysicalGroup(3, {3});
  gmsh::model::setPhysicalName(3, om_pml_bypass, "omega_pml_bypass");
  
  int om_pml_core = gmsh::model::addPhysicalGroup(3, {4});
  gmsh::model::setPhysicalName(3, om_pml_core, "omega_pml_core");

  //int bypass = gmsh::model::addPhysicalGroup(2, {10});// 9 no pml
  //gmsh::model::setPhysicalName(2, bypass, "bypass_surf");
  
  int core = gmsh::model::addPhysicalGroup(2, {17});//14 no pml, 15 pml, 16 bypass pml
  gmsh::model::setPhysicalName(2, core, "core_surf");
  
  //factory::synchronize();
  //int gam_ext = gmsh::model::addPhysicalGroup(2, {18,19,20}); // no pml 2,3,4
  //gmsh::model::setPhysicalName(2, gam_ext, "gamma_ext");

  // Trying options
  gmsh::option::setNumber("Mesh.MeshSizeFromCurvature", 20);
  //gmsh::option::setNumber("Mesh.MeshSizeExtendFromBoundary", 0);
  gmsh::option::setNumber("Mesh.MeshSizeFromPoints", 1);
  gmsh::option::setNumber("Mesh.Algorithm", 6);
  gmsh::option::setNumber("Mesh.Algorithm3D", 10);
  //gmsh::option::setNumber("Mesh.AngleToleranceFacetOverlap", 1e-6);
  gmsh::option::setNumber("Mesh.MeshSizeMax", lc);
  gmsh::option::setNumber("Mesh.MeshSizeMin", lc/10);

  // Attractor around the tips
  std::vector<double> pList;
  int nodesToPropagate = 4;
  for(int i = Tip1-nodesToPropagate; i <= Tip1+nodesToPropagate; i++) pList.push_back(i);
  for(int i = Tip2-nodesToPropagate; i <= Tip2+nodesToPropagate; i++) pList.push_back(i);
  pList.push_back(Tip3);
  gmsh::model::mesh::field::add("Distance", 1);
  gmsh::model::mesh::field::setNumbers(1, "PointsList", pList);
  //gmsh::model::mesh::field::setNumbers(1, "CurvesList", {2});
  gmsh::model::mesh::field::setNumber(1, "Sampling", 100);
  
  gmsh::model::mesh::field::add("Threshold", 2);
  gmsh::model::mesh::field::setNumber(2, "InField", 1);
  gmsh::model::mesh::field::setNumber(2, "SizeMin", lc/10.);
  gmsh::model::mesh::field::setNumber(2, "SizeMax", lc);
  gmsh::model::mesh::field::setNumber(2, "DistMin", lc/10.);
  gmsh::model::mesh::field::setNumber(2, "DistMax", lc);
  
  gmsh::model::mesh::field::add("Min", 7);
  gmsh::model::mesh::field::setNumbers(7, "FieldsList", {2});
  gmsh::model::mesh::field::setAsBackgroundMesh(7);
  
  //gmsh::option::setNumber("Mesh.ElementOrder", MeshElemOrder);
  //gmsh::option::setNumber("Mesh.SecondOrderLinear", 1);
  //gmsh::model::mesh::generate(3);

  //gmsh::write("turnex3DTest.msh");
  //exit(1);
  
  //gmsh::finalize();
}


