#include "gmsh.h"

#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshddm/MPIInterface.h>
#include <gmshfem/AnalyticalFunction.h>
#include <gmshfem/Message.h>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;

using namespace gmshfem;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::problem;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::analytics;
using namespace gmshfem::post;

void circlesConcentric(const unsigned int nDom, const double r0, const double rInf, const double rPml, const double lc)
{
  if(nDom < 2) {
    gmshfem::msg::error << "nDom should be at least equal to 2." << gmshfem::msg::endl;
    exit(0);
  }
  gmsh::model::add("circlesConcentric");

  std::vector< int > tagsCircle(nDom + 2);
  for(unsigned int i = 0; i <= nDom; ++i) {
    tagsCircle[i] = gmsh::model::occ::addCircle(0., 0., 0., std::sqrt(double(i * (rInf * rInf - r0 * r0)) / nDom + r0 * r0));
  }
  tagsCircle[nDom + 1] = gmsh::model::occ::addCircle(0., 0., 0., std::sqrt(double(nDom * (rPml * rPml - r0 * r0)) / nDom + r0 * r0));

  std::vector< int > tagsCurve(nDom + 2);
  for(unsigned int i = 0; i <= (nDom + 1); ++i) {
    tagsCurve[i] = gmsh::model::occ::addCurveLoop({tagsCircle[i]});
  }

  std::vector< int > tagsSurface(nDom + 1);
  for(unsigned int i = 0; i <= nDom; ++i) {
    tagsSurface[i] = gmsh::model::occ::addPlaneSurface({tagsCurve[i], tagsCurve[i + 1]});
  }

  gmsh::model::occ::synchronize();

  gmsh::model::addPhysicalGroup(1, {tagsCircle[0]}, 91);
  gmsh::model::setPhysicalName(1, 91, "gammaScat");

  gmsh::model::addPhysicalGroup(1, {tagsCircle[nDom]}, 92);
  gmsh::model::setPhysicalName(1, 92, "gammaInf");

  gmsh::model::addPhysicalGroup(1, {tagsCircle[nDom + 1]}, 93);
  gmsh::model::setPhysicalName(1, 93, "gammaPml");


  for(unsigned int i = 0; i < nDom; ++i) {
    gmsh::model::addPhysicalGroup(2, {tagsCircle[i]}, i + 1);
    gmsh::model::setPhysicalName(2, i + 1, "omega_" + std::to_string(i));

    if(i != nDom - 1) {
      gmsh::model::addPhysicalGroup(1, {tagsCircle[i + 1]}, 200 + i);
      gmsh::model::setPhysicalName(1, 200 + i, "sigma_" + std::to_string(i));
    }
  }

  gmsh::model::addPhysicalGroup(2, {tagsCircle[nDom]}, 1 + nDom);
  gmsh::model::setPhysicalName(2, 1 + nDom, "omega_pml");

  std::vector< std::pair< int, int > > out;
  gmsh::model::getEntities(out, 0);
  gmsh::model::mesh::setSize(out, lc);
  gmsh::model::mesh::generate();
  gmsh::write("m.msh");
}

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  unsigned int nDom = 3;
  //gmshDdm.userDefinedParameter(nDom, "nDom");

  double rInf = 5.;
  double r0 = 1;
  double lc = 0.1;

  int Npml = 4;
  double rPml = rInf + Npml * lc;

  gmshDdm.userDefinedParameter(rInf, "rInf");
  gmshDdm.userDefinedParameter(r0, "r0");
  gmshDdm.userDefinedParameter(lc, "lc");

  int FEMorder = 3;
  std::string gauss = "Gauss10";
  gmshDdm.userDefinedParameter(gauss, "gauss");

  const double pi = 3.14159265358979323846264338327950288;
  // transmission conditions
  std::string Transmission = "Taylor2";
  gmshDdm.userDefinedParameter(Transmission, "transmission");
  double alpha = pi / 4.;
  gmshDdm.userDefinedParameter(alpha, "alpha");
  msg::info << " Transmission condition " << Transmission << " with rotation alpha = " << alpha << msg::endl;
  // Build geometry and mesh using gmsh API
  circlesConcentric(nDom, r0, rInf, rPml, lc);

  // Define domain
  Subdomain omega(nDom);
  Subdomain gammaPml(nDom);
  Subdomain gammaInf(nDom);
  Subdomain gammaScat(nDom);
  Interface sigma(nDom);

  for(unsigned int i = 0; i < nDom; ++i) { // ddm entities

    if(i == (nDom - 1)) {
      gammaPml(i) = Domain(1, 93);
      gammaInf(i) = Domain(1, 92);
      omega(i) = (Domain(2, i + 1) | Domain(2, i + 2));
    }
    else {
      omega(i) = Domain(2, i + 1);
    }

    gammaScat(i) = Domain(1, 91);

    if(i != 0) {
      sigma(i, i - 1) = Domain(1, 200 + i - 1);
    }
    if(i != nDom - 1) {
      sigma(i, i + 1) = Domain(1, 200 + i);
    }
  }

  // Define topology
  std::vector< std::vector< unsigned int > > topology(nDom);
  for(unsigned int i = 0; i < nDom; ++i) {
    if(i != 0) {
      topology[i].push_back(i - 1);
    }
    if(i != nDom - 1) {
      topology[i].push_back(i + 1);
    }
  }

  std::complex< double > im(0., 1.);

  // Create DDM formulation
  gmshddm::problem::Formulation< std::complex< double > > formulation("helmholtz", topology);

  double w = 2 * pi;
  double theta = pi / 3.;

  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega | gammaScat | gammaPml | sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);

  Function< std::complex< double >, Degree::Degree0 > *solution = nullptr;

  solution = new AnalyticalFunction< helmholtz2D::ScatteringByASoftCylinder< std::complex< double > > >(w, r0, 0., 0., 2 * w, theta);
  for(unsigned int i = 0; i < nDom; ++i) {
    // physical source constraint
    u(i).addConstraint(gammaScat(i), *solution);
  }

  ScalarFunction< std::complex< double > > S, dS;
  if(Transmission == "Taylor0") {
    S = -im * w * exp(im * alpha / 2.);
    dS = 0.;
  }
  else if(Transmission == "Taylor2") {
    S = -im * w * cos(alpha / 2.);
    dS = -im * exp(-im * alpha / 2.0) / (2.0 * w);
  }

  // add interface field
  formulation.addInterfaceField(g);

  // PML parameters - to be used as outgoing boundary condition
  const double WPml = rPml - rInf;
  ScalarFunction< std::complex< double > > cosT = x< std::complex< double > >() / r2d< std::complex< double > >();
  ScalarFunction< std::complex< double > > sinT = y< std::complex< double > >() / r2d< std::complex< double > >();
  ScalarFunction< std::complex< double > > dampingProfileR = heaviside(abs(r2d< std::complex< double > >()) - rInf) / (WPml - (r2d< std::complex< double > >() - rInf));
  ScalarFunction< std::complex< double > > dampingProfileInt = -heaviside(abs(r2d< std::complex< double > >()) - rInf) * ln((WPml - (r2d< std::complex< double > >() - rInf)) / WPml);
  ScalarFunction< std::complex< double > > cR = 1. + im * dampingProfileR / w;
  ScalarFunction< std::complex< double > > cStretch = 1. + im * (1. / r2d< std::complex< double > >()) * dampingProfileInt / w;
  ScalarFunction< std::complex< double > > S_PML = cR * cStretch;
  TensorFunction< std::complex< double > > D = tensor< std::complex< double > >(cStretch / cR * cosT * cosT + cR / cStretch * sinT * sinT,
                                                                                cStretch / cR * cosT * sinT - cR / cStretch * cosT * sinT,
                                                                                0.,
                                                                                cStretch / cR * cosT * sinT - cR / cStretch * cosT * sinT,
                                                                                cStretch / cR * sinT * sinT + cR / cStretch * cosT * cosT,
                                                                                0.,
                                                                                0., 0., 0.);

  // Add terms to the formulation
  for(unsigned int i = 0; i < nDom; ++i) {
    // VOLUME TERMS
    if(i == (nDom - 1)) { // pre-processing is slower with the PML formulation
      formulation(i).integral(D * grad(dof(u(i))), grad(tf(u(i))), omega(i), gauss);
      formulation(i).integral(-w * w * S_PML * dof(u(i)), tf(u(i)), omega(i), gauss);
    }
    else {
      formulation(i).integral(grad(dof(u(i))), grad(tf(u(i))), omega(i), gauss);
      formulation(i).integral(-w * w * dof(u(i)), tf(u(i)), omega(i), gauss);
    }
    // Artificial source
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i).integral(S * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
      formulation(i).integral(-dS * grad(dof(u(i))), grad(tf(u(i))), sigma(i, j), gauss);
      formulation(i).integral(formulation.artificialSource(-g(j, i)), tf(u(i)), sigma(i, j), gauss);
    }

    // INTERFACE TERMS
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i, j).integral(dof(g(i, j)), tf(g(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(formulation.artificialSource(g(j, i)), tf(g(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(-2.0 * S * u(i), tf(g(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(2.0 * dS * grad(u(i)), grad(tf(g(i, j))), sigma(i, j), gauss);
    }
  }

  // Solve the DDM formulation
  double tolerence = 1e-6;
  gmshDdm.userDefinedParameter(tolerence, "tol");
  int maxIter = 1000;
  gmshDdm.userDefinedParameter(maxIter, "maxIter");
  formulation.pre();
  formulation.solve("gmres", tolerence, maxIter);

  // L2 error
  double num = 0., den = 0.;
  Subdomain omega_err(nDom);
  for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
    if(gmshddm::mpi::isItMySubdomain(i)) {
      omega_err(i) = Domain(2, i + 1);
      save(+u(i), omega(i), "u_" + std::to_string(i));
      save(*solution, omega_err(i), "uex_" + std::to_string(i));
      save(*solution - u(i), omega_err(i), "err_" + std::to_string(i));

      std::complex< double > denLocal = integrate(pow(abs(*solution), 2), omega_err(i), gauss);
      std::complex< double > numLocal = integrate(pow(abs(*solution - u(i)), 2), omega_err(i), gauss);
      num += numLocal.real();
      den += denLocal.real();
      msg::info << "Local L2 error = " << sqrt(numLocal.real() / denLocal.real()) << " on subdomain " << i << msg::endl;
    }
  }

  msg::info << "Global L2 error = " << sqrt(num / den) << msg::endl;

  return 0;
}
