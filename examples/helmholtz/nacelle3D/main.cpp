#include <iostream>
#include <fstream>
#include <gmshddm/GmshDdm.h>
#include <gmshddm/Formulation.h>
#include <gmshddm/MPIInterface.h>
#include <gmshfem/Message.h>
#include <gmsh.h>
#include "nacelleFunctions.h"

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;
using namespace gmshddm::mpi;

using namespace gmshfem;
using namespace gmshfem::problem;
using namespace gmshfem::domain;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;
using namespace gmshfem::equation;

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  // rank and size
  const unsigned int MPI_Rank = getMPIRank();

  const double pi = 3.14159265358979323846264338327950288;
  const std::complex< double > im(0., 1.);

  std::string mesh = "";
  gmshDdm.userDefinedParameter(mesh, "mesh");

  int nDom = 2;
  gmshDdm.userDefinedParameter(nDom, "nDom");

  double h_near = 0.1; // mesh size near field
  gmshDdm.userDefinedParameter(h_near, "h_near");

  double h_far = 0.1; // mesh size far field
  gmshDdm.userDefinedParameter(h_far, "h_far");

  double meshSizeFactor = 1.0; // mesh size factor
  gmshDdm.userDefinedParameter(meshSizeFactor, "meshSizeFactor");
  gmsh::option::setNumber("Mesh.MeshSizeFactor", meshSizeFactor);

  double R = 2.5; // cylinder radius
  gmshDdm.userDefinedParameter(R, "R");

  double D = 3.; // duct length
  gmshDdm.userDefinedParameter(D, "D");

  double L_duct = -0.4; // fan face position

  double k = 2*pi*1800./(340.*meshSizeFactor); // running frequency
  if (!getMPIRank()) {
    msg::info << " - running wavenumber k = " << k << "" << msg::endl;
  }
  // Input BC
  int m = 6; // azimuthal mode number
  gmshDdm.userDefinedParameter(m, "m");
  int n = 0; // radial mode number
  gmshDdm.userDefinedParameter(n, "n");

  double delta_pml=0., delta_apml=0.;
  bool PML = false;
  bool Active_PML = false;
  if (PML)
    delta_pml = 2.*h_far;
  if (Active_PML)
    delta_apml = 2.*h_near;

  // normalized impedance (by local value of rho0*c0) for Myers BC - single dof perforated plate liner
  bool SetLiner = false;
  gmshDdm.userDefinedParameter(SetLiner, "SetLiner");
  double Res = 2, If = 0.020, hh = 0.02286; // resistance,inertance and liner depth
  ScalarFunction< std::complex<double> > Z = Res - im*( k*If + 1./tan(k*hh) ); // normalized impedance (by rho0*c0)

  int FEMorder = 2;
  gmshDdm.userDefinedParameter(FEMorder, "FEMorder");
  std::string gauss = "Gauss" + std::to_string(2 * FEMorder + 2);

  std::string Transmission = "Taylor2";
  gmshDdm.userDefinedParameter(Transmission, "TC");

  double alpha = -pi / 2.;
  gmshDdm.userDefinedParameter(alpha, "alpha");

  gmsh::option::setNumber("Mesh.Binary", 1);
  if(mesh.empty()) {
    if(!getMPIRank()) {
      // build geometry
      Nacelle(h_near, h_far, D, R, L_duct, delta_pml, delta_apml, nDom);
      gmsh::option::setNumber("Mesh.ElementOrder", FEMorder);
      gmsh::option::setNumber("Mesh.SecondOrderLinear", 1);
      // save all, as partitioning will create some entities without physical groups
      gmsh::option::setNumber("Mesh.SaveAll", 1);
      gmsh::model::mesh::generate(3);
      gmsh::model::mesh::partition(nDom);
      // save partitioned mesh in single file for mono-process runs
      gmsh::option::setNumber("Mesh.PartitionSplitMeshFiles", 0);
      gmsh::write("Nacelle.msh");
      // save partitioned mesh in separate files for distributed runs
      gmsh::option::setNumber("Mesh.PartitionSplitMeshFiles", 1);
      gmsh::write("Nacelle.msh");
    }
    mesh = "Nacelle";
  }

  barrier();

  gmsh::model::add(mesh);
  // read partitioned mesh
  if(getMPISize() == 1) {
    gmsh::merge(mesh + ".msh");
  }
  else {
    for(unsigned int i = 0; i < static_cast< unsigned int >(nDom); ++i) {
      if (mpi::isItMySubdomain(i)) {
        gmsh::merge(mesh + "_" + std::to_string(i + 1) + ".msh");
      }
    }
  }
  if(gmsh::model::getNumberOfPartitions() != nDom)
    msg::error << "Wrong number of partitions in mesh file " << gmsh::model::getNumberOfPartitions() << " vs. " << nDom << msg::endl;

  Domain omegaPhy = gmshfem::domain::Domain("omega");
  Domain omegaPml, omegaAPml;
  Domain fanFaceMono = gmshfem::domain::Domain("fanFace");
  Domain linerMono = gmshfem::domain::Domain("liner");
  Domain gammaExtMono;
  if (PML) {
    omegaPml = gmshfem::domain::Domain("omega_pml");
  }
  else {
    gammaExtMono = gmshfem::domain::Domain("gamma_phy");
  }
  if (Active_PML)
    omegaAPml = gmshfem::domain::Domain("omega_apml");

  Subdomain omega_phy = Subdomain::buildSubdomainOf(omegaPhy);
  Subdomain omega_apml(nDom);
  Subdomain omega_pml(nDom);
  Subdomain fanFace = Subdomain::buildSubdomainOf(fanFaceMono);
  Subdomain liner = Subdomain::buildSubdomainOf(linerMono);
  Subdomain gammaExt(nDom);
  if (PML) {
    omega_pml = Subdomain::buildSubdomainOf(omegaPml);
  }
  else {
    gammaExt = Subdomain::buildSubdomainOf(gammaExtMono);
  }
  if (Active_PML)
    omega_apml = Subdomain::buildSubdomainOf(omegaAPml);

  std::vector< std::vector< unsigned int > > topology;
  Interface sigma = Interface::buildInterface(topology);

  float pointsByWlFar = 2*pi*FEMorder / (k*h_far*meshSizeFactor);
  float pointsByWlNear = 2*pi*FEMorder / (k*h_near*meshSizeFactor);
  if (!getMPIRank()) {
    msg::info << " - dof density by wavelength (far) = " << pointsByWlFar << "" << msg::endl;
    msg::info << " - dof density by wavelength (near) = " << pointsByWlNear << "" << msg::endl;
    if(pointsByWlFar < 6 || pointsByWlNear < 6) {
      msg::warning << " - less than 6 dofs per wavelength !" << msg::endl;
    }
  }

  // Initialize annular duct mode
  std::complex<double> kx;
  double krmn;
  ScalarFunction< std::complex<double> > psi, dy_psi, dz_psi;
  getInputMode(m, n, k, 0., kx, krmn, psi, dy_psi, dz_psi, 3);
  VectorFunction< std::complex<double> > grad_psi = vector< std::complex<double> > (-im*kx*psi, dy_psi, dz_psi);
  if(!MPI_Rank)
    msg::info << " - Axial wavenumber kx = " << kx << ", radial wavenumber krmn = " << krmn << " , k0inf = " << k << msg::endl;
  // Initialize PML and active PML
  TensorFunction< std::complex< double > > J_PML_inv_T, J_PML_inv_TA;
  ScalarFunction< std::complex< double > > detJpml, detJpmlA;
  GetCylindricalPML(L_duct, delta_pml, delta_apml, k, R, D, J_PML_inv_T, J_PML_inv_TA, detJpml, detJpmlA);

  gmshddm::problem::Formulation< std::complex< double > > formulation("Helmholtz", topology);
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega_phy | omega_pml | omega_apml | liner | fanFace | gammaExt | sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
  formulation.addInterfaceField(g);

  ScalarFunction< std::complex< double > > Si=0., dS=0.;
  if ( Transmission == "Taylor0" ) {
    if(!MPI_Rank)
      msg::info << " - Use Sommerfeld transmission condition with rotation angle = " << alpha << "" << msg::endl;
    Si = im*k*exp(im*alpha/2.);
    dS = 0.;
  }
  else if ( Transmission == "Taylor2" ) {
    if(!MPI_Rank)
      msg::info << " - Use second order transmission condition with rotation angle = " << alpha << "" << msg::endl;
    Si = im*k*cos(alpha/2.);
    dS = im*exp(-im*alpha/2.) / (2.0*k);
  }

  for(auto i = 0; i < nDom; ++i) {
    // Volume weak form
    formulation(i).integral( grad(dof( u(i) )), grad(tf( u(i) )), omega_phy(i), gauss);
    formulation(i).integral(- k * k * dof( u(i) ), tf( u(i) ), omega_phy(i), gauss);

    if (PML) {
      // Pml
      formulation(i).integral( detJpml * J_PML_inv_T*grad(dof( u(i) )), J_PML_inv_T*grad(tf( u(i) )), omega_pml(i), gauss, term::ProductType::Scalar);
      formulation(i).integral(- k * k * detJpml * dof( u(i) ), tf( u(i) ), omega_pml(i), gauss);
    }
    else {
      // Sommerfeld BC
      formulation(i).integral( im * k * dof(u(i)) , tf(u(i)), gammaExt(i), gauss);
    }

    if (Active_PML) {
      // Active Pml
      formulation(i).integral( detJpmlA * J_PML_inv_TA*grad(dof(u(i))), J_PML_inv_TA*grad(tf(u(i))), omega_apml(i), gauss, term::ProductType::Scalar);
      formulation(i).integral( - detJpmlA * k * k * dof(u(i)), tf(u(i)), omega_apml(i), gauss);
      // Active PML - incident field
      formulation(i).integral(formulation.physicalSource(- detJpmlA * J_PML_inv_TA* grad_psi ), J_PML_inv_TA * grad(tf(u(i))), omega_apml(i), gauss, term::ProductType::Scalar);
      formulation(i).integral(formulation.physicalSource(+ (k*k) * detJpmlA * psi), tf(u(i)), omega_apml(i), gauss);
    }
    // Flux term
    formulation(i).integral(formulation.physicalSource(-im*kx*psi), tf(u(i)), fanFace(i), gauss);

    if (SetLiner) {
      // Myer's BC on Liner
      formulation(i).integral( im*k / Z * dof(u(i)), tf(u(i)), liner(i), gauss);
    }

    // Artificial source
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      if(!sigma(i,j).isEmpty()) {
        formulation(i).integral(formulation.artificialSource( -g(j,i) ), tf(u(i)), sigma(i,j), gauss);
        // Transmission conditions terms
        formulation(i).integral( Si * dof(u(i)), tf(u(i)), sigma(i,j), gauss);
        formulation(i).integral(- dS * grad( dof(u(i)) ), grad( tf(u(i)) ), sigma(i,j), gauss);
      }
    }
    // Interface terms
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      if(!sigma(i,j).isEmpty()) {
        formulation(i,j).integral(dof(g(i,j)), tf(g(i,j)), sigma(i,j), gauss);
        formulation(i,j).integral(formulation.artificialSource( g(j,i) ), tf(g(i,j)), sigma(i,j), gauss);
        // Transmission conditions terms
        formulation(i,j).integral(- 2. * Si * u(i) , tf(g(i,j)), sigma(i,j), gauss);
        formulation(i,j).integral( 2. * dS * grad( u(i) ), grad( tf(g(i,j)) ), sigma(i,j), gauss);
      }
    }
  }

  // Solve DDM
  double tolerence = 1e-6;
  int maxIter = 100;
  gmshDdm.userDefinedParameter(maxIter, "maxIter");
  formulation.pre();
  formulation.solve("gmres", tolerence, maxIter, true);

  // save solutions
  gmsh::option::setNumber("Mesh.Binary", 1);
  gmsh::option::setNumber("PostProcessing.Binary", 1);
  gmsh::option::setNumber("PostProcessing.SaveMesh", 0);
  for(int i = 0; i < nDom; ++i) {
    if(gmshddm::mpi::isItMySubdomain(i)) {
      int tag = save(u(i), omega_phy(i) | omega_pml(i) | omega_apml(i), "u", "", "", true, 0, 0., i + 1);
      gmsh::view::write(tag, "u_" + std::to_string(i + 1) + ".msh");

      // also save some cuts
      for(int step = 0; step < 2; step++) {
        gmsh::view::option::setNumber(tag, "TimeStep", step);
        gmsh::view::option::setNumber(tag, "AdaptVisualizationGrid", 1);
        gmsh::plugin::setNumber("CutPlane", "View", gmsh::view::getIndex(tag));
        gmsh::plugin::setNumber("CutPlane", "RecurLevel", FEMorder);
        gmsh::plugin::setNumber("CutPlane", "TargetError", -1); // 1e-4 to get decent AMR
        { // z = 0
          gmsh::plugin::setNumber("CutPlane", "A", 0);
          gmsh::plugin::setNumber("CutPlane", "B", 0);
          gmsh::plugin::setNumber("CutPlane", "C", 1);
          gmsh::plugin::setNumber("CutPlane", "D", 0);
          int tag2 = gmsh::plugin::run("CutPlane");
          std::string name2 = std::string("u_cut_z0_step") + std::to_string(step) + "_" + std::to_string(i + 1);
          gmsh::view::option::setString(tag2, "Name", name2);
          gmsh::view::write(tag2, name2 + ".pos");
        }
        { // y = 0
          gmsh::plugin::setNumber("CutPlane", "A", 0);
          gmsh::plugin::setNumber("CutPlane", "B", 1);
          gmsh::plugin::setNumber("CutPlane", "C", 0);
          gmsh::plugin::setNumber("CutPlane", "D", 0);
          int tag2 = gmsh::plugin::run("CutPlane");
          std::string name2 = std::string("u_cut_y0_step") + std::to_string(step) + "_" + std::to_string(i + 1);
          gmsh::view::option::setString(tag2, "Name", name2);
          gmsh::view::write(tag2, name2 + ".pos");
        }
        { // x = -0.39, 1.176, 2
          std::vector<double> x = {-0.39, 1.176, 2};
          for(auto d : x) {
            gmsh::plugin::setNumber("CutPlane", "A", 1);
            gmsh::plugin::setNumber("CutPlane", "B", 0);
            gmsh::plugin::setNumber("CutPlane", "C", 0);
            gmsh::plugin::setNumber("CutPlane", "D", -d);
            int tag2 = gmsh::plugin::run("CutPlane");
            std::string name2 = std::string("u_cut_x") + std::to_string(d) + "_step" + std::to_string(step) + "_" + std::to_string(i + 1);
            gmsh::view::option::setString(tag2, "Name", name2);
            gmsh::view::write(tag2, name2 + ".pos");
          }
        }
      }
    }
  }
  barrier();

  // monodomain solution
  bool ComputeMono = false;
  gmshDdm.userDefinedParameter(ComputeMono, "ComputeMono");
  if (ComputeMono) {
    gmshfem::field::Field< std::complex< double >, Form::Form0 > uMono("uMono", omegaPml | omegaPhy | omegaAPml | fanFaceMono | linerMono | gammaExtMono, FunctionSpaceTypeForm0::HierarchicalH1, FEMorder);
    gmshfem::problem::Formulation< std::complex< double > > monodomain("Helmholtz");
    if (PML) {
      monodomain.integral( detJpml * J_PML_inv_T*grad(dof(uMono)) , J_PML_inv_T*grad(tf(uMono)) , omegaPml, gauss, term::ProductType::Scalar);
      monodomain.integral(- k*k * detJpml * dof(uMono), tf(uMono), omegaPml, gauss);
    }
    else {
      monodomain.integral( im * k * dof(uMono), tf(uMono), gammaExtMono, gauss);
    }
    // Helmholz weak form
    monodomain.integral( grad(dof(uMono)) , grad(tf(uMono)) , omegaPhy, gauss);
    monodomain.integral(- k*k * dof(uMono), tf(uMono), omegaPhy, gauss);

    // Myer's BC on Liner
    if (SetLiner) {
      monodomain.integral( im*k / Z * dof(uMono), tf(uMono), linerMono, gauss);
    }

    if (Active_PML) {
      monodomain.integral( detJpmlA * J_PML_inv_TA*grad(dof(uMono)) , J_PML_inv_TA*grad(tf(uMono)) , omegaAPml, gauss, term::ProductType::Scalar);
      monodomain.integral(- detJpmlA * k * k *  dof(uMono), tf(uMono), omegaAPml, gauss);
      // Active PML - incident field
      monodomain.integral(- detJpmlA * J_PML_inv_TA * grad_psi, J_PML_inv_TA*grad(tf(uMono)) , omegaAPml, gauss, term::ProductType::Scalar);
      monodomain.integral(+ k*k * detJpmlA * psi , tf(uMono), omegaAPml, gauss);
    }
    // Flux term
    monodomain.integral(-im*kx*psi, tf(uMono), fanFaceMono, gauss);

    monodomain.pre();
    monodomain.assemble();
    msg::info << "Memory usage " << monodomain.getEstimatedFactorizationMemoryUsage() << msg::endl;
    monodomain.solve();

    // Monodomain L2 error
    double local_err;
    for(auto i = 0; i < nDom; ++i) {
      if (gmshddm::mpi::isItMySubdomain(i)) {
        std::complex< double > denLocal = integrate(pow(abs( uMono ), 2), omega_phy(i) | omega_pml(i) | omega_apml(i), gauss);
        std::complex< double > numLocal = integrate(pow(abs(uMono - u(i)), 2), omega_phy(i) | omega_pml(i) | omega_apml(i), gauss);
        local_err = sqrt(numLocal.real() / denLocal.real());
        gmshfem::msg::info << "ddm-monodomain L2 error = " << local_err << " on subdomain " << i << msg::endl;
      }
    }

  } // end Monodomain

  return 0;
}
