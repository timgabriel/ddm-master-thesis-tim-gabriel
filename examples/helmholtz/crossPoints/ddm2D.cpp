#include "ddm2D.h"

#include "mesh.h"
#include "SubproblemDomains.h"
#include "Subproblem2D.h"

#include <gmshfem/GmshFem.h>
#include <gmshfem/FieldInterface.h>
#include <gmshfem/Formulation.h>
#include <gmshfem/AnalyticalFunction.h>
#include <gmshfem/Post.h>
#include <gmshfem/Function.h>
#include <gmshfem/io.h>

#include <gmshddm/GmshDdm.h>
#include <gmshddm/Subdomain.h>
#include <gmshddm/Interface.h>
#include <gmshddm/SubdomainField.h>
#include <gmshddm/InterfaceField.h>
#include <gmshddm/Formulation.h>
#include <gmshddm/MPIInterface.h>

#include <algorithm>
#include <fstream>

using gmshfem::equation::dof;
using gmshfem::equation::tf;
using gmshfem::function::operator-;
using gmshfem::function::operator*;
using gmshfem::function::abs;

namespace D2 {


  void ddm()
  {
    gmshddm::common::GmshDdm *gmshDdm = gmshddm::common::GmshDdm::currentInstance();

    // ************************
    // P H Y S I C S
    // ************************
    double pi = 3.14159265359;
    double k = 4. * pi;
    gmshDdm->userDefinedParameter(k, "k");
    double R = 0.5;
    gmshDdm->userDefinedParameter(R, "R");
    std::string benchmark = "scattering"; // scattering, marmousi
    gmshDdm->userDefinedParameter(benchmark, "benchmark");

    // ************************
    // M E S H
    // ************************
    double lc = 0.03333333333333; // Marmousi = 10; other = 0.03333333333333
    gmshDdm->userDefinedParameter(lc, "lc");
    int meshOrder = 1;
    gmshDdm->userDefinedParameter(meshOrder, "meshOrder");

    // ************************
    // M O D E L I N G
    // ************************
    std::string boundary = "sommerfeld"; // sommerfeld, pml, habc
    gmshDdm->userDefinedParameter(boundary, "boundary");
    std::string boundaryExt = "sommerfeld"; // sommerfeld, pml, habc
    gmshDdm->userDefinedParameter(boundaryExt, "boundaryExt");
    std::string pmlMethod = "continuous"; // continuous, discontinuous, withoutMultiplier
    gmshDdm->userDefinedParameter(pmlMethod, "pmlMethod");
    std::string pmlMethodExt = "continuous"; // continuous, discontinuous, withoutMultiplier
    gmshDdm->userDefinedParameter(pmlMethodExt, "pmlMethodExt");
    std::string pmlType = "hs"; // hs, h, q
    gmshDdm->userDefinedParameter(pmlType, "pmlType");
    std::string pmlTypeExt = "hs"; // hs, h, q
    gmshDdm->userDefinedParameter(pmlTypeExt, "pmlTypeExt");
    int scatterPosX = 0, scatterPosY = 0;
    gmshDdm->userDefinedParameter(scatterPosX, "scatterPosX");
    gmshDdm->userDefinedParameter(scatterPosY, "scatterPosY");


    unsigned int nDomX = 2, nDomY = 2;
    gmshDdm->userDefinedParameter(nDomX, "nDomX");
    gmshDdm->userDefinedParameter(nDomY, "nDomY");
    unsigned int iterMax = 1000;
    gmshDdm->userDefinedParameter(iterMax, "iterMax");
    double res = 1e-6;
    gmshDdm->userDefinedParameter(res, "res");
    double sizeX = 2., sizeY = 2.;
    gmshDdm->userDefinedParameter(sizeX, "sizeX");
    gmshDdm->userDefinedParameter(sizeY, "sizeY");
    double twist = 0.;
    gmshDdm->userDefinedParameter(twist, "twist");

    unsigned int N = 6;
    gmshDdm->userDefinedParameter(N, "N");
    double pmlSize = N * lc;
    if(pmlSize == 0.) {
      pmlSize = 0.3;
    }

    unsigned int NExt = N;
    gmshDdm->userDefinedParameter(NExt, "NExt");
    double pmlSizeExt = NExt * lc;
    if(pmlSizeExt == 0.) {
      pmlSizeExt = 0.3;
    }

    int fieldOrder = 1;
    gmshDdm->userDefinedParameter(fieldOrder, "fieldOrder");
    int neumannOrder = 1;
    gmshDdm->userDefinedParameter(neumannOrder, "neumannOrder");
    std::string gauss = "Gauss" + std::to_string(2 * std::max(fieldOrder, neumannOrder) + 1);
    gmshDdm->userDefinedParameter(gauss, "gauss");
    double stab = 0.00016666666;
    gmshDdm->userDefinedParameter(stab, "stab");
    double thetaPade = 0.;
    gmshDdm->userDefinedParameter(thetaPade, "thetaPade");
    thetaPade *= pi;
    bool switchOffInteriorCrossPoints = false;
    gmshDdm->userDefinedParameter(switchOffInteriorCrossPoints, "switchOffInteriorCrossPoints");

    // ************************
    // P O S T
    // ************************
    bool monoDomainError = false;
    gmshDdm->userDefinedParameter(monoDomainError, "monoDomainError");
    bool analyticError = false;
    gmshDdm->userDefinedParameter(analyticError, "analyticError");
    std::string fileName = "none";
    gmshDdm->userDefinedParameter(fileName, "file");
    bool saveEMono = false;
    gmshDdm->userDefinedParameter(saveEMono, "saveEMono");
    bool saveUMono = false;
    gmshDdm->userDefinedParameter(saveUMono, "saveUMono");
    bool saveU = false;
    gmshDdm->userDefinedParameter(saveU, "saveU");
    bool saveE = false;
    gmshDdm->userDefinedParameter(saveE, "saveE");
    bool wavenumberPlot = false;
    gmshDdm->userDefinedParameter(wavenumberPlot, "wavenumber");
    bool meshPlot = false;
    gmshDdm->userDefinedParameter(meshPlot, "mesh");
    bool scalingPlot = false;
    gmshDdm->userDefinedParameter(scalingPlot, "scaling");
    bool saveMesh = false;
    gmshDdm->userDefinedParameter(saveMesh, "saveMesh");
    std::string mesh = "";
    gmshDdm->userDefinedParameter(mesh, "mesh");
    bool saveIteration = false;
    gmshDdm->userDefinedParameter(saveIteration, "saveIteration");

    gmsh::option::setNumber("Mesh.Binary", 1);
    gmsh::option::setNumber("Mesh.Format", 1);
    if(mesh != "") {
      gmsh::open(mesh + ".msh");
    }
    else {
      if(benchmark == "scattering") {
        if(twist == 0.) {
          checkerboard(nDomX, nDomY, sizeX, sizeY, R, lc, (boundary == "pml"), pmlSize, (boundaryExt == "pml"), pmlSizeExt, meshOrder, scatterPosX, scatterPosY);
        }
        else {
          checkerboardTwist(nDomX, nDomY, sizeX, sizeY, R, lc, (boundary == "pml"), pmlSize, (boundaryExt == "pml"), pmlSizeExt, meshOrder, twist, scatterPosX, scatterPosY);
        }
      }
      else if (benchmark == "marmousi") {
        checkerboard(nDomX, nDomY, 9192./nDomX, 2904./nDomY, 0., lc, (boundary == "pml"), pmlSize, (boundaryExt == "pml"), pmlSizeExt, meshOrder, -1, -1, true);
        sizeX = 9192./nDomX;
        sizeY = 2904./nDomY;
      }
      if(saveMesh) {
        gmsh::write("mesh.msh");
      }
    }

    gmshfem::msg::info << "Running 'ddm2D'" << gmshfem::msg::endl;
    gmshfem::msg::info << "Parameters:" << gmshfem::msg::endl;
    gmshfem::msg::info << " * physics:" << gmshfem::msg::endl;
    if(benchmark == "scattering") {
      gmshfem::msg::info << "   - k: " << k << " (" << k/pi << "*pi" << ")" << gmshfem::msg::endl;
    }
    else if (benchmark == "marmousi") {
      gmshfem::msg::info << "   - k: for Marmousi" << gmshfem::msg::endl;
    }
    gmshfem::msg::info << "   - R: " << R << gmshfem::msg::endl;
    gmshfem::msg::info << " * mesh:" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - lc: " << lc << " (eta_h = " << (2*pi/k)/lc << ")" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - meshOrder: " << meshOrder << gmshfem::msg::endl;
    gmshfem::msg::info << " * modeling:" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - grid: (" << nDomX << ", " << nDomY << ")" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - twist: " << twist*100. << "\%" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - iterMax: " << iterMax << gmshfem::msg::endl;
    gmshfem::msg::info << "   - res: " << res << gmshfem::msg::endl;
    gmshfem::msg::info << "   - subdomain size: (" << sizeX << ", " << sizeY << ")" << gmshfem::msg::endl;
    gmshfem::msg::info << "   - boundary: " << boundary << gmshfem::msg::endl;
    gmshfem::msg::info << "   - boundaryExt: " << boundaryExt << gmshfem::msg::endl;
    if(boundary == "pml") {
      gmshfem::msg::info << "   - N: " << N << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlSize: " << pmlSize << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlType: " << pmlType << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlMethod: " << pmlMethod << gmshfem::msg::endl;
    }
    else if(boundary == "habc") {
      gmshfem::msg::info << "   - N: " << N << gmshfem::msg::endl;
      gmshfem::msg::info << "   - thetaPade: " << thetaPade << " (" << thetaPade/pi << "*pi" << ")" << gmshfem::msg::endl;
    }
    if(boundaryExt == "pml") {
      gmshfem::msg::info << "   - NExt: " << NExt << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlSizeExt: " << pmlSizeExt << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlTypeExt: " << pmlTypeExt << gmshfem::msg::endl;
      gmshfem::msg::info << "   - pmlMethodExt: " << pmlMethodExt << gmshfem::msg::endl;
    }
    else if(boundary == "habc") {
      gmshfem::msg::info << "   - N: " << NExt << gmshfem::msg::endl;
    }
    gmshfem::msg::info << "   - gauss: " << gauss << gmshfem::msg::endl;
    gmshfem::msg::info << "   - fieldOrder: " << fieldOrder << gmshfem::msg::endl;
    gmshfem::msg::info << "   - neumannOrder: " << neumannOrder << gmshfem::msg::endl;
    gmshfem::msg::info << "   - interior cross-points: " <<  (switchOffInteriorCrossPoints ? "switch off" : "switch on") << gmshfem::msg::endl;
    gmshfem::msg::info << "   - stab: " << stab << " * lc (= " << stab * lc << ")" << gmshfem::msg::endl;
    

    // source
    gmshfem::analytics::AnalyticalFunction< gmshfem::analytics::helmholtz2D::ScatteringByASoftCylinder< std::complex< double > > > fAnalytic(k, R, 1., 1., 2 * k);

    const unsigned int nDom = nDomX * nDomY;
    // Define domain
    gmshddm::domain::Subdomain omega(nDom);
    gmshfem::domain::Domain gamma;

    std::vector< gmshddm::domain::Subdomain > pml(4, nDom);
    std::vector< gmshddm::domain::Subdomain > pmlCorner(4, nDom);
    std::vector< gmshddm::domain::Subdomain > sigma(4, nDom);
    std::vector< gmshddm::domain::Interface > sigmaInterface(4, nDom);
    std::vector< std::pair< gmshddm::domain::Subdomain, gmshddm::domain::Subdomain > > pmlBnd(4, std::make_pair(gmshddm::domain::Subdomain(nDom), gmshddm::domain::Subdomain(nDom)) );
    std::vector< std::pair< gmshddm::domain::Interface, gmshddm::domain::Interface > > pmlBndInterface(4, std::make_pair(gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom)));

    std::vector< gmshddm::domain::Subdomain > corner(4, nDom);
    std::vector< std::pair< gmshddm::domain::Interface, gmshddm::domain::Interface > > cornerInterface(4, std::make_pair(gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom)));
    // Define topology
    std::vector< std::vector< unsigned int > > topology(nDom);
    std::vector< std::string > dir {"E", "N", "W", "S"};
    for(unsigned int i = 0; i < static_cast< unsigned int >(nDomX); ++i) {
      for(unsigned int j = 0; j < static_cast< unsigned int >(nDomY); ++j) {
        unsigned int index = i * nDomY + j;

        const std::string subdomainTag = "_" + std::to_string(i) + "_" + std::to_string(j);
        omega(index) = gmshfem::domain::Domain("omega" + subdomainTag);

        for(unsigned int b = 0; b < 4; ++b) {
          if(boundary == "pml" || boundaryExt == "pml") {
            pml[b](index) = gmshfem::domain::Domain("pml" + dir[b] + subdomainTag);
          }
          sigma[b](index) = gmshfem::domain::Domain("sigma" + dir[b] + subdomainTag);
        }
        
        for(unsigned int c = 0; c < 4; ++c) {
          if(boundary == "pml" || boundaryExt == "pml") {
            pmlCorner[c](index) = gmshfem::domain::Domain("pmlCorner" + dir[c] + dir[(c+1)%4] + subdomainTag);
            pmlBnd[c].first(index) = gmshfem::domain::Domain("pmlBnd" + dir[c] + "_second" + subdomainTag);
            pmlBnd[c].second(index) = gmshfem::domain::Domain("pmlBnd" + dir[(c+1)%4] + "_first" + subdomainTag);
          }
          corner[c](index) = gmshfem::domain::Domain("corner" + dir[c] + dir[(c+1)%4] + subdomainTag);
        }

        if(i != static_cast< unsigned int >(nDomX) - 1) { // E
          if(boundary == "pml" || boundaryExt == "pml") {
            pmlBndInterface[3].first(index, (i+1) * nDomY + j) = pmlBnd[3].first(index);
            pmlBndInterface[0].second(index, (i+1) * nDomY + j) = pmlBnd[0].second(index);
          }
          sigmaInterface[0](index, (i+1) * nDomY + j) = sigma[0](index);
          
          cornerInterface[3].first(index, (i+1) * nDomY + j) = corner[3](index);
          cornerInterface[0].second(index, (i+1) * nDomY + j) = corner[0](index);
          
          topology[index].push_back((i+1) * nDomY + j);
        }

        if(j != static_cast< unsigned int >(nDomY) - 1) { // N
          if(boundary == "pml" || boundaryExt == "pml") {
            pmlBndInterface[0].first(index, i * nDomY + (j+1)) = pmlBnd[0].first(index);
            pmlBndInterface[1].second(index, i * nDomY + (j+1)) = pmlBnd[1].second(index);
          }
          sigmaInterface[1](index, i * nDomY + (j+1)) = sigma[1](index);
          
          cornerInterface[0].first(index, i * nDomY + (j+1)) = corner[0](index);
          cornerInterface[1].second(index, i * nDomY + (j+1)) = corner[1](index);

          topology[index].push_back(i * nDomY + (j+1));
        }

        if(i != 0) { // W
          if(boundary == "pml" || boundaryExt == "pml") {
            pmlBndInterface[1].first(index, (i-1) * nDomY + j) = pmlBnd[1].first(index);
            pmlBndInterface[2].second(index, (i-1) * nDomY + j) = pmlBnd[2].second(index);
          }
          sigmaInterface[2](index, (i-1) * nDomY + j) = sigma[2](index);
          
          cornerInterface[1].first(index, (i-1) * nDomY + j) = corner[1](index);
          cornerInterface[2].second(index, (i-1) * nDomY + j) = corner[2](index);

          topology[index].push_back((i-1) * nDomY + j);
        }

        if(j != 0) { // S
          if(boundary == "pml" || boundaryExt == "pml") {
            pmlBndInterface[2].first(index, i * nDomY + (j-1)) = pmlBnd[2].first(index);
            pmlBndInterface[3].second(index, i * nDomY + (j-1)) = pmlBnd[3].second(index);
          }
          sigmaInterface[3](index, i * nDomY + (j-1)) = sigma[3](index);
          
          cornerInterface[2].first(index, i * nDomY + (j-1)) = corner[2](index);
          cornerInterface[3].second(index, i * nDomY + (j-1)) = corner[3](index);

          topology[index].push_back(i * nDomY + (j-1));
        }
      }
    }
    
    if(benchmark == "scattering") {
      gamma = gmshfem::domain::Domain("gamma");
    }
    else if (benchmark == "marmousi") {
      gamma = corner[0](3) |
              corner[0](7) |
              corner[0](11) |
              corner[0](15) |
              corner[0](19) |
              corner[0](23) |
              corner[0](27) |
              corner[0](31) |
              corner[0](35);
    }

    // Kappa definition
    gmshfem::function::ScalarFunction< std::complex< double > > kappa;
    std::vector< double > x, y;
    std::vector< std::vector< std::complex< double > > > data;
    if(benchmark == "scattering") {
      kappa = k;
    }
    else if (benchmark == "marmousi") {
      // Read the velocity map
      std::ifstream file("../marmousi.dat", std::ios::binary);

      unsigned int Nx = 0, Ny = 0;
      file.read((char *) &Nx, sizeof(unsigned int));
      file.read((char *) &Ny, sizeof(unsigned int));
      gmshfem::msg::info << "Reading: Nx = " << Nx << ", Ny = " << Ny << "." << gmshfem::msg::endl;

      x.resize(Nx);
      y.resize(Ny);
      file.read((char *) &x[0], Nx * sizeof(double));
      file.read((char *) &y[0], Ny * sizeof(double));
      for(unsigned int i = 0; i < Ny; ++i) {
        y[i] += 2904.;
      }
      // Hack to avoid problem at the boundaries
      x[0] -= 1e-3;
      y[0] -= 1e-3;
      x[Nx-1] += 1e-3;
      y[Ny-1] += 1e-3;

      data.resize(Nx, std::vector< std::complex< double > >(Ny));
      std::vector< double > dataRaw(Nx * Ny);
      file.read((char *) &dataRaw[0], Nx * Ny * sizeof(double));
      for(unsigned int j = 0; j < Ny; ++j) {
        for(unsigned int i = 0; i < Nx; ++i) {
          data[i][j] = dataRaw[j * Nx + i];
        }
      }
      file.close();
      
      const double min = *std::min_element(dataRaw.begin(), dataRaw.end());
      const double max = *std::max_element(dataRaw.begin(), dataRaw.end());
      gmshfem::msg::info << "min = " << min << ", max = " << max << gmshfem::msg::endl;

      kappa = 2. * pi * 100. / gmshfem::function::bilinearInterpolation< std::complex< double > >(&x, &y, &data);
      for(unsigned int k = 0; k < nDomX * nDomY; ++k) {
        if(gmshddm::mpi::isItMySubdomain(k)) {
          gmshfem::post::save(kappa, omega(k), "k_" + std::to_string(k), "msh");
        }
      }
    }

    // Fields definition
    gmshddm::field::SubdomainField< std::complex< double >, gmshfem::field::Form::Form0 > u("u", omega | sigma[0] | sigma[1] | sigma[2] | sigma[3], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    std::vector< gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 > * > gContinuous;
    std::vector< gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form2 > * > gDiscontinuous;
    for(unsigned int f = 0; f < 4; ++f) {
      gContinuous.push_back(new gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 >("gC_" + dir[f], sigmaInterface[f], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder));
      gDiscontinuous.push_back(new gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form2 >("gD_" + dir[f], sigmaInterface[f], gmshfem::field::FunctionSpaceTypeForm2::P_HierarchicalHCurl, neumannOrder));
    }

    std::vector< std::pair< gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 > *, gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 > * > > gCornerPmlContinuous;
    std::vector< std::pair< gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form2 > *, gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form2 > * > > gCornerPmlDiscontinuous;
    std::vector< std::vector< std::pair< gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 > *, gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 > * > > > gCornerHABC;
    for(unsigned int f = 0; f < 4; ++f) {
      gCornerPmlContinuous.push_back(std::make_pair(
        new gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 >("gCornerPmlContinuous_" + dir[f] + dir[(f+1)%4] + "_first", pmlBndInterface[f].first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
        new gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 >("gCornerPmlContinuous_" + dir[f] + dir[(f+1)%4] + "_second", pmlBndInterface[f].second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)));
      gCornerPmlDiscontinuous.push_back(std::make_pair(
        new gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form2 >("gCornerPmlDiscontinuous_" + dir[f] + dir[(f+1)%4] + "_first", pmlBndInterface[f].first, gmshfem::field::FunctionSpaceTypeForm2::P_HierarchicalHCurl, neumannOrder),
        new gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form2 >("gCornerPmlDiscontinuous_" + dir[f] + dir[(f+1)%4] + "_second", pmlBndInterface[f].second, gmshfem::field::FunctionSpaceTypeForm2::P_HierarchicalHCurl, neumannOrder)));
      gCornerHABC.push_back(std::vector< std::pair< gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 > *, gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 > * > >(std::max(N, NExt)));
      for(unsigned int n = 0; n < std::max(N, NExt); ++n) {
        gCornerHABC[f][n] = std::make_pair(
          new gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 >("gCornerHABC_" + dir[f] + dir[(f+1)%4] + "_" + std::to_string(n) + "_first", cornerInterface[f].first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder),
          new gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 >("gCornerHABC_" + dir[f] + dir[(f+1)%4] + "_" + std::to_string(n) + "_second", cornerInterface[f].second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder));
      }
    }
    
    gmshddm::problem::Formulation< std::complex< double > > formulation("HelmholtzDDMScattering", topology);

    for(unsigned int f = 0; f < 4; ++f) {
      formulation.addInterfaceField(*gContinuous[f], *gContinuous[(f+2)%4]);
      formulation.addInterfaceField(*gDiscontinuous[f], *gDiscontinuous[(f+2)%4]);

      // Pmls
      formulation.addInterfaceField(*gCornerPmlContinuous[f].first, *gCornerPmlContinuous[(f-1)%4].second);
      formulation.addInterfaceField(*gCornerPmlContinuous[f].second, *gCornerPmlContinuous[(f+1)%4].first);
      formulation.addInterfaceField(*gCornerPmlDiscontinuous[f].first, *gCornerPmlDiscontinuous[(f-1)%4].second);
      formulation.addInterfaceField(*gCornerPmlDiscontinuous[f].second, *gCornerPmlDiscontinuous[(f+1)%4].first);

      // HABC
      for(unsigned int n = 0; n < std::max(N, NExt); ++n) {
        formulation.addInterfaceField(*gCornerHABC[f][n].first, *gCornerHABC[(f-1)%4][n].second);
        formulation.addInterfaceField(*gCornerHABC[f][n].second, *gCornerHABC[(f+1)%4][n].first);
      }
    }
    
    if(benchmark == "scattering") {
      u(scatterPosX * nDomY + scatterPosY).addConstraint(gamma, /*fAnalytic*/ 1.);
    }
    else if (benchmark == "marmousi") {
      u(3).addConstraint(corner[0](3), 1.);
      u(7).addConstraint(corner[0](7), 1.);
      u(11).addConstraint(corner[0](11), 1.);
      u(15).addConstraint(corner[0](15), 1.);
      u(19).addConstraint(corner[0](19), 1.);
      u(23).addConstraint(corner[0](23), 1.);
      u(27).addConstraint(corner[0](27), 1.);
      u(31).addConstraint(corner[0](31), 1.);
      u(35).addConstraint(corner[0](35), 1.);
    }
    
    std::vector< std::vector< Subproblem * > > subproblem(nDomX);
    for(unsigned int i = 0; i < nDomX; ++i) {
      subproblem[i].resize(nDomY);
      for(unsigned int j = 0; j < nDomY; ++j) {
        unsigned int index = i * nDomY + j;
        formulation(index).integral(-grad(dof(u(index))), grad(tf(u(index))), omega(index), gauss);
        formulation(index).integral(kappa * kappa * dof(u(index)), tf(u(index)), omega(index), gauss);
        
        SubproblemDomains domains;
        domains.setOmega(omega(index));
        domains.setSigma({ sigma[0](index), sigma[1](index), sigma[2](index), sigma[3](index) });
        domains.setPml({ pml[0](index), pml[1](index), pml[2](index), pml[3](index) });
        domains.setPmlBnd({ std::make_pair(pmlBnd[0].first(index), pmlBnd[0].second(index)), std::make_pair(pmlBnd[1].first(index), pmlBnd[1].second(index)), std::make_pair(pmlBnd[2].first(index), pmlBnd[2].second(index)), std::make_pair(pmlBnd[3].first(index), pmlBnd[3].second(index)) });
        domains.setPmlCorner({ pmlCorner[0](index), pmlCorner[1](index), pmlCorner[2](index), pmlCorner[3](index) });
        domains.setCorner({ corner[0](index), corner[1](index), corner[2](index), corner[3](index) });
        
        SubproblemParameters parameters;
        parameters.setGauss(gauss);
        parameters.setKappa(kappa);
        parameters.setNeumannOrder(neumannOrder);
        parameters.setFieldOrder(fieldOrder);
        parameters.setStab(stab * lc * lc);
        
        std::string bnd = boundary;
        if(boundary == "habc") {
          bnd += "_" + std::to_string(N) + "_" + std::to_string(thetaPade);
        }
        else if(boundary == "pml") {
          if(pmlMethod == "continuous") {
            bnd += "Continuous_" + std::to_string(pmlSize) + "_" + pmlType;
          }
          else if(pmlMethod == "discontinuous") {
            bnd += "Discontinuous_" + std::to_string(pmlSize) + "_" + pmlType;
          }
          else if(pmlMethod == "withoutMultiplier") {
            bnd += "WithoutMultiplier_" + std::to_string(pmlSize) + "_" + pmlType;
          }
        }
        std::string bndExt = boundaryExt;
        if(boundaryExt == "habc") {
          bndExt += "_" + std::to_string(NExt) + "_" + std::to_string(thetaPade);
        }
        else if(boundaryExt == "pml") {
          if(pmlMethodExt == "continuous") {
            bndExt += "Continuous_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
          }
          else if(pmlMethodExt == "discontinuous") {
            bndExt += "Discontinuous_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
          }
          else if(pmlMethodExt == "withoutMultiplier") {
            bndExt += "WithoutMultiplier_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
          }
        }
        gmshfem::msg::info << "Subdomain (" << i << ", " << j << ") has boundaries [" << (i == nDomX-1 ? bndExt : bnd) << ", " << (j == nDomY-1 ? bndExt : bnd) << ", " << (i == 0 ? bndExt : bnd) << ", " << (j == 0 ? bndExt : bnd) << "]" << gmshfem::msg::endl;
        std::vector< unsigned int > activatedCrossPoints = {0, 1, 2, 3};
        if(switchOffInteriorCrossPoints) {
          activatedCrossPoints.clear();
          if(i == nDomX-1 && j == nDomY-1) {
            activatedCrossPoints.push_back(0);
          }
          if(i == 0 && j == nDomY-1) {
            activatedCrossPoints.push_back(1);
          }
          if(i == 0 && j == 0) {
            activatedCrossPoints.push_back(2);
          }
          if(i == nDomX-1 && j == 0) {
            activatedCrossPoints.push_back(3);
          }
        }
        subproblem[i][j] = new Subproblem(formulation(index), u(index).name(), domains, parameters, (i == nDomX-1 ? bndExt : bnd), (j == nDomY-1 ? bndExt : bnd), (i == 0 ? bndExt : bnd), (j == 0 ? bndExt : bnd), activatedCrossPoints);
        subproblem[i][j]->writeFormulation();
        
        unsigned int Ns[4] = {N, N, N, N};
        if(i == nDomX-1) {
          Ns[0] = NExt;
        }
        if(j == nDomY-1) {
          Ns[1] = NExt;
        }
        if(i == 0) {
          Ns[2] = NExt;
        }
        if(j == 0) {
          Ns[3] = NExt;
        }
        
        // coupling
        for(unsigned int b = 0; b < 4; ++b) {
          for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
            const unsigned int jndex = topology[index][jj];
            
            Boundary *boundary = subproblem[i][j]->getBoundary(b);
            if(dynamic_cast< Sommerfeld * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gContinuous[(b+2)%4])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(dynamic_cast< HABC * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gContinuous[(b+2)%4])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(dynamic_cast< PmlContinuous * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gContinuous[(b+2)%4])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(dynamic_cast< PmlDiscontinuous * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- gmshfem::function::normal< std::complex< double > >() * (*gDiscontinuous[(b+2)%4])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(dynamic_cast< PmlWithoutMultiplier * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gContinuous[(b+2)%4])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
          }
        }
        
        for(unsigned int c = 0; c < 4; ++c) {
          for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
            const unsigned int jndex = topology[index][jj];
            
            Corner *corner = subproblem[i][j]->getCorner(c);
            if(corner == nullptr) {
              continue;
            }
            if(auto cr = dynamic_cast< HABC_HABC * >(corner)) {
              if(!cornerInterface[c].first(index, jndex).isEmpty()) {
                for(unsigned int n = 0; n < Ns[c]; ++n) {
                  auto termId = formulation(index).integral(- (*gCornerHABC[(c-1)%4][n].second)(jndex, index), tf(*cr->firstBoundary()->getUHABC(n)), cornerInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
              if(!cornerInterface[c].second(index, jndex).isEmpty()) {
                for(unsigned int n = 0; n < Ns[(c+1)%4]; ++n) {
                  auto termId = formulation(index).integral(- (*gCornerHABC[(c+1)%4][n].first)(jndex, index), tf(*cr->secondBoundary()->getUHABC(n)), cornerInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuous_PmlContinuous * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< PmlDiscontinuous_PmlDiscontinuous * >(corner)) {
              gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- n * (*gCornerPmlDiscontinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- n * (*gCornerPmlDiscontinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< PmlWithoutMultiplier_PmlWithoutMultiplier * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuous_Sommerfeld * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< Sommerfeld_PmlContinuous * >(corner)) {
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- (*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< PmlDiscontinuous_Sommerfeld * >(corner)) {
              gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- n * (*gCornerPmlDiscontinuous[(c-1)%4].second)(jndex, index), tf(*cr->firstBoundary()->getUPml()), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< Sommerfeld_PmlDiscontinuous * >(corner)) {
              gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                auto termId = formulation(index).integral(- n * (*gCornerPmlDiscontinuous[(c+1)%4].first)(jndex, index), tf(*cr->secondBoundary()->getUPml()), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
              }
            }
            else if(auto cr = dynamic_cast< HABC_Sommerfeld * >(corner)) {
              if(!cornerInterface[c].first(index, jndex).isEmpty()) {
                for(unsigned int n = 0; n < Ns[c]; ++n) {
                  auto termId = formulation(index).integral(- (*gCornerHABC[(c-1)%4][n].second)(jndex, index), tf(*cr->firstBoundary()->getUHABC(n)), cornerInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
            }
            else if(auto cr = dynamic_cast< Sommerfeld_HABC * >(corner)) {
              if(!cornerInterface[c].second(index, jndex).isEmpty()) {
                for(unsigned int n = 0; n < Ns[(c+1)%4]; ++n) {
                  auto termId = formulation(index).integral(- (*gCornerHABC[(c+1)%4][n].first)(jndex, index), tf(*cr->secondBoundary()->getUHABC(n)), cornerInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                }
              }
            }
          }
        }
        
        // interface
        for(unsigned int b = 0; b < 4; ++b) {
          for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
            const unsigned int jndex = topology[index][jj];
            
            Boundary *boundary = subproblem[i][j]->getBoundary(b);
            if(auto bnd = dynamic_cast< Sommerfeld * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gContinuous[(b+2)%4])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
              }
            }
            else if(auto bnd = dynamic_cast< HABC * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gContinuous[(b+2)%4])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
              }
            }
            else if(auto bnd = dynamic_cast< PmlContinuous * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gContinuous[(b+2)%4])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
              }
            }
            else if(auto bnd = dynamic_cast< PmlDiscontinuous * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gDiscontinuous[b])(index, jndex)), tf((*gDiscontinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gDiscontinuous[(b+2)%4])(jndex, index), tf((*gDiscontinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gDiscontinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
              }
            }
            else if(auto bnd = dynamic_cast< PmlWithoutMultiplier * >(boundary)) {
              if(!sigmaInterface[b](index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gContinuous[(b+2)%4])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
              }
            }
          }
        }
        
        for(unsigned int c = 0; c < 4; ++c) {
          for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
            const unsigned int jndex = topology[index][jj];
            
            Corner *corner = subproblem[i][j]->getCorner(c);
            if(corner == nullptr) {
              continue;
            }
            if(auto cr = dynamic_cast< HABC_HABC * >(corner)) {
              if(!cornerInterface[c].first(index, jndex).isEmpty()) {
                for(unsigned int n = 0; n < Ns[c]; ++n) {
                  formulation(index, jndex).integral(dof((*gCornerHABC[c][n].first)(index, jndex)), tf((*gCornerHABC[c][n].first)(index, jndex)), cornerInterface[c].first(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gCornerHABC[(c-1)%4][n].second)(jndex, index), tf((*gCornerHABC[c][n].first)(index, jndex)), cornerInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2. * *cr->getV(n, 0), tf((*gCornerHABC[c][n].first)(index, jndex)), cornerInterface[c].first(index, jndex), gauss);
                }
              }
              if(!cornerInterface[c].second(index, jndex).isEmpty()) {
                for(unsigned int n = 0; n < Ns[(c+1)%4]; ++n) {
                  formulation(index, jndex).integral(dof((*gCornerHABC[c][n].second)(index, jndex)), tf((*gCornerHABC[c][n].second)(index, jndex)), cornerInterface[c].second(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gCornerHABC[(c+1)%4][n].first)(jndex, index), tf((*gCornerHABC[c][n].second)(index, jndex)), cornerInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2. * *cr->getV(n, 1), tf((*gCornerHABC[c][n].second)(index, jndex)), cornerInterface[c].second(index, jndex), gauss);
                }
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuous_PmlContinuous * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].first)(index, jndex)), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(0), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].second)(index, jndex)), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(1), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< PmlDiscontinuous_PmlDiscontinuous * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlDiscontinuous[c].first)(index, jndex)), tf((*gCornerPmlDiscontinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlDiscontinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlDiscontinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(0), tf((*gCornerPmlDiscontinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlDiscontinuous[c].second)(index, jndex)), tf((*gCornerPmlDiscontinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlDiscontinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlDiscontinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(1), tf((*gCornerPmlDiscontinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< PmlWithoutMultiplier_PmlWithoutMultiplier * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].first)(index, jndex)), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(0), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
              }
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].second)(index, jndex)), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(1), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< PmlContinuous_Sommerfeld * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].first)(index, jndex)), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(), tf((*gCornerPmlContinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< Sommerfeld_PmlContinuous * >(corner)) {
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlContinuous[c].second)(index, jndex)), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlContinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(), tf((*gCornerPmlContinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< PmlDiscontinuous_Sommerfeld * >(corner)) {
              if(!pmlBndInterface[c].first(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlDiscontinuous[c].first)(index, jndex)), tf((*gCornerPmlDiscontinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlDiscontinuous[(c-1)%4].second)(jndex, index), tf((*gCornerPmlDiscontinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(), tf((*gCornerPmlDiscontinuous[c].first)(index, jndex)), pmlBndInterface[c].first(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< Sommerfeld_PmlDiscontinuous * >(corner)) {
              if(!pmlBndInterface[c].second(index, jndex).isEmpty()) {
                formulation(index, jndex).integral(dof((*gCornerPmlDiscontinuous[c].second)(index, jndex)), tf((*gCornerPmlDiscontinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                auto termId = formulation(index, jndex).integral((*gCornerPmlDiscontinuous[(c+1)%4].first)(jndex, index), tf((*gCornerPmlDiscontinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
                formulation.artificialSourceTerm(termId);
                formulation(index, jndex).integral(2. * *cr->getV(), tf((*gCornerPmlDiscontinuous[c].second)(index, jndex)), pmlBndInterface[c].second(index, jndex), gauss);
              }
            }
            else if(auto cr = dynamic_cast< HABC_Sommerfeld * >(corner)) {
              if(!cornerInterface[c].first(index, jndex).isEmpty()) {
                for(unsigned int n = 0; n < Ns[c]; ++n) {
                  formulation(index, jndex).integral(dof((*gCornerHABC[c][n].first)(index, jndex)), tf((*gCornerHABC[c][n].first)(index, jndex)), cornerInterface[c].first(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gCornerHABC[(c-1)%4][n].second)(jndex, index), tf((*gCornerHABC[c][n].first)(index, jndex)), cornerInterface[c].first(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2. * *cr->getV(n), tf((*gCornerHABC[c][n].first)(index, jndex)), cornerInterface[c].first(index, jndex), gauss);
                }
              }
            }
            else if(auto cr = dynamic_cast< Sommerfeld_HABC * >(corner)) {
              if(!cornerInterface[c].second(index, jndex).isEmpty()) {
                for(unsigned int n = 0; n < Ns[(c+1)%4]; ++n) {
                  formulation(index, jndex).integral(dof((*gCornerHABC[c][n].second)(index, jndex)), tf((*gCornerHABC[c][n].second)(index, jndex)), cornerInterface[c].second(index, jndex), gauss);
                  auto termId = formulation(index, jndex).integral((*gCornerHABC[(c+1)%4][n].first)(jndex, index), tf((*gCornerHABC[c][n].second)(index, jndex)), cornerInterface[c].second(index, jndex), gauss);
                  formulation.artificialSourceTerm(termId);
                  formulation(index, jndex).integral(2. * *cr->getV(n), tf((*gCornerHABC[c][n].second)(index, jndex)), cornerInterface[c].second(index, jndex), gauss);
                }
              }
            }
          }
        }
      }
    }
    
    gmshfem::common::Timer pre = formulation.pre();
    gmshfem::common::Timer solve = formulation.solve("gmres", res, iterMax, false, false);
    
//    {
//      Boundary *boundary = subproblem[0][0]->getBoundary(0);
//      auto bnd = dynamic_cast< PmlWithoutMultiplier * >(boundary);
//      gmshfem::post::save(*bnd->getV(), bnd->getV()->domain(), bnd->getV()->name(), "msh");
//    }
//    {
//      Boundary *boundary = subproblem[1][0]->getBoundary(1);
//      auto bnd = dynamic_cast< PmlContinuous * >(boundary);
//      gmshfem::post::save(*bnd->getUPml(), bnd->getUPml()->domain(), bnd->getUPml()->name(), "msh");
//    }
//    {
//      Corner *corner = subproblem[0][0]->getCorner(0);
//      auto cr = dynamic_cast< Sommerfeld_PmlContinuous * >(corner);
//      gmshfem::post::save(*cr->getV(), cr->getV()->domain(), cr->getV()->name(), "msh");
//    }
//
//    gmshfem::post::save((*gContinuous[2])(1, 0), (*gContinuous[2])(1, 0).domain(), (*gContinuous[2])(1, 0).name(), "msh");
//    gmshfem::post::save((*gContinuous[0])(0, 1), (*gContinuous[0])(0, 1).domain(), (*gContinuous[0])(0, 1).name(), "msh");
//
//    gmshfem::post::save((*gCornerPmlContinuous[2].second)(1, 0), (*gCornerPmlContinuous[2].second)(1, 0).domain(), (*gCornerPmlContinuous[2].second)(1, 0).name(), "msh");
//    gmshfem::post::save((*gCornerPmlContinuous[0].second)(0, 1), (*gCornerPmlContinuous[0].second)(0, 1).domain(), (*gCornerPmlContinuous[0].second)(0, 1).name(), "msh");

    if(saveIteration) {
      gmshfem::common::CSVio file(fileName, ';', gmshfem::common::OpeningMode::Append);
      file << nDomX << nDomY << formulation.numberOfIterations() << gmshfem::csv::endl;
      file.close();
    }
    
    for(unsigned int i = 0; i < nDomX; ++i) {
      for(unsigned int j = 0; j < nDomY; ++j) {
        unsigned int index = i * nDomY + j;
        if(gmshddm::mpi::isItMySubdomain(index)) {
          if(saveU) {
            gmshfem::post::save(u(index), omega(index), "u_" + std::to_string(index), "msh");
          }
        }
      }
    }
    
    if(monoDomainError) {
      gmshfem::problem::Formulation< std::complex< double > > formulationMono("HelmholtzMonoDomain");

      std::vector< gmshfem::domain::Domain > sigmaMono(4);
      std::vector< gmshfem::domain::Domain > pmlMono(4);
      std::vector< gmshfem::domain::Domain > pmlCornerMono(4);
      std::vector< std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > > pmlBndMono(4);
      std::vector< gmshfem::domain::Domain > cornerMono(4);
      for(unsigned int i = 0; i < nDomX; ++i) {
        sigmaMono[1] |= sigma[1](i * nDomY + nDomY - 1);
        sigmaMono[3] |= sigma[3](i * nDomY);

        pmlMono[1] |= pml[1](i * nDomY + nDomY - 1);
        pmlMono[3] |= pml[3](i * nDomY);
      }

      for(unsigned int i = 0; i < nDomY; ++i) {
        sigmaMono[0] |= sigma[0](i + (nDomX - 1) * nDomY);
        sigmaMono[2] |= sigma[2](i);

        pmlMono[0] |= pml[0](i + (nDomX - 1) * nDomY);
        pmlMono[2] |= pml[2](i);
      }

      pmlCornerMono[0] = pmlCorner[0](nDomX * nDomY - 1);
      pmlCornerMono[1] = pmlCorner[1](nDomY - 1);
      pmlCornerMono[2] = pmlCorner[2](0);
      pmlCornerMono[3] = pmlCorner[3]((nDomX - 1) * nDomY);

      pmlBndMono[0] = std::make_pair( pmlBnd[0].first(nDomX * nDomY - 1), pmlBnd[0].second(nDomX * nDomY - 1) );
      pmlBndMono[1] = std::make_pair( pmlBnd[1].first(nDomY - 1), pmlBnd[1].second(nDomY - 1) );
      pmlBndMono[2] = std::make_pair( pmlBnd[2].first(0), pmlBnd[2].second(0) );
      pmlBndMono[3] = std::make_pair( pmlBnd[3].first((nDomX - 1) * nDomY), pmlBnd[3].second((nDomX - 1) * nDomY) );

      cornerMono[0] = corner[0](nDomX * nDomY - 1);
      cornerMono[1] = corner[1](nDomY - 1);
      cornerMono[2] = corner[2](0);
      cornerMono[3] = corner[3]((nDomX - 1) * nDomY);

      gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > uMono("uMono", omega.getUnion(), gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

      formulationMono.integral(-grad(dof(uMono)), grad(tf(uMono)), omega.getUnion(), gauss);
      formulationMono.integral(kappa * kappa * dof(uMono), tf(uMono), omega.getUnion(), gauss);

      if(benchmark == "scattering") {
        uMono.addConstraint(gamma, fAnalytic);
      }
      else if (benchmark == "marmousi") {
        uMono.addConstraint(gamma, 1.);
      }
      
      SubproblemDomains domains;
      domains.setOmega(omega.getUnion());
      domains.setSigma(sigmaMono);
      domains.setPml(pmlMono);
      domains.setPmlBnd(pmlBndMono);
      domains.setPmlCorner(pmlCornerMono);
      domains.setCorner(cornerMono);
          
      SubproblemParameters parameters;
      parameters.setGauss(gauss);
      parameters.setKappa(kappa);
      parameters.setNeumannOrder(neumannOrder);
      parameters.setFieldOrder(fieldOrder);
      parameters.setStab(stab * lc);
      
      std::string bndExt = boundaryExt;
      if(boundaryExt == "habc") {
        bndExt += "_" + std::to_string(NExt) + "_" + std::to_string(thetaPade);
      }
      else if(boundaryExt == "pml") {
        if(pmlMethodExt == "continuous") {
          bndExt += "Continuous_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
        }
        else if(pmlMethodExt == "discontinuous") {
          bndExt += "Discontinuous_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
        }
        else if(pmlMethodExt == "withoutMultiplier") {
          bndExt += "WithoutMultiplier_" + std::to_string(pmlSizeExt) + "_" + pmlTypeExt;
        }
      }
      gmshfem::msg::info << "Monodomain has boundaries [" << bndExt << ", " << bndExt << ", " << bndExt << ", " << bndExt << "]" << gmshfem::msg::endl;
      Subproblem subproblem(formulationMono, uMono.name(), domains, parameters, bndExt, bndExt, bndExt, bndExt, {0,1,2,3});
      subproblem.writeFormulation();
        
      formulationMono.pre();
      formulationMono.assemble();
      formulationMono.solve();

      double num = 0.;
      double den = 0.;
      for(unsigned int i = 0; i < nDomX * nDomY; ++i) {
        num += std::real(gmshfem::post::integrate(pow(abs(u(i) - uMono), 2), omega(i), gauss));
        den += std::real(gmshfem::post::integrate(pow(abs(uMono), 2), omega(i), gauss));
      }

      gmshfem::msg::info << "Error mono L2 = " << std::sqrt(num/den) << gmshfem::msg::endl;

      for(unsigned int i = 0; i < nDomX * nDomY; ++i) {
        if(gmshddm::mpi::isItMySubdomain(i)) {
          if(saveEMono) {
            gmshfem::post::save(u(i) - uMono, omega(i), "eMono_" + std::to_string(i));
          }
          if(saveUMono) {
            gmshfem::post::save(uMono, omega(i), "uMono_" + std::to_string(i));
          }
        }
      }
      
      if(fileName != "none") {
        gmshfem::common::CSVio file(fileName, ';', gmshfem::common::OpeningMode::Append);
//        file << iterMax << formulation.relativeResidual().back() << std::sqrt(num/den) << gmshfem::csv::endl;
        file.close();
      }
    }
    
    if(analyticError) {
      double num = 0., den = 0.;
      for(unsigned int i = 0; i < nDomX * nDomY; ++i) {
        if(gmshddm::mpi::isItMySubdomain(i)) {
          num += std::real(gmshfem::post::integrate(pow(abs(u(i) - fAnalytic), 2), omega(i), gauss));
          den += std::real(gmshfem::post::integrate(pow(abs(fAnalytic), 2), omega(i), gauss));
        }
        if(saveE){
          gmshfem::post::save(u(i) - fAnalytic, omega(i), "e_" + std::to_string(i), "msh");
        }
      }
      gmshfem::msg::info << "Error L2 = " << std::sqrt(num/den) << gmshfem::msg::endl;
      gmshfem::common::CSVio file("results", ';', gmshfem::common::OpeningMode::Append);
      file << N << std::sqrt(num/den) << pre+solve << gmshfem::csv::endl;
      file.close();
    }
    
    if(wavenumberPlot) {
      gmshfem::common::CSVio file(fileName, ';', gmshfem::common::OpeningMode::Append);
//      file << k << formulation.relativeResidual().size()-1 << gmshfem::csv::endl;
      file.close();
    }
    
    if(meshPlot) {
      gmshfem::common::CSVio file(fileName, ';', gmshfem::common::OpeningMode::Append);
//      file << (2*pi/k)/lc << lc << 1./lc << formulation.relativeResidual().size()-1 << gmshfem::csv::endl;
      file.close();
    }
    
    if(scalingPlot) {
      gmshfem::common::CSVio file(fileName, ';', gmshfem::common::OpeningMode::Append);
      file << nDomX << nDomY << formulation.numberOfIterations() << solve + pre << gmshfem::csv::endl;
      file.close();
    }
    
    for(unsigned int i = 0; i < nDomX; ++i) {
      for(unsigned int j = 0; j < nDomY; ++j) {
        delete subproblem[i][j];
      }
    }
    
    for(unsigned int f = 0; f < 4; ++f) {
      delete gContinuous[f];
      delete gDiscontinuous[f];
      delete gCornerPmlContinuous[f].first;
      delete gCornerPmlContinuous[f].second;
      delete gCornerPmlDiscontinuous[f].first;
      delete gCornerPmlDiscontinuous[f].second;
      for(unsigned int n = 0; n < std::max(N, NExt); ++n) {
        delete gCornerHABC[f][n].first;
        delete gCornerHABC[f][n].second;
      }
    }
  }


}
