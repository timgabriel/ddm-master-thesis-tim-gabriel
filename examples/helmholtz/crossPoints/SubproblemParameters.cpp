#include "SubproblemParameters.h"

SubproblemParameters::SubproblemParameters()
{

}

SubproblemParameters::~SubproblemParameters()
{
}

void SubproblemParameters::setGauss(const std::string &gauss)
{
  _gauss = gauss;
}

std::string SubproblemParameters::getGauss() const
{
  return _gauss;
}

void SubproblemParameters::setKappa(const gmshfem::function::ScalarFunction< std::complex< double > > &kappa)
{
  _kappa = kappa;
}

gmshfem::function::ScalarFunction< std::complex< double > > SubproblemParameters::getKappa() const
{
  return _kappa;
}

void SubproblemParameters::setNeumannOrder(const unsigned int neumannOrder)
{
  _neumannOrder = neumannOrder;
}

unsigned int SubproblemParameters::getNeumannOrder() const
{
  return _neumannOrder;
}

void SubproblemParameters::setFieldOrder(const unsigned int fieldOrder)
{
  _fieldOrder = fieldOrder;
}

unsigned int SubproblemParameters::getFieldOrder() const
{
  return _fieldOrder;
}

void SubproblemParameters::setStab(const double stab)
{
  _stab = stab;
}

double SubproblemParameters::getStab() const
{
  return _stab;
}
