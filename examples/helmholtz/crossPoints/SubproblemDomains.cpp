#include "SubproblemDomains.h"

namespace D2 {


  SubproblemDomains::SubproblemDomains() : _omega(), _sigma(), _pml(), _pmlBnd(), _pmlCorner(), _corner()
  {

  }

  SubproblemDomains::~SubproblemDomains()
  {
  }
    
  void SubproblemDomains::setOmega(const gmshfem::domain::Domain &omega)
  {
    _omega = omega;
  }

  void SubproblemDomains::setSigma(const std::vector< gmshfem::domain::Domain > &sigma)
  {
    _sigma = sigma;
  }

  gmshfem::domain::Domain SubproblemDomains::getSigma(const unsigned int b) const
  {
    return _sigma[b];
  }

  void SubproblemDomains::setPml(const std::vector< gmshfem::domain::Domain > &pml)
  {
    _pml = pml;
  }

  gmshfem::domain::Domain SubproblemDomains::getPml(const unsigned int b) const
  {
    return _pml[b];
  }

  void SubproblemDomains::setPmlBnd(const std::vector< std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > > &pmlBnd)
  {
    _pmlBnd = pmlBnd;
  }

  std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > SubproblemDomains::getPmlBnd(const unsigned int c) const
  {
    return _pmlBnd[c];
  }

  void SubproblemDomains::setPmlCorner(const std::vector< gmshfem::domain::Domain > &pmlCorner)
  {
    _pmlCorner = pmlCorner;
  }

  gmshfem::domain::Domain SubproblemDomains::getPmlCorner(const unsigned int c) const
  {
    return _pmlCorner[c];
  }

  void SubproblemDomains::setCorner(const std::vector< gmshfem::domain::Domain > &corner)
  {
    _corner = corner;
  }

  gmshfem::domain::Domain SubproblemDomains::getCorner(const unsigned int c) const
  {
    return _corner[c];
  }


}


namespace D3 {


  SubproblemDomains::SubproblemDomains() : _omega(), _sigma(), _pml(), _pmlBnd(), _pmlEdge(), _edge(), _pmlEdgeBnd(), _pmlCorner(), _corner()
  {

  }

  SubproblemDomains::~SubproblemDomains()
  {
  }
    
  void SubproblemDomains::setOmega(const gmshfem::domain::Domain &omega)
  {
    _omega = omega;
  }

  void SubproblemDomains::setSigma(const std::vector< gmshfem::domain::Domain > &sigma)
  {
    _sigma = sigma;
  }

  gmshfem::domain::Domain SubproblemDomains::getSigma(const unsigned int b) const
  {
    return _sigma[b];
  }

  void SubproblemDomains::setPml(const std::vector< gmshfem::domain::Domain > &pml)
  {
    _pml = pml;
  }

  gmshfem::domain::Domain SubproblemDomains::getPml(const unsigned int b) const
  {
    return _pml[b];
  }

  void SubproblemDomains::setPmlBnd(const std::vector< std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > > &pmlBnd)
  {
    _pmlBnd = pmlBnd;
  }

  std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > SubproblemDomains::getPmlBnd(const unsigned int e) const
  {
    return _pmlBnd[e];
  }

  void SubproblemDomains::setPmlEdge(const std::vector< gmshfem::domain::Domain > &pmlEdge)
  {
    _pmlEdge = pmlEdge;
  }

  gmshfem::domain::Domain SubproblemDomains::getPmlEdge(const unsigned int e) const
  {
    return _pmlEdge[e];
  }

  void SubproblemDomains::setEdge(const std::vector< gmshfem::domain::Domain > &edge)
  {
    _edge = edge;
  }

  gmshfem::domain::Domain SubproblemDomains::getEdge(const unsigned int e) const
  {
    return _edge[e];
  }
  
  void SubproblemDomains::setPmlEdgeBnd(const std::vector< std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > > &pmlEdgeBnd)
  {
    _pmlEdgeBnd = pmlEdgeBnd;
  }

  std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > SubproblemDomains::getPmlEdgeBnd(const unsigned int c) const
  {
    return _pmlEdgeBnd[c];
  }

  void SubproblemDomains::setPmlCorner(const std::vector< gmshfem::domain::Domain > &pmlCorner)
  {
    _pmlCorner = pmlCorner;
  }

  gmshfem::domain::Domain SubproblemDomains::getPmlCorner(const unsigned int c) const
  {
    return _pmlCorner[c];
  }

  void SubproblemDomains::setCorner(const std::vector< gmshfem::domain::Domain > &corner)
  {
    _corner = corner;
  }

  gmshfem::domain::Domain SubproblemDomains::getCorner(const unsigned int c) const
  {
    return _corner[c];
  }
  
  void SubproblemDomains::setCornerEdge(const std::vector< std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > > &cornerEdge)
  {
    _cornerEdge = cornerEdge;
  }
  
  std::tuple< gmshfem::domain::Domain, gmshfem::domain::Domain, gmshfem::domain::Domain > SubproblemDomains::getCornerEdge(const unsigned int c) const
  {
    return _cornerEdge[c];
  }
  
  void SubproblemDomains::saveDebug() const
  {
    const std::string ori[6] = {"E", "N", "W", "S", "D", "U"};
    
    _omega.saveDebug("omega");
    for(unsigned int b = 0; b < 6; ++b) {
      _sigma[b].saveDebug("sigma" + ori[b]);
      _pml[b].saveDebug("pml" + ori[b]);
    }
    for(unsigned int e = 0; e < 12; ++e) {
      if(e < 4) {
        _pmlBnd[e].first.saveDebug("pmlBnd" + ori[e%4] + "D_first");
        _pmlBnd[e].second.saveDebug("pmlBnd" + ori[e%4] + "D_second");
        _pmlEdge[e].saveDebug("pmlEdge" + ori[e%4] + "D");
        _edge[e].saveDebug("edge" + ori[e%4] + "D");
      }
      else if(e >= 4 && e < 8) {
        _pmlBnd[e].first.saveDebug("pmlBnd" + ori[e%4] + ori[(e+1)%4] + "_first");
        _pmlBnd[e].second.saveDebug("pmlBnd" + ori[e%4] + ori[(e+1)%4] + "_second");
        _pmlEdge[e].saveDebug("pmlEdge" + ori[e%4] + ori[(e+1)%4]);
        _edge[e].saveDebug("edge" + ori[e%4] + ori[(e+1)%4]);
      }
      else {
        _pmlBnd[e].first.saveDebug("pmlBnd" + ori[e%4] + "U_first");
        _pmlBnd[e].second.saveDebug("pmlBnd" + ori[e%4] + "U_second");
        _pmlEdge[e].saveDebug("pmlEdge" + ori[e%4] + "U");
        _edge[e].saveDebug("edge" + ori[e%4] + "U");
      }
    }
    for(unsigned int c = 0; c < 8; ++c) {
      if(c < 4) {
        std::get<0>(_pmlEdgeBnd[c]).saveDebug("pmlEdgeBnd" + ori[c%4] + ori[(c+1)%4] + "D_first");
        std::get<1>(_pmlEdgeBnd[c]).saveDebug("pmlEdgeBnd" + ori[c%4] + ori[(c+1)%4] + "D_second");
        std::get<2>(_pmlEdgeBnd[c]).saveDebug("pmlEdgeBnd" + ori[c%4] + ori[(c+1)%4] + "D_third");
        _pmlCorner[c].saveDebug("pmlCorner" + ori[c%4] + ori[(c+1)%4] + "D");
        _corner[c].saveDebug("corner" + ori[c%4] + ori[(c+1)%4] + "D");
        std::get<0>(_cornerEdge[c]).saveDebug("cornerEdge" + ori[c%4] + ori[(c+1)%4] + "D_first");
        std::get<1>(_cornerEdge[c]).saveDebug("cornerEdge" + ori[c%4] + ori[(c+1)%4] + "D_second");
        std::get<2>(_cornerEdge[c]).saveDebug("cornerEdge" + ori[c%4] + ori[(c+1)%4] + "D_third");
      }
      else {
        std::get<0>(_pmlEdgeBnd[c]).saveDebug("pmlEdgeBnd" + ori[c%4] + ori[(c+1)%4] + "U_first");
        std::get<1>(_pmlEdgeBnd[c]).saveDebug("pmlEdgeBnd" + ori[c%4] + ori[(c+1)%4] + "U_second");
        std::get<2>(_pmlEdgeBnd[c]).saveDebug("pmlEdgeBnd" + ori[c%4] + ori[(c+1)%4] + "U_third");
        _pmlCorner[c].saveDebug("pmlCorner" + ori[c%4] + ori[(c+1)%4] + "U");
        _corner[c].saveDebug("corner" + ori[c%4] + ori[(c+1)%4] + "U");
        std::get<0>(_cornerEdge[c]).saveDebug("cornerEdge" + ori[c%4] + ori[(c+1)%4] + "U_first");
        std::get<1>(_cornerEdge[c]).saveDebug("cornerEdge" + ori[c%4] + ori[(c+1)%4] + "U_second");
        std::get<2>(_cornerEdge[c]).saveDebug("cornerEdge" + ori[c%4] + ori[(c+1)%4] + "U_third");
      }
    }
  }


}
