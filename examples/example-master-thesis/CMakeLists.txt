cmake_minimum_required(VERSION 3.0 FATAL_ERROR)

set(CMAKE_BUILD_TYPE Debug)

project(example CXX)

include(${CMAKE_CURRENT_SOURCE_DIR}/../example.cmake)

# PETSc
if(PETSC_DIR)
  set(ENV_PETSC_DIR ${PETSC_DIR})
else()
  set(ENV_PETSC_DIR $ENV{PETSC_DIR})
endif()

if(PETSC_ARCH)
  set(ENV_PETSC_ARCH ${PETSC_ARCH})
else()
  set(ENV_PETSC_ARCH $ENV{PETSC_ARCH})
endif()

set(PETSC_POSSIBLE_CONF_FILES
    ${ENV_PETSC_DIR}/${ENV_PETSC_ARCH}/conf/petscvariables
    ${ENV_PETSC_DIR}/${ENV_PETSC_ARCH}/lib/petsc-conf/petscvariables
    ${ENV_PETSC_DIR}/${ENV_PETSC_ARCH}/lib/petsc/conf/petscvariables)

foreach(FILE ${PETSC_POSSIBLE_CONF_FILES})
  if(EXISTS ${FILE})
    # old-style PETSc installations (using PETSC_DIR and PETSC_ARCH)
    message(STATUS "Using PETSc dir: ${ENV_PETSC_DIR}")
    message(STATUS "Using PETSc arch: ${ENV_PETSC_ARCH}")
    # find includes by parsing the petscvariables file
    file(STRINGS ${FILE} PETSC_VARIABLES NEWLINE_CONSUME)
  endif()
endforeach()

message(STATUS "Found Petsc variables: ${PETSC_VARIABLES}")

if(PETSC_VARIABLES)
  # try to find PETSC_CC_INCLUDES for PETSc >= 3.4
  string(REGEX MATCH "PETSC_CC_INCLUDES = [^\n\r]*" PETSC_PACKAGES_INCLUDES ${PETSC_VARIABLES})
  if(PETSC_PACKAGES_INCLUDES)
    string(REPLACE "PETSC_CC_INCLUDES = " "" PETSC_PACKAGES_INCLUDES ${PETSC_PACKAGES_INCLUDES})
  else()
    # try to find PETSC_PACKAGES_INCLUDES in older versions
    list(APPEND EXTRA_INCS ${ENV_PETSC_DIR}/include)
    list(APPEND EXTRA_INCS ${ENV_PETSC_DIR}/${ENV_PETSC_ARCH}/include)
    string(REGEX MATCH "PACKAGES_INCLUDES = [^\n\r]*" PETSC_PACKAGES_INCLUDES ${PETSC_VARIABLES})
    string(REPLACE "PACKAGES_INCLUDES = " "" PETSC_PACKAGES_INCLUDES ${PETSC_PACKAGES_INCLUDES})
  endif()

  if(PETSC_PACKAGES_INCLUDES)
    if(PETSC_PACKAGES_INCLUDES)
      string(REPLACE "-I" "" PETSC_PACKAGES_INCLUDES ${PETSC_PACKAGES_INCLUDES})
      string(REPLACE " " ";" PETSC_PACKAGES_INCLUDES ${PETSC_PACKAGES_INCLUDES})
      foreach(VAR ${PETSC_PACKAGES_INCLUDES})
        list(APPEND EXTRA_INCS ${VAR})
      endforeach()
    endif()
  endif()

  find_library(PETSC_LIBS petsc PATHS ${ENV_PETSC_DIR}/${ENV_PETSC_ARCH}/lib NO_DEFAULT_PATH)

  if(SLEPC_DIR)
    set(ENV_SLEPC_DIR ${SLEPC_DIR})
  else()
    set(ENV_SLEPC_DIR $ENV{SLEPC_DIR})
  endif()
  find_library(SLEPC_LIB slepc PATHS ${ENV_SLEPC_DIR}/${ENV_PETSC_ARCH}/lib NO_DEFAULT_PATH)
  if(SLEPC_LIB)
    find_path(SLEPC_INC "slepc.h" PATHS ${ENV_SLEPC_DIR} PATH_SUFFIXES include ${ENV_PETSC_ARCH}/include include/slepc NO_DEFAULT_PATH)
    if(SLEPC_INC)
      message(STATUS "Using SLEPc dir: ${ENV_SLEPC_DIR}")
      list(APPEND EXTRA_LIBS ${SLEPC_LIB})
      list(APPEND EXTRA_INCS ${SLEPC_INC})
      find_path(SLEPC_INC2 "slepcconf.h" PATHS ${ENV_SLEPC_DIR} PATH_SUFFIXES ${ENV_PETSC_ARCH}/include NO_DEFAULT_PATH)
      if(SLEPC_INC2)
        list(APPEND EXTRA_INCS ${SLEPC_INC2})
      endif()
    endif()
  endif()

  list(APPEND EXTRA_LIBS ${PETSC_LIBS})

  string(REGEX MATCH "PACKAGES_LIBS = [^\n\r]*" PLIBS ${PETSC_VARIABLES})
  if(PLIBS)
    string(REPLACE "PACKAGES_LIBS = " "" PLIBS ${PLIBS})
    string(STRIP ${PLIBS} PLIBS)
    list(APPEND EXTRA_LIBS "${PLIBS}")
  endif()

  string(REGEX MATCH "PETSC_EXTERNAL_LIB_BASIC = [^\n\r]*" PLIBS_BASIC ${PETSC_VARIABLES})
  if(PLIBS_BASIC)
    string(REPLACE "PETSC_EXTERNAL_LIB_BASIC = " "" PLIBS_BASIC ${PLIBS_BASIC})
    string(STRIP ${PLIBS_BASIC} PLIBS_BASIC)
    list(APPEND EXTRA_LIBS "${PLIBS_BASIC}")
  endif()

  string(REGEX MATCH "PCC_LINKER_LIBS = [^\n\r]*" LLIBS ${PETSC_VARIABLES})
  if(LLIBS)
    string(REPLACE "PCC_LINKER_LIBS = " "" LLIBS ${LLIBS})
    string(STRIP ${LLIBS} LLIBS)
    list(APPEND EXTRA_LIBS "${LLIBS}")
  endif()

  list(APPEND EXTRA_LIBS ${PETSC_LIBS})
else()
  find_library(PETSC_LIBS petsc)
  find_path(PETSC_INC "petsc.h" PATH_SUFFIXES include/petsc)
  if(PETSC_LIBS AND PETSC_INC)
    list(APPEND EXTRA_LIBS ${PETSC_LIBS})
    list(APPEND EXTRA_INCS ${PETSC_INC})
  endif()
  find_library(SLEPC_LIBS slepc)
  find_path(SLEPC_INC "slepc.h" PATH_SUFFIXES include/slepc)
  if(SLEPC_LIBS AND SLEPC_INC)
    list(APPEND EXTRA_LIBS ${SLEPC_LIBS})
    list(APPEND EXTRA_INCS ${SLEPC_INC})
  endif()
endif()
include_directories(${EXTRA_INCS})


add_executable(example main.cpp mesh.cpp Subproblem2D.cpp SubproblemDomains.cpp SubproblemParameters.cpp ${EXTRA_INCS})
target_link_libraries(example ${EXTRA_LIBS})
