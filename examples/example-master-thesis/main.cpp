#include "mesh.h"
#include "SubproblemDomains.h"
#include "Subproblem2D.h"
#include "SubproblemParameters.h"

#include <gmshfem/GmshFem.h>
#include <gmshfem/FieldInterface.h>
#include <gmshfem/Formulation.h>
#include <gmshfem/AnalyticalFunction.h>
#include <gmshfem/Post.h>
#include <gmshfem/Function.h>
#include <gmshfem/io.h>

#include <gmshddm/GmshDdm.h>
#include <gmshddm/Subdomain.h>
#include <gmshddm/Interface.h>
#include <gmshddm/SubdomainField.h>
#include <gmshddm/InterfaceField.h>
#include <gmshddm/Formulation.h>
#include <gmshddm/MPIInterface.h>
#include <gmshddm/Solvers.h>

#include <petscksp.h>
#include <boost/math/special_functions/hankel.hpp>
#include <algorithm>
#include <fstream>

using namespace gmshfem;
using namespace gmshfem::function;
using namespace gmshfem::analytics;
using namespace gmshfem::post;
using namespace gmshfem::equation;

using namespace gmshddm;
using namespace gmshddm::problem;

using namespace D2;

#define EPS 1e-6


void writeAb(gmshddm::problem::Formulation<std::complex<double>> & formulation, std::string number);
void writeAb(gmshfem::problem::Formulation<std::complex<double>> & formulation, std::string number);

int main(int argc, char **argv)
{
	gmshddm::common::GmshDdm GmshDdm(argc, argv);
	gmshddm::common::GmshDdm* gmshDdm = &GmshDdm;
	double pi = 3.14159265359;
	const std::complex< double > im(0., 1.);


	// ************************************************************************
	//                         P A R A M E T E R S
	// ************************************************************************


	// ************************
	// P R O B L E M
	// ************************
	std::string benchmark = "homogeneous"; 		// homogeneous, marmousi
	gmshDdm->userDefinedParameter(benchmark, "benchmark");

	double f, lenX, lenY, dEmX, dEmY;
	unsigned nEmX, nEmY;

	if (benchmark == "homogeneous") {
		f = 1.;
		lenX = 2.5, lenY = 2.5;
		nEmX = 3; nEmY = 3;
		dEmX = 1./4, dEmY = 1./4;
	}
	else if (benchmark == "marmousi") {
		f = 2.;
		lenX = 9192., lenY = 3116.;
		nEmX = 3; nEmY = 1; // nEmX = 122;
		dEmX = 72., dEmY = 3*dEmX;
	}
	else {
		gmshfem::msg::error << "No benchmark called '" << benchmark << "'" << gmshfem::msg::endl;
		return -1;
	}
	

	gmshDdm->userDefinedParameter(f, "f");
	gmshDdm->userDefinedParameter(lenX, "lenX");
	gmshDdm->userDefinedParameter(lenY, "lenY");
	gmshDdm->userDefinedParameter(nEmX, "nEmX");
	gmshDdm->userDefinedParameter(nEmY, "nEmY");
	gmshDdm->userDefinedParameter(dEmX, "dEmX");
	gmshDdm->userDefinedParameter(dEmY, "dEmY");

	double k;
	if (benchmark == "homogeneous") {
		k = 2*pi*f;
	}
	else if (benchmark == "marmousi") {
		k = 2*pi*f/1500;
	}
	// gmshDdm->userDefinedParameter(k, "k");


	// ************************
	// M O D E L I N G
	// ************************
	unsigned nDomX, nDomY;
	double lc;
	int order;
	if (benchmark == "homogeneous") {
		nDomX = 3;
		nDomY = 3;
		lc = 0.04;
		order = 1;
	}
	else if (benchmark == "marmousi") {
		nDomX = 5;
		nDomY = 5;
		lc = 30; // 10
		order = 2;
	}
	gmshDdm->userDefinedParameter(nDomX, "nDomX");
	gmshDdm->userDefinedParameter(nDomY, "nDomY");
	gmshDdm->userDefinedParameter(lc, "lc");
	gmshDdm->userDefinedParameter(order, "order");

	std::string boundary = "sommerfeld"; 			// sommerfeld
	gmshDdm->userDefinedParameter(boundary, "boundary");
	std::string boundaryExt = "sommerfeld"; 		// sommerfeld
	gmshDdm->userDefinedParameter(boundaryExt, "boundaryExt");

	int meshOrder = 1;
	gmshDdm->userDefinedParameter(meshOrder, "meshOrder");
	int fieldOrder = order;
	gmshDdm->userDefinedParameter(fieldOrder, "fieldOrder");
	int neumannOrder = order;
	gmshDdm->userDefinedParameter(neumannOrder, "neumannOrder");
	std::string gauss = "Gauss" + std::to_string(2 * std::max(fieldOrder, neumannOrder) + 1);
	gmshDdm->userDefinedParameter(gauss, "gauss");

	// ************************
	// S O L V E R
	// ************************
	unsigned int maxIter = 1000;
	gmshDdm->userDefinedParameter(maxIter, "maxIter");
	double res = 1e-6;
	gmshDdm->userDefinedParameter(res, "res");
	double stab = 0.00016666666;
	gmshDdm->userDefinedParameter(stab, "stab");
	std::string solverType = "GMRES";
	gmshDdm->userDefinedParameter(solverType, "solver");
	int restart = 150;
	gmshDdm->userDefinedParameter(restart, "restart");
	int reuse = 4000;
	gmshDdm->userDefinedParameter(reuse, "reuse");
	int reuseSelect = 0;
	gmshDdm->userDefinedParameter(reuseSelect, "reuseSelect");
	bool reuseSol = false;
	gmshDdm->userDefinedParameter(reuseSol, "reuseSol");
	bool trueResidual = false;
	gmshDdm->userDefinedParameter(trueResidual, "trueResidual");
	bool modifiedGS = true;
	gmshDdm->userDefinedParameter(modifiedGS, "modifiedGS");
	bool innerReuse = true;
	gmshDdm->userDefinedParameter(innerReuse, "innerReuse");


	unsigned maxSys = UINT_MAX;
	gmshDdm->userDefinedParameter(maxSys, "maxSys");


	// ************************
	// P O S T
	// ************************
	std::string fileName = "numIter";
	gmshDdm->userDefinedParameter(fileName, "file");
	bool saveMesh = false;
	gmshDdm->userDefinedParameter(saveMesh, "saveMesh");
	std::string mesh = "";
	gmshDdm->userDefinedParameter(mesh, "mesh");
	bool saves2 = false;
	gmshDdm->userDefinedParameter(saves2, "saves2");
	bool exactSol = false;
	gmshDdm->userDefinedParameter(exactSol, "exactSol");
	bool noSkinDom = exactSol;
	gmshDdm->userDefinedParameter(noSkinDom, "noSkinDom");
	bool saveU = false;
	gmshDdm->userDefinedParameter(saveU, "saveU");
	bool saveUfl = true;
	gmshDdm->userDefinedParameter(saveUfl, "saveUfl");
	bool save_iAb = false;
	gmshDdm->userDefinedParameter(save_iAb, "save_iAb");
	bool save_vAb = false;
	gmshDdm->userDefinedParameter(save_vAb, "save_vAb");
	bool scalingPlot = true;
	gmshDdm->userDefinedParameter(scalingPlot, "scaling");

	gmsh::option::setNumber("Mesh.Binary", 1);
	gmsh::option::setNumber("Mesh.Format", 1);



	// ************************************************************************
	//                            L O G   I N F O
	// ************************************************************************

	gmshfem::msg::info << "Running 'ddm2D'" << gmshfem::msg::endl;
	gmshfem::msg::info << "Parameters:" << gmshfem::msg::endl;
	gmshfem::msg::info << " * physics:" << gmshfem::msg::endl;
	if(benchmark == "scattering") {
		gmshfem::msg::info << "   - k: " << k << " (" << k/pi << "*pi" << ")" << gmshfem::msg::endl;
	} else if (benchmark == "marmousi") {
		gmshfem::msg::info << "   - k: for Marmousi" << gmshfem::msg::endl;
	}
	gmshfem::msg::info << "   - f: " << f << gmshfem::msg::endl;
	gmshfem::msg::info << "   - nEmX: " << nEmX << gmshfem::msg::endl;
	gmshfem::msg::info << "   - nEmY: " << nEmY << gmshfem::msg::endl;
	gmshfem::msg::info << "   - dEmX: " << dEmX << gmshfem::msg::endl;
	gmshfem::msg::info << "   - dEmY: " << dEmY << gmshfem::msg::endl;
	gmshfem::msg::info << " * mesh:" << gmshfem::msg::endl;
	gmshfem::msg::info << "   - lc: " << lc << " (eta_h = " << (2*pi/k)/lc << ")" << gmshfem::msg::endl;
	gmshfem::msg::info << "   - meshOrder: " << meshOrder << gmshfem::msg::endl;
	gmshfem::msg::info << " * modeling:" << gmshfem::msg::endl;
	gmshfem::msg::info << "   - grid: (" << nDomX << ", " << nDomY << ")" << gmshfem::msg::endl;
	gmshfem::msg::info << "   - solver: " << solverType << gmshfem::msg::endl;
	gmshfem::msg::info << "   - true residual: " << trueResidual << gmshfem::msg::endl;
	gmshfem::msg::info << "   - modified GS: " << modifiedGS << gmshfem::msg::endl;
	gmshfem::msg::info << "   - restart: " << restart << gmshfem::msg::endl;
	gmshfem::msg::info << "   - reuse: " << reuse << gmshfem::msg::endl;
	gmshfem::msg::info << "   - reuseSelect: " << reuseSelect << gmshfem::msg::endl;
	gmshfem::msg::info << "   - maxIter: " << maxIter << gmshfem::msg::endl;
	gmshfem::msg::info << "   - res: " << res << gmshfem::msg::endl;
	gmshfem::msg::info << "   - domain size: (" << lenX << ", " << lenY << ")" << gmshfem::msg::endl;
	gmshfem::msg::info << "   - gauss: " << gauss << gmshfem::msg::endl;
	gmshfem::msg::info << "   - fieldOrder: " << fieldOrder << gmshfem::msg::endl;
	gmshfem::msg::info << "   - neumannOrder: " << neumannOrder << gmshfem::msg::endl;
	gmshfem::msg::info << "   - stab: " << stab << " * lc (= " << stab * lc << ")" << gmshfem::msg::endl;



	// ************************************************************************
	//                         M O D E L I Z A T I O N
	// ************************************************************************

	// ************************
	// G E O M E T R Y
	// ************************
	gmsh::model::add("helmholtzModel");

	std::vector< std::pair< double, double > > points;
	double  xCenter = lenX/2, 		yCenter = lenY/2, 
			xWidth = (nEmX-1)*dEmX, yWidth = (nEmY-1)*dEmY;

	if(mesh != "") {
		gmsh::open(mesh + ".msh");

	} else {
		if (benchmark == "marmousi") {
			yCenter = lenY - dEmY;
		}

		for (unsigned j = 0; j < nEmY; ++j) {
		for (unsigned i = 0; i < nEmX; ++i) {
			 points.push_back( std::make_pair(xCenter - xWidth/2 + i*xWidth/(nEmX == 1 ? 1 : nEmX-1), 
											 yCenter - yWidth/2 + j*yWidth/(nEmY == 1 ? 1 : nEmY-1)) );
		}
		}

		createDomains(nDomX, nDomY, lenX/nDomX, lenY/nDomY, lc, meshOrder, points);

		if(saveMesh) {
			gmsh::write("mesh.msh");
		}
	}


	// ************************
	// D O M A I N S
	// ************************
	const unsigned int nDom = nDomX * nDomY;
	gmshddm::domain::Subdomain omega(nDom);

	std::vector< gmshddm::domain::Subdomain > sigma(4, nDom);
	std::vector< gmshddm::domain::Interface > sigmaInterface(4, nDom);

	std::vector< gmshddm::domain::Subdomain > corner(4, nDom);
	std::vector< std::pair< gmshddm::domain::Interface, gmshddm::domain::Interface > > cornerInterface(4, std::make_pair(gmshddm::domain::Interface(nDom), gmshddm::domain::Interface(nDom)));

	std::vector< std::vector< unsigned int > > topology(nDom);

	std::vector< std::string > dir {"E", "N", "W", "S"};
	for(unsigned int i = 0; i < static_cast< unsigned int >(nDomX); ++i) {
	for(unsigned int j = 0; j < static_cast< unsigned int >(nDomY); ++j) {
		unsigned int index = i * nDomY + j;

		const std::string subdomainTag = "_" + std::to_string(i) + "_" + std::to_string(j);
		omega(index) = gmshfem::domain::Domain("omega" + subdomainTag);


		for(unsigned int b = 0; b < 4; ++b) {
			sigma[b](index) = gmshfem::domain::Domain("sigma" + dir[b] + subdomainTag);
		}

		for(unsigned int c = 0; c < 4; ++c) {
			corner[c](index) = gmshfem::domain::Domain("corner" + dir[c] + dir[(c+1)%4] + subdomainTag);
		}

		if(i != static_cast< unsigned int >(nDomX) - 1) { // E
			sigmaInterface[0](index, (i+1) * nDomY + j) = sigma[0](index);

			cornerInterface[3].first(index, (i+1) * nDomY + j) = corner[3](index);
			cornerInterface[0].second(index, (i+1) * nDomY + j) = corner[0](index);

			topology[index].push_back((i+1) * nDomY + j);
		}

		if(j != static_cast< unsigned int >(nDomY) - 1) { // N
			sigmaInterface[1](index, i * nDomY + (j+1)) = sigma[1](index);

			cornerInterface[0].first(index, i * nDomY + (j+1)) = corner[0](index);
			cornerInterface[1].second(index, i * nDomY + (j+1)) = corner[1](index);

			topology[index].push_back(i * nDomY + (j+1));
		}

		if(i != 0) { // W
			sigmaInterface[2](index, (i-1) * nDomY + j) = sigma[2](index);

			cornerInterface[1].first(index, (i-1) * nDomY + j) = corner[1](index);
			cornerInterface[2].second(index, (i-1) * nDomY + j) = corner[2](index);

			topology[index].push_back((i-1) * nDomY + j);
		}

		if(j != 0) { // S
			sigmaInterface[3](index, i * nDomY + (j-1)) = sigma[3](index);

			cornerInterface[2].first(index, i * nDomY + (j-1)) = corner[2](index);
			cornerInterface[3].second(index, i * nDomY + (j-1)) = corner[3](index);

			topology[index].push_back(i * nDomY + (j-1));
		}
	}
	}
	
	std::vector< gmshfem::domain::Domain > embeddedPoints(points.size());
	for (unsigned i = 0; i < points.size(); ++i) {
		embeddedPoints[i] = gmshfem::domain::Domain("embeddedPoint_" + std::to_string(i));
	}



	// ************************************************************************
	//                         S I M U L A T I O N
	// ************************************************************************


	// ************************
	// F O R M U L A T I O N
	// ************************
	gmshddm::field::SubdomainField< std::complex< double >, gmshfem::field::Form::Form0 > u("u", omega | sigma[0] | sigma[1] | sigma[2] | sigma[3], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
	std::vector< gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 > * > gContinuous;
	for(unsigned int f = 0; f < 4; ++f) {
		gContinuous.push_back(
			new gmshddm::field::InterfaceField< std::complex< double >, gmshfem::field::Form::Form0 >("gC_" + dir[f], sigmaInterface[f], gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder)
		);
	}

	gmshddm::problem::Formulation< std::complex< double > > formulation("HelmholtzDDMScattering", topology);

	for(unsigned int f = 0; f < 4; ++f) {
		formulation.addInterfaceField(*gContinuous[f], *gContinuous[(f+2)%4]);
	}

	std::vector< std::vector< Subproblem * > > subproblem(nDomX);
	for(unsigned int i = 0; i < nDomX; ++i) {
		subproblem[i].resize(nDomY);
		for(unsigned int j = 0; j < nDomY; ++j) {
			unsigned int index = i * nDomY + j;
			formulation(index).integral(-grad(dof(u(index))), grad(tf(u(index))), omega(index), gauss);
			// formulation(index).integral(kappa * kappa * dof(u(index)), tf(u(index)), omega(index), gauss);

			SubproblemDomains domains;
			domains.setOmega( omega(index) );
			domains.setSigma({ sigma[0](index), sigma[1](index), sigma[2](index), sigma[3](index) });
			domains.setCorner({ corner[0](index), corner[1](index), corner[2](index), corner[3](index) });

			SubproblemParameters parameters;
			parameters.setGauss(gauss);
			// parameters.setKappa(kappa);
			parameters.setNeumannOrder(neumannOrder);
			parameters.setFieldOrder(fieldOrder);
			parameters.setStab(stab * lc * lc);


			std::string bnd = boundary;
			std::string bndExt = boundaryExt;
			gmshfem::msg::info << "Subdomain (" << i << ", " << j << ") has boundaries [" 
				<< (i == nDomX-1 ? bndExt : bnd) << ", " << (j == nDomY-1 ? bndExt : bnd) 
				<< ", " << (i == 0 ? bndExt : bnd) << ", " << (j == 0 ? bndExt : bnd) 
				<< "]" << gmshfem::msg::endl;
			std::vector< unsigned int > activatedCrossPoints = {0, 1, 2, 3};

			subproblem[i][j] = new Subproblem(formulation(index), u(index).name(), domains, parameters, (i == nDomX-1 ? bndExt : bnd), (j == nDomY-1 ? bndExt : bnd), (i == 0 ? bndExt : bnd), (j == 0 ? bndExt : bnd), activatedCrossPoints);
			subproblem[i][j]->writeFormulation();
			// subproblem[i][j]->writeKappa(kappa);

			// coupling
			for(unsigned int b = 0; b < 4; ++b) {
			for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
				const unsigned int jndex = topology[index][jj];

				Boundary *boundary = subproblem[i][j]->getBoundary(b);
				if(dynamic_cast< Sommerfeld * >(boundary)) {
					if(!sigmaInterface[b](index, jndex).isEmpty()) {
						auto termId = formulation(index).integral(- (*gContinuous[(b+2)%4])(jndex, index), tf(u(index)), sigmaInterface[b](index, jndex), gauss);
						formulation.artificialSourceTerm(termId);
					}
				}
			}
			}
			/*
			for(unsigned int c = 0; c < 4; ++c) {
			for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
				Corner *corner = subproblem[i][j]->getCorner(c);
				if(corner == nullptr) {
					continue;
				}
			}
			}
			*/
			// interface
			for(unsigned int b = 0; b < 4; ++b) {
			for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
				const unsigned int jndex = topology[index][jj];
			
				Boundary *boundary = subproblem[i][j]->getBoundary(b);
				if(auto bnd = dynamic_cast< Sommerfeld * >(boundary)) {
					if(!sigmaInterface[b](index, jndex).isEmpty()) {
						formulation(index, jndex).integral(dof((*gContinuous[b])(index, jndex)), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
						auto termId = formulation(index, jndex).integral((*gContinuous[(b+2)%4])(jndex, index), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
						formulation.artificialSourceTerm(termId);
						formulation(index, jndex).integral(2. * *bnd->getV(), tf((*gContinuous[b])(index, jndex)), sigmaInterface[b](index, jndex), gauss);
					}
				}
			}
			}
			/*
			for(unsigned int c = 0; c < 4; ++c) {
			for(unsigned int jj = 0; jj < topology[index].size(); ++jj) {
				Corner *corner = subproblem[i][j]->getCorner(c);
				if(corner == nullptr) {
					continue;
				}
			}
			}
			*/
		}
	}


	// ************************
	// W A V E N U M B E R S
	// ************************
	std::vector< std::string > kFields;
	if(benchmark == "homogeneous") {
		kFields = {"1"};
		// kFields = {"1.0", "2.0"};
	}
	else if (benchmark == "marmousi") {		
		kFields = {"./convertedpaper2_extended_sst_gd_ls_m_blkc0.pos"};
		/*
		kFields = { "convertedpaper2_extended_sst_gd_ls_m_initialc0.pos",
			"convertedpaper2_extended_sst_gd_lsg0s0m1c0.pos",
			"convertedpaper2_extended_sst_gd_lsg0s0m2c0.pos",
			"convertedpaper2_extended_sst_gd_lsg0s0m3c0.pos",
			"convertedpaper2_extended_sst_gd_lsg0s0m4c0.pos",
			"convertedpaper2_extended_sst_gd_lsg0s0m5c0.pos",
			"convertedpaper2_extended_sst_gd_lsg0s0m6c0.pos",
			"convertedpaper2_extended_sst_gd_lsg0s0m7c0.pos",
			"convertedpaper2_extended_sst_gd_lsg0s0m8c0.pos" };
		*/
	}



	std::unique_ptr < AbstractIterativeSolver< std::complex< double > > > solver;
	if (solverType == "GCR") {
		solver = std::make_unique< GCRIterativeSolver< std::complex<double> > >(restart, reuse, reuseSelect, true, reuseSol, trueResidual, modifiedGS);
	}
	else if (solverType == "Orthodir") {
		solver = std::make_unique< GCRIterativeSolver< std::complex<double> > >(restart, reuse, reuseSelect, false, reuseSol, trueResidual, modifiedGS);
	}
	else if (solverType == "SGMRES") {
		solver = std::make_unique< SGMRESIterativeSolver< std::complex<double> > >(restart, reuse, reuseSelect, false, reuseSol, trueResidual, modifiedGS);
	}
	else if (solverType == "RB-SGMRES") {
		solver = std::make_unique< SGMRESIterativeSolver< std::complex<double> > >(restart, reuse, reuseSelect, true, reuseSol, trueResidual, modifiedGS);
	}
	else if (solverType == "GMRES") {
		solver = std::make_unique< GMRESIterativeSolver< std::complex<double> > >(restart, reuse, reuseSelect, reuseSol, trueResidual, modifiedGS);
	}
	else
		solver = std::make_unique< KSPIterativeSolver< std::complex<double> > >(solverType);


	for(unsigned kf = 0; kf < kFields.size(); ++kf) {
		auto kField = kFields[kf];


		// ************************
		// W A V E N U M B E R
		// ************************
		gmshfem::function::ScalarFunction< std::complex< double > > s2;
		gmshfem::function::ScalarFunction< std::complex< double > > kappa;

		if(benchmark == "homogeneous") {
			s2 = std::atof(kField.c_str());
		}
		else if (benchmark == "marmousi") {
			gmsh::merge(kField);
			std::vector<int> tags;
			gmsh::view::getTags(tags);
			auto view = tags.back();
			s2 = gmshfem::function::probeScalarView<std::complex<double>>(view);

			if (saves2) {
				for(unsigned int m = 0; m < nDomX * nDomY; ++m) {
					if(gmshddm::mpi::isItMySubdomain(m)) {
						gmshfem::post::save(s2, omega(m), "s2_" + std::to_string(m), "msh");
						//gmshfem::post::save(1/sqrt(s2), omega(m), "k_" + std::to_string(m), "msh");
					}
				}
			}
			
			// solver->recomputeAV();
		}
		kappa = 2. * pi * f * sqrt(s2);

		std::string fn = "_";
		if (kFields.size() > 1)
			fn = fn + std::to_string(kf) + "_";


		// ************************
		// S O L V E
		// ************************
		for(unsigned int i = 0; i < nDomX; ++i) {
		for(unsigned int j = 0; j < nDomY; ++j) {
			unsigned int index = i * nDomY + j;
			formulation(index).integral(kappa * kappa * dof(u(index)), tf(u(index)), omega(index), gauss);
			subproblem[i][j]->writeKappa(kappa);
		}
		}

		gmshfem::common::Timer pre, solve;

		if (points.size() < maxSys)
			maxSys = points.size();


		pre = formulation.pre();
		for (unsigned s = 0; s < maxSys; ++s) {
			gmshfem::msg::info << "Resolution of model " << kf << " with emitter " << s << "." << gmshfem::msg::endl;

			std::string fname = fn;
			if (points.size() > 1)
				fname = fname + std::to_string(s) + "_";

			// Add source points
			int signs[] = {+1, -1};
			for (auto isign : signs) {
			for (auto jsign : signs) {
				unsigned iEm = (unsigned) std::floor( (points[s].first + isign*EPS) / (lenX/nDomX) );
				unsigned jEm = (unsigned) std::floor( (points[s].second + jsign*EPS) / (lenY/nDomY) );
				if (iEm < nDomX && jEm < nDomY) {
					unsigned int indexEm = iEm * nDomY + jEm;
					formulation(indexEm).integral(formulation.physicalSource(1./4), tf(u(indexEm)), embeddedPoints[s], gauss);
				}
			}
			}
			

			// Solve
			solve = formulation.solve(*solver, res, maxIter);


			// Exact solution
			ScalarFunction< std::complex<double> > solution;
			if (benchmark == "homogeneous" && exactSol) {
				ScalarFunction< std::complex<double> > rx = x< std::complex<double> >() - points[s].first;
				ScalarFunction< std::complex<double> > ry = y< std::complex<double> >() - points[s].second;
				ScalarFunction< std::complex<double> > kr = k * sqrt(rx*rx + ry*ry);
				if (noSkinDom)
					solution = (im/4) * (cylBesselJ< std::complex<double> >(0,kr) + im * cylNeumann< std::complex<double> >(0,kr));
				else
					solution = (im/4) * (cylBesselJ< std::complex<double> >(0,kr) + im);
			}

			std::complex< double > num = 0., den = 0.;


			// Save solutions
			for(unsigned i = 0; i < nDomX; ++i) {
			for(unsigned j = 0; j < nDomY; ++j) {
				unsigned index = i * nDomY + j;
				if(gmshddm::mpi::isItMySubdomain(index)) {

					// Remove nan values in the exact solution
					gmshfem::domain::Domain solDomain;
					if (noSkinDom && (points[s].first >= i*(lenX/nDomX) && 
									  points[s].first <= (i+1)*(lenX/nDomX) && 
									  points[s].second >= j*(lenY/nDomY) && 
									  points[s].second <= (j+1)*(lenY/nDomY) )) {
						auto skinLayer = omega(index).getSkinLayer(embeddedPoints[s]);

						int newEntityTag = gmsh::model::addDiscreteEntity(2);

						std::vector<std::vector<std::size_t> > elementTags;
						std::vector<std::vector<std::size_t> > nodeTags;
						std::vector< int > elementTypes;

						std::vector<std::size_t> OmegaElementTags = {};
						std::vector<std::vector<std::size_t> > omegaElementTags = {OmegaElementTags};
						std::vector<std::size_t> OmegaNodeTags = {};
						std::vector<std::vector<std::size_t> > omegaNodeTags = {OmegaNodeTags};
						std::vector< int > omegaElementTypes = {2};
						for(auto it = omega(index).cbegin(); it != omega(index).cend(); ++it) {
							gmsh::model::mesh::getElements(elementTypes, elementTags, nodeTags, it->first, it->second);
							auto ElementTags = elementTags[0];
							auto NodeTags = nodeTags[0];
							OmegaElementTags.insert( OmegaElementTags.end(), ElementTags.begin(), ElementTags.end() );
							OmegaNodeTags.insert( OmegaNodeTags.end(), NodeTags.begin(), NodeTags.end() );
						}

						std::vector<std::size_t> SkinElementTags = {};
						std::vector<std::vector<std::size_t> > skinElementTags = {SkinElementTags};
						std::vector<std::size_t> SkinNodeTags = {};
						std::vector<std::vector<std::size_t> > skinNodeTags = {SkinNodeTags};
						std::vector< int > skinElementTypes = {2};
						for(auto it = skinLayer.cbegin(); it != skinLayer.cend(); ++it) {
							gmsh::model::mesh::getElements(elementTypes, elementTags, nodeTags, it->first, it->second);
							auto ElementTags = elementTags[0];
							auto NodeTags = nodeTags[0];
							SkinElementTags.insert( SkinElementTags.end(), ElementTags.begin(), ElementTags.end() );
							SkinNodeTags.insert( SkinNodeTags.end(), NodeTags.begin(), NodeTags.end() );
						}


						for(unsigned i = 0; i < SkinElementTags.size(); ++i) {
							auto it = std::find(OmegaElementTags.begin(), OmegaElementTags.end(), SkinElementTags[i]);
							if (it != OmegaElementTags.end()) {
								auto j = it - OmegaElementTags.begin();
								OmegaElementTags.erase(it);
								OmegaNodeTags.erase(OmegaNodeTags.begin() + 3*j, OmegaNodeTags.begin() + 3*(j+1));
							}
						}
					
						gmsh::model::mesh::addElementsByType(newEntityTag, omegaElementTypes[0], OmegaElementTags, OmegaNodeTags);

						solDomain.addEntity(2, newEntityTag);
					}
					else {
						solDomain = omega(index);
					}


					// Save solution
					if( saveU || ( saveUfl && (s == 0 || s == points.size()-1) ) ) {
						if(gmshddm::mpi::isItMySubdomain(index)) {
							gmshfem::post::save(u(index), solDomain, "u" + fname + std::to_string(index), "msh");
						}
					}


					// Save exact solution
					if(benchmark == "homogeneous" && exactSol) {
						num += integrate(pow(abs(solution - u(index)), 2), solDomain, gauss);
						den += integrate(pow(abs(solution), 2), solDomain, gauss);

						if( saveU || ( saveUfl && (s == 0 || s == points.size()-1) ) ) {
							gmshfem::post::save(solution, solDomain, "exact_u" + fname + std::to_string(index), "msh");
							gmshfem::post::save(solution - u(index), solDomain, "error" + fname + std::to_string(index), "msh");
						}
					}


					if(save_vAb) {
						writeAb(formulation(index), fname + std::to_string(s));
					}
				}
			}
			}

			if(benchmark == "homogeneous" && exactSol && noSkinDom) {
				msg::info << "L_2 error = " << sqrt(num) << ", rel_L_2 error = " << sqrt(num / den) << msg::endl;
				gmshfem::common::CSVio file("convergence", ';', gmshfem::common::OpeningMode::Append);
				file << std::real(sqrt(num)) << std::real(sqrt(num/den)) << csv::endl;
			}

			if(save_iAb) {
				writeAb(formulation, fname + std::to_string(s));
			}

			if(scalingPlot) {
				gmshfem::common::CSVio file(fileName, ';', gmshfem::common::OpeningMode::Append);
				file << nDomX << nDomY << formulation.numberOfIterations() << solve + pre << gmshfem::csv::endl;
				file.close();
			}

			// Remove source points
			for (auto isign : signs) {
			for (auto jsign : signs) {
				unsigned iEm = (unsigned) std::floor( (points[s].first + isign*EPS) / (lenX/nDomX) );
				unsigned jEm = (unsigned) std::floor( (points[s].second + jsign*EPS) / (lenY/nDomY) );
				if (iEm < nDomX && jEm < nDomY) {
					unsigned int indexEm = iEm * nDomY + jEm;
					formulation(indexEm).integral(formulation.physicalSource(-1./4), tf(u(indexEm)), embeddedPoints[s], gauss);
				}
			}
			}

		}


		for(unsigned int i = 0; i < nDomX; ++i) {
		for(unsigned int j = 0; j < nDomY; ++j) {
			unsigned int index = i * nDomY + j;
			formulation(index).integral(-kappa * kappa * dof(u(index)), tf(u(index)), omega(index), gauss);
			subproblem[i][j]->writeKappa(-kappa);
		}
		}

	}

	for(unsigned int i = 0; i < nDomX; ++i) {
	for(unsigned int j = 0; j < nDomY; ++j) {
		delete subproblem[i][j];
	}
	}

	for(unsigned int f = 0; f < 4; ++f) {
		delete gContinuous[f];
	}


}


void writeAb(gmshddm::problem::Formulation<std::complex<double>> & formulation, std::string number) {
	gmshfem::algebra::MatrixCRS< std::complex<double> > A;
	gmshfem::algebra::Vector< std::complex<double> > b;

	A = formulation.computeMatrix();
	b = formulation.getRHS();

	A.saveSpyPlot("spyplot" + number);

	std::string fileName = "iA" + number;
	A.save(fileName);

	fileName = "ib" + number;
	b.save(fileName);

	return;
}

void writeAb(gmshfem::problem::Formulation<std::complex<double>> & formulation, std::string number) {
	gmshfem::algebra::MatrixCRS< std::complex<double> > A;
	gmshfem::algebra::Vector< std::complex<double> > b;

	formulation.getLHS(A);
	formulation.getRHS(b);

	std::string fileName = "vA" + number;
	A.save(fileName);

	fileName = "vb" + number;
	b.save(fileName);

	return;
}
