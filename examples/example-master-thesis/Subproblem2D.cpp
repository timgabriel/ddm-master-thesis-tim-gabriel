#include "Subproblem2D.h"

using gmshfem::equation::dof;
using gmshfem::equation::tf;
using gmshfem::function::operator-;
using gmshfem::function::operator+;
using gmshfem::function::operator*;

namespace D2 {


  // **********************************
  // Boundary
  // **********************************

  Boundary::Boundary(const unsigned int boundary) : _boundary(boundary)
  {
  }

  Boundary::~Boundary()
  {
  }

  std::string Boundary::orientation() const
  {
    switch (_boundary) {
      case 0:
        return "E";
        break;
      case 1:
        return "N";
        break;
      case 2:
        return "W";
        break;
      case 3:
        return "S";
        break;
      default:
        break;
    }
    return "null";
  }

  // **********************************
  // Corner
  // **********************************

  Corner::Corner(const unsigned int corner, Boundary *first, Boundary *second) : _corner(corner), _bnd {first, second}
  {
  }

  Corner::~Corner()
  {
  }

  std::string Corner::orientation() const
  {
    switch (_corner) {
      case 0:
        return "EN";
        break;
      case 1:
        return "NW";
        break;
      case 2:
        return "WS";
        break;
      case 3:
        return "SE";
        break;
      default:
        break;
    }
    return "null";
  }

  // **********************************
  // Subproblem
  // **********************************

  void Subproblem::_parseParameters(std::string &method, std::vector< std::string > &parameters, const std::string &str) const
  {
    method.clear();
    parameters.clear();
    std::string *current = &method;
    for(unsigned int i = 0; i < str.size(); ++i) {
      if(str[i] == '_') {
        parameters.push_back(std::string());
        current = &parameters.back();
      }
      else {
        (*current) += str[i];
      }
    }
  }

  Subproblem::Subproblem(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters, const std::string &E, const std::string &N, const std::string &W, const std::string &S, const std::vector< unsigned int > activatedCrossPoints) : 
  _domains(domains), _parameters(parameters), _formulation(formulation), _fieldName(fieldName), _boundary(4, nullptr), _corner(4, nullptr)
  {
    std::string type[4] = {E, N, W, S};
    std::string method[4];
    std::vector< std::string > bndParameters[4];
    for(unsigned int i = 0; i < 4; ++i) {
      _parseParameters(method[i], bndParameters[i], type[i]);
    }
    
    for(unsigned int b = 0; b < 4; ++b) {
      if(method[b] == "sommerfeld") { // str: sommerfeld
        _boundary[b] = new Sommerfeld(b);
      }
      /*
      else if(method[b] == "habc") { // str: habc_[N]_[theta]
        const unsigned int N = std::stoi(bndParameters[b][0]);
        const double theta = std::stof(bndParameters[b][1]);
        _boundary[b] = new HABC(b, N, theta);
      }
      else if(method[b] == "pmlContinuous") { // str: pmlContinuous_[N_pml]_[Type]
        const double N_pml  = std::stof(bndParameters[b][0]);
        const std::string type  = bndParameters[b][1];
        _boundary[b] = new PmlContinuous(b, N_pml, type);
      }
      else if(method[b] == "pmlDiscontinuous") { // str: pmlDiscontinuous_[N_pml]_[Type]
        const double N_pml = std::stof(bndParameters[b][0]);
        const std::string type  = bndParameters[b][1];
        _boundary[b] = new PmlDiscontinuous(b, N_pml, type);
      }
      else if(method[b] == "pmlWithoutMultiplier") { // str: pmlWithoutMultiplier_[N_pml]_[Type]
        const double N_pml = std::stof(bndParameters[b][0]);
        const std::string type  = bndParameters[b][1];
        _boundary[b] = new PmlWithoutMultiplier(b, N_pml, type);
      }
      else {
        gmshfem::msg::error << "Unknown method '" << method[b] << "'" << gmshfem::msg::endl;
      }
      */
    }
    
    for(unsigned int c = 0; c < 4; ++c) {
      auto it = std::find(activatedCrossPoints.begin(), activatedCrossPoints.end(), c);
      if(it == activatedCrossPoints.end()) {
        continue;
      }
    
      if(method[c] == "sommerfeld" && method[(c+1)%4] == "sommerfeld") {
      }
      /*
      else if(method[c] == "habc" && method[(c+1)%4] == "habc") {
        _corner[c] = new HABC_HABC(c, static_cast< HABC * >(_boundary[c]), static_cast< HABC * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "habc" && method[(c+1)%4] == "sommerfeld") {
        _corner[c] = new HABC_Sommerfeld(c, static_cast< HABC * >(_boundary[c]), static_cast< Sommerfeld * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "sommerfeld" && method[(c+1)%4] == "habc") {
        _corner[c] = new Sommerfeld_HABC(c, static_cast< Sommerfeld * >(_boundary[c]), static_cast< HABC * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlContinuous" && method[(c+1)%4] == "pmlContinuous") {
        _corner[c] = new PmlContinuous_PmlContinuous(c, static_cast< PmlContinuous * >(_boundary[c]), static_cast< PmlContinuous * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlDiscontinuous" && method[(c+1)%4] == "pmlDiscontinuous") {
        _corner[c] = new PmlDiscontinuous_PmlDiscontinuous(c, static_cast< PmlDiscontinuous * >(_boundary[c]), static_cast< PmlDiscontinuous * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlWithoutMultiplier" && method[(c+1)%4] == "pmlWithoutMultiplier") {
        _corner[c] = new PmlWithoutMultiplier_PmlWithoutMultiplier(c, static_cast< PmlWithoutMultiplier * >(_boundary[c]), static_cast< PmlWithoutMultiplier * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlContinuous" && method[(c+1)%4] == "sommerfeld") {
        _corner[c] = new PmlContinuous_Sommerfeld(c, static_cast< PmlContinuous * >(_boundary[c]), static_cast< Sommerfeld * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "sommerfeld" && method[(c+1)%4] == "pmlContinuous") {
        _corner[c] = new Sommerfeld_PmlContinuous(c, static_cast< Sommerfeld * >(_boundary[c]), static_cast< PmlContinuous * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "pmlDiscontinuous" && method[(c+1)%4] == "sommerfeld") {
        _corner[c] = new PmlDiscontinuous_Sommerfeld(c, static_cast< PmlDiscontinuous * >(_boundary[c]), static_cast< Sommerfeld * >(_boundary[(c+1)%4]));
      }
      else if(method[c] == "sommerfeld" && method[(c+1)%4] == "pmlDiscontinuous") {
        _corner[c] = new Sommerfeld_PmlDiscontinuous(c, static_cast< Sommerfeld * >(_boundary[c]), static_cast< PmlDiscontinuous * >(_boundary[(c+1)%4]));
      }
      else {
        gmshfem::msg::warning << "Any corner treatment between '" << method[c] << "' and '" << method[(c+1)%4] << "'" << gmshfem::msg::endl;
      }
      */
    }
  }

  Subproblem::~Subproblem()
  {
    for(unsigned int c = 0; c < 4; ++c) {
      if(_corner[c] != nullptr) {
        delete _corner[c];
      }
    }
    for(unsigned int b = 0; b < 4; ++b) {
      if(_boundary[b] != nullptr) {
        delete _boundary[b];
      }
    }
  }

  void Subproblem::writeFormulation()
  {
    for(unsigned int b = 0; b < 4; ++b) {
      _boundary[b]->writeFormulation(_formulation, _fieldName, _domains, _parameters);
    }
    
    for(unsigned int c = 0; c < 4; ++c) {
      if(_corner[c] != nullptr) {
        _corner[c]->writeFormulation(_formulation, _domains, _parameters);
      }
    }
  }
  
  void Subproblem::writeKappa(const gmshfem::function::ScalarFunction< std::complex< double > > &kappa)
  {
    for(unsigned int b = 0; b < 4; ++b) {
      _boundary[b]->writeKappa(_formulation, _fieldName, _domains, _parameters, kappa);
    }
    
    for(unsigned int c = 0; c < 4; ++c) {
      if(_corner[c] != nullptr) {
        _corner[c]->writeKappa(_formulation, _domains, _parameters, kappa);
      }
    }
    
    
  }

  Boundary *Subproblem::getBoundary(const unsigned int b) const
  {
    return _boundary[b];
  }

  Corner *Subproblem::getCorner(const unsigned int c) const
  {
    return _corner[c];
  }

  // **********************************
  // Sommerfeld
  // **********************************

  Sommerfeld::Sommerfeld(const unsigned int boundary) : Boundary(boundary)
  {
  }

  Sommerfeld::~Sommerfeld()
  {
    delete _v;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* Sommerfeld::getV() const
  {
    return _v;
  }

  void Sommerfeld::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    // const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();
    
    _v = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *u = static_cast< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * >(formulation.getField(fieldName));
      
    const std::complex< double > im(0., 1.);
    formulation.integral(-dof(*_v), tf(*u), sigma, gauss);

    formulation.integral(dof(*_v), tf(*_v), sigma, gauss);
    // formulation.integral(im * kappa * dof(*u), tf(*_v), sigma, gauss);
  }
  
  void Sommerfeld::writeKappa(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters, const gmshfem::function::ScalarFunction< std::complex< double > > &kappa)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    const std::string gauss = parameters.getGauss();
        
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *u = static_cast< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * >(formulation.getField(fieldName));
      
    const std::complex< double > im(0., 1.);
    formulation.integral(im * kappa * dof(*u), tf(*_v), sigma, gauss);
  }

  // **********************************
  // HABC
  // **********************************

  HABC::HABC(const unsigned int boundary, const unsigned int N, const double theta) : Boundary(boundary), _N(N), _theta(theta)
  {
  }

  HABC::~HABC()
  {
    delete _v;
    for(unsigned int m = 0; m < _N; ++m) {
      delete _uHABC[m];
    }
  }

  unsigned int HABC::getN() const
  {
    return _N;
  }

  double HABC::getTheta() const
  {
    return _theta;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * HABC::getUHABC(const unsigned int m) const
  {
    if(m > _N) throw gmshfem::common::Exception("Try to get uHABC field " + std::to_string(m));
    return _uHABC[m];
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* HABC::getV() const
  {
    return _v;
  }

  void HABC::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();
    
    _uHABC.resize(_N);
    for(unsigned int m = 0; m < _N; ++m) {
      _uHABC[m] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uHABC_" + orientation() + "_" + std::to_string(m), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    }
    
    _v = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *u = static_cast< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * >(formulation.getField(fieldName));
    
    const double pi = 3.141592653589793238462643383279;
    const std::complex< double > expPTheta(std::cos(_theta), std::sin(_theta));
    const std::complex< double > expPTheta2(std::cos(_theta/2.), std::sin(_theta/2.));
    const double M = 2 * _N + 1;
    double cPade[_N];
    for(unsigned int m = 0; m < _N; ++m) {
      cPade[m] = std::tan((m+1) * pi / M) * std::tan((m+1) * pi / M);
    }

    const std::complex< double > im(0., 1.);
    
    formulation.integral(-dof(*_v), tf(*u), sigma, gauss);
      
    formulation.integral( dof(*_v), tf(*_v), sigma, gauss);
    formulation.integral( im * kappa * expPTheta2 * dof(*u), tf(*_v), sigma, gauss);
    
    for(unsigned int m = 0; m < _N; ++m) {
      formulation.integral( im * kappa * expPTheta2 * 2./M * cPade[m] * dof(*u), tf(*_v), sigma, gauss);
      formulation.integral( im * kappa * expPTheta2 * 2./M * cPade[m] * dof(*_uHABC[m]), tf(*_v), sigma, gauss);
    }
    
    // auxiliary
    for(unsigned int m = 0; m < _N; ++m) {
      formulation.integral(grad(dof(*_uHABC[m])), grad(tf(*_uHABC[m])), sigma, gauss);
      formulation.integral(- kappa * kappa * (expPTheta * cPade[m] + 1.) * dof(*_uHABC[m]), tf(*_uHABC[m]), sigma, gauss);
      formulation.integral(- kappa * kappa * expPTheta * (cPade[m] + 1.) * dof(*u), tf(*_uHABC[m]), sigma, gauss);
    }
  }
  
  // **********************************
  // PML
  // **********************************

  Pml::Pml(const unsigned int boundary, const double pmlSize, const std::string type) : Boundary(boundary), _size(pmlSize), _type(type)
  {
  }

  Pml::~Pml()
  {
  }

  double Pml::getSize() const
  {
    return _size;
  }

  gmshfem::function::ScalarFunction< std::complex< double > > Pml::pmlCoefficients(gmshfem::function::TensorFunction< std::complex< double > > &D, gmshfem::function::ScalarFunction< std::complex< double > > &E,  gmshfem::domain::Domain &bnd, const gmshfem::function::ScalarFunction< std::complex< double > > &kappa) const
  {
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod;
    const std::complex< double > im(0., 1.);

    gmshfem::function::ScalarFunction< std::complex< double > > distSigma;
    gmshfem::function::ScalarFunction< std::complex< double > > sigma;
    if(orientation() == "E" || orientation() == "W") {
      distSigma = abs(gmshfem::function::x< std::complex< double > >() - gmshfem::function::x< std::complex< double > >(bnd));
      if(bnd.maxDim() == 0) {
        kappaMod = changeOfCoordinates(kappa, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
      }
      else {
        kappaMod = changeOfCoordinates(kappa, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(), gmshfem::function::z< double >());
      }
    }
    else if(orientation() == "N" || orientation() == "S") {
      distSigma = abs(gmshfem::function::y< std::complex< double > >() - gmshfem::function::y< std::complex< double > >(bnd));
      if(bnd.maxDim() == 0) {
        kappaMod = changeOfCoordinates(kappa, gmshfem::function::x< double >(bnd), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >(bnd));
      }
      else {
        kappaMod = changeOfCoordinates(kappa, gmshfem::function::x< double >(), gmshfem::function::y< double >(bnd), gmshfem::function::z< double >());
      }
    }
    
    if(_type == "hs") {
      sigma = 1. / (_size - distSigma) - 1./ _size;
    }
    else if(_type == "h") {
      sigma = 1. / (_size - distSigma);
    }
    else if(_type == "q") {
      const double sigmaStar = 86.435; // N = 6 : 86.435; N = 1 : 186
      sigma = sigmaStar * distSigma * distSigma / (_size * _size);
    }
    
    gmshfem::function::ScalarFunction< std::complex< double > > kMod = 1. + im * sigma / kappaMod;
    if(orientation() == "E" || orientation() == "W") {
      D = gmshfem::function::tensorDiag< std::complex< double > >(1./kMod, kMod, kMod);
      E = kMod;
    }
    else if(orientation() == "N" || orientation() == "S") {
      D = gmshfem::function::tensorDiag< std::complex< double > >(kMod, 1./kMod, kMod);
      E = kMod;
    }
    
    return kappaMod;
  }

  // **********************************
  // PML continuous
  // **********************************

  PmlContinuous::PmlContinuous(const unsigned int boundary, const double pmlSize, const std::string type) : Pml(boundary, pmlSize, type)
  {
  }

  PmlContinuous::~PmlContinuous()
  {
    delete _v;
    delete _uPml;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * PmlContinuous::getUPml() const
  {
    return _uPml;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * PmlContinuous::getV() const
  {
    return _v;
  }

  void PmlContinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    gmshfem::domain::Domain pml = domains.getPml(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();
    // const double stab = parameters.getStab();
    
    _uPml = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uPml_" + orientation(), pml, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _v = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *u = static_cast< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * >(formulation.getField(fieldName));
    
    gmshfem::function::TensorFunction< std::complex< double > > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod = pmlCoefficients(D, E, sigma, kappa);
        
    formulation.integral(- D * grad(dof(*_uPml)), grad(tf(*_uPml)), pml, gauss);
    formulation.integral(kappaMod * kappaMod * E * dof(*_uPml), tf(*_uPml), pml, gauss);
      
    formulation.integral(dof(*_v), tf(*_uPml), sigma, gauss);
    formulation.integral(- dof(*_v), tf(*u), sigma, gauss);
      
    formulation.integral(dof(*_uPml), tf(*_v), sigma, gauss);
    formulation.integral(- dof(*u), tf(*_v), sigma, gauss);
        
    const std::complex< double > im(0.,1.);
    gmshfem::domain::Domain inf;
    if(_boundary == 0) {
      inf.addEntity(1, 9);
    }
    else if(_boundary == 2) {
      inf.addEntity(1, 41);
    }
    
    formulation.integral(im * kappaMod * dof(*_uPml), tf(*_uPml), inf, gauss);
    
    // penalization
    // formulation.integral(stab * dof(*_v), tf(*_v), sigma, gauss);
  }

  // **********************************
  // PML discontinuous
  // **********************************

  PmlDiscontinuous::PmlDiscontinuous(const unsigned int boundary, const double pmlSize, const std::string type) : Pml(boundary, pmlSize, type)
  {
  }

  PmlDiscontinuous::~PmlDiscontinuous()
  {
    delete _v;
    delete _uPml;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * PmlDiscontinuous::getUPml() const
  {
    return _uPml;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 > * PmlDiscontinuous::getV() const
  {
    return _v;
  }

  void PmlDiscontinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    gmshfem::domain::Domain pml = domains.getPml(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();
    const double stab = parameters.getStab();
    
    _uPml = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uPml_" + orientation(), pml, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
    _v = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm2::P_HierarchicalHCurl, neumannOrder);
    
    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *u = static_cast< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * >(formulation.getField(fieldName));
    
    gmshfem::function::TensorFunction< std::complex< double > > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod = pmlCoefficients(D, E, sigma, kappa);
      
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
      
    formulation.integral(- D * grad(dof(*_uPml)), grad(tf(*_uPml)), pml, gauss);
    formulation.integral(kappaMod * kappaMod * E * dof(*_uPml), tf(*_uPml), pml, gauss);
      
    formulation.integral(n * dof(*_v), tf(*_uPml), sigma, gauss);
    formulation.integral(- n * dof(*_v), tf(*u), sigma, gauss);
      
    formulation.integral(dof(*_uPml), n * tf(*_v), sigma, gauss);
    formulation.integral(- dof(*u), n * tf(*_v), sigma, gauss);
    
    // penalization
    formulation.integral(stab * dof(*_v), tf(*_v), sigma, gauss);
  }
  
  // **********************************
  // PML without multiplier
  // **********************************

  PmlWithoutMultiplier::PmlWithoutMultiplier(const unsigned int boundary, const double pmlSize, const std::string type) : Pml(boundary, pmlSize, type)
  {
  }

  PmlWithoutMultiplier::~PmlWithoutMultiplier()
  {
    delete _v;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * PmlWithoutMultiplier::getUPml() const
  {
    return _uPml;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * PmlWithoutMultiplier::getV() const
  {
    return _v;
  }

  void PmlWithoutMultiplier::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const std::string &fieldName, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain sigma = domains.getSigma(_boundary);
    gmshfem::domain::Domain pml = domains.getPml(_boundary);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();
    
    _uPml = static_cast< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * >(formulation.getField(fieldName));
    _uPml->domain(_uPml->domain() | pml);
    _v = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("v_" + orientation(), sigma, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
        
    gmshfem::function::TensorFunction< std::complex< double > > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod = pmlCoefficients(D, E, sigma, kappa);
            
    formulation.integral(- D * grad(dof(*_uPml)), grad(tf(*_uPml)), pml, gauss);
    formulation.integral(kappaMod * kappaMod * E * dof(*_uPml), tf(*_uPml), pml, gauss);
    
    gmshfem::domain::SkinLayer pmlLayer = pml.getSkinLayer(sigma);
    formulation.integral(dof(*_v), tf(*_v), sigma, gauss);
    formulation.integral(- D * grad(dof(*_uPml)), grad(tf(*_v)), pmlLayer, gauss);
    formulation.integral(kappaMod * kappaMod * E * dof(*_uPml), tf(*_v), pmlLayer, gauss);
  }


  // **********************************
  // HABC_HABC
  // **********************************

  HABC_HABC::HABC_HABC(const unsigned int corner, HABC *first, HABC *second) : Corner(corner, first, second)
  {
  }

  HABC_HABC::~HABC_HABC()
  {
    const unsigned int N0 = static_cast< HABC * >(_bnd[0])->getN();
    const unsigned int N1 = static_cast< HABC * >(_bnd[1])->getN();
    for(unsigned int m = 0; m < N0; ++m) {
      for(unsigned int n = 0; n < N1; ++n) {
        delete _uHABC_C[m][n];
      }
    }
    for(unsigned int m = 0; m < N0; ++m) {
      delete _vC[0][m];
    }
    for(unsigned int n = 0; n < N1; ++n) {
      delete _vC[1][n];
    }
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *HABC_HABC::getV(const unsigned int m, const unsigned int i) const
  {
    return _vC[i][m];
  }
  
  HABC *HABC_HABC::firstBoundary() const
  {
    return static_cast< HABC * >(_bnd[0]);
  }

  HABC *HABC_HABC::secondBoundary() const
  {
    return static_cast< HABC * >(_bnd[1]);
  }

  void HABC_HABC::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    // TODO: Only work when both HABC are the same
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();
    
    const unsigned int N0 = static_cast< HABC * >(_bnd[0])->getN();
    const unsigned int N1 = static_cast< HABC * >(_bnd[1])->getN();
    
    const double theta0 = static_cast< HABC * >(_bnd[0])->getTheta();
    const double theta1 = static_cast< HABC * >(_bnd[1])->getTheta();

    _uHABC_C.resize(N0);
    for(unsigned int m = 0; m < N0; ++m) {
      _uHABC_C[m].resize(N1);
      for(unsigned int n = 0; n < N1; ++n) {
        _uHABC_C[m][n] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uHABC_Corner_" + orientation() + "_" + std::to_string(m) + "_" + std::to_string(n), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);
      }
    }
    
    _vC[0].resize(N0);
    for(unsigned int m = 0; m < N0; ++m) {
      _vC[0][m] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vC_" + orientation() + "_" + std::to_string(m) + "_first", corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    }
    _vC[1].resize(N1);
    for(unsigned int n = 0; n < N1; ++n) {
      _vC[1][n] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vC_" + orientation() + "_" + std::to_string(n) + "_second", corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    }
    
    const double pi = 3.141592653589793238462643383279;
    
    const std::complex< double > expPTheta_0(std::cos(theta0), std::sin(theta0));
    const std::complex< double > expPTheta2_0(std::cos(theta0/2.), std::sin(theta0/2.));
    
    const std::complex< double > expPTheta_1(std::cos(theta1), std::sin(theta1));
    const std::complex< double > expPTheta2_1(std::cos(theta1/2.), std::sin(theta1/2.));
    
    const double M0 = 2 * N0 + 1;
    double cPade0[N0];
    for(unsigned int m = 0; m < N0; ++m) {
      cPade0[m] = std::tan((m+1) * pi / M0) * std::tan((m+1) * pi / M0);
    }
    
    const double M1 = 2 * N1 + 1;
    double cPade1[N1];
    for(unsigned int n = 0; n < N1; ++n) {
      cPade1[n] = std::tan((n+1) * pi / M1) * std::tan((n+1) * pi / M1);
    }

    const std::complex< double > im(0., 1.);
    
    std::vector< gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * > uHABC[2];
    uHABC[0].resize(N0);
    uHABC[1].resize(N1);
    
    for(unsigned int m = 0; m < N0; ++m) {
      uHABC[0][m] = static_cast< HABC * >(_bnd[0])->getUHABC(m);
      formulation.integral(-dof(*_vC[0][m]), tf(*uHABC[0][m]), corner, gauss);

      formulation.integral( dof(*_vC[0][m]), tf(*_vC[0][m]), corner, gauss);
      formulation.integral(-im * kappa * expPTheta2_1 * dof(*uHABC[0][m]), tf(*_vC[0][m]), corner, gauss);
      
      for(unsigned int n = 0; n < N1; ++n) {
        formulation.integral(-im * kappa * expPTheta2_1 * 2./M1 * cPade1[n] * dof(*uHABC[0][m]), tf(*_vC[0][m]), corner, gauss);
        formulation.integral(-im * kappa * expPTheta2_1 * 2./M1 * cPade1[n] * dof(*_uHABC_C[m][n]), tf(*_vC[0][m]), corner, gauss);
      }
    }
    
    for(unsigned int n = 0; n < N1; ++n) {
      uHABC[1][n] = static_cast< HABC * >(_bnd[1])->getUHABC(n);
      formulation.integral(-dof(*_vC[1][n]), tf(*uHABC[1][n]), corner, gauss);

      formulation.integral( dof(*_vC[1][n]), tf(*_vC[1][n]), corner, gauss);
      formulation.integral(-im * kappa * expPTheta2_0 * dof(*uHABC[1][n]), tf(*_vC[1][n]), corner, gauss);
      
      for(unsigned int m = 0; m < N0; ++m) {
        formulation.integral(-im * kappa * expPTheta2_0 * 2./M0 * cPade0[m] * dof(*uHABC[1][n]), tf(*_vC[1][n]), corner, gauss);
        formulation.integral(-im * kappa * expPTheta2_0 * 2./M0 * cPade0[m] * dof(*_uHABC_C[m][n]), tf(*_vC[1][n]), corner, gauss);
      }
    }
    
    // auxiliary
    for(unsigned int m = 0; m < N0; ++m) {
      for(unsigned int n = 0; n < N1; ++n) {
        formulation.integral( (expPTheta_0 * cPade0[m] + expPTheta_1 * cPade1[n] + 1.) * dof(*_uHABC_C[m][n]), tf(*_uHABC_C[m][n]), corner, gauss);
        formulation.integral( expPTheta_1 * (cPade1[n] + 1.) * dof(*uHABC[0][m]), tf(*_uHABC_C[m][n]), corner, gauss);
        formulation.integral( expPTheta_0 * (cPade0[m] + 1.) * dof(*uHABC[1][n]), tf(*_uHABC_C[m][n]), corner, gauss);
      }
    }
  }

  // **********************************
  // PML continuous _ PML continuous
  // **********************************

  PmlContinuous_PmlContinuous::PmlContinuous_PmlContinuous(const unsigned int corner, PmlContinuous *first, PmlContinuous *second) : Corner(corner, first, second)
  {
  }

  PmlContinuous_PmlContinuous::~PmlContinuous_PmlContinuous()
  {
    delete _uPmlCorner;
    delete _vC[0];
    delete _vC[1];
    delete _vCorner;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlContinuous_PmlContinuous::getUPml() const
  {
    return _uPmlCorner;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlContinuous_PmlContinuous::getV(const unsigned int i) const
  {
    return _vC[i];
  }

  PmlContinuous *PmlContinuous_PmlContinuous::firstBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[0]);
  }

  PmlContinuous *PmlContinuous_PmlContinuous::secondBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[1]);
  }

  void PmlContinuous_PmlContinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();
    // const double stab = parameters.getStab();

    _uPmlCorner = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

    _vC[0] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vC[1] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uPml[2];
    uPml[0] = static_cast< PmlContinuous * >(_bnd[0])->getUPml();
    uPml[1] = static_cast< PmlContinuous * >(_bnd[1])->getUPml();

    gmshfem::function::TensorFunction< std::complex< double > > D[2];
    gmshfem::function::ScalarFunction< std::complex< double > > E[2];
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod = static_cast< PmlContinuous * >(_bnd[0])->pmlCoefficients(D[0], E[0], corner, kappa);
    static_cast< PmlContinuous * >(_bnd[1])->pmlCoefficients(D[1], E[1], corner, kappa);

    formulation.integral(- D[0] * D[1] * grad(dof(*_uPmlCorner)), grad(tf(*_uPmlCorner)), pmlCorner, gauss);
    formulation.integral(kappaMod * kappaMod * E[0] * E[1] * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCorner, gauss);

    formulation.integral(dof(*_vC[0]), tf(*_uPmlCorner), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vC[0]), tf(*uPml[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_uPmlCorner), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(- dof(*uPml[0]), tf(*_vC[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_vC[1]), tf(*_uPmlCorner), pmlBnd.second, gauss);
    formulation.integral(- dof(*_vC[1]), tf(*uPml[1]), pmlBnd.second, gauss);

    formulation.integral(dof(*_uPmlCorner), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(- dof(*uPml[1]), tf(*_vC[1]), pmlBnd.second, gauss);
    
    // corner equation
    _vCorner = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    formulation.integral(dof(*_vC[0]), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*_vC[0]), corner, gauss);

    formulation.integral(-dof(*_vC[1]), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*_vC[1]), corner, gauss);

    formulation.integral(dof(*static_cast< PmlContinuous * >(_bnd[0])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*static_cast< PmlContinuous * >(_bnd[0])->getV()), corner, gauss);

    formulation.integral(-dof(*static_cast< PmlContinuous * >(_bnd[1])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*static_cast< PmlContinuous * >(_bnd[1])->getV()), corner, gauss);
    
    // penalization
    // formulation.integral(stab * dof(*_vC[0]), tf(*_vC[0]), pmlBnd.first, gauss);
    // formulation.integral(stab * dof(*_vC[1]), tf(*_vC[1]), pmlBnd.second, gauss);
  }

  // **********************************
  // PML discontinuous _ PML discontinuous
  // **********************************

  PmlDiscontinuous_PmlDiscontinuous::PmlDiscontinuous_PmlDiscontinuous(const unsigned int corner, PmlDiscontinuous *first, PmlDiscontinuous *second) : Corner(corner, first, second)
  {
  }

  PmlDiscontinuous_PmlDiscontinuous::~PmlDiscontinuous_PmlDiscontinuous()
  {
    delete _uPmlCorner;
    delete _vC[0];
    delete _vC[1];
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlDiscontinuous_PmlDiscontinuous::getUPml() const
  {
    return _uPmlCorner;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >* PmlDiscontinuous_PmlDiscontinuous::getV(const unsigned int i) const
  {
    return _vC[i];
  }

  PmlDiscontinuous *PmlDiscontinuous_PmlDiscontinuous::firstBoundary() const
  {
    return static_cast< PmlDiscontinuous * >(_bnd[0]);
  }

  PmlDiscontinuous *PmlDiscontinuous_PmlDiscontinuous::secondBoundary() const
  {
    return static_cast< PmlDiscontinuous * >(_bnd[1]);
  }

  void PmlDiscontinuous_PmlDiscontinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const unsigned int fieldOrder = parameters.getFieldOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();
    const double stab = parameters.getStab();

    _uPmlCorner = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("uPmlCorner_" + orientation(), pmlCorner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, fieldOrder);

    _vC[0] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm2::P_HierarchicalHCurl, neumannOrder);
    _vC[1] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm2::P_HierarchicalHCurl, neumannOrder);

    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uPml[2];
    uPml[0] = static_cast< PmlDiscontinuous * >(_bnd[0])->getUPml();
    uPml[1] = static_cast< PmlDiscontinuous * >(_bnd[1])->getUPml();

    gmshfem::function::TensorFunction< std::complex< double > > D[2];
    gmshfem::function::ScalarFunction< std::complex< double > > E[2];
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod = static_cast< PmlDiscontinuous * >(_bnd[0])->pmlCoefficients(D[0], E[0], corner, kappa);
    static_cast< PmlDiscontinuous * >(_bnd[1])->pmlCoefficients(D[1], E[1], corner, kappa);
    
    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();

    formulation.integral(- D[0] * D[1] * grad(dof(*_uPmlCorner)), grad(tf(*_uPmlCorner)), pmlCorner, gauss);
    formulation.integral(kappaMod * kappaMod * E[0] * E[1] * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCorner, gauss);

    formulation.integral(n * dof(*_vC[0]), tf(*_uPmlCorner), pmlBnd.first, gauss);
    formulation.integral(- n * dof(*_vC[0]), tf(*uPml[0]), pmlBnd.first, gauss);

    formulation.integral(dof(*_uPmlCorner), n * tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(- dof(*uPml[0]), n * tf(*_vC[0]), pmlBnd.first, gauss);

    formulation.integral(n * dof(*_vC[1]), tf(*_uPmlCorner), pmlBnd.second, gauss);
    formulation.integral(- n * dof(*_vC[1]), tf(*uPml[1]), pmlBnd.second, gauss);

    formulation.integral(dof(*_uPmlCorner), n * tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(- dof(*uPml[1]), n * tf(*_vC[1]), pmlBnd.second, gauss);
    
    // penalization
    formulation.integral(stab * dof(*_vC[0]), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(stab * dof(*_vC[1]), tf(*_vC[1]), pmlBnd.second, gauss);
  }
  
  // **********************************
  // PML without multiplier _ PML without multiplier
  // **********************************

  PmlWithoutMultiplier_PmlWithoutMultiplier::PmlWithoutMultiplier_PmlWithoutMultiplier(const unsigned int corner, PmlWithoutMultiplier *first, PmlWithoutMultiplier *second) : Corner(corner, first, second)
  {
  }

  PmlWithoutMultiplier_PmlWithoutMultiplier::~PmlWithoutMultiplier_PmlWithoutMultiplier()
  {
    delete _vC[0];
    delete _vC[1];
    delete _vCorner;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlWithoutMultiplier_PmlWithoutMultiplier::getUPml() const
  {
    return _uPmlCorner;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlWithoutMultiplier_PmlWithoutMultiplier::getV(const unsigned int i) const
  {
    return _vC[i];
  }

  PmlWithoutMultiplier *PmlWithoutMultiplier_PmlWithoutMultiplier::firstBoundary() const
  {
    return static_cast< PmlWithoutMultiplier * >(_bnd[0]);
  }

  PmlWithoutMultiplier *PmlWithoutMultiplier_PmlWithoutMultiplier::secondBoundary() const
  {
    return static_cast< PmlWithoutMultiplier * >(_bnd[1]);
  }

  void PmlWithoutMultiplier_PmlWithoutMultiplier::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();

    _uPmlCorner = static_cast< PmlWithoutMultiplier * >(_bnd[0])->getUPml();
    _uPmlCorner->domain(_uPmlCorner->domain() | pmlCorner);

    _vC[0] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    _vC[1] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::function::TensorFunction< std::complex< double > > D[2];
    gmshfem::function::ScalarFunction< std::complex< double > > E[2];
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod = static_cast< PmlWithoutMultiplier * >(_bnd[0])->pmlCoefficients(D[0], E[0], corner, kappa);
    static_cast< PmlWithoutMultiplier * >(_bnd[1])->pmlCoefficients(D[1], E[1], corner, kappa);
    
    formulation.integral(- D[0] * D[1] * grad(dof(*_uPmlCorner)), grad(tf(*_uPmlCorner)), pmlCorner, gauss);
    formulation.integral(kappaMod * kappaMod * E[0] * E[1] * dof(*_uPmlCorner), tf(*_uPmlCorner), pmlCorner, gauss);
    
    gmshfem::domain::SkinLayer layerPmlCornerFirst = pmlCorner.getSkinLayer(pmlBnd.first);
    gmshfem::domain::SkinLayer layerPmlCornerSecond = pmlCorner.getSkinLayer(pmlBnd.second);

    formulation.integral(dof(*_vC[0]), tf(*_vC[0]), pmlBnd.first, gauss);
    formulation.integral(- D[0] * D[1] * grad(dof(*_uPmlCorner)), grad(tf(*_vC[0])), layerPmlCornerFirst, gauss);
    formulation.integral(kappaMod * kappaMod * E[0] * E[1] * dof(*_uPmlCorner), tf(*_vC[0]), layerPmlCornerFirst, gauss);
    formulation.integral(dof(*_vC[1]), tf(*_vC[0]), pmlBnd.second, gauss);

    formulation.integral(dof(*_vC[1]), tf(*_vC[1]), pmlBnd.second, gauss);
    formulation.integral(- D[0] * D[1] * grad(dof(*_uPmlCorner)), grad(tf(*_vC[1])), layerPmlCornerSecond, gauss);
    formulation.integral(kappaMod * kappaMod * E[0] * E[1] * dof(*_uPmlCorner), tf(*_vC[1]), layerPmlCornerSecond, gauss);
    formulation.integral(dof(*_vC[0]), tf(*_vC[1]), pmlBnd.first, gauss);

    formulation.integral(- dof(*_vC[0]), tf(*static_cast< PmlWithoutMultiplier * >(_bnd[0])->getV()), pmlBnd.first, gauss);
    formulation.integral(- dof(*_vC[1]), tf(*static_cast< PmlWithoutMultiplier * >(_bnd[1])->getV()), pmlBnd.second, gauss);

    // corner equation
    _vCorner = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vCorner_" + orientation(), corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    
    formulation.integral(dof(*_vC[0]), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*_vC[0]), corner, gauss);

    formulation.integral(-dof(*_vC[1]), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*_vC[1]), corner, gauss);
      
    formulation.integral(dof(*static_cast< PmlWithoutMultiplier * >(_bnd[0])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(dof(*_vCorner), tf(*static_cast< PmlWithoutMultiplier * >(_bnd[0])->getV()), corner, gauss);

    formulation.integral(-dof(*static_cast< PmlWithoutMultiplier * >(_bnd[1])->getV()), tf(*_vCorner), corner, gauss);
    formulation.integral(-dof(*_vCorner), tf(*static_cast< PmlWithoutMultiplier * >(_bnd[1])->getV()), corner, gauss);
    
//    gmshfem::domain::SkinLayer layerCorner = pmlCorner.getSkinLayer(corner);
//
//    formulation.integral(2. * dof(*_vCorner), tf(*_vCorner), corner, gauss);
//    formulation.integral(- D[0] * D[1] * grad(dof(*_uPmlCorner)), grad(tf(*_vCorner)), layerCorner, gauss);
//    formulation.integral(kappaMod * kappaMod * E[0] * E[1] * dof(*_uPmlCorner), tf(*_vCorner), layerCorner, gauss);
//
//    formulation.integral(dof(*_vCorner), tf(*_vC[0]), corner, gauss);
//    formulation.integral(dof(*_vCorner), tf(*_vC[1]), corner, gauss);
  }

  // **********************************
  // PML continuous _ Sommerfeld
  // **********************************

  PmlContinuous_Sommerfeld::PmlContinuous_Sommerfeld(const unsigned int corner, PmlContinuous *first, Sommerfeld *second) : Corner(corner, first, second)
  {
  }

  PmlContinuous_Sommerfeld::~PmlContinuous_Sommerfeld()
  {
    delete _vC;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* PmlContinuous_Sommerfeld::getV() const
  {
    return _vC;
  }

  PmlContinuous *PmlContinuous_Sommerfeld::firstBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[0]);
  }

  Sommerfeld *PmlContinuous_Sommerfeld::secondBoundary() const
  {
    return static_cast< Sommerfeld * >(_bnd[1]);
  }

  void PmlContinuous_Sommerfeld::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();

    _vC = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uPml;
    uPml = static_cast< PmlContinuous * >(_bnd[0])->getUPml();

    // To get kappa at the corner
    gmshfem::function::TensorFunction< std::complex< double > > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod = static_cast< PmlContinuous * >(_bnd[0])->pmlCoefficients(D, E, corner, kappa);

    const std::complex< double > im(0., 1.);
//    formulation.integral(-dof(*_vC), tf(*uPml), pmlBnd.first, gauss);

//    formulation.integral(dof(*_vC), tf(*_vC), pmlBnd.first, gauss);
    formulation.integral(im * E * kappaMod * dof(*uPml), tf(*uPml), pmlBnd.first, gauss);
  }

  // **********************************
  // Sommerfeld _ PML continuous
  // **********************************

  Sommerfeld_PmlContinuous::Sommerfeld_PmlContinuous(const unsigned int corner, Sommerfeld *first, PmlContinuous *second) : Corner(corner, first, second)
  {
  }

  Sommerfeld_PmlContinuous::~Sommerfeld_PmlContinuous()
  {
    delete _vC;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >* Sommerfeld_PmlContinuous::getV() const
  {
    return _vC;
  }

  Sommerfeld *Sommerfeld_PmlContinuous::firstBoundary() const
  {
    return static_cast< Sommerfeld * >(_bnd[0]);
  }

  PmlContinuous *Sommerfeld_PmlContinuous::secondBoundary() const
  {
    return static_cast< PmlContinuous * >(_bnd[1]);
  }

  void Sommerfeld_PmlContinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();

    _vC = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);

    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uPml;
    uPml = static_cast< PmlContinuous * >(_bnd[1])->getUPml();

    // To get kappa at the corner
    gmshfem::function::TensorFunction< std::complex< double > > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod = static_cast< PmlContinuous * >(_bnd[1])->pmlCoefficients(D, E, corner, kappa);

    const std::complex< double > im(0., 1.);
//    formulation.integral(-dof(*_vC), tf(*uPml), pmlBnd.second, gauss);

//    formulation.integral(dof(*_vC), tf(*_vC), pmlBnd.second, gauss);
    formulation.integral(im * E * kappaMod * dof(*uPml), tf(*uPml), pmlBnd.second, gauss);
  }
  
  // **********************************
  // PML discontinuous _ Sommerfeld
  // **********************************

  PmlDiscontinuous_Sommerfeld::PmlDiscontinuous_Sommerfeld(const unsigned int corner, PmlDiscontinuous *first, Sommerfeld *second) : Corner(corner, first, second)
  {
  }

  PmlDiscontinuous_Sommerfeld::~PmlDiscontinuous_Sommerfeld()
  {
    delete _vC;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >* PmlDiscontinuous_Sommerfeld::getV() const
  {
    return _vC;
  }

  PmlDiscontinuous *PmlDiscontinuous_Sommerfeld::firstBoundary() const
  {
    return static_cast< PmlDiscontinuous * >(_bnd[0]);
  }

  Sommerfeld *PmlDiscontinuous_Sommerfeld::secondBoundary() const
  {
    return static_cast< Sommerfeld * >(_bnd[1]);
  }

  void PmlDiscontinuous_Sommerfeld::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();

    _vC = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >("vC_" + orientation() + "_first", pmlBnd.first, gmshfem::field::FunctionSpaceTypeForm2::P_HierarchicalHCurl, neumannOrder);

    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uPml;
    uPml = static_cast< PmlDiscontinuous * >(_bnd[0])->getUPml();

    // To get kappa at the corner
    gmshfem::function::TensorFunction< std::complex< double > > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod = static_cast< PmlDiscontinuous * >(_bnd[0])->pmlCoefficients(D, E, corner, kappa);

    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    const std::complex< double > im(0., 1.);
    formulation.integral(-n * dof(*_vC), tf(*uPml), pmlBnd.first, gauss);

    formulation.integral(dof(*_vC), tf(*_vC), pmlBnd.first, gauss);
    formulation.integral(im * kappaMod * dof(*uPml), n * tf(*_vC), pmlBnd.first, gauss);
  }

  // **********************************
  // Sommerfeld _ PML discontinuous
  // **********************************

  Sommerfeld_PmlDiscontinuous::Sommerfeld_PmlDiscontinuous(const unsigned int corner, Sommerfeld *first, PmlDiscontinuous *second) : Corner(corner, first, second)
  {
  }

  Sommerfeld_PmlDiscontinuous::~Sommerfeld_PmlDiscontinuous()
  {
    delete _vC;
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >* Sommerfeld_PmlDiscontinuous::getV() const
  {
    return _vC;
  }

  Sommerfeld *Sommerfeld_PmlDiscontinuous::firstBoundary() const
  {
    return static_cast< Sommerfeld * >(_bnd[0]);
  }

  PmlDiscontinuous *Sommerfeld_PmlDiscontinuous::secondBoundary() const
  {
    return static_cast< PmlDiscontinuous * >(_bnd[1]);
  }

  void Sommerfeld_PmlDiscontinuous::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    gmshfem::domain::Domain pmlCorner = domains.getPmlCorner(_corner);
    std::pair< gmshfem::domain::Domain, gmshfem::domain::Domain > pmlBnd = domains.getPmlBnd(_corner);

    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();

    _vC = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form2 >("vC_" + orientation() + "_second", pmlBnd.second, gmshfem::field::FunctionSpaceTypeForm2::P_HierarchicalHCurl, neumannOrder);

    gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *uPml;
    uPml = static_cast< PmlDiscontinuous * >(_bnd[1])->getUPml();

    // To get kappa at the corner
    gmshfem::function::TensorFunction< std::complex< double > > D;
    gmshfem::function::ScalarFunction< std::complex< double > > E;
    gmshfem::function::ScalarFunction< std::complex< double > > kappaMod = static_cast< PmlDiscontinuous * >(_bnd[1])->pmlCoefficients(D, E, corner, kappa);

    gmshfem::function::VectorFunction< std::complex< double > > n = gmshfem::function::normal< std::complex< double > >();
    const std::complex< double > im(0., 1.);
    formulation.integral(-n * dof(*_vC), tf(*uPml), pmlBnd.second, gauss);

    formulation.integral(dof(*_vC), tf(*_vC), pmlBnd.second, gauss);
    formulation.integral(im * E * kappaMod * dof(*uPml), n * tf(*_vC), pmlBnd.second, gauss);
  }
  
  // **********************************
  // HABC_Sommerfeld
  // **********************************

  HABC_Sommerfeld::HABC_Sommerfeld(const unsigned int corner, HABC *first, Sommerfeld *second) : Corner(corner, first, second)
  {
  }

  HABC_Sommerfeld::~HABC_Sommerfeld()
  {
    const unsigned int N = static_cast< HABC * >(_bnd[0])->getN();
    for(unsigned int m = 0; m < N; ++m) {
      delete _vC[m];
    }
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *HABC_Sommerfeld::getV(const unsigned int m) const
  {
    return _vC[m];
  }
  
  HABC *HABC_Sommerfeld::firstBoundary() const
  {
    return static_cast< HABC * >(_bnd[0]);
  }

  Sommerfeld *HABC_Sommerfeld::secondBoundary() const
  {
    return static_cast< Sommerfeld * >(_bnd[1]);
  }

  void HABC_Sommerfeld::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();
    
    const unsigned int N = static_cast< HABC * >(_bnd[0])->getN();
    
    _vC.resize(N);
    for(unsigned int m = 0; m < N; ++m) {
      _vC[m] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vC_" + orientation() + "_" + std::to_string(m) + "_first", corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    }
    
    const std::complex< double > im(0., 1.);
    
    for(unsigned int m = 0; m < N; ++m) {
      gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * uHABC = static_cast< HABC * >(_bnd[0])->getUHABC(m);
      
      formulation.integral(dof(*_vC[m]), tf(*uHABC), corner, gauss);

      formulation.integral(dof(*_vC[m]), tf(*_vC[m]), corner, gauss);
      formulation.integral(im * kappa * dof(*uHABC), tf(*_vC[m]), corner, gauss);
    }
  }
  
  // **********************************
  // Sommerfeld_HABC
  // **********************************

  Sommerfeld_HABC::Sommerfeld_HABC(const unsigned int corner, Sommerfeld *first, HABC *second) : Corner(corner, first, second)
  {
  }

  Sommerfeld_HABC::~Sommerfeld_HABC()
  {
    const unsigned int N = static_cast< HABC * >(_bnd[1])->getN();
    for(unsigned int m = 0; m < N; ++m) {
      delete _vC[m];
    }
  }

  gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > *Sommerfeld_HABC::getV(const unsigned int m) const
  {
    return _vC[m];
  }
  
  Sommerfeld *Sommerfeld_HABC::firstBoundary() const
  {
    return static_cast< Sommerfeld * >(_bnd[0]);
  }

  HABC *Sommerfeld_HABC::secondBoundary() const
  {
    return static_cast< HABC * >(_bnd[1]);
  }

  void Sommerfeld_HABC::writeFormulation(gmshfem::problem::Formulation< std::complex< double > > &formulation, const SubproblemDomains &domains, const SubproblemParameters &parameters)
  {
    gmshfem::domain::Domain corner = domains.getCorner(_corner);
    
    const unsigned int neumannOrder = parameters.getNeumannOrder();
    const std::string gauss = parameters.getGauss();
    const gmshfem::function::ScalarFunction< std::complex< double > > kappa = parameters.getKappa();
    
    const unsigned int N = static_cast< HABC * >(_bnd[1])->getN();
    
    _vC.resize(N);
    for(unsigned int m = 0; m < N; ++m) {
      _vC[m] = new gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 >("vC_" + orientation() + "_" + std::to_string(m) + "_second", corner, gmshfem::field::FunctionSpaceTypeForm0::HierarchicalH1, neumannOrder);
    }
    
    const std::complex< double > im(0., 1.);
    
    for(unsigned int m = 0; m < N; ++m) {
      gmshfem::field::Field< std::complex< double >, gmshfem::field::Form::Form0 > * uHABC = static_cast< HABC * >(_bnd[1])->getUHABC(m);
      
      formulation.integral(dof(*_vC[m]), tf(*uHABC), corner, gauss);

      formulation.integral(dof(*_vC[m]), tf(*_vC[m]), corner, gauss);
      formulation.integral(im * kappa * dof(*uHABC), tf(*_vC[m]), corner, gauss);
    }
  }

}
