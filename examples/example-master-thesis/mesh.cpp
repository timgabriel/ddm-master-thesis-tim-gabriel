#include "mesh.h"

#include "gmsh.h"

#include <gmshfem/Message.h>

#include <tuple>
#include <algorithm>

#define EPS 1e-6

namespace D2 {

	void createDomains(const unsigned int nDomX, const unsigned int nDomY, 
					  const double xSize, const double ySize, 
					  const double lc, const int order, 
					  const std::vector< std::pair< double, double > > &points)
	{
		gmsh::vectorpair dimTags;
		gmsh::vectorpair toolDimTags;
		double xMin = 0., yMin = 0., zMin = 0., xMax = 0., yMax = 0., zMax = 0.;

		/*
			Geometry
		*/
		
		unsigned index;
		unsigned jndex;
		auto tag = [=] (unsigned i, unsigned j) {return int((nDomX+1)*j + i + 1);};

				
		// Coners
		for(index = 0; index < nDomX+1; ++index) {
		for(jndex = 0; jndex < nDomY+1; ++jndex) {
			gmsh::model::occ::addPoint(index*xSize, jndex*ySize, 0., lc, tag(index, jndex));			
		}
		}
		
		gmsh::model::occ::synchronize();


		
		// Embedded points
		for (unsigned i = 0; i < points.size(); ++i) {
		/*
			xMin = points[i].first - EPS*xSize;  xMax = points[i].first + EPS*xSize;
			yMin = points[i].second - EPS*xSize; yMax = points[i].second + EPS*xSize;
			zMin = - EPS*xSize;                  zMax = EPS*xSize;
			gmsh::model::occ::getEntitiesInBoundingBox(xMin, yMin, zMin, xMax, yMax, zMax, dimTags, 0);
			if (!dimTags.size())
			*/
			if ((std::fmod(points[i].first, xSize) < EPS || std::fmod(points[i].first, xSize) > xSize-EPS) &&
				(std::fmod(points[i].second, ySize) < EPS || std::fmod(points[i].second, ySize) > ySize-EPS))
				continue;
			
			gmsh::model::occ::addPoint(points[i].first, points[i].second, 0.0, lc, (nDomX+1)*(nDomY+1) + i+1);
		}

		gmsh::model::occ::synchronize();




		// Lines
		for(index = 0; index < nDomX; ++index) {		
		for(jndex = 0; jndex < nDomY; ++jndex) {
			gmsh::model::occ::addLine(tag(index, jndex), tag(index+1, jndex), 2*tag(index, jndex));
			gmsh::model::occ::addLine(tag(index, jndex), tag(index, jndex+1), 2*tag(index, jndex)+1);
		}
		}
		
		jndex = nDomY;
		for(index = 0; index < nDomX; ++index) {		
			gmsh::model::occ::addLine(tag(index, jndex), tag(index+1, jndex), 2*tag(index, jndex));
		}
		
		index = nDomX;
		for(jndex = 0; jndex < nDomY; ++jndex) {
			gmsh::model::occ::addLine(tag(index, jndex), tag(index, jndex+1), 2*tag(index, jndex)+1);
		}

		gmsh::model::occ::synchronize();








		// Embed point on vertical line 
		for(index = 0; index < nDomX+1; ++index) {
			toolDimTags.clear();
			for (unsigned i = 0; i < points.size(); ++i) {
				if (abs(index*xSize - points[i].first) < EPS) {
					if (std::fmod(points[i].second, ySize) < EPS || std::fmod(points[i].second, ySize) > ySize-EPS)
						continue;

					toolDimTags.push_back({ 0, (nDomX+1)*(nDomY+1) + i+1 });
				}
			}


			if (toolDimTags.empty()) 
				continue;


			gmsh::vectorpair objectDimTags;
			gmsh::model::getBoundingBox(0, tag(index, 0), xMin, yMin, zMin, xMin, yMin, zMin);
			gmsh::model::getBoundingBox(0, tag(index, nDomY), xMax, yMax, zMax, xMax, yMax, zMax);
			
			gmsh::model::occ::getEntitiesInBoundingBox(xMin-EPS*xSize, yMin-EPS*ySize, zMin-EPS, xMax+EPS*xSize, yMax+EPS*ySize, zMax+EPS, objectDimTags, 1);
			/*
			for (auto objectDimTag : objectDimTags)
				std::cout << "(" << objectDimTag.first << ", "<< objectDimTag.second << ")" << std::endl;
			*/
			gmsh::vectorpair outDimTags;
			std::vector<gmsh::vectorpair> outDimTagsMap;
			gmsh::model::occ::fragment(objectDimTags, toolDimTags, outDimTags,
										outDimTagsMap, -1, false, false);
			/*
			for (auto outDimTag : outDimTags)
				std::cout << "(" << outDimTag.first << ", "<< outDimTag.second << ")" << std::endl;
			*/
			
			for (auto outDimTag : outDimTags) {
				if (outDimTag.first != 1)
					continue;
				
				for (auto it  = objectDimTags.begin(); it < objectDimTags.end(); ++it) {
					if (outDimTag.second == it->second) {
						objectDimTags.erase(it);
						break;
					}
				}
			}

			/*
			for (auto objectDimTag : objectDimTags)
				std::cout << "(" << objectDimTag.first << ", "<< objectDimTag.second << ")" << std::endl;
			*/
			gmsh::model::occ::remove(objectDimTags);
	
		}
	
		gmsh::model::occ::synchronize();

		// Embed point on horizontal line 
		for(jndex = 0; jndex < nDomY+1; ++jndex) {
			toolDimTags.clear();
			for (unsigned i = 0; i < points.size(); ++i) {
				if (abs(jndex*ySize - points[i].second) < EPS) {
					if (std::fmod(points[i].first, xSize) < EPS || std::fmod(points[i].first, xSize) > xSize-EPS)
						continue;
					toolDimTags.push_back({ 0, (nDomX+1)*(nDomY+1) + i+1 });
				}
			}

			if (toolDimTags.empty()) 
				continue;

			gmsh::vectorpair objectDimTags;
			gmsh::model::getBoundingBox(0, tag(0, jndex), xMin, yMin, zMin, xMin, yMin, zMin);
			gmsh::model::getBoundingBox(0, tag(nDomX, jndex), xMax, yMax, zMax, xMax, yMax, zMax);
			
			gmsh::model::occ::getEntitiesInBoundingBox(xMin-EPS*xSize, yMin-EPS*ySize, zMin-EPS, xMax+EPS*xSize, yMax+EPS*ySize, zMax+EPS, objectDimTags, 1);
			/*
			for (auto objectDimTag : objectDimTags)
				std::cout << "(" << objectDimTag.first << ", "<< objectDimTag.second << ")" << std::endl;
			*/
			gmsh::vectorpair outDimTags;
			std::vector<gmsh::vectorpair> outDimTagsMap;
			gmsh::model::occ::fragment(objectDimTags, toolDimTags, 
									   outDimTags, outDimTagsMap, 
									   -1, false, false);
			
			for (auto outDimTag : outDimTags) {
				if (outDimTag.first != 1)
					continue;
				
				for (auto it  = objectDimTags.begin(); it < objectDimTags.end(); ++it) {
					if (outDimTag.second == it->second) {
						objectDimTags.erase(it);
						break;
					}
				}
			}
			
			gmsh::model::occ::remove(objectDimTags);
			
		}
		
		gmsh::model::occ::synchronize();


		// Surfaces
		for(index = 0; index < nDomX; ++index) {
		for(jndex = 0; jndex < nDomY; ++jndex) {
			std::vector<int> loop;
			gmsh::model::getBoundingBox(0, tag(index, jndex), xMin, yMin, zMin, xMin, yMin, zMin);
			gmsh::model::getBoundingBox(0, tag(index+1, jndex), xMax, yMax, zMax, xMax, yMax, zMax);
			gmsh::model::occ::getEntitiesInBoundingBox(xMin-EPS*xSize, yMin-EPS*ySize, zMin-EPS, xMax+EPS*xSize, yMax+EPS*ySize, zMax+EPS, dimTags, 1);
			for (auto it = dimTags.begin(); it < dimTags.end(); ++it)
				loop.push_back(it->second);
			xMin = xMax; yMin = yMax; zMin = zMax;
			gmsh::model::getBoundingBox(0, tag(index+1, jndex+1), xMax, yMax, zMax, xMax, yMax, zMax);
			gmsh::model::occ::getEntitiesInBoundingBox(xMin-EPS*xSize, yMin-EPS*ySize, zMin-EPS, xMax+EPS*xSize, yMax+EPS*ySize, zMax+EPS, dimTags, 1);
			for (auto it = dimTags.begin(); it < dimTags.end(); ++it)
				loop.push_back(it->second);
			gmsh::model::getBoundingBox(0, tag(index, jndex+1), xMin, yMin, zMin, xMin, yMin, zMin);
			gmsh::model::occ::getEntitiesInBoundingBox(xMin-EPS*xSize, yMin-EPS*ySize, zMin-EPS, xMax+EPS*xSize, yMax+EPS*ySize, zMax+EPS, dimTags, 1);
			for (auto it = dimTags.end()-1; it >= dimTags.begin(); --it)
				loop.push_back(-it->second);
			xMax = xMin; yMax = yMin; zMax = zMin;
			gmsh::model::getBoundingBox(0, tag(index, jndex), xMin, yMin, zMin, xMin, yMin, zMin);
			gmsh::model::occ::getEntitiesInBoundingBox(xMin-EPS*xSize, yMin-EPS*ySize, zMin-EPS, xMax+EPS*xSize, yMax+EPS*ySize, zMax+EPS, dimTags, 1);
			for (auto it = dimTags.end()-1; it >= dimTags.begin(); --it)
				loop.push_back(-it->second);
				
			gmsh::model::occ::addCurveLoop(loop, tag(index, jndex));
			gmsh::model::occ::addPlaneSurface({ tag(index, jndex) }, tag(index, jndex));
		}
		}
	
		gmsh::model::occ::synchronize();




		/*
			Embed points
		*/
		// In surface
		for(index = 0; index < nDomX; ++index) {
		for(jndex = 0; jndex < nDomY; ++jndex) {
			toolDimTags.clear();
			for (unsigned i = 0; i < points.size(); ++i) {
				if (index*xSize < points[i].first  && points[i].first < (index+1)*xSize &&
					jndex*ySize < points[i].second && points[i].second < (jndex+1)*ySize) {
					if ((std::fmod(points[i].first, xSize) < EPS || std::fmod(points[i].first, xSize) > xSize-EPS) &&
						(std::fmod(points[i].second, ySize) < EPS || std::fmod(points[i].second, ySize) > ySize-EPS))
						continue;
					toolDimTags.push_back({ 0, (nDomX+1)*(nDomY+1) + i+1 });
				}	
			}
	
			if (toolDimTags.empty()) 
				continue;

			gmsh::vectorpair outDimTags;
			std::vector<gmsh::vectorpair> outDimTagsMap;
			gmsh::model::occ::fragment({ {2, tag(index, jndex)} }, toolDimTags, 
									   outDimTags, outDimTagsMap, 
									   -1, false, false);
			/*
			for (auto outDimTag : outDimTags)
				std::cout << "(" << outDimTag.first << ", " << outDimTag.second << ")" << std::endl;
			std::cout << "- - - - - - - -" << std::endl;
			*/
			gmsh::model::occ::remove({ {2, tag(index, jndex)} });
			
		}
		}

		gmsh::model::occ::synchronize();



		/*
			Physical groups
		*/
		
		std::vector< std::string > dir {"E", "N", "W", "S"};
		std::vector< int > pts;
		for(index = 0; index < nDomX; ++index) {
		for(jndex = 0; jndex < nDomY; ++jndex) {
			std::string subdomainTag = "_" + std::to_string(index) + "_" + std::to_string(jndex);

			pts = { tag(index+1, jndex), tag(index+1, jndex+1), tag(index, jndex+1), tag(index, jndex) }; 

			for(unsigned i = 0; i < 4; ++i) {
				// Corners
				gmsh::model::addPhysicalGroup(0, {pts[(i+1)%4]}, -1, "corner" + dir[i] + dir[(i+1)%4] + subdomainTag);

				// Lines
				gmsh::model::getBoundingBox(0, pts[i%4], xMin, yMin, zMin, xMin, yMin, zMin);
				gmsh::model::getBoundingBox(0, pts[(i+1)%4], xMax, yMax, zMax, xMax, yMax, zMax);
				if(xMin > xMax) std::swap(xMin, xMax);
				if(yMin > yMax) std::swap(yMin, yMax);
				if(zMin > zMax) std::swap(zMin, zMax);

				gmsh::model::occ::getEntitiesInBoundingBox(xMin-EPS*xSize, yMin-EPS*ySize, zMin-EPS, xMax+EPS*xSize, yMax+EPS*ySize, zMax+EPS, dimTags, 1);


				std::vector< int > lineTags = {};
				/*
				if (dimTags.size() > 1) {
					
					for (auto dimTag : dimTags)
						std::cout << "(" << dimTag.first << ", " << dimTag.second << ")" << std::endl;
					auto minIt = dimTags.begin();
					for (auto it = dimTags.begin() + 1; it < dimTags.end(); ++it) {
						if (it->second < minIt->second) {
							minIt = it;
						}
					}

					// dimTags.erase(minIt);
					for (auto dimTag : dimTags)
						std::cout << "(" << dimTag.first << ", " << dimTag.second << ")" << std::endl;
					std::cout << "----------------" << std::endl;
					
					//lineTags.push_back(minIt->second);
				}
				*/


				for (auto dimTag : dimTags)
					lineTags.push_back(dimTag.second);
				gmsh::model::addPhysicalGroup(1, {lineTags}, -1, "sigma" + dir[i] + subdomainTag);
			}


			// Surfaces
			gmsh::model::getBoundingBox(0, pts[3], xMin, yMin, zMin, xMin, yMin, zMin);
			gmsh::model::getBoundingBox(0, pts[1], xMax, yMax, zMax, xMax, yMax, zMax);
			gmsh::model::occ::getEntitiesInBoundingBox(xMin-EPS*xSize, yMin-EPS*ySize, zMin-EPS, xMax+EPS*xSize, yMax+EPS*ySize, zMax+EPS, dimTags, 2);
			gmsh::model::addPhysicalGroup(2, {dimTags[0].second}, -1, "omega_" + std::to_string(index) + "_" + std::to_string(jndex));


			// Embedded points
			for (unsigned i = 0; i < points.size(); ++i) {
					if ((std::fmod(points[i].first, xSize) < EPS || std::fmod(points[i].first, xSize) > xSize-EPS) &&
						(std::fmod(points[i].second, ySize) < EPS || std::fmod(points[i].second, ySize) > ySize-EPS)) {

					xMin = points[i].first - EPS*xSize;  xMax = points[i].first + EPS*xSize;
					yMin = points[i].second - EPS*xSize; yMax = points[i].second + EPS*xSize;
					zMin = - EPS*xSize;                  zMax = EPS*xSize;
					gmsh::model::occ::getEntitiesInBoundingBox(xMin, yMin, zMin, xMax, yMax, zMax, dimTags, 0);
					gmsh::model::addPhysicalGroup(0, {dimTags[0].second}, -1, "embeddedPoint_" + std::to_string(i));

				}
				else
					gmsh::model::addPhysicalGroup(0, {(int) ((nDomX+1)*(nDomY+1) + i+1)}, -1, "embeddedPoint_" + std::to_string(i));

			}


		}
		}

		gmsh::model::occ::synchronize();
		gmsh::model::mesh::generate();
		gmsh::model::mesh::setOrder(order);
	}


}
