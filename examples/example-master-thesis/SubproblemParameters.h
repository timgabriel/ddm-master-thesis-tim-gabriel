#ifndef H_SUBPROBLEMPARAMETERS
#define H_SUBPROBLEMPARAMETERS

#include <gmshfem/Function.h>

class SubproblemParameters
{
 private:
  std::string _gauss;
  gmshfem::function::ScalarFunction< std::complex< double > > _kappa;
  unsigned int _neumannOrder;
  unsigned int _fieldOrder;
  double _stab;
  
 public:
  SubproblemParameters();
  ~SubproblemParameters();
  
  void setGauss(const std::string &gauss);
  std::string getGauss() const;
  
  void setKappa(const gmshfem::function::ScalarFunction< std::complex< double > > &kappa);
  gmshfem::function::ScalarFunction< std::complex< double > > getKappa() const;
  
  void setNeumannOrder(const unsigned int neumannOrder);
  unsigned int getNeumannOrder() const;
  
  void setFieldOrder(const unsigned int fieldOrder);
  unsigned int getFieldOrder() const;
  
  void setStab(const double stab);
  double getStab() const;
};

#endif // H_SUBPROBLEMPARAMETERS
