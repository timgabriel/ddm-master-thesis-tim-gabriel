#ifndef H_MESH
#define H_MESH

#include <gmshfem/Function.h>

namespace D2 {

	void createDomains(const unsigned int nDomX, const unsigned int nDomY, 
					   const double xSize, const double ySize, 
					   const double lc, const int order, 
					   const std::vector< std::pair< double, double > > &points = {});
  
}

#endif // H_MESH
