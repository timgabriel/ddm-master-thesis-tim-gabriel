// Gmsh project created on Thu Oct  6 12:29:07 2022
//SetFactory("OpenCASCADE");
//+
Point(1) = {0, 0, 0, 0.05};
//+
Point(2) = {0.8, 0, 0, 0.05};
//+
Point(3) = {1.2, 0, 0, 0.05};
//+
Point(4) = {2.0, 0, 0, 0.05};
//+
Point(5) = {2, 1, 0, 0.05};
//+
Point(6) = {1.2, 1, 0, 0.05};
//+
Point(7) = {0.8, 1, 0, 0.05};
//+
Point(8) = {0.0, 1, 0, 0.05};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 7};
//+
Line(3) = {7, 8};
//+
Line(4) = {8, 1};
//+
Line(5) = {3, 2};
//+
Line(6) = {6, 3};
//+
Line(7) = {6, 7};
//+
Line(8) = {3, 4};
//+
Line(9) = {4, 5};
//+
Line(10) = {5, 6};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {7, -2, -5, -6};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {10, 6, 8, 9};
//+
Plane Surface(3) = {3};
//+
Physical Surface("left", 11) = {1};
//+
Physical Surface("center", 12) = {2};
//+
Physical Surface("right", 13) = {3};
//+
Physical Curve("gammaLeft", 14) = {4};
//+
Physical Curve("gammaRight", 15) = {9};
//+
Physical Curve("interface1", 16) = {2};
//+
Physical Curve("interface2", 17) = {6};
