#include "gmsh.h"

#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshddm/MPIInterface.h>
#include <gmshfem/Message.h>


using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;
using namespace gmshddm::mpi;

using namespace gmshfem;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::problem;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;


int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  unsigned int nDom = 2;
  int order = 1;
  double lc = 0.05;
  std::string gauss = "Gauss10";

  gmshDdm.userDefinedParameter(nDom, "nDom");
  gmshDdm.userDefinedParameter(order, "order");
  gmshDdm.userDefinedParameter(lc, "lc");
  gmshDdm.userDefinedParameter(gauss, "gauss");

  gmsh::open("../domain.geo"); // Assume we run from the "build" subfolder
  gmsh::model::mesh::generate();

  // Domains are partitions of the x-axis.
  Domain left("left"); // 0 to 1.2
  Domain center("center"); // 0.8 to 1.2
  Domain right("right"); // 0.8 to 2

  Domain gammaLeft("gammaLeft"); // 0
  Domain interface1("interface1"); // 0.8
  Domain interface2("interface2"); // 1.2
  Domain gammaRight("gammaRight"); // 2.0

  Subdomain omega(nDom);
  Interface sigma(nDom);
  omega(0) = gammaLeft | left | interface1 | center;
  omega(1) = gammaRight | center | interface2 | right;

  sigma(0, 1) = interface1 | interface2;
  sigma(1, 0) = interface2 | interface1;

  std::vector< std::vector< unsigned int > > topology(nDom);
  topology[0] = {1};
  topology[1] = {0};

  // Create DDM formulation
  // And BCs on the subdomain problems
  gmshddm::problem::Formulation< std::complex< double > > formulation("Laplace", topology);
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega,  FunctionSpaceTypeForm0::HierarchicalH1, order);

  // Toy problem with solution u(x,y) = x
  u(0).addConstraint(gammaLeft, 0);
  u(1).addConstraint(gammaRight, 2);
  auto trueSolution = x<std::complex< double >>();


  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, order);
  formulation.addInterfaceField(g);

  // First domain
  // Laplacian(u) = 0 on the domain, and u matches g(1,0) on the interface
  formulation(0).integral(grad(dof(u(0))), grad(tf(u(0))), omega(0), gauss);
  formulation(0).integral((-g(1, 0)), tf(u(0)), interface2, gauss);
  formulation(0).integral(dof(u(0)), tf(u(0)), interface2, gauss);
  
  // First interface (What 0 sends to 1)
  // Compute g(0,1) such that g(0,1) + g(1,0) = 2 u(0)
  formulation(0, 1).integral(-dof(g(0, 1)), tf(g(0, 1)), sigma(0, 1), gauss);
  formulation(0, 1).integral(-(g(1, 0)), tf(g(0, 1)), sigma(0, 1), gauss);
  formulation(0, 1).integral((2*u(0)), tf(g(0, 1)), sigma(0, 1), gauss);

  // Second domain
  formulation(1).integral(grad(dof(u(1))), grad(tf(u(1))), omega(1), gauss);
  formulation(1).integral((-g(0, 1)), tf(u(1)), interface1, gauss);
  formulation(1).integral(dof(u(1)), tf(u(1)), interface1, gauss);

  // Second interface
  formulation(1, 0).integral(-dof(g(1, 0)), tf(g(1, 0)), sigma(1, 0), gauss);
  formulation(1, 0).integral(-(g(0, 1)), tf(g(1, 0)), sigma(1, 0), gauss);
  formulation(1, 0).integral((2*u(1)), tf(g(1, 0)), sigma(1, 0), gauss);


  formulation.pre();
  int iterMax = 1000;
  gmshDdm.userDefinedParameter(iterMax, "iter");
  formulation.solve("jacobi", 1e-10, iterMax);


  for(unsigned int i = 0; i < nDom; ++i) {
    if(isItMySubdomain(i)) {
      save(u(i), omega(i), "u_" + std::to_string(i));
      save(u(i) - trueSolution, omega(i), "err_" + std::to_string(i));
    }
  }


  return 0;
}
