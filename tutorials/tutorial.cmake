# The following commands are useful for the demos, and can be included in each
# demos's CMakeLists.txt; they fill the EXTRA_INCS and EXTRA_LIBS variables.

# Detect if we use the MinGW compilers on Cygwin - if we do, handle the build as
# a pure Windows build
if(CYGWIN OR MSYS)
  if(CMAKE_CXX_COMPILER_ID MATCHES "GNU" OR
     CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpmachine
                    OUTPUT_VARIABLE CXX_COMPILER_MACHINE
                    OUTPUT_STRIP_TRAILING_WHITESPACE)
    if(CXX_COMPILER_MACHINE MATCHES "mingw")
      set(WIN32 1)
      add_definitions(-DWIN32)
      set(CMAKE_FIND_LIBRARY_PREFIXES "lib" "")
      set(CMAKE_FIND_LIBRARY_SUFFIXES ".a" ".so" ".lib" ".LIB" ".dll" ".DLL")
    endif()
  endif()
endif()

# Make sure we have C++17 support
set(CMAKE_CXX_STANDARD 17)

# Enable all warnings
include(CheckCXXCompilerFlag)
check_cxx_compiler_flag("-Wall" HAVE_WALL)
if(HAVE_WALL)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
endif()

# until Eigen is updated
check_cxx_compiler_flag("-Wno-unused-but-set-variable" HAVE_UNUSED)
if(HAVE_UNUSED)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-unused-but-set-variable")
endif()

# until Boost is updated and we remove all use of sprintf
check_cxx_compiler_flag("-Wno-deprecated-declarations" HAVE_DEPRECATED)
if(HAVE_DEPRECATED)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-deprecated-declarations")
endif()

# remove "could not create compact unwind" linker warnings on macOS; but breaks
# exception handling
if(APPLE)
  # set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,-no_compact_unwind")
endif()

# Find Gmsh
find_library(GMSH_LIB gmsh)
if(NOT GMSH_LIB)
  message(FATAL_ERROR "Could not find Gmsh library")
endif(NOT GMSH_LIB)
find_path(GMSH_INC gmsh.h)
if(NOT GMSH_INC)
  message(FATAL_ERROR "Could not find Gmsh header 'gmsh.h'")
endif(NOT GMSH_INC)
list(APPEND EXTRA_INCS ${GMSH_INC})
list(APPEND EXTRA_LIBS ${GMSH_LIB})

# Find GmshFEM
find_library(GMSHFEM_LIB gmshfem)
if(NOT GMSHFEM_LIB)
  message(FATAL_ERROR "Could not find GmshFEM library")
endif(NOT GMSHFEM_LIB)
find_path(GMSHFEM_INC gmshfem/GmshFem.h)
if(NOT GMSHFEM_INC)
  message(FATAL_ERROR "Could not find GmshFEM header 'gmshfem/GmshFem.h'")
endif(NOT GMSHFEM_INC)
list(APPEND EXTRA_LIBS ${GMSHFEM_LIB})
list(APPEND EXTRA_INCS ${GMSHFEM_INC} ${GMSHFEM_INC}/gmshfem)

# Find Eigen if GmshFEM was not compiled with contrib Eigen
if(NOT EXISTS ${GMSHFEM_INC}/gmshfem/Eigen)
  find_path(EIGEN_INC "Eigen/Dense" PATH_SUFFIXES eigen3)
  if(EIGEN_INC)
    list(APPEND EXTRA_INCS ${EIGEN_INC})
  endif()
endif()

# Find GmshDDM
find_library(GMSHDDM_LIB gmshddm)
if(NOT GMSHDDM_LIB)
  message(FATAL_ERROR "Could not find GmshDDM library")
endif(NOT GMSHDDM_LIB)
find_path(GMSHDDM_INC gmshddm/GmshDdm.h)
if(NOT GMSHDDM_INC)
  message(FATAL_ERROR "Could not find GmshDDM header 'gmshddm/GmshDdm.h'")
endif(NOT GMSHDDM_INC)
list(APPEND EXTRA_LIBS ${GMSHDDM_LIB})
list(APPEND EXTRA_INCS ${GMSHDDM_INC})

# OpenMP
find_package(OpenMP)
if(OPENMP_FOUND)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

if(APPLE AND EXISTS "/usr/local/opt/libomp")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Xpreprocessor -fopenmp -I/usr/local/opt/libomp/include")
  list(APPEND EXTRA_LIBS "-L/usr/local/opt/libomp/lib -lomp")
  message(STATUS "Found OpenMP[Homebrew]")
elseif(APPLE AND EXISTS "/opt/local/lib/libomp")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Xpreprocessor -fopenmp -I/opt/local/include/libomp")
  list(APPEND EXTRA_LIBS "-L/opt/local/lib/libomp -lomp")
  message(STATUS "Found OpenMP[MacPorts]")
elseif(APPLE AND EXISTS "/opt/homebrew/opt/libomp")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Xpreprocessor -fopenmp -I/opt/homebrew/opt/libomp/include")
  list(APPEND EXTRA_LIBS "-L/opt/homebrew/opt/libomp/lib -lomp")
  message(STATUS "Found OpenMP[Homebrew]")
endif()

# MPI
find_package(MPI)
if(MPI_FOUND)
  list(APPEND EXTRA_INCS ${MPI_CXX_INCLUDE_PATH})
  list(APPEND EXTRA_LIBS ${MPI_CXX_LIBRARIES})
  set(CMAKE_C_COMPILER ${MPI_C_COMPILER})
  set(CMAKE_CXX_COMPILER ${MPI_CXX_COMPILER})
  set(CMAKE_Fortran_COMPILER ${MPI_Fortran_COMPILER})
endif()

include_directories(${EXTRA_INCS})
