import gmsh
import sys

# > python tintin.py 8
# > mpirun -np 8 build/tuto -mesh tintin -nDom 8

gmsh.initialize(sys.argv)

if len(sys.argv) < 2:
    print('Usage: {} nDom ...'.format(sys.argv[0]))
    exit(0)

nDom = int(sys.argv[1])

gmsh.option.setNumber('General.NumThreads', 8)
gmsh.option.setNumber('Geometry.OCCBoundsUseStl', 1)
gmsh.option.setNumber('Mesh.MeshSizeFromCurvature', 15)
gmsh.option.setNumber('Mesh.MeshSizeMin', 1)
gmsh.option.setNumber('Mesh.MeshSizeMax', 30)
gmsh.option.setNumber('Mesh.Binary', 1)
gmsh.option.setNumber('Mesh.Algorithm3D', 10)
gmsh.option.setNumber('Mesh.PartitionSplitMeshFiles', 1)

# from https://grabcad.com/library/rocket-44
gmsh.merge('tintin.brep')

v = gmsh.model.getEntities(3)
b = [(3, gmsh.model.occ.addBox(-500, -200, -500, 1000, 1500, 1000))]
gmsh.model.occ.cut(b, v, 100)
scat = gmsh.model.occ.getEntitiesInBoundingBox(-400,-150,-400, 400, 1300, 400, dim=2)
ext = [e for e in gmsh.model.occ.getEntities(2) if e not in scat]
gmsh.model.occ.synchronize()

gmsh.model.addPhysicalGroup(2, [e[1] for e in scat], 1)
gmsh.model.addPhysicalGroup(2, [e[1] for e in ext], 2)
gmsh.model.addPhysicalGroup(3, [100], 3)

gmsh.model.setPhysicalName(2, 1, 'gamma_scat')
gmsh.model.setPhysicalName(2, 2, 'gamma_ext')
gmsh.model.setPhysicalName(3, 3, 'omega')

gmsh.model.mesh.generate(3)
gmsh.model.mesh.partition(nDom)
gmsh.write('tintin.msh')

gmsh.finalize()
