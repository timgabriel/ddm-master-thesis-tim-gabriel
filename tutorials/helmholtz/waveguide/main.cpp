#include "gmsh.h"

#include <gmshddm/Formulation.h>
#include <gmshddm/GmshDdm.h>
#include <gmshddm/MPIInterface.h>
#include <gmshfem/Message.h>

using namespace gmshddm;
using namespace gmshddm::common;
using namespace gmshddm::domain;
using namespace gmshddm::problem;
using namespace gmshddm::field;
using namespace gmshddm::mpi;

using namespace gmshfem;
using namespace gmshfem::domain;
using namespace gmshfem::equation;
using namespace gmshfem::problem;
using namespace gmshfem::field;
using namespace gmshfem::function;
using namespace gmshfem::post;

void waveguide(const unsigned int nDom, const double l, const double L, const double lc)
{
  if(nDom < 2) {
    gmshfem::msg::error << "nDom should be at least equal to 2." << gmshfem::msg::endl;
    exit(0);
  }
  gmsh::model::add("waveguide");

  const double domainSize = L / nDom;

  std::vector< std::vector< int > > borders(nDom);
  std::vector< int > omegas(nDom);
  std::vector< int > sigmas(nDom - 1);

  int p[2];

  p[0] = gmsh::model::geo::addPoint(0., l, 0., lc);
  p[1] = gmsh::model::geo::addPoint(0., 0., 0., lc);

  int line = gmsh::model::geo::addLine(p[0], p[1]);
  gmsh::model::addPhysicalGroup(1, {line}, 1);
  gmsh::model::setPhysicalName(1, 1, "gammaDir");

  std::vector< int > cornerTags;
  for(unsigned int i = 0; i < nDom; ++i) {
    std::vector< std::pair< int, int > > extrudeEntities;
    gmsh::model::geo::extrude({std::make_pair(1, line)}, domainSize, 0., 0., extrudeEntities, {static_cast< int >((domainSize) / lc)}, std::vector< double >(), true);
    line = extrudeEntities[0].second;
    omegas[i] = extrudeEntities[1].second;
    borders[i].push_back(extrudeEntities[2].second);
    borders[i].push_back(extrudeEntities[3].second);
    sigmas[i] = line;
    if(i != nDom - 1) {
      cornerTags.push_back(3 + cornerTags.size());
      cornerTags.push_back(3 + cornerTags.size());
    }
  }

  gmsh::model::addPhysicalGroup(1, {line}, 2);
  gmsh::model::setPhysicalName(1, 2, "gammaInf");

  for(unsigned int i = 0; i < borders.size(); ++i) {
    gmsh::model::addPhysicalGroup(1, borders[i], 100 + i);
    gmsh::model::setPhysicalName(1, 100 + i, "border_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < omegas.size(); ++i) {
    gmsh::model::addPhysicalGroup(2, {omegas[i]}, i + 1);
    gmsh::model::setPhysicalName(2, i + 1, "omega_" + std::to_string(i));
  }

  for(unsigned int i = 0; i < sigmas.size(); ++i) {
    gmsh::model::addPhysicalGroup(1, {sigmas[i]}, 200 + i);
    gmsh::model::setPhysicalName(1, 200 + i, "sigma_" + std::to_string(i) + "_" + std::to_string(i + 1));
  }

  gmsh::model::addPhysicalGroup(0, cornerTags, 1000);
  gmsh::model::setPhysicalName(0, 1000, "corner");

  gmsh::model::geo::synchronize();
  gmsh::model::mesh::generate();
  //gmsh::write("test.msh");
}

int main(int argc, char **argv)
{
  GmshDdm gmshDdm(argc, argv);

  double pi = 3.14159265359;

  unsigned int nDom = 3;
  gmshDdm.userDefinedParameter(nDom, "nDom");
  double L = 5;
  double l = 1;
  double lc = 0.02;
  double k = 2 * pi;
  double kIn = 1.;
  int order = 1;
  gmshDdm.userDefinedParameter(L, "L");
  gmshDdm.userDefinedParameter(l, "l");
  gmshDdm.userDefinedParameter(lc, "lc");
  gmshDdm.userDefinedParameter(k, "k");
  gmshDdm.userDefinedParameter(kIn, "kIn");
  gmshDdm.userDefinedParameter(order, "order");
  std::string gauss = "Gauss10";
  gmshDdm.userDefinedParameter(gauss, "gauss");

  // Build geometry and mesh using gmsh API
  waveguide(nDom, l, L, lc);

  // Define domain
  Subdomain omega(nDom);
  Subdomain gammaInf(nDom);
  Subdomain gammaDir(nDom);
  Subdomain border(nDom);
  Interface sigma(nDom);
  Domain corner = Domain("corner");

  for(unsigned int i = 0; i < nDom; ++i) {
    omega(i) = Domain(2, i + 1);

    if(i == nDom - 1) {
      gammaInf(i) = Domain(1, 2);
    }

    if(i == 0) {
      gammaDir(i) = Domain(1, 1);
    }

    border(i) = Domain(1, 100 + i);

    if(i != 0) {
      sigma(i, i - 1) = Domain(1, 200 + i - 1);
    }
    if(i != nDom - 1) {
      sigma(i, i + 1) = Domain(1, 200 + i);
    }
  }

  // Define topology
  std::vector< std::vector< unsigned int > > topology(nDom);
  for(unsigned int i = 0; i < nDom; ++i) {
    if(i != 0) {
      topology[i].push_back(i - 1);
    }
    if(i != nDom - 1) {
      topology[i].push_back(i + 1);
    }
  }


  // Create DDM formulation
  gmshddm::problem::Formulation< std::complex< double > > formulation("Helmholtz", topology);

  // Create fields
  SubdomainField< std::complex< double >, Form::Form0 > u("u", omega | gammaDir | gammaInf | border | sigma, FunctionSpaceTypeForm0::HierarchicalH1, order);
  for(unsigned int i = 0; i < nDom; ++i) {
    u(i).addConstraint(border(i), 0.);
  }
  u(0).addConstraint(gammaDir(0), sin< std::complex< double > >(pi * y< std::complex< double > >() / l));
  InterfaceField< std::complex< double >, Form::Form0 > g("g", sigma, FunctionSpaceTypeForm0::HierarchicalH1, order);
  for(unsigned int i = 0; i < nDom; ++i) {
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      g(i, j).addConstraint(corner, 0.);
    }
  }

  std::complex< double > im(0., 1.);

  // Tell to the formulation that g is field that have to be exchanged between subdomains
  formulation.addInterfaceField(g);

  // Add terms to the formulation
  for(unsigned int i = 0; i < nDom; ++i) {
    // VOLUME TERMS
    formulation(i).integral(grad(dof(u(i))), grad(tf(u(i))), omega(i), gauss);
    formulation(i).integral(-k * k * dof(u(i)), tf(u(i)), omega(i), gauss);
    formulation(i).integral(-im * k * dof(u(i)), tf(u(i)), gammaInf(i), gauss);

    // Artificial source
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i).integral(-im * k * dof(u(i)), tf(u(i)), sigma(i, j), gauss);
      formulation(i).integral(formulation.artificialSource(-g(j, i)), tf(u(i)), sigma(i, j), gauss);
    }

    // INTERFACE TERMS
    for(unsigned int jj = 0; jj < topology[i].size(); ++jj) {
      const unsigned int j = topology[i][jj];
      formulation(i, j).integral(dof(g(i, j)), tf(g(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(formulation.artificialSource(g(j, i)), tf(g(i, j)), sigma(i, j), gauss);
      formulation(i, j).integral(2. * im * k * u(i), tf(g(i, j)), sigma(i, j), gauss);
    }
  }

  // Solve the DDM formulation
  formulation.pre();
  int iterMax = 1000;
  gmshDdm.userDefinedParameter(iterMax, "iter");
  formulation.solve("gmres", 1e-6, iterMax);

  for(unsigned int i = 0; i < nDom; ++i) {
    if(isItMySubdomain(i)) {
      save(u(i), omega(i), "u_" + std::to_string(i));
    }
  }

  bool save_matrix = false;
  gmshDdm.userDefinedParameter(save_matrix, "save_iteration_matrix");
  if (save_matrix) {
    auto mat = formulation.computeMatrix();
    msg::info << "nnz = " << mat.numberOfNonZero() << ", symmetric = " << mat.isSymmetric() << ", hermitian = " << mat.isHermitian() << msg::endl;
    mat.save("Mymatrix");
    mat.saveSpyPlot("Mymatrix");
  }

  return 0;
}
