// GmshDDM - Copyright (C) 2019-2021, A. Royer, C. Geuzaine, Université de Liège
// GmshDDM - Copyright (C) 2022, A. Royer, C. Geuzaine, B. Martin, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_SOLVERS
#define H_GMSHDDM_SOLVERS

#include "InterfaceCompoundField.h"
#include "InterfaceField.h"
#include "SubdomainCompoundField.h"
#include "SubdomainField.h"

#include <gmshfem/Formulation.h>
#include <unordered_map>
#include <vector>

typedef struct _p_Mat *Mat;
typedef struct _p_Vec *Vec;
typedef struct _p_KSP *KSP;

#ifdef HAVE_PETSC
#include <petsc.h>
#endif

namespace gmshddm
{


  namespace problem
  {
    // Defines whether the solver needs a A product (e.g. Jacobi) or I-A product (e.g. most Krylov solvers)
    enum class ProductType {A, I_A};

    template< class T_Scalar >
    class AbstractIterativeSolver
    {

     public:

      virtual ~AbstractIterativeSolver() = default;
      virtual void solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol=1e-6, int maxIter=100) = 0;
      virtual std::string solverName() const = 0;
      virtual ProductType productType() const = 0;

      std::vector< gmshfem::scalar::Precision< T_Scalar > > absoluteResidual() const
      {
        return _absoluteResidual;
      }

      std::vector< gmshfem::scalar::Precision< T_Scalar > > relativeResidual() const
      {
        return _relativeResidual;
      }

     protected:
      std::vector< gmshfem::scalar::Precision< T_Scalar > > _absoluteResidual;
      std::vector< gmshfem::scalar::Precision< T_Scalar > > _relativeResidual;
      void _runIterationOperations(const unsigned int iteration, const gmshfem::scalar::Precision< T_Scalar > rnorm);
      void _clearIterationOperations();
    };

    template< class T_Scalar >
    class JacobiIterativeSolver : public AbstractIterativeSolver< T_Scalar >
    {
     public:
      virtual void solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol = 1e-6, int maxIter = 100) override;
      virtual std::string solverName() const override;
      virtual ProductType productType() const override;
    };

    template< class T_Scalar >
    class GCRIterativeSolver : public AbstractIterativeSolver< T_Scalar >
    {
     public:
      GCRIterativeSolver(const unsigned restart = 30, const unsigned reuse = 0, const unsigned reuseSelect = 0, const bool residualBased = true, const bool reuseSol = false, const bool trueResidual = false, const bool modifiedGS = true);
      ~GCRIterativeSolver() override;
      virtual void solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol = 1e-6, int maxIter = 100);
      virtual std::string solverName() const override;
      virtual ProductType productType() const override;
      virtual void recomputeAV();

     private:
      std::string _solverName;
      bool        _residualBased, _reuseSol, _trueResidual, _modifiedGS, _recomputeAV;
      unsigned    _restart, _reuse, _reuseSelect, _iReuse;
      Vec         _x, *_V, *_AV;
      PetscReal   *_coeff;
    };

    template< class T_Scalar >
    class SGMRESIterativeSolver : public AbstractIterativeSolver< T_Scalar >
    {
     public:
      SGMRESIterativeSolver(const unsigned restart = 30, const unsigned reuse = 30, const unsigned reuseSelect = 0, const bool residualBased = false, const bool reuseSol = false, const bool trueResidual = false, const bool modifiedGS = true);
      ~SGMRESIterativeSolver() override;
      virtual void solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol = 1e-6, int maxIter = 100);
      virtual std::string solverName() const override;
      virtual ProductType productType() const override;

     private:
      std::string _solverName;
      bool        _residualBased, _reuseSol, _trueResidual, _modifiedGS;
      unsigned    _restart, _reuse, _reuseSelect, _nReuse;
      Vec         _x, *_V, *_C;
      PetscScalar **_H;
      PetscReal   *_coeff;
    };

    template< class T_Scalar >
    class GMRESIterativeSolver : public AbstractIterativeSolver< T_Scalar >
    {
     public:
      GMRESIterativeSolver(const unsigned restart = 30, const unsigned reuse = 30, const unsigned reuseSelect = 0, const bool reuseSol = false, const bool trueResidual = false, const bool modifiedGS = true);
      ~GMRESIterativeSolver() override;
      virtual void solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol = 1e-6, int maxIter = 100);
      virtual std::string solverName() const override;
      virtual ProductType productType() const override;

     private:
      bool        _reuseSol, _trueResidual, _modifiedGS;
      unsigned    _restart, _reuse, _reuseSelect, _nReuse;
      Vec         _x, *_V, *_C;
      PetscScalar *_c, *_s, **_H;
      PetscReal   *_coeff;
    };

    template< class T_Scalar >
    class KSPIterativeSolver : public AbstractIterativeSolver< T_Scalar >
    {
     public:
      KSPIterativeSolver(const std::string &name) :
        _solverName(name) {}
      virtual void solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol = 1e-6, int maxIter = 100) override;
      virtual std::string solverName() const override;
      virtual ProductType productType() const override;


     private:
      std::string _solverName;

      // T_Scalar, PetscScalar, T_Interger
      template< class T_Scalar1, class T_Scalar2, class T_Integer >
      struct PetscInterface {
        static int kspPrint(KSP ksp, T_Integer it, gmshfem::scalar::Precision< T_Scalar2 > rnorm, void *mctx)
        {
          KSPIterativeSolver< T_Scalar1 > *solver = static_cast< KSPIterativeSolver< T_Scalar1 > * >(mctx);
          solver->_runIterationOperations(it, rnorm);
          return 0;
        }

      };
    };

  } // namespace problem

} // namespace gmshddm


#endif // H_GMSHDDM_SOLVERS
