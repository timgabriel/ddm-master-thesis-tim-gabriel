// GmshDDM - Copyright (C) 2019-2021, A. Royer, C. Geuzaine, B. Martin, Université de Liège
// GmshDDM - Copyright (C) 2022, A. Royer, C. Geuzaine, B. Martin, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "Solvers.h"

#include "Formulation.h"
#include "MPIInterface.h"
#include "resources.h"

#include <gmshfem/Message.h>

#include <regex>


#ifdef HAVE_PETSC
#include <petsc.h>
#endif


namespace gmshddm
{


  namespace problem
  {


    // Instantiate the required templates
    template class AbstractIterativeSolver< std::complex< double > >;
    template class AbstractIterativeSolver< std::complex< float > >;
    template class JacobiIterativeSolver< std::complex< double > >;
    template class JacobiIterativeSolver< std::complex< float > >;
    template class GCRIterativeSolver< std::complex< double > >;
    template class GCRIterativeSolver< std::complex< float > >;
    template class SGMRESIterativeSolver< std::complex< double > >;
    template class SGMRESIterativeSolver< std::complex< float > >;
    template class GMRESIterativeSolver< std::complex< double > >;
    template class GMRESIterativeSolver< std::complex< float > >;
    template class KSPIterativeSolver< std::complex< double > >;
    template class KSPIterativeSolver< std::complex< float > >;

    // Projection of t in B
    template< class T_Scalar >
    void project(Vec t, unsigned n, T_Scalar *val, Vec *B, bool modifiedGS) {
      if (modifiedGS) {
        for (unsigned i = 0; i < n; ++i) {
          VecDot(t, B[i], &val[i]);
          VecAXPY(t, -val[i], B[i]);
        }
      } else {
        VecMDot(t, n, B, val);
        for (unsigned i = 0; i < n; ++i) val[i] = -val[i];
        VecMAXPY(t, n, val, B);
        for (unsigned i = 0; i < n; ++i) val[i] = -val[i];
      }
    };

    template< class T_Scalar >
    void AbstractIterativeSolver< T_Scalar >::_runIterationOperations(const unsigned int iteration, const gmshfem::scalar::Precision< T_Scalar > rnorm)
    {
      _absoluteResidual.push_back(rnorm);
      _relativeResidual.push_back(rnorm / _absoluteResidual[0]);

      const unsigned int MPI_Rank = mpi::getMPIRank();
      if(MPI_Rank == 0) {
        gmshfem::msg::info << " - Iteration " << iteration << ": absolute residual = " << _absoluteResidual.back() << ", relative residual = " << _relativeResidual.back() << gmshfem::msg::endl;
      }
      gmshddm::common::s_printResources("DDM Iteration");
    }

    template< class T_Scalar >
    void AbstractIterativeSolver< T_Scalar >::_clearIterationOperations()
    {
      _absoluteResidual.clear();
      _relativeResidual.clear();
    }


    template< class T_Scalar >
    void JacobiIterativeSolver< T_Scalar >::solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol, int maxIter)
    {


      double r0;
      VecNorm(RHS, NORM_2, &r0);
      const double stopCriterion = relTol * r0;

#ifndef HAVE_PETSC
      gmshfem::msg::error << "PETSc is required for the Jacobi iterative solver." << gmshfem::msg::endl;
#endif

      Vec W; // residual
      VecDuplicate(RHS, &W);

      VecSet(X, 0.);
      PetscReal norm = 1;
      int i = 0;
      do { // jacobi
        MatVectProductA< T_Scalar >(A, X, Y);
        VecAYPX(Y, 1., RHS);
        VecWAXPY(W, -1., X, Y);
        VecCopy(Y, X);

        VecNorm(W, NORM_2, &norm);
        this->_runIterationOperations(i, norm);
        i++;

      } while(norm > stopCriterion && i < maxIter);
      VecDestroy(&W);
    }

    template< class T_Scalar >
    std::string JacobiIterativeSolver< T_Scalar >::solverName() const
    {
      return "jacobi";
    }

    template< class T_Scalar >
    ProductType JacobiIterativeSolver< T_Scalar >::productType() const
    {
      return ProductType::A;
    }


    // -----------------------
    //           GCR
    // -----------------------
    template< class T_Scalar >
    GCRIterativeSolver< T_Scalar >::GCRIterativeSolver(const unsigned restart, const unsigned reuse, const unsigned reuseSelect, const bool residualBased, const bool reuseSol, const bool trueResidual, const bool modifiedGS): 
    _residualBased(residualBased), _reuseSol(reuseSol), _trueResidual(trueResidual), _modifiedGS(modifiedGS), _recomputeAV(false), _restart(restart), _reuse(reuse), _reuseSelect(reuseSelect), _iReuse(), _x(), _V(), _AV(), _coeff()
    {
      if (!_residualBased)
        _solverName = "Orthodir with recycling";
      else
        _solverName = "GCR with recycling";
    }

    template< class T_Scalar >
    GCRIterativeSolver< T_Scalar >::~GCRIterativeSolver()
    {
      if (_x) VecDestroy(&_x);
      for (unsigned i = 0; i < _restart + _reuse; ++i) {
        if (_V[i] != 0ULL) VecDestroy(&_V[i]);
        if (_AV[i] != 0ULL) VecDestroy(&_AV[i]);
      }
      if (_coeff) PetscFree(_coeff);
    }
    
    template< class T_Scalar >
    void GCRIterativeSolver< T_Scalar >::solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol, int maxIter)
    {
      PetscScalar Av_dot_r, *val;
      PetscReal   norm_r, norm_Av;
      PetscInt    it = 0;
      unsigned    n, nTot, nReuse = std::min(_iReuse, _reuse);
      Vec         v, Av, r;

      PetscMalloc1(_reuse + _restart, &val);
      VecDuplicate(RHS, &r);
      if (_V == 0ULL) PetscCalloc1(_reuse + _restart, &_V);
      if (_AV == 0ULL) PetscCalloc1(_reuse + _restart, &_AV);

      if (_coeff == 0ULL  && _reuse && 2 <= _reuseSelect && _reuseSelect <= 9)
        PetscMalloc1(_reuse + _restart, &_coeff);

      PetscReal norm_tr;  // True resiudal
      Vec tr;
      if (_trueResidual) VecDuplicate(RHS, &tr);


      this->_clearIterationOperations();

      // Initial residual
      MatVectProductI_A< T_Scalar >(A, X, r);
      VecAYPX(r, -1.0, RHS);
      VecNorm(r, NORM_2, &norm_r);
      this->_runIterationOperations(it, norm_r);


      /*
       *  Reuse solution
       */ 
      if (_reuseSol) {
        if (_x) {
          VecCopy(_x, Y);
          VecCopy(Y, X);

          // Residual
          MatVectProductI_A< T_Scalar >(A, X, r);
          VecAYPX(r, -1.0, RHS);
          VecNorm(r, NORM_2, &norm_r);
          if(mpi::getMPIRank() == 0) {
            gmshfem::msg::info << " - Reuse sol  " << ": absolute residual = " << norm_r << ", relative residual = " << norm_r / AbstractIterativeSolver<T_Scalar>::_absoluteResidual[0] << gmshfem::msg::endl;
          }
        }
        else
          VecDuplicate(X, &_x);
      }


      /* 
       *  Reuse directions
       */
      if (nReuse) {

        // If recompute AV if A changes
        if (_recomputeAV) {
          for (unsigned i = 0; i < nReuse; ++i) {
            v = _V[i];
            Av = _AV[i];
            MatVectProductI_A< T_Scalar >(A, v, Av);

            // A*A-orthogonalization w.r.t. other directions
            project(Av, i, val, _AV, _modifiedGS);
            for (unsigned j = 0; j < i; ++j) val[j] = -val[j];
            VecMAXPY(v, i, val, _V);

            VecNorm(Av, NORM_2, &norm_Av);
            VecScale(v, 1.0 / norm_Av);
            VecScale(Av, 1.0 / norm_Av);
          }
          _recomputeAV = false;
        }

        // Update solution and residual
        project(r, nReuse,  val, _AV, _modifiedGS);
        VecMAXPY(Y, nReuse, val, _V);
        VecCopy(Y, X);

        // coeff for reuseSelect
        if (_reuse && 2 <= _reuseSelect && _reuseSelect <= 5) {
          for (unsigned i = 0; i < nReuse; ++i) {
            _coeff[i] = PetscRealPart(PetscConj(val[i]) * val[i]) / (norm_r*norm_r);
          }

          if (2 <= _reuseSelect && _reuseSelect <= 3) {
            for (unsigned i = 0; i < nReuse; ++i) {
              _coeff[i] = norm_r * ( 1 - PetscSqrtReal(1 - _coeff[i]) );
            }
          }
        }

        VecNorm(r, NORM_2, &norm_r);
        if(mpi::getMPIRank() == 0) {
          gmshfem::msg::info << " - Reuse dir  : absolute residual = " << norm_r << ", relative residual = " << norm_r / AbstractIterativeSolver<T_Scalar>::_absoluteResidual[0] << gmshfem::msg::endl;
        }
      }


      /*
       *  Solve
       */
      do {
        // Residual
        if (it) {
          MatVectProductI_A< T_Scalar >(A, X, r);
          VecAYPX(r, -1.0, RHS);
          VecNorm(r, NORM_2, &norm_r);
        }
        Av = r;


        for(n = 0, nTot = nReuse ; n < _restart; ++n, ++nTot) {

          // Convergence criterion
          if (norm_r/this->_absoluteResidual[0] <= relTol || it >= maxIter || norm_r == 0)
            break;


          // New direction
          if (_V[nTot] == 0ULL) VecDuplicate(RHS, &_V[nTot]);
          v = _V[nTot];
          if (_residualBased)
            VecCopy(r, v);
          else
            VecCopy(Av, v);

          if (_AV[nTot] == 0ULL) VecDuplicate(RHS, &_AV[nTot]);
          Av = _AV[nTot];
          MatVectProductI_A< T_Scalar >(A, v, Av);

          // A*A-Orthogonalization
          project(Av, nTot, val, _AV, _modifiedGS);
          for (unsigned j = 0; j < nTot; ++j) val[j] = -val[j];
          VecMAXPY(v, nTot, val, _V);


          // Normalization and update solution and residual
          VecDotNorm2(r, Av, &Av_dot_r, &norm_Av);
          if (norm_Av == 0) return;
          norm_Av = PetscSqrtReal(norm_Av);
          Av_dot_r /= norm_Av;
          VecScale(v, 1.0 / norm_Av);
          VecScale(Av, 1.0 / norm_Av);

          VecAXPY(Y, Av_dot_r, v);
          VecCopy(Y, X);
          VecAXPY(r, -Av_dot_r, Av);

          // coeff for reuseSelect
          if (_reuse && 2 <= _reuseSelect && _reuseSelect <= 5) {
            _coeff[nTot] = norm_r;
          }

          VecNorm(r, NORM_2, &norm_r);

          if (_reuse) {
            if (2 <= _reuseSelect && _reuseSelect <= 3) {
              _coeff[nTot] -= norm_r;
            }
            else if (4 <= _reuseSelect && _reuseSelect <= 5) {
              _coeff[nTot] = (_coeff[nTot] - norm_r ) / _coeff[nTot];
              _coeff[nTot] *= _coeff[nTot];
            }
          }


          ++it;
          this->_runIterationOperations(it, norm_r);

          if (_trueResidual) {
            MatVectProductI_A< T_Scalar >(A, X, tr);
            VecAYPX(tr, -1.0, RHS);
            VecNorm(tr, NORM_2, &norm_tr);
            gmshfem::msg::info << " - Verifica° "<< it <<": abs true residual = " << norm_tr << ", rel true residual = " << norm_tr / AbstractIterativeSolver<T_Scalar>::_absoluteResidual[0] << gmshfem::msg::endl;
          }

        }


        // coeff for reuseSelect
        if (_reuse && 6 <= _reuseSelect && _reuseSelect <= 9) {
          for (unsigned i = 0; i < nTot-1; ++i) {
            _coeff[i] = PetscRealPart(PetscConj(val[i]) * val[i]) / (norm_Av*norm_Av);
          }
          _coeff[nTot-1] = 1;

          if (6 <= _reuseSelect && _reuseSelect <= 7) {
            for (unsigned i = 0; i < nTot; ++i) {
              _coeff[i] = norm_Av * ( 1 - PetscSqrtReal(1 - _coeff[i]) );
            }
          }
        }


        // Store directions
        if (_reuse) {

          // Largest or smallest residual decrease (2) or (3)
          // Largest or smallest relative residual decrease (4) or (5)
          // Largest or smallest orthogonalization contribution (6) or (7)
          // Largest or smallest relative orthogonalization contribution (8) or (9)
          if (2 <= _reuseSelect && _reuseSelect <= 9) {

            for (unsigned i = _reuse; i < nTot; ++i) {
              unsigned jExtreme = 0;
              for (unsigned j = 1; j < _reuse; ++j) {
                if ( ((_reuseSelect % 2) == 0 && _coeff[j] < _coeff[jExtreme]) ||
                     ((_reuseSelect % 2) == 1 && _coeff[j] > _coeff[jExtreme]) ) {
                  jExtreme = j;
                }
              }

              while (i < nTot) {
                if ( ((_reuseSelect % 2) == 0 && _coeff[i] > _coeff[jExtreme]) ||
                     ((_reuseSelect % 2) == 1 && _coeff[i] < _coeff[jExtreme]) ) {
                  VecCopy(_V[i], _V[jExtreme]);
                  VecCopy(_AV[i], _AV[jExtreme]);
                  _coeff[jExtreme] = _coeff[i];
                  break;
                }
                ++i;
              }

            }

          }

          // Last directions
          else if (_reuseSelect == 1) {
            for(unsigned i = _reuse; i < nTot; ++i) {
              VecCopy(_V[i], _V[(_iReuse + i - nReuse) % _reuse]);
              VecCopy(_AV[i], _AV[(_iReuse + i - nReuse) % _reuse]);
            }
          }

          // First directions
          // Nothing to do

          _iReuse += n;
          nReuse = std::min(_iReuse, _reuse);
        }


      } while(norm_r/this->_absoluteResidual[0] > relTol && it < maxIter && norm_r > 0);


      if (_reuseSol)
        VecCopy(X, _x);

      PetscFree(val);
      VecDestroy(&r);
      if (_trueResidual) VecDestroy(&tr);

    }

    template< class T_Scalar >
    std::string GCRIterativeSolver< T_Scalar >::solverName() const
    {
      return _solverName;
    }

    template< class T_Scalar >
    ProductType GCRIterativeSolver< T_Scalar >::productType() const
    {
      return ProductType::I_A;
    }

    template< class T_Scalar >
    void GCRIterativeSolver< T_Scalar >::recomputeAV()
    {
      _recomputeAV = true;
    }


    // -----------------------
    //    Simpler GMRES
    // -----------------------
    template< class T_Scalar >
    SGMRESIterativeSolver< T_Scalar >::SGMRESIterativeSolver(const unsigned restart, const unsigned reuse, const unsigned reuseSelect, const bool residualBased, const bool reuseSol, const bool trueResidual, const bool modifiedGS):
    _residualBased(residualBased), _reuseSol(reuseSol), _trueResidual(trueResidual), _modifiedGS(modifiedGS), _restart(restart), _reuse(reuse), _reuseSelect(reuseSelect), _nReuse(), _x(), _V(), _C(), _coeff()
    {
      if (!_residualBased)
        _solverName = "SGMRES with recycling";
      else
        _solverName = "RB-SGMRES with recycling";

      PetscCalloc1(_restart + _reuse, &_H);
    }
    
    template< class T_Scalar >
    SGMRESIterativeSolver< T_Scalar >::~SGMRESIterativeSolver()
    {
      for (unsigned i = 0; i < _restart + _reuse; ++i) {
        PetscFree(_H[i]);
        VecDestroy(&_V[i]);
        if (i == _restart+_reuse-1 || _C[i] != _V[i+1]) VecDestroy(&_C[i]);
      }
      PetscFree(_H);
      PetscFree(_V);
      PetscFree(_C);
      PetscFree(_coeff);
    }

    template< class T_Scalar >
    void SGMRESIterativeSolver< T_Scalar >::solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol, int maxIter)
    {
      PetscScalar a, *h, *g;
      PetscReal   norm_r, norm_c;
      PetscInt    it = 0;
      unsigned    n, nTot;
      Vec         c, r;

      h = 0ULL;
      PetscMalloc1(_restart + _reuse, &g);
      if (_V == 0ULL) PetscCalloc1(_reuse + _restart, &_V);
      if (_C == 0ULL) PetscCalloc1(_reuse + _restart, &_C);
      if (_coeff == 0ULL && 2 <= _reuseSelect && _reuseSelect <= 9)
        PetscMalloc1(_reuse + _restart, &_coeff);
      VecDuplicate(RHS, &r);

      PetscScalar *y;   // True residual
      PetscReal norm_tr;
      Vec tr;
      if (_trueResidual) PetscMalloc1(_restart + _reuse, &y);
      if (_trueResidual) VecDuplicate(RHS, &tr);


      this->_clearIterationOperations();

      // Initial residual
      MatVectProductI_A< T_Scalar >(A, X, r);
      VecAYPX(r, -1.0, RHS);
      VecNorm(r, NORM_2, &norm_r);
      this->_runIterationOperations(it, norm_r);


      /*
       *  Reuse solution
       */ 
      if (_reuseSol) {
        if (_x) {
          VecCopy(_x, Y);
          VecCopy(Y, X);

          // Residual
          MatVectProductI_A< T_Scalar >(A, X, r);
          VecAYPX(r, -1.0, RHS);
          VecNorm(r, NORM_2, &norm_r);
          if(mpi::getMPIRank() == 0)
            gmshfem::msg::info << " - Reuse sol  " << ": absolute residual = " << norm_r << ", relative residual = " << norm_r / AbstractIterativeSolver<T_Scalar>::_absoluteResidual[0] << gmshfem::msg::endl;
        }
        else
          VecDuplicate(X, &_x);
      }


      /*
       *  Reuse directions
       */
      if (_nReuse) {
        // Right-hand sides
        project(r, _nReuse, g, _C, _modifiedGS);

        // coeff for reuseSelect
        if (_reuse && 2 <= _reuseSelect && _reuseSelect <= 5) {
          for (unsigned i = 0; i < _nReuse; ++i) {
            _coeff[i] = PetscRealPart(PetscConj(g[i]) * g[i]) / (norm_r*norm_r);
          }

          if (2 <= _reuseSelect && _reuseSelect <= 3) {
            for (unsigned i = 0; i < _nReuse; ++i) {
              _coeff[i] = norm_r * ( 1 - PetscSqrtReal(1 - _coeff[i]) );
            }
          }
        }

        VecNorm(r, NORM_2, &norm_r);
        if(mpi::getMPIRank() == 0)
          gmshfem::msg::info << " - Reuse dir  : absolute residual = " << norm_r << ", relative residual = " << norm_r / AbstractIterativeSolver<T_Scalar>::_absoluteResidual[0] << gmshfem::msg::endl;
      }


      /*
       *  Solve
       */
      do {
        // Reset residual and right-hand side
        if (it > 0) {
          MatVectProductI_A< T_Scalar >(A, X, r);
          VecAYPX(r, -1.0, RHS);
          VecNorm(r, NORM_2, &norm_r);

          for(unsigned i = 0; i < _nReuse; ++i) g[i] = 0;
        }
        c = r;


        for(n = 0, nTot = _nReuse; n < _restart; ++n, ++nTot) {

          // Convergence criterion
          if (norm_r/this->_absoluteResidual[0] <= relTol || it >= maxIter || norm_r == 0)
            break;


          // New direction
          if (_residualBased || n == 0) {
            if (_V[nTot] == 0ULL) VecDuplicate(RHS, &_V[nTot]);
            VecCopy(r, _V[nTot]);
          }
          else {
            if (_V[nTot] != 0ULL && nTot > 0 && _V[nTot] != _C[nTot-1]) VecDestroy(&_V[nTot]);
            _V[nTot] = c;
          }
          if (!_C[nTot]) VecDuplicate(RHS, &_C[nTot]);
          c = _C[nTot];
          MatVectProductI_A< T_Scalar >(A, _V[nTot], c);

          // Arnoldi-like iteration
          if (_H[nTot] == 0ULL)
            PetscMalloc1(nTot+1, &_H[nTot]);
          h = _H[nTot];
          project(c, nTot, h, _C, _modifiedGS);

          // Normalization and update residual
          VecDotNorm2(r, c, g + nTot, &norm_c);
          if (norm_c == 0) return;
          norm_c = PetscSqrtReal(norm_c);
          h[nTot] = norm_c;
          g[nTot] /= norm_c;
          VecScale(c, 1.0 / norm_c);

          VecAXPY(r, -g[nTot], c);


          // coeff for reuseSelect
          if (_reuse && 2 <= _reuseSelect && _reuseSelect <= 5) {
            _coeff[nTot] = norm_r;
          }

          VecNorm(r, NORM_2, &norm_r);

          if (_reuse) {
            if (2 <= _reuseSelect && _reuseSelect <= 3) {
              _coeff[nTot] -= norm_r;
            }
            else if (4 <= _reuseSelect && _reuseSelect <= 5) {
              _coeff[nTot] = (_coeff[nTot] - norm_r ) / _coeff[nTot];
              _coeff[nTot] *= _coeff[nTot];
            }
          }


          ++it;
          this->_runIterationOperations(it, norm_r);


          if (_trueResidual) {
            for(unsigned j = nTot+1; j-- > 0; ) {
              y[j] = g[j];
              for(unsigned k = j+1; k < nTot+1; ++k) y[j] -= _H[k][j] * y[k];
              y[j] /= _H[j][j];
            }
            VecMAXPY(Y, nTot+1, y, _V);

            MatVectProductI_A< T_Scalar >(A, Y, tr);
            VecAYPX(tr, -1.0, RHS);
            VecNorm(tr, NORM_2, &norm_tr);
            gmshfem::msg::info << " - Verifica° "<< it <<": abs true residual = " << norm_tr << ", rel true residual = " << norm_tr / AbstractIterativeSolver<T_Scalar>::_absoluteResidual[0] << gmshfem::msg::endl;

            VecCopy(X,Y);
          }

        }


        // Find solution
        for(unsigned j = nTot; j-- > 0; ) {
          for(unsigned k = j+1; k < nTot; ++k) g[j] -= _H[k][j] * g[k];
          g[j] /= _H[j][j];
        }
        VecMAXPY(Y, nTot, g, _V);
        VecCopy(Y,X);


        // coeff for reuseSelect
        if (_reuse && 6 <= _reuseSelect && _reuseSelect <= 9) {
          for (unsigned i = 0; i < nTot; ++i) {
            _coeff[i] = PetscRealPart(PetscConj(h[i]) * h[i]) / (norm_c*norm_c);
          }

          if (6 <= _reuseSelect && _reuseSelect <= 7) {
            for (unsigned i = 0; i < nTot; ++i) {
              _coeff[i] = norm_c * ( 1 - PetscSqrtReal(1 - _coeff[i]) );
            }
          }
        }


        // Store directions
        if (_reuse) {

          // Construct C = AV
          if (!_residualBased && _reuseSelect != 0) {
            for (unsigned i = _nReuse+1; i < nTot; ++i) {
              VecDuplicate(RHS, &_V[i]);
              VecCopy(_C[i-1], _V[i]);
            }
          }

          // Largest or smallest residual decrease (2) or (3)
          // Largest or smallest relative residual decrease (4) or (5)
          // Largest or smallest orthogonalization contribution (6) or (7)
          // Largest or smallest relative orthogonalization contribution (8) or (9)
          if (2 <= _reuseSelect && _reuseSelect <= 9 && nTot > _reuse) {
            std::vector<unsigned> idx(nTot);
            for (unsigned i = 0; i < nTot; ++i) idx[i] = i;

            // Select directions
            for (unsigned i = _reuse; i < nTot; ++i) {
              unsigned jExtreme = 0;
              for (unsigned j = 1; j < _reuse; ++j) {
                if ( ((_reuseSelect % 2) == 0 && _coeff[idx[j]] < _coeff[idx[jExtreme]]) ||
                     ((_reuseSelect % 2) == 1 && _coeff[idx[j]] > _coeff[idx[jExtreme]]) ) {
                  jExtreme = j;
                }
              }

              while (i < nTot) {
                if ( ((_reuseSelect % 2) == 0 && _coeff[idx[i]] > _coeff[idx[jExtreme]]) ||
                     ((_reuseSelect % 2) == 1 && _coeff[idx[i]] < _coeff[idx[jExtreme]]) ) {

                  unsigned tmp = idx[jExtreme];
                  for (unsigned j = jExtreme+1; j < _reuse; ++j) idx[j-1] = idx[j];
                  idx[_reuse-1] = idx[i];
                  unsigned j = i;
                  while (j > _reuse) {
                    if (idx[j-1] < tmp) break;
                    idx[j] = idx[j-1];
                    --j;
                  }
                  idx[j] = tmp;
                  
                  break;
                }
                ++i;
              }
            }

            // Remove dependances
            for (unsigned i = nTot; i-- > _reuse; ) {
              unsigned index = idx[i];
              if (index > idx[_reuse-1]) continue;

              a = _H[index][index];
              for (unsigned j = 0; j < _reuse; ++j) {
                unsigned jndex = idx[j];
                if (jndex < index) continue;

                h =_H[jndex];
                for (unsigned k = 0; k < index; ++k) h[k] -= h[index] / a * _H[index][k];
                VecAXPY(_V[jndex], -h[index]/a, _V[index]);
              }

            }

            // Form the bases and the triangular matrix
            Vec tmp;
            for (unsigned i = 0; i < _reuse; ++i) {
              if (i == idx[i]) continue;
              tmp = _V[i];
              _V[i] = _V[idx[i]];
              _V[idx[i]] = tmp;

              tmp = _C[i];
              _C[i] = _C[idx[i]];
              _C[idx[i]] = tmp;

              for (unsigned j = 0; j <= i; ++j)
                _H[i][j] = _H[idx[i]][idx[j]];
            }

          }

          // Latest
          else if (_reuseSelect == 1) {
            if (nTot > _reuse) {
              unsigned shift = nTot - _reuse;
              for (unsigned i = shift; i-- > 0; ) {
                a = _H[i][i];
                for (unsigned j = shift; j < nTot; ++j) {
                  h =_H[j];
                  for (unsigned k = 0; k < i; ++k) h[k] -= h[i] / a * _H[i][k];
                  VecAXPY(_V[j], -h[i]/a, _V[i]);
                }
              }

              Vec tmp;
              for (unsigned i = 0; i < _reuse; ++i) {
                tmp = _V[i];
                _V[i] = _V[i+shift];
                _V[i+shift] = tmp;

                tmp = _C[i];
                _C[i] = _C[i+shift];
                _C[i+shift] = tmp;

                for (unsigned j = 0; j <= i; ++j)
                  _H[i][j] = _H[i+shift][j+shift];
              }
            }
          }

          // Earliest
          // Nothing to do


          _nReuse = std::min(nTot, _reuse);
        }


      } while(norm_r/this->_absoluteResidual[0] > relTol && it < maxIter && norm_r > 0);


      if (_reuseSol)
        VecCopy(X, _x);

      PetscFree(g);
      VecDestroy(&r);
      if (_trueResidual) VecDestroy(&tr);
      if (_trueResidual) PetscFree(y);

    }

    template< class T_Scalar >
    std::string SGMRESIterativeSolver< T_Scalar >::solverName() const
    {
      return _solverName;
    }

    template< class T_Scalar >
    ProductType SGMRESIterativeSolver< T_Scalar >::productType() const
    {
      return ProductType::I_A;
    }


    // -----------------------
    //         GMRES
    // -----------------------
    template< class T_Scalar >
    GMRESIterativeSolver< T_Scalar >::GMRESIterativeSolver(const unsigned restart, const unsigned reuse, const unsigned reuseSelect, const bool reuseSol, const bool trueResidual, const bool modifiedGS):
    _reuseSol(reuseSol), _trueResidual(trueResidual), _modifiedGS(modifiedGS), _restart(restart), _reuse(reuse), _reuseSelect(reuseSelect), _nReuse(), _x(), _V(), _C(), _coeff()
    {
      PetscCalloc1(_restart + _reuse, &_H);
      PetscCalloc1(_restart + _reuse, &_c);
      PetscCalloc1(_restart + _reuse, &_s);
    }
    
    template< class T_Scalar >
    GMRESIterativeSolver< T_Scalar >::~GMRESIterativeSolver()
    {
      for (unsigned i = 0; i < _restart + _reuse; ++i) {
        PetscFree(_H[i]);
        VecDestroy(&_V[i]);
        VecDestroy(&_C[i]);
      }
      PetscFree(_H);
      PetscFree(_V);
      PetscFree(_C);
      PetscFree(_c);
      PetscFree(_s);
      PetscFree(_coeff);
    }

    template< class T_Scalar >
    void GMRESIterativeSolver< T_Scalar >::solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol, int maxIter)
    {
      PetscScalar a, *h, *g;
      PetscReal   norm_r, norm_c;
      PetscInt    it = 0;
      unsigned    n, nTot;
      Vec         c, r;

      h = 0ULL;
      PetscMalloc1(_restart + _reuse + 1, &g);
      if (_V == 0ULL) PetscCalloc1(_reuse + _restart, &_V);
      if (_C == 0ULL) PetscCalloc1(_reuse + _restart, &_C);
      if (_coeff == 0ULL && 2 <= _reuseSelect && _reuseSelect <= 9) PetscMalloc1(_reuse + _restart, &_coeff);
      VecDuplicate(RHS, &r);
      VecDuplicate(RHS, &c);

      PetscScalar *y;   // True residual
      PetscReal norm_tr;
      Vec tr;
      if (_trueResidual) PetscMalloc1(_restart + _reuse, &y);
      if (_trueResidual) VecDuplicate(RHS, &tr);


      this->_clearIterationOperations();

      // Initial residual
      MatVectProductI_A< T_Scalar >(A, X, r);
      VecAYPX(r, -1.0, RHS);
      VecNorm(r, NORM_2, &norm_r);
      this->_runIterationOperations(it, norm_r);


      /*
       *  Reuse solution
       */ 
      if (_reuseSol) {
        if (_x) {
          VecCopy(_x, Y);
          VecCopy(Y, X);

          // Residual
          MatVectProductI_A< T_Scalar >(A, X, r);
          VecAYPX(r, -1.0, RHS);
          VecNorm(r, NORM_2, &norm_r);
          if(mpi::getMPIRank() == 0)
            gmshfem::msg::info << " - Reuse sol  " << ": absolute residual = " << norm_r << ", relative residual = " << norm_r / AbstractIterativeSolver<T_Scalar>::_absoluteResidual[0] << gmshfem::msg::endl;
        }
        else
          VecDuplicate(X, &_x);
      }


      /*
       *  Reuse directions
       */
      if (_nReuse) {
        // Right-hand sides
        project(r, _nReuse, g, _C, _modifiedGS);

        // coeff for reuseSelect
        if (_reuse && 2 <= _reuseSelect && _reuseSelect <= 5) {
          for (unsigned i = 0; i < _nReuse; ++i) {
            _coeff[i] = PetscRealPart(PetscConj(g[i]) * g[i]) / (norm_r*norm_r);
          }

          if (2 <= _reuseSelect && _reuseSelect <= 3) {
            for (unsigned i = 0; i < _nReuse; ++i) {
              _coeff[i] = norm_r * ( 1 - PetscSqrtReal(1 - _coeff[i]) );
            }
          }
        }

        VecNorm(r, NORM_2, &norm_r);
        if(mpi::getMPIRank() == 0)
          gmshfem::msg::info << " - Reuse dir  : absolute residual = " << norm_r << ", relative residual = " << norm_r / AbstractIterativeSolver<T_Scalar>::_absoluteResidual[0] << gmshfem::msg::endl;
      }


      /*
       *  Solve
       */
      do {
        // Residual
        if (it > 0) {
          MatVectProductI_A< T_Scalar >(A, X, r);
          VecAYPX(r, -1.0, RHS);
          VecNorm(r, NORM_2, &norm_r);

          for(unsigned i = 0; i < _nReuse; ++i) g[i] = 0;
        }

        VecCopy(r, c);
        VecNormalize(c, &norm_r);
        g[_nReuse] = norm_r;


        for(n = 0, nTot = _nReuse; n < _restart; ++n, ++nTot) {

          // Convergence criterion
          if (norm_r/this->_absoluteResidual[0] <= relTol || it >= maxIter || norm_r == 0)
            break;


          // New direction
          if (_V[nTot] == 0ULL) VecDuplicate(RHS, &_V[nTot]);
          Vec tmp = _V[nTot];
          _V[nTot] = c;
          c = tmp;
          MatVectProductI_A< T_Scalar >(A, _V[nTot], c);

          // Compute Hessenberg column
          if (_H[nTot] == 0ULL) PetscMalloc1(nTot+1, &_H[nTot]);
          h = _H[nTot];
          project(c, _nReuse, h, _C, _modifiedGS);
          project(c, n+1, h + _nReuse, _V + _nReuse, _modifiedGS);

          // Plane rotations
          for (unsigned j = _nReuse; j < nTot; ++j) {
            a = _c[j] * h[j] + _s[j] * h[j+1];
            h[j+1] = -PetscConj(_s[j]) * h[j] + PetscConj(_c[j]) * h[j+1];
            h[j] = a;
          }

          VecNormalize(c, &norm_c);
          a = PetscConj(h[nTot])*h[nTot];
          _c[nTot] = PetscSqrtReal( a * (a + norm_c*norm_c) );
          _s[nTot] = h[nTot] * norm_c / _c[nTot];
          h[nTot] = h[nTot] * _c[nTot] / a;
          _c[nTot] = a / _c[nTot];

          g[nTot+1] = -PetscConj(_s[nTot]) * g[nTot];
          g[nTot] = _c[nTot] * g[nTot];


          // coeff for reuseSelect
          if (_reuse && 2 <= _reuseSelect && _reuseSelect <= 5) {
            _coeff[nTot] = norm_r;
          }

          norm_r = abs(g[nTot+1]);

          if (_reuse) {
            if (2 <= _reuseSelect && _reuseSelect <= 3) {
              _coeff[nTot] -= norm_r;
            }
            else if (4 <= _reuseSelect && _reuseSelect <= 5) {
              _coeff[nTot] = (_coeff[nTot] - norm_r ) / _coeff[nTot];
              _coeff[nTot] *= _coeff[nTot];
            }
          }


          ++it;
          this->_runIterationOperations(it, norm_r);


          if (_trueResidual) {
            for(unsigned j = nTot+1; j-- > 0; ) {
              y[j] = g[j];
              for(unsigned k = j+1; k < nTot+1; ++k) {
                y[j] -= _H[k][j] * y[k];
              }
              y[j] /= _H[j][j];
            }
            // VecMAXPY(Y, nTot+1, y, _V);
            for (unsigned i = 0; i < nTot+1; ++i)
              VecAXPY(Y, y[i], _V[i]);
            MatVectProductI_A< T_Scalar >(A, Y, tr);
            VecAYPX(tr, -1.0, RHS);
            VecNorm(tr, NORM_2, &norm_tr);
            gmshfem::msg::info << " - Verifica° "<< it <<": abs true residual = " << norm_tr << ", rel true residual = " << norm_tr / AbstractIterativeSolver<T_Scalar>::_absoluteResidual[0] << gmshfem::msg::endl;
            VecCopy(X,Y);
          }

        }


        // Find solution
        for(unsigned j = nTot; j-- > 0; ) {
          for(unsigned k = j+1; k < nTot; ++k) {
            g[j] -= _H[k][j] * g[k];
          }
          g[j] /= _H[j][j];
        }

        //VecMAXPY(Y, _nReuse, g, _V);
        for (unsigned i = 0; i < nTot; ++i)
          VecAXPY(Y, g[i], _V[i]);
        VecCopy(Y,X);


        // coeff for reuseSelect
        if (_reuse && 6 <= _reuseSelect && _reuseSelect <= 9) {
          for (unsigned i = 0; i < nTot; ++i) {
            _coeff[i] = PetscRealPart(PetscConj(h[i]) * h[i]) / (norm_c*norm_c);
          }

          if (6 <= _reuseSelect && _reuseSelect <= 7) {
            for (unsigned i = 0; i < nTot; ++i) {
              _coeff[i] = norm_c * ( 1 - PetscSqrtReal(1 - _coeff[i]) );
            }
          }
        }


        // Store directions
        if (_reuse) {

          // Construct C = AV
          for (unsigned i = _nReuse; i < nTot; ++i) {
            if (_C[i] == 0ULL) VecDuplicate(RHS, &_C[i]);
            VecCopy(_V[i], _C[i]);
          }

          for (unsigned i = _nReuse; i+1 < nTot; ++i) {
            VecAXPBY(_C[i+1], -_s[i], _c[i], _C[i]);
            VecAXPBY(_C[i], PetscConj(_s[i]), PetscConj(_c[i]), _V[i+1]);
          }
          if (nTot - _nReuse > 0) VecAXPBY(_C[nTot-1], PetscConj(_s[nTot-1]), PetscConj(_c[nTot-1]), c);

          // Largest or smallest residual decrease (2) or (3)
          // Largest or smallest relative residual decrease (4) or (5)
          // Largest or smallest orthogonalization contribution (6) or (7)
          // Largest or smallest relative orthogonalization contribution (8) or (9)
          if (2 <= _reuseSelect && _reuseSelect <= 9 && nTot > _reuse) {
            std::vector<unsigned> idx(nTot);
            for (unsigned i = 0; i < nTot; ++i) idx[i] = i;

            // Select directions
            for (unsigned i = _reuse; i < nTot; ++i) {
              unsigned jExtreme = 0;
              for (unsigned j = 1; j < _reuse; ++j) {
                if ( ((_reuseSelect % 2) == 0 && _coeff[idx[j]] < _coeff[idx[jExtreme]]) ||
                     ((_reuseSelect % 2) == 1 && _coeff[idx[j]] > _coeff[idx[jExtreme]]) ) {
                  jExtreme = j;
                }
              }

              while (i < nTot) {
                if ( ((_reuseSelect % 2) == 0 && _coeff[idx[i]] > _coeff[idx[jExtreme]]) ||
                     ((_reuseSelect % 2) == 1 && _coeff[idx[i]] < _coeff[idx[jExtreme]]) ) {

                  unsigned tmp = idx[jExtreme];
                  for (unsigned j = jExtreme+1; j < _reuse; ++j) idx[j-1] = idx[j];
                  idx[_reuse-1] = idx[i];
                  unsigned j = i;
                  while (j > _reuse) {
                    if (idx[j-1] < tmp) break;
                    idx[j] = idx[j-1];
                    --j;
                  }
                  idx[j] = tmp;
                  
                  break;
                }
                ++i;
              }
            }


            for (unsigned i = nTot; i-- > _reuse; ) {
              unsigned index = idx[i];
              if (index > idx[_reuse-1]) continue;

              a = _H[index][index];
              for (unsigned j = 0; j < _reuse; ++j) {
                unsigned jndex = idx[j];
                if (jndex < index) continue;

                h =_H[jndex];
                for (unsigned k = 0; k < index; ++k) h[k] -= h[index] / a * _H[index][k];
                VecAXPY(_V[jndex], -h[index]/a, _V[index]);

              }

            }

            // Form the bases and the triangular matrix
            Vec tmp;
            for (unsigned i = 0; i < _reuse; ++i) {
              if (i == idx[i]) continue;
              tmp = _V[i];
              _V[i] = _V[idx[i]];
              _V[idx[i]] = tmp;

              tmp = _C[i];
              _C[i] = _C[idx[i]];
              _C[idx[i]] = tmp;

              for (unsigned j = 0; j <= i; ++j)
                _H[i][j] = _H[idx[i]][idx[j]];
            }

          }

          // Latest
          else if (_reuseSelect == 1) {
            if (nTot > _reuse) {
              unsigned shift = nTot - _reuse;
              for (unsigned i = shift; i-- > 0; ) {
                a = _H[i][i];
                for (unsigned j = shift; j < nTot; ++j) {
                  h =_H[j];
                  for (unsigned k = 0; k < i; ++k) h[k] -= h[i] / a * _H[i][k];
                  VecAXPY(_V[j], -h[i]/a, _V[i]);
                }
              }

              Vec tmp;
              for (unsigned i = 0; i < _reuse; ++i) {
                tmp = _V[i];
                _V[i] = _V[i+shift];
                _V[i+shift] = tmp;

                tmp = _C[i];
                _C[i] = _C[i+shift];
                _C[i+shift] = tmp;

                for (unsigned j = 0; j <= i; ++j)
                  _H[i][j] = _H[i+shift][j+shift];
              }
            }
          }

          // Earliest
          // Nothing to do


          _nReuse = std::min(nTot, _reuse);
        }


      } while(norm_r/this->_absoluteResidual[0] > relTol && it < maxIter && norm_r > 0);


      if (_reuseSol)
        VecCopy(X, _x);

      PetscFree(g);
      VecDestroy(&r);
      VecDestroy(&c);
      if (_trueResidual) VecDestroy(&tr);
      if (_trueResidual) PetscFree(y);

    }

    template< class T_Scalar >
    std::string GMRESIterativeSolver< T_Scalar >::solverName() const
    {
      return "GMRES with recycling";
    }

    template< class T_Scalar >
    ProductType GMRESIterativeSolver< T_Scalar >::productType() const
    {
      return ProductType::I_A;
    }

    template< class T_Scalar >
    void KSPIterativeSolver< T_Scalar >::solve(Mat A, Vec X, Vec Y, Vec RHS, double relTol, int maxIter)
    {


#ifndef HAVE_PETSC
      gmshfem::msg::error << "PETSc is required for KSP iterative solvers." << gmshfem::msg::endl;
#endif
      std::string solverBase = _solverName;
      std::set< std::string > solverMod;
      {
        const std::regex sep("\\+");
        std::list< std::string > split;
        std::copy(std::sregex_token_iterator(_solverName.begin(), _solverName.end(), sep, -1),
                  std::sregex_token_iterator(),
                  std::back_inserter(split));

        if(!split.empty()) {
          std::list< std::string >::iterator it = split.begin();
          solverBase = *(it++);
          for(; it != split.end(); it++)
            solverMod.insert(*it);
        }
      }
      KSP ksp;
      PC pc;
      KSPCreate(MPI_COMM_WORLD, &ksp);
      KSPSetFromOptions(ksp);
      KSPSetOperators(ksp, A, A);
      KSPSetType(ksp, solverBase.c_str());
      KSPMonitorSet(ksp, PetscInterface< T_Scalar, PetscScalar, PetscInt >::kspPrint, this, NULL);
      KSPSetTolerances(ksp, relTol, PETSC_DEFAULT, PETSC_DEFAULT, maxIter);
      KSPGetPC(ksp, &pc);
      PCSetType(pc, PCNONE);
      if(solverBase == "gmres") {
        KSPGMRESSetRestart(ksp, maxIter);
        if(solverMod.count("mgs")) {
          KSPGMRESSetOrthogonalization(ksp, KSPGMRESModifiedGramSchmidtOrthogonalization);
          gmshfem::msg::info << "Using modified Gram-Schmidt in GMRES" << gmshfem::msg::endl;
        }
      }
      else if(solverBase == "bcgsl")
        KSPBCGSLSetEll(ksp, 6);
      KSPSolve(ksp, RHS, Y);
      KSPDestroy(&ksp);
    }

    template< class T_Scalar >
    std::string KSPIterativeSolver< T_Scalar >::solverName() const
    {
      return this->_solverName;
    }

    template< class T_Scalar >
    ProductType KSPIterativeSolver< T_Scalar >::productType() const
    {
      return ProductType::I_A;
    }


  } // namespace problem


} // namespace gmshddm
