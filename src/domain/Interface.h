// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_INTERFACEDOMAIN
#define H_GMSHDDM_INTERFACEDOMAIN

#include <gmshfem/Domain.h>
#include <unordered_map>
#include <vector>

namespace gmshddm
{


  namespace domain
  {
    class Subdomain;
  }

  namespace domain
  {


    class Interface
    {
     private:
      std::vector< std::unordered_map< unsigned int, gmshfem::domain::Domain > > _domains;
      mutable gmshfem::domain::Domain _emptyDomain;

     public:
      Interface(const unsigned int nDom);
      Interface(const Interface &other);
      Interface(Interface &&other);
      ~Interface();

      unsigned int numberOfSubdomains() const;
      unsigned int numberOfNeighbors(unsigned int i) const;

      const gmshfem::domain::Domain &operator()(unsigned int i, unsigned int j) const;
      gmshfem::domain::Domain &operator()(unsigned int i, unsigned int j);

      const std::unordered_map< unsigned int, gmshfem::domain::Domain > &operator[](unsigned int i) const;

      bool operator==(const Interface &other) const;
      bool operator!=(const Interface &other) const;
      Interface &operator=(const Interface &other);
      Interface &operator=(Interface &&other);

      Interface &operator|=(const Interface &other);
      Interface operator|(const Interface &other) const;

      Interface &operator&=(const Interface &other);
      Interface operator&(const Interface &other) const;

      static Interface buildInterface(std::vector< std::vector< unsigned int > > &topology);
      // Should be done: Create a function that return the interfaces for cross-points and cross-edges
      // static Interface buildCrossInterface(std::vector< std::vector< unsigned int > > &topology, int dim);

      friend class Subdomain;
    };


  } // namespace domain


} // namespace gmshddm

#endif // H_GMSHDDM_INTERFACEDOMAIN
