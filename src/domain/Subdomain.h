// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_VOLUMEDOMAIN
#define H_GMSHDDM_VOLUMEDOMAIN

#include <gmshfem/Domain.h>
#include <vector>

namespace gmshddm
{


  namespace domain
  {
    class Interface;
  }

  namespace domain
  {


    class Subdomain
    {
     private:
      std::vector< gmshfem::domain::Domain > _domains;

     public:
      Subdomain(const unsigned int nDom);
      Subdomain(const Subdomain &other);
      Subdomain(Subdomain &&other);
      Subdomain(const Interface &other);
      Subdomain(Interface &&other);
      ~Subdomain();

      unsigned int numberOfSubdomains() const;

      gmshfem::domain::Domain getUnion() const;

      const gmshfem::domain::Domain &operator()(unsigned int i) const;
      gmshfem::domain::Domain &operator()(unsigned int i);

      bool operator==(const Subdomain &other) const;
      bool operator!=(const Subdomain &other) const;
      Subdomain &operator=(const Subdomain &other);
      Subdomain &operator=(Subdomain &&other);
      Subdomain &operator=(const Interface &other);
      Subdomain &operator=(Interface &&other);

      Subdomain &operator|=(const Subdomain &other);
      Subdomain operator|(const Subdomain &other) const;

      Subdomain &operator&=(const Subdomain &other);
      Subdomain operator&(const Subdomain &other) const;

      Subdomain &operator|=(const Interface &other);
      Subdomain operator|(const Interface &other) const;

      static Subdomain buildSubdomainOf(const gmshfem::domain::Domain &parent);

      friend class Interface;
    };


  } // namespace domain


} // namespace gmshddm

#endif // H_GMSHDDM_VOLUMEDOMAIN
