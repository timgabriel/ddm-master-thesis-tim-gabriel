// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "Subdomain.h"

#include "Interface.h"

#include <gmsh.h>
#include <gmshfem/Message.h>

namespace gmshddm
{


  namespace domain
  {


    Subdomain::Subdomain(const unsigned int nDom) :
      _domains(nDom)
    {
    }

    Subdomain::Subdomain(const Subdomain &other) :
      _domains(other._domains)
    {
    }

    Subdomain::Subdomain(Subdomain &&other) :
      _domains(std::move(other._domains))
    {
    }

    Subdomain::Subdomain(const Interface &other) :
      _domains(other._domains.size())
    {
      for(auto i = 0ULL; i < _domains.size(); ++i) {
        for(auto it = other._domains[i].begin(); it != other._domains[i].end(); ++it) {
          _domains[i] |= it->second;
        }
      }
    }

    Subdomain::Subdomain(Interface &&other) :
      _domains(other._domains.size())
    {
      for(auto i = 0ULL; i < _domains.size(); ++i) {
        for(auto it = other._domains[i].begin(); it != other._domains[i].end(); ++it) {
          _domains[i] |= it->second;
        }
      }
      other._domains.clear();
    }

    Subdomain::~Subdomain()
    {
    }

    unsigned int Subdomain::numberOfSubdomains() const
    {
      return _domains.size();
    }

    gmshfem::domain::Domain Subdomain::getUnion() const
    {
      gmshfem::domain::Domain un;
      for(auto i = 0ULL; i < _domains.size(); ++i) {
        un |= _domains[i];
      }
      return un;
    }

    const gmshfem::domain::Domain &Subdomain::operator()(unsigned int i) const
    {
      return _domains[i];
    }

    gmshfem::domain::Domain &Subdomain::operator()(unsigned int i)
    {
      return _domains[i];
    }

    bool Subdomain::operator==(const Subdomain &other) const
    {
      return _domains == other._domains;
    }

    bool Subdomain::operator!=(const Subdomain &other) const
    {
      return !((*this) == other);
    }

    Subdomain &Subdomain::operator=(const Subdomain &other)
    {
      _domains = other._domains;
      return *this;
    }

    Subdomain &Subdomain::operator=(Subdomain &&other)
    {
      _domains = std::move(other._domains);
      return *this;
    }

    Subdomain &Subdomain::operator=(const Interface &other)
    {
      for(auto i = 0ULL; i < _domains.size(); ++i) {
        _domains[i] = gmshfem::domain::Domain();
        for(auto it = other._domains[i].begin(); it != other._domains[i].end(); ++it) {
          _domains[i] |= it->second;
        }
      }
      return *this;
    }

    Subdomain &Subdomain::operator=(Interface &&other)
    {
      for(auto i = 0ULL; i < _domains.size(); ++i) {
        _domains[i] = gmshfem::domain::Domain();
        for(auto it = other._domains[i].begin(); it != other._domains[i].end(); ++it) {
          _domains[i] |= it->second;
        }
      }
      other._domains.clear();
      return *this;
    }

    Subdomain &Subdomain::operator|=(const Subdomain &other)
    {
      for(auto i = 0ULL; i < _domains.size(); ++i) {
        _domains[i] |= other._domains[i];
      }
      return *this;
    }

    Subdomain Subdomain::operator|(const Subdomain &other) const
    {
      Subdomain domain(*this);
      domain |= other;

      return domain;
    }

    Subdomain &Subdomain::operator&=(const Subdomain &other)
    {
      for(auto i = 0ULL; i < _domains.size(); ++i) {
        _domains[i] &= other._domains[i];
      }
      return *this;
    }

    Subdomain Subdomain::operator&(const Subdomain &other) const
    {
      Subdomain domain(*this);
      domain &= other;

      return domain;
    }

    Subdomain &Subdomain::operator|=(const Interface &other)
    {
      for(auto i = 0ULL; i < _domains.size(); ++i) {
        for(auto it = other._domains[i].begin(); it != other._domains[i].end(); ++it) {
          _domains[i] |= it->second;
        }
      }
      return *this;
    }

    Subdomain Subdomain::operator|(const Interface &other) const
    {
      Subdomain domain(*this);
      domain |= other;

      return domain;
    }

    Subdomain Subdomain::buildSubdomainOf(const gmshfem::domain::Domain &parent)
    {
      const int numPart = gmsh::model::getNumberOfPartitions();
      Subdomain subdomains(numPart);
      std::vector< std::pair< int, int > > entities;
      gmsh::model::getEntities(entities);
      for(auto i = 0ULL; i < entities.size(); ++i) {
        int parentDim, parentTag;
        gmsh::model::getParent(entities[i].first, entities[i].second, parentDim, parentTag);
        if((entities[i].first != parentDim || entities[i].second != parentTag) && entities[i].first == parentDim) {
          if(parent.have(std::make_pair(parentDim, parentTag))) {
            std::vector< int > part;
            gmsh::model::getPartitions(entities[i].first, entities[i].second, part);
            for(auto j = 0ULL; j < part.size(); ++j) {
              if(part[j] < 1 || part[j] > numPart) {
                throw gmshfem::common::Exception("Invalid Gmsh partition " + std::to_string(part[j]));
              }
              subdomains(part[j] - 1).addEntity(entities[i].first, entities[i].second);
            }
          }
        }
      }

      return subdomains;
    }


  } // namespace domain


} // namespace gmshddm
