// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "MPIInterface.h"

#include "Options.h"
#include "gmshddmDefines.h"

#include <gmshfem/Message.h>

#ifdef HAVE_MPI
#include <mpi.h>
#endif

namespace gmshddm
{


  namespace mpi
  {


    unsigned int getMPISize()
    {
#ifdef HAVE_MPI
      int MPI_size;
      unsigned int MPI_Size;
      MPI_Comm_size(MPI_COMM_WORLD, &MPI_size);
      MPI_Size = (unsigned int)MPI_size;
      return MPI_Size;
#else
      return 1;
#endif
    }

    unsigned int getMPIRank()
    {
#ifdef HAVE_MPI
      int MPI_rank;
      unsigned int MPI_Rank;
      MPI_Comm_rank(MPI_COMM_WORLD, &MPI_rank);
      MPI_Rank = (unsigned int)MPI_rank;
      return MPI_Rank;
#else
      return 0;
#endif
    }

    bool isItMySubdomain(const unsigned int iDom)
    {
      if(common::Options::instance()->processAffinity == problem::ProcessAffinity::ABCABC) {
        return iDom % getMPISize() == getMPIRank();
      }
      else if(common::Options::instance()->processAffinity == problem::ProcessAffinity::AABBCC) {
        return iDom / 2 % getMPISize() == getMPIRank();
      }
      return false;
    }

    unsigned int rankOfSubdomain(const unsigned int iDom)
    {
      if(common::Options::instance()->processAffinity == problem::ProcessAffinity::ABCABC) {
        return iDom % getMPISize();
      }
      else if(common::Options::instance()->processAffinity == problem::ProcessAffinity::AABBCC) {
        return iDom / 2 % getMPISize();
      }
      return 0;
    }

    void barrier()
    {
#ifdef HAVE_MPI
      MPI_Barrier(MPI_COMM_WORLD);
#endif
    }


  } // namespace mpi


} // namespace gmshddm
