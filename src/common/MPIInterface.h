// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_MPIINTERFACE
#define H_GMSHDDM_MPIINTERFACE

namespace gmshddm
{


  namespace mpi
  {


    unsigned int getMPISize();
    unsigned int getMPIRank();
    bool isItMySubdomain(const unsigned int iDom);
    unsigned int rankOfSubdomain(const unsigned int iDom);
    void barrier();


  } // namespace mpi


} // namespace gmshddm

#endif // H_GMSHDDM_MPIINTERFACE
