// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "GmshDdm.h"

#include "Options.h"
#include "gmshddmDefines.h"

#include <gmshfem/ArgsManager.h>
#include <gmshfem/Exception.h>
#include <gmshfem/GmshFem.h>
#include <gmshfem/Message.h>

#ifdef HAVE_PETSC
#include "petsc.h"
#endif // HAVE_PETSC

namespace gmshddm
{


  namespace common
  {

    static GmshDdm *s_gmshddm = nullptr;

    //Unary commands
    void info(gmshfem::common::ArgsManager *argsManager)
    {
      gmshfem::msg::print << gmshfem::msg::endl;
      gmshfem::msg::print << "###################" << gmshfem::msg::endl;
      gmshfem::msg::print << "#  G m s h D D M  #" << gmshfem::msg::endl;
      gmshfem::msg::print << "###################" << gmshfem::msg::endl;
      gmshfem::msg::print << "# Version: " << GMSHDDM_MAJOR_VERSION << "." << GMSHDDM_MINOR_VERSION << "." << GMSHDDM_PATCH_VERSION << gmshfem::msg::endl;
      gmshfem::msg::print << "# Build date: " << GMSHDDM_DATE << gmshfem::msg::endl;
      gmshfem::msg::print << "# Build host: " << GMSHDDM_HOST << gmshfem::msg::endl;
      gmshfem::msg::print << "# Packager: " << GMSHDDM_PACKAGER << gmshfem::msg::endl;
      gmshfem::msg::print << "# Build OS: " << GMSHDDM_OS << gmshfem::msg::endl;
      gmshfem::msg::print << "# Build type: " << GMSHDDM_CMAKE_BUILD_TYPE << gmshfem::msg::endl;
      gmshfem::msg::print << "# Build options: " << GMSHDDM_CONFIG_OPTIONS << gmshfem::msg::endl;
      gmshfem::msg::print << "# CXX flags: " << GMSHDDM_CXX_FLAGS << gmshfem::msg::endl;
#ifdef HAVE_PETSC
      gmshfem::msg::print << "# PETSc arithmetic: " << gmshfem::scalar::Name< PetscScalar >::name << gmshfem::msg::endl;
#endif
      gmshfem::msg::print << gmshfem::msg::endl;
    }

    void version(gmshfem::common::ArgsManager *argsManager)
    {
      gmshfem::msg::print << "GmshDDM " << GMSHDDM_MAJOR_VERSION << "." << GMSHDDM_MINOR_VERSION << "." << GMSHDDM_PATCH_VERSION << gmshfem::msg::endl;
    }

    //Binary commands
    void affinity(gmshfem::common::ArgsManager *argsManager, const gmshfem::common::CommandLine::Parameter &parameters)
    {
      switch(parameters.i) {
      case 1:
        common::Options::instance()->processAffinity = problem::ProcessAffinity::ABCABC;
        break;
      case 2:
        common::Options::instance()->processAffinity = problem::ProcessAffinity::AABBCC;
        break;
        break;
      default: break;
      }
    }

    GmshDdm::GmshDdm(int argc, char **argv) :
      _gmshFem(nullptr)
    {
      if(s_gmshddm == nullptr) {
        s_gmshddm = this;

        _gmshFem = new gmshfem::common::GmshFem("GmshDDM");
        gmshfem::common::ArgsManager *argsManager = _gmshFem->getArgsManager();

        //Unary commands
        gmshfem::common::UnaryCL *unaryCL = nullptr;
        unaryCL = new gmshfem::common::UnaryCL("infoDDM", "Get compile information about GmshDDM", true, true, info);
        argsManager->insert(unaryCL);
        unaryCL = new gmshfem::common::UnaryCL("memoryDDM", "Print memory information of the DDM algorithm", false, true, [](gmshfem::common::ArgsManager *argsManager) { gmshddm::common::Options::instance()->memory = true; });
        argsManager->insert(unaryCL);
        unaryCL = new gmshfem::common::UnaryCL("versionDDM", "Get GmshDDM version", true, true, version);
        argsManager->insert(unaryCL);

        //Binary commands
        gmshfem::common::BinaryCL *binaryCL = nullptr;
        binaryCL = new gmshfem::common::BinaryCL("affinity", "Choose the process/subdomain affinity (1:ABCABC type, 2:AABBCC type)", false, true, gmshfem::common::CommandLine::Parameter::Type::Integer, affinity);
        argsManager->insert(binaryCL);

        _gmshFem->finalizeInit(argc, argv);
      }
      else {
        throw gmshfem::common::Exception("Only one object of class GmshDdm can be instantiated");
      }
    }

    GmshDdm::~GmshDdm()
    {
      if(_gmshFem) {
        delete _gmshFem;
      }
    }

    void GmshDdm::setVerbosity(const int verbose)
    {
      _gmshFem->setVerbosity(verbose);
    }

    void GmshDdm::userDefinedParameter(int &value, const std::string &name) const
    {
      _gmshFem->userDefinedParameter(value, name);
    }

    void GmshDdm::userDefinedParameter(unsigned int &value, const std::string &name) const
    {
      _gmshFem->userDefinedParameter(value, name);
    }

    void GmshDdm::userDefinedParameter(double &value, const std::string &name) const
    {
      _gmshFem->userDefinedParameter(value, name);
    }

    void GmshDdm::userDefinedParameter(float &value, const std::string &name) const
    {
      _gmshFem->userDefinedParameter(value, name);
    }

    void GmshDdm::userDefinedParameter(std::string &value, const std::string &name) const
    {
      _gmshFem->userDefinedParameter(value, name);
    }

    void GmshDdm::userDefinedParameter(bool &value, const std::string &name) const
    {
      _gmshFem->userDefinedParameter(value, name);
    }

    // static method
    GmshDdm *GmshDdm::currentInstance()
    {
      return s_gmshddm;
    }


  } // namespace common


} // namespace gmshddm
