// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_OPTIONSENUMS
#define H_GMSHDDM_OPTIONSENUMS

namespace gmshddm
{


  namespace problem
  {


    enum class ProcessAffinity {
      ABCABC = 0,
      AABBCC = 1
    };


  } // namespace problem


} // namespace gmshddm

#endif // H_GMSHDDM_OPTIONSENUMS
