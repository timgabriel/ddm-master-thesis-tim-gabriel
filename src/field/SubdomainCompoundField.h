// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_SUBDOMAINCOMPOUNDFIELD
#define H_GMSHDDM_SUBDOMAINCOMPOUNDFIELD

#include "Subdomain.h"

#include <gmshfem/FieldInterface.h>
#include <string>
#include <vector>


namespace gmshddm
{


  namespace field
  {


    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    class SubdomainCompoundField
    {
     private:
      std::string _name;
      std::vector< gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields > > _fields;

     public:
      SubdomainCompoundField();
      SubdomainCompoundField(const std::string &name, const domain::Subdomain &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const unsigned int degree = 1);
      SubdomainCompoundField(const SubdomainCompoundField< T_Scalar, T_Form, T_NumFields > &other);
      SubdomainCompoundField(SubdomainCompoundField< T_Scalar, T_Form, T_NumFields > &&other);
      ~SubdomainCompoundField();

      SubdomainCompoundField &operator=(const SubdomainCompoundField< T_Scalar, T_Form, T_NumFields > &other);

      gmshfem::field::Form form() const;

      const gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields > &operator()(const unsigned int i) const;
      gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields > &operator()(const unsigned int i);
    };


  } // namespace field


} // namespace gmshddm


#endif // H_GMSHDDM_SUBDOMAINCOMPOUNDFIELD
