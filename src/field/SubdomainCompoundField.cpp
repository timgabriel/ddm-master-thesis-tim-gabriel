// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#include "SubdomainCompoundField.h"

#include <gmshfem/Exception.h>
#include <string>

namespace gmshddm
{


  namespace field
  {

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    SubdomainCompoundField< T_Scalar, T_Form, T_NumFields >::SubdomainCompoundField() :
      _name(), _fields()
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    SubdomainCompoundField< T_Scalar, T_Form, T_NumFields >::SubdomainCompoundField(const std::string &name, const domain::Subdomain &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const unsigned int degree) :
      _name(name), _fields()
    {
      _fields.reserve(domains.numberOfSubdomains());
      for(auto i = 0U; i < domains.numberOfSubdomains(); ++i) {
        _fields.emplace_back(_name + "_" + std::to_string(i), domains(i), type, degree);
      }
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    SubdomainCompoundField< T_Scalar, T_Form, T_NumFields >::SubdomainCompoundField(const SubdomainCompoundField< T_Scalar, T_Form, T_NumFields > &other) :
      _name(other._name), _fields(other._fields)
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    SubdomainCompoundField< T_Scalar, T_Form, T_NumFields >::SubdomainCompoundField(SubdomainCompoundField< T_Scalar, T_Form, T_NumFields > &&other) :
      _name(std::move(other._name)), _fields(std::move(other._fields))
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    SubdomainCompoundField< T_Scalar, T_Form, T_NumFields > &SubdomainCompoundField< T_Scalar, T_Form, T_NumFields >::operator=(const SubdomainCompoundField< T_Scalar, T_Form, T_NumFields > &other)
    {
      _name = other._name;
      _fields = other._fields;
      return *this;
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    SubdomainCompoundField< T_Scalar, T_Form, T_NumFields >::~SubdomainCompoundField()
    {
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    gmshfem::field::Form SubdomainCompoundField< T_Scalar, T_Form, T_NumFields >::form() const
    {
      return T_Form;
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    const gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields > &SubdomainCompoundField< T_Scalar, T_Form, T_NumFields >::operator()(const unsigned int i) const
    {
      if(i >= _fields.size()) {
        throw gmshfem::common::Exception("Trying to access compound field(" + std::to_string(i) + ") of compound field '" + _name + "'");
      }

      return _fields[i];
    }

    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields > &SubdomainCompoundField< T_Scalar, T_Form, T_NumFields >::operator()(const unsigned int i)
    {
      if(i >= _fields.size()) {
        throw gmshfem::common::Exception("Trying to access compound field(" + std::to_string(i) + ") of compound field '" + _name + "'");
      }

      return _fields[i];
    }


    template class SubdomainCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 2 >;
    template class SubdomainCompoundField< std::complex< double >, gmshfem::field::Form::Form1, 2 >;
    template class SubdomainCompoundField< std::complex< double >, gmshfem::field::Form::Form2, 2 >;
    template class SubdomainCompoundField< std::complex< double >, gmshfem::field::Form::Form3, 2 >;

    template class SubdomainCompoundField< double, gmshfem::field::Form::Form0, 2 >;
    template class SubdomainCompoundField< double, gmshfem::field::Form::Form1, 2 >;
    template class SubdomainCompoundField< double, gmshfem::field::Form::Form2, 2 >;
    template class SubdomainCompoundField< double, gmshfem::field::Form::Form3, 2 >;


    template class SubdomainCompoundField< std::complex< float >, gmshfem::field::Form::Form0, 2 >;
    template class SubdomainCompoundField< std::complex< float >, gmshfem::field::Form::Form1, 2 >;
    template class SubdomainCompoundField< std::complex< float >, gmshfem::field::Form::Form2, 2 >;
    template class SubdomainCompoundField< std::complex< float >, gmshfem::field::Form::Form3, 2 >;

    template class SubdomainCompoundField< float, gmshfem::field::Form::Form0, 2 >;
    template class SubdomainCompoundField< float, gmshfem::field::Form::Form1, 2 >;
    template class SubdomainCompoundField< float, gmshfem::field::Form::Form2, 2 >;
    template class SubdomainCompoundField< float, gmshfem::field::Form::Form3, 2 >;


    template class SubdomainCompoundField< std::complex< double >, gmshfem::field::Form::Form0, 3 >;
    template class SubdomainCompoundField< std::complex< double >, gmshfem::field::Form::Form1, 3 >;
    template class SubdomainCompoundField< std::complex< double >, gmshfem::field::Form::Form2, 3 >;
    template class SubdomainCompoundField< std::complex< double >, gmshfem::field::Form::Form3, 3 >;

    template class SubdomainCompoundField< double, gmshfem::field::Form::Form0, 3 >;
    template class SubdomainCompoundField< double, gmshfem::field::Form::Form1, 3 >;
    template class SubdomainCompoundField< double, gmshfem::field::Form::Form2, 3 >;
    template class SubdomainCompoundField< double, gmshfem::field::Form::Form3, 3 >;


    template class SubdomainCompoundField< std::complex< float >, gmshfem::field::Form::Form0, 3 >;
    template class SubdomainCompoundField< std::complex< float >, gmshfem::field::Form::Form1, 3 >;
    template class SubdomainCompoundField< std::complex< float >, gmshfem::field::Form::Form2, 3 >;
    template class SubdomainCompoundField< std::complex< float >, gmshfem::field::Form::Form3, 3 >;

    template class SubdomainCompoundField< float, gmshfem::field::Form::Form0, 3 >;
    template class SubdomainCompoundField< float, gmshfem::field::Form::Form1, 3 >;
    template class SubdomainCompoundField< float, gmshfem::field::Form::Form2, 3 >;
    template class SubdomainCompoundField< float, gmshfem::field::Form::Form3, 3 >;


  } // namespace field


} // namespace gmshddm
