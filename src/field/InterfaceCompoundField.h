// GmshDDM - Copyright (C) 2019-2022, A. Royer, C. Geuzaine, Université de Liège
//
// See the LICENSE.txt file for license information. Please report all
// issues on https://gitlab.onelab.info/gmsh/ddm/issues

#ifndef H_GMSHDDM_INTERFACECOMPOUNDFIELD
#define H_GMSHDDM_INTERFACECOMPOUNDFIELD

#include "Interface.h"
#include "InterfaceFieldInterface.h"

#include <gmshfem/FieldInterface.h>
#include <string>
#include <unordered_map>
#include <vector>


namespace gmshddm
{


  namespace field
  {


    template< class T_Scalar, gmshfem::field::Form T_Form, unsigned int T_NumFields >
    class InterfaceCompoundField : public InterfaceFieldInterface< T_Scalar >
    {
      std::vector< std::unordered_map< unsigned int, gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields > > > _fields;

     public:
      InterfaceCompoundField();
      InterfaceCompoundField(const std::string &name, const domain::Interface &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const unsigned int degree = 1);
      InterfaceCompoundField(const std::string &name, const domain::Interface &domains, const gmshfem::field::FunctionSpaceOfForm< T_Form > &type, const std::vector< unsigned int > &degree);
      InterfaceCompoundField(const InterfaceCompoundField< T_Scalar, T_Form, T_NumFields > &other);
      InterfaceCompoundField(InterfaceCompoundField< T_Scalar, T_Form, T_NumFields > &&other);
      ~InterfaceCompoundField();

      InterfaceCompoundField &operator=(const InterfaceCompoundField< T_Scalar, T_Form, T_NumFields > &other);

      gmshfem::field::Form form() const;

      bool isDefined(const unsigned int i, const unsigned int j) const override;
      const gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields > &operator()(const unsigned int i, const unsigned int j) const override;
      gmshfem::field::CompoundField< T_Scalar, T_Form, T_NumFields > &operator()(const unsigned int i, const unsigned int j) override;
    };


  } // namespace field


} // namespace gmshddm


#endif // H_GMSHDDM_INTERFACECOMPOUNDFIELD
